(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _test_test_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./test/test.component */ "./src/app/test/test.component.ts");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./home/home.component */ "./src/app/home/home.component.ts");
/* harmony import */ var _not_found_not_found_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./not-found/not-found.component */ "./src/app/not-found/not-found.component.ts");
/* harmony import */ var _avis_avis_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./avis/avis.component */ "./src/app/avis/avis.component.ts");
/* harmony import */ var _video_video_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./video/video.component */ "./src/app/video/video.component.ts");









const routes = [
    { path: '', redirectTo: '/', pathMatch: 'full' },
    { path: '', component: _video_video_component__WEBPACK_IMPORTED_MODULE_6__["VideoComponent"] },
    { path: 'avis', component: _avis_avis_component__WEBPACK_IMPORTED_MODULE_5__["AvisComponent"] },
    { path: 'home', component: _home_home_component__WEBPACK_IMPORTED_MODULE_3__["HomeComponent"] },
    { path: 'test', component: _test_test_component__WEBPACK_IMPORTED_MODULE_2__["TestComponent"] },
    { path: 'cpn', loadChildren: () => __webpack_require__.e(/*! import() | cpn-cpn-module */ "cpn-cpn-module").then(__webpack_require__.bind(null, /*! ./cpn/cpn.module */ "./src/app/cpn/cpn.module.ts")).then(m => m.CpnModule) },
    { path: '**', component: _not_found_not_found_component__WEBPACK_IMPORTED_MODULE_4__["NotFoundComponent"] },
];
class AppRoutingModule {
}
AppRoutingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({ type: AppRoutingModule });
AppRoutingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({ factory: function AppRoutingModule_Factory(t) { return new (t || AppRoutingModule)(); }, imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)], _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](AppRoutingModule, { imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]], exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AppRoutingModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
                imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)],
                exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
            }]
    }], null, null); })();


/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");



class AppComponent {
    constructor(router) {
        this.title = 'frontCrm';
    }
}
AppComponent.ɵfac = function AppComponent_Factory(t) { return new (t || AppComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"])); };
AppComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: AppComponent, selectors: [["app-root"]], decls: 1, vars: 0, template: function AppComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "router-outlet");
    } }, directives: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterOutlet"]], styles: [".loader[_ngcontent-%COMP%] {\n    display: inline-block;\n    width: 40px;\n    height: 40px;\n    position: absolute;\n    left: 0;\n    right: 0;\n    margin-left: auto;\n    margin-right: auto;\n    top: calc(50% - 50px);\n    transform: translateY(-50%);\n  }\n  \n  .loader[_ngcontent-%COMP%]:after {\n    content: ' ';\n    display: block;\n    width: 30px;\n    height: 30px;\n    border-radius: 50%;\n    border: 5px solid #fff;\n    border-color: #fff transparent #fff transparent;\n    animation: loader 1.2s linear infinite;\n  }\n  \n  @keyframes loader {\n    0% {\n      transform: rotate(0deg);\n    }\n    100% {\n      transform: rotate(360deg);\n    }\n  }\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXBwLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxxQkFBcUI7SUFDckIsV0FBVztJQUNYLFlBQVk7SUFDWixrQkFBa0I7SUFDbEIsT0FBTztJQUNQLFFBQVE7SUFDUixpQkFBaUI7SUFDakIsa0JBQWtCO0lBQ2xCLHFCQUFxQjtJQUNyQiwyQkFBMkI7RUFDN0I7O0VBRUE7SUFDRSxZQUFZO0lBQ1osY0FBYztJQUNkLFdBQVc7SUFDWCxZQUFZO0lBQ1osa0JBQWtCO0lBQ2xCLHNCQUFzQjtJQUN0QiwrQ0FBK0M7SUFDL0Msc0NBQXNDO0VBQ3hDOztFQUVBO0lBQ0U7TUFDRSx1QkFBdUI7SUFDekI7SUFDQTtNQUNFLHlCQUF5QjtJQUMzQjtFQUNGIiwiZmlsZSI6InNyYy9hcHAvYXBwLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubG9hZGVyIHtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgd2lkdGg6IDQwcHg7XG4gICAgaGVpZ2h0OiA0MHB4O1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBsZWZ0OiAwO1xuICAgIHJpZ2h0OiAwO1xuICAgIG1hcmdpbi1sZWZ0OiBhdXRvO1xuICAgIG1hcmdpbi1yaWdodDogYXV0bztcbiAgICB0b3A6IGNhbGMoNTAlIC0gNTBweCk7XG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVZKC01MCUpO1xuICB9XG4gIFxuICAubG9hZGVyOmFmdGVyIHtcbiAgICBjb250ZW50OiAnICc7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgd2lkdGg6IDMwcHg7XG4gICAgaGVpZ2h0OiAzMHB4O1xuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgICBib3JkZXI6IDVweCBzb2xpZCAjZmZmO1xuICAgIGJvcmRlci1jb2xvcjogI2ZmZiB0cmFuc3BhcmVudCAjZmZmIHRyYW5zcGFyZW50O1xuICAgIGFuaW1hdGlvbjogbG9hZGVyIDEuMnMgbGluZWFyIGluZmluaXRlO1xuICB9XG4gIFxuICBAa2V5ZnJhbWVzIGxvYWRlciB7XG4gICAgMCUge1xuICAgICAgdHJhbnNmb3JtOiByb3RhdGUoMGRlZyk7XG4gICAgfVxuICAgIDEwMCUge1xuICAgICAgdHJhbnNmb3JtOiByb3RhdGUoMzYwZGVnKTtcbiAgICB9XG4gIH0iXX0= */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AppComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-root',
                templateUrl: './app.component.html',
                styleUrls: ['./app.component.css']
            }]
    }], function () { return [{ type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"] }]; }, null); })();


/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/animations.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _material_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./material-module */ "./src/app/material-module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _security_token_interceptor_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./security/token-interceptor.service */ "./src/app/security/token-interceptor.service.ts");
/* harmony import */ var ngx_countdown__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ngx-countdown */ "./node_modules/ngx-countdown/__ivy_ngcc__/fesm2015/ngx-countdown.js");
/* harmony import */ var _test_test_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./test/test.component */ "./src/app/test/test.component.ts");
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/material/icon */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/icon.js");
/* harmony import */ var _fullcalendar_angular__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @fullcalendar/angular */ "./node_modules/@fullcalendar/angular/__ivy_ngcc__/fesm2015/fullcalendar-angular.js");
/* harmony import */ var _fullcalendar_daygrid__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @fullcalendar/daygrid */ "./node_modules/@fullcalendar/daygrid/main.js");
/* harmony import */ var _fullcalendar_interaction__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @fullcalendar/interaction */ "./node_modules/@fullcalendar/interaction/main.js");
/* harmony import */ var _map_french_map_french_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./map-french/map-french.component */ "./src/app/map-french/map-french.component.ts");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./home/home.component */ "./src/app/home/home.component.ts");
/* harmony import */ var _not_found_not_found_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./not-found/not-found.component */ "./src/app/not-found/not-found.component.ts");
/* harmony import */ var _baseUrl__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./baseUrl */ "./src/app/baseUrl.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @ng-select/ng-select */ "./node_modules/@ng-select/ng-select/__ivy_ngcc__/fesm2015/ng-select-ng-select.js");
/* harmony import */ var _angular_slider_ngx_slider__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! @angular-slider/ngx-slider */ "./node_modules/@angular-slider/ngx-slider/__ivy_ngcc__/fesm2015/angular-slider-ngx-slider.js");
/* harmony import */ var ng_lazyload_image__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ng-lazyload-image */ "./node_modules/ng-lazyload-image/__ivy_ngcc__/fesm2015/ng-lazyload-image.js");
/* harmony import */ var _avis_avis_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./avis/avis.component */ "./src/app/avis/avis.component.ts");
/* harmony import */ var _video_video_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./video/video.component */ "./src/app/video/video.component.ts");


/**************** library      **********************************/





/**************** component      **********************************/

















 // <-- import it

_fullcalendar_angular__WEBPACK_IMPORTED_MODULE_12__["FullCalendarModule"].registerPlugins([
    _fullcalendar_daygrid__WEBPACK_IMPORTED_MODULE_13__["default"],
    _fullcalendar_interaction__WEBPACK_IMPORTED_MODULE_14__["default"]
]);
class AppModule {
}
AppModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineNgModule"]({ type: AppModule, bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"]] });
AppModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjector"]({ factory: function AppModule_Factory(t) { return new (t || AppModule)(); }, providers: [
        { provide: 'baseUrl', useValue: _baseUrl__WEBPACK_IMPORTED_MODULE_18__["baseUrl"] },
        { provide: _angular_common__WEBPACK_IMPORTED_MODULE_19__["APP_BASE_HREF"], useValue: '/' },
        {
            provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HTTP_INTERCEPTORS"],
            useClass: _security_token_interceptor_service__WEBPACK_IMPORTED_MODULE_8__["TokenInterceptorService"],
            multi: true,
        },
        { provide: ng_lazyload_image__WEBPACK_IMPORTED_MODULE_22__["LAZYLOAD_IMAGE_HOOKS"], useClass: ng_lazyload_image__WEBPACK_IMPORTED_MODULE_22__["ScrollHooks"] }
    ], imports: [[
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
            _app_routing_module__WEBPACK_IMPORTED_MODULE_5__["AppRoutingModule"],
            _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_2__["BrowserAnimationsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"],
            _material_module__WEBPACK_IMPORTED_MODULE_6__["MaterialModule"],
            ngx_countdown__WEBPACK_IMPORTED_MODULE_9__["CountdownModule"],
            _fullcalendar_angular__WEBPACK_IMPORTED_MODULE_12__["FullCalendarModule"],
            _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_20__["NgSelectModule"],
            _angular_material_icon__WEBPACK_IMPORTED_MODULE_11__["MatIconModule"],
            _angular_slider_ngx_slider__WEBPACK_IMPORTED_MODULE_21__["NgxSliderModule"],
            ng_lazyload_image__WEBPACK_IMPORTED_MODULE_22__["LazyLoadImageModule"]
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵsetNgModuleScope"](AppModule, { declarations: [_app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"],
        _test_test_component__WEBPACK_IMPORTED_MODULE_10__["TestComponent"],
        _map_french_map_french_component__WEBPACK_IMPORTED_MODULE_15__["MapFrenchComponent"],
        _home_home_component__WEBPACK_IMPORTED_MODULE_16__["HomeComponent"],
        _not_found_not_found_component__WEBPACK_IMPORTED_MODULE_17__["NotFoundComponent"],
        _avis_avis_component__WEBPACK_IMPORTED_MODULE_23__["AvisComponent"],
        _video_video_component__WEBPACK_IMPORTED_MODULE_24__["VideoComponent"]], imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
        _app_routing_module__WEBPACK_IMPORTED_MODULE_5__["AppRoutingModule"],
        _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_2__["BrowserAnimationsModule"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
        _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"],
        _material_module__WEBPACK_IMPORTED_MODULE_6__["MaterialModule"],
        ngx_countdown__WEBPACK_IMPORTED_MODULE_9__["CountdownModule"],
        _fullcalendar_angular__WEBPACK_IMPORTED_MODULE_12__["FullCalendarModule"],
        _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_20__["NgSelectModule"],
        _angular_material_icon__WEBPACK_IMPORTED_MODULE_11__["MatIconModule"],
        _angular_slider_ngx_slider__WEBPACK_IMPORTED_MODULE_21__["NgxSliderModule"],
        ng_lazyload_image__WEBPACK_IMPORTED_MODULE_22__["LazyLoadImageModule"]] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](AppModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"],
        args: [{
                declarations: [
                    _app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"],
                    _test_test_component__WEBPACK_IMPORTED_MODULE_10__["TestComponent"],
                    _map_french_map_french_component__WEBPACK_IMPORTED_MODULE_15__["MapFrenchComponent"],
                    _home_home_component__WEBPACK_IMPORTED_MODULE_16__["HomeComponent"],
                    _not_found_not_found_component__WEBPACK_IMPORTED_MODULE_17__["NotFoundComponent"],
                    _avis_avis_component__WEBPACK_IMPORTED_MODULE_23__["AvisComponent"],
                    _video_video_component__WEBPACK_IMPORTED_MODULE_24__["VideoComponent"]
                ],
                imports: [
                    _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                    _app_routing_module__WEBPACK_IMPORTED_MODULE_5__["AppRoutingModule"],
                    _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_2__["BrowserAnimationsModule"],
                    _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                    _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                    _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"],
                    _material_module__WEBPACK_IMPORTED_MODULE_6__["MaterialModule"],
                    ngx_countdown__WEBPACK_IMPORTED_MODULE_9__["CountdownModule"],
                    _fullcalendar_angular__WEBPACK_IMPORTED_MODULE_12__["FullCalendarModule"],
                    _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_20__["NgSelectModule"],
                    _angular_material_icon__WEBPACK_IMPORTED_MODULE_11__["MatIconModule"],
                    _angular_slider_ngx_slider__WEBPACK_IMPORTED_MODULE_21__["NgxSliderModule"],
                    ng_lazyload_image__WEBPACK_IMPORTED_MODULE_22__["LazyLoadImageModule"]
                ],
                providers: [
                    { provide: 'baseUrl', useValue: _baseUrl__WEBPACK_IMPORTED_MODULE_18__["baseUrl"] },
                    { provide: _angular_common__WEBPACK_IMPORTED_MODULE_19__["APP_BASE_HREF"], useValue: '/' },
                    {
                        provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HTTP_INTERCEPTORS"],
                        useClass: _security_token_interceptor_service__WEBPACK_IMPORTED_MODULE_8__["TokenInterceptorService"],
                        multi: true,
                    },
                    { provide: ng_lazyload_image__WEBPACK_IMPORTED_MODULE_22__["LAZYLOAD_IMAGE_HOOKS"], useClass: ng_lazyload_image__WEBPACK_IMPORTED_MODULE_22__["ScrollHooks"] }
                ],
                bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"]],
                schemas: [_angular_core__WEBPACK_IMPORTED_MODULE_1__["CUSTOM_ELEMENTS_SCHEMA"]]
            }]
    }], null, null); })();


/***/ }),

/***/ "./src/app/avis/avis.component.ts":
/*!****************************************!*\
  !*** ./src/app/avis/avis.component.ts ***!
  \****************************************/
/*! exports provided: AvisComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AvisComponent", function() { return AvisComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var src_app_services_cpn_avis_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/cpn/avis.service */ "./src/app/services/cpn/avis.service.ts");






class AvisComponent {
    constructor(fb, avisSevice) {
        this.fb = fb;
        this.avisSevice = avisSevice;
        this.AvisForm = this.fb.group({
            email: [null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]],
            username: ['',],
            phone: ['',],
            site: ['',],
            finance: ['',],
            buget: ['',],
            comment: ['',],
            proj: ['',],
            note: ['',],
        });
    }
    onSubmit() {
        console.log('avis', this.AvisForm.value);
        const formData = new FormData();
        formData.append('email', this.AvisForm.get('email').value);
        formData.append('username', this.AvisForm.get('username').value);
        formData.append('phone', this.AvisForm.get('phone').value);
        formData.append('site', this.AvisForm.get('site').value);
        formData.append('finance', this.AvisForm.get('finance').value);
        formData.append('buget', this.AvisForm.get('buget').value);
        formData.append('comment', this.AvisForm.get('comment').value);
        formData.append('proj', this.AvisForm.get('proj').value);
        formData.append('note', this.AvisForm.get('note').value);
        if (formData) {
            this.avisSevice.addAvis(formData).subscribe(res => {
                if (!res.error) {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.fire({
                        icon: 'success',
                        title: 'save reussie',
                        showConfirmButton: false,
                        timer: 2000
                    });
                    location.href = '/';
                }
                else {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: res.message + ' !',
                    });
                }
            }, error => {
                console.log(error);
                sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'error 500 !',
                });
            });
        }
    }
    rating(val) {
        this.AvisForm.get('note').setValue(val);
    }
    ngOnInit() {
    }
}
AvisComponent.ɵfac = function AvisComponent_Factory(t) { return new (t || AvisComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_cpn_avis_service__WEBPACK_IMPORTED_MODULE_3__["AvisService"])); };
AvisComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: AvisComponent, selectors: [["app-avis"]], decls: 132, vars: 1, consts: [[1, "container"], [3, "formGroup", "ngSubmit"], [1, "myCard"], [1, "row"], [1, "col-md-6"], [1, "myLeftCtn"], ["novalidate", "", 1, "myForm", "text-center", "needs-validation"], [1, "block"], [1, "circle"], ["src", "assets/cpnimages/logo/logo-cpn-blanc.png", "alt", "", 1, "logo1"], [1, "left"], [1, "col"], [1, "form-group"], [1, "fas", "fa-user"], ["type", "text", "formControlName", "username", "placeholder", "Username", "id", "username", "required", "", 1, "myInput"], [1, "invalid-feedback"], [1, "fas", "fa-envelope"], ["formControlName", "email", "placeholder", "Email", "type", "text", "id", "email", "required", "", 1, "myInput"], [1, "fas", "fa-phone"], ["type", "text", "formControlName", "phone", "id", "phone", "placeholder", "telephone", "required", "", 1, "myInput"], [1, "fas", "fa-link"], ["type", "text", "formControlName", "site", "id", "url", "placeholder", "lien site web", "required", "", 1, "myInput"], ["type", "submit", "value", "Envoyer", 1, "butt"], ["src", "assets/cpnimages/logo/logo-cpn-blanc.png", "alt", "", 1, "background"], [1, "myRightCtn"], [1, "form-check-label"], [1, "radio"], [1, "form-check"], ["type", "radio", "formControlName", "finance", "id", "finance1", "name", "finance", "value", "oui", "checked", "", 1, "form-check-input"], ["for", "finance1", 1, "form-check-label"], ["type", "radio", "formControlName", "finance", "id", "finance2", "name", "finance", "value", "non", 1, "form-check-input"], ["for", "finance2", 1, "form-check-label"], [1, "rate"], ["type", "radio", "id", "star1", "name", "rate", "value", "1", 3, "click"], ["for", "star1", "title", "text"], ["type", "radio", "id", "star2", "name", "rate", "value", "2", 3, "click"], ["for", "star2", "title", "text"], ["type", "radio", "id", "star3", "name", "rate", "value", "3", 3, "click"], ["for", "star3", "title", "text"], ["type", "radio", "id", "star4", "name", "rate", "value", "4", 3, "click"], ["for", "star4", "title", "text"], ["type", "radio", "id", "star5", "name", "rate", "value", "5", 3, "click"], ["for", "star5", "title", "text"], ["type", "radio", "id", "star6", "name", "rate", "value", "6", 3, "click"], ["for", "star6", "title", "text"], ["type", "radio", "formControlName", "buget", "id", "buget1", "name", "buget", "value", "oui", "checked", "", 1, "form-check-input"], ["for", "buget1", 1, "form-check-label"], ["type", "radio", "formControlName", "buget", "id", "buget2", "name", "buget", "value", "non", 1, "form-check-input"], ["for", "buget2", 1, "form-check-label"], ["formControlName", "comment", "type", "text", "id", "commentaire", "placeholder", "commentaire", "required", "", 1, "myInput"], [1, "form-group", "end"], ["type", "radio", "formControlName", "proj", "id", "proj1", "name", "proj", "value", "financ\u00E9 mon d\u00E9veloppement application", "checked", "", 1, "form-check-input"], ["for", "proj1", 1, "form-check-label"], ["type", "radio", "formControlName", "proj", "id", "proj2", "name", "proj", "value", "financ\u00E9 mon CRM -ERP d\u2019entreprise", 1, "form-check-input"], ["for", "proj2", 1, "form-check-label"], ["type", "radio", "formControlName", "proj", "id", "proj3", "name", "proj", "value", "financ\u00E9 ma communication r\u00E9seaux sociaux", 1, "form-check-input"], ["for", "proj3", 1, "form-check-label"], ["type", "radio", "formControlName", "proj", "id", "proj4", "name", "proj", "value", "financ\u00E9 un projet d\u2019innovation digitale", 1, "form-check-input"], ["for", "proj4", 1, "form-check-label"], ["type", "radio", "formControlName", "proj", "id", "proj5", "name", "proj", "value", "non", 1, "form-check-input"], ["for", "proj5", 1, "form-check-label"]], template: function AvisComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "form", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngSubmit", function AvisComponent_Template_form_ngSubmit_1_listener() { return ctx.onSubmit(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "img", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "header", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "Identifi\u00E9 vous");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](16, "i", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](17, "input", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "div", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19, "Please fill out this field.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](22, "i", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](23, "input", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "div", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](25, "Please fill out this field.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](29, "i", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](30, "input", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "div", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](32, "Please fill out this field.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](35, "i", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](36, "input", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "div", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](38, "Please fill out this field.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](39, "input", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](41, "img", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "div", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "header");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](46, "Enqu\u00EAte de satisfaction");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](48, "votre avis a un r\u00E9\u00E9l int\u00E9ret pour nous dans le cadre de l\u2019am\u00E9lioration de notre service");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](49, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](51, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](52, "label", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](53, " Avez-vous b\u00E9n\u00E9ficier d\u2019un financement attribuer par le CPN ?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](54, "div", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](55, "div", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](56, "input", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](57, "Oui ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](58, "label", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](59, "div", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](60, "input", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](61, "Non ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](62, "label", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](63, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](64, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](65, "label", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](66, " Note sur l\u2019agent du CPN.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](67, "div", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](68, "input", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AvisComponent_Template_input_click_68_listener() { return ctx.rating(6); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](69, "label", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](70, "1 stars");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](71, "input", 35);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AvisComponent_Template_input_click_71_listener() { return ctx.rating(5); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](72, "label", 36);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](73, "2 stars");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](74, "input", 37);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AvisComponent_Template_input_click_74_listener() { return ctx.rating(4); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](75, "label", 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](76, "3 stars");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](77, "input", 39);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AvisComponent_Template_input_click_77_listener() { return ctx.rating(3); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](78, "label", 40);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](79, "4 stars");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](80, "input", 41);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AvisComponent_Template_input_click_80_listener() { return ctx.rating(2); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](81, "label", 42);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](82, "5 star");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](83, "input", 43);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AvisComponent_Template_input_click_83_listener() { return ctx.rating(1); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](84, "label", 44);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](85, "6 star");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](86, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](87, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](88, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](89, "label", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](90, " Grace au financement le Reste a charge n\u2019a pas eu de cons\u00E9quence sur ma tr\u00E9sorerie il a \u00E9t\u00E9 adapt\u00E9 \u00E0 votre Budget ?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](91, "div", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](92, "div", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](93, "input", 45);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](94, "Oui ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](95, "label", 46);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](96, "div", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](97, "input", 47);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](98, "Non ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](99, "label", 48);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](100, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](101, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](102, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](103, "label", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](104, "le Cabinet de propulsion Num\u00E9rique vous a t\u2019il apport\u00E9 une aide majeur pour r\u00E9alis\u00E9 votre projet digital en terme de financement, d\u2019accompagnement ou d\u2019expertise ?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](105, "textarea", 49);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](106, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](107, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](108, "div", 50);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](109, "label", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](110, " Avez-vous d\u2019autre projet num\u00E9rique \u00E0 financer au cours de l\u2019ann\u00E9e ? (si oui lequel)");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](111, "div", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](112, "div", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](113, "input", 51);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](114, "financ\u00E9 mon d\u00E9veloppement application ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](115, "label", 52);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](116, "div", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](117, "input", 53);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](118, "financ\u00E9 mon CRM -ERP d\u2019entreprise ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](119, "label", 54);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](120, "div", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](121, "input", 55);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](122, "financ\u00E9 ma communication r\u00E9seaux sociaux. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](123, "label", 56);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](124, "div", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](125, "input", 57);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](126, "financ\u00E9 un projet d\u2019innovation digitale. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](127, "label", 58);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](128, "div", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](129, "input", 59);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](130, "Non ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](131, "label", 60);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx.AvisForm);
    } }, directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControlName"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["RequiredValidator"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["RadioControlValueAccessor"]], styles: ["body[_ngcontent-%COMP%]\r\n{\r\n    background: #fbf3ff;\r\n}\r\n.logo[_ngcontent-%COMP%]{\r\n    width: 66px;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: flex-end;\r\n    align-items: flex-end;\r\n    position: fixed;\r\n    top: 547px;\r\n    left: 549px;;\r\n}\r\n.form-check[_ngcontent-%COMP%] {\r\n    display: block;\r\n    min-height: 1.5rem;\r\n    padding-left: 2em;\r\n    margin-bottom: 0.125rem;\r\n}\r\n.background[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    width: 76%;\r\n    height: 65%;\r\n    z-index: 1;\r\n    opacity: 1px;\r\n    opacity: 0.13;\r\n    display: flex;\r\n    justify-content: center;\r\n    align-items: center;\r\n    flex-direction: column;\r\n    text-align: center;\r\n    left: 74px;\r\n    top: 97px;\r\n}\r\n.container[_ngcontent-%COMP%]\r\n{\r\n    position: absolute;\r\n    max-width: 800px;\r\n    height: 618px;\r\n    margin: auto;\r\n    top: 50%;\r\n    left: 35%;\r\n    transform: translate(-50%,-50%);\r\n}\r\n.myRightCtn[_ngcontent-%COMP%]\r\n{\r\n    position: relative;\r\n    background-image: linear-gradient(45deg, #6246ff, #111D5E );\r\n    border-radius: 25px;\r\n    height: 100%;\r\n    padding: 25px;\r\n    color: rgb(192, 192, 192);\r\n    font-size: 12px;\r\n    display: flex;\r\n  justify-content: center;\r\n  align-items: initial;\r\n\r\n}\r\n.logo1[_ngcontent-%COMP%]{\r\n    width: 55%;\r\ndisplay: flex;\r\nflex-direction: column;\r\njustify-content: center;\r\nalign-items: center;\r\ntop: 25px;\r\nposition: absolute;\r\nleft: 20px;\r\n}\r\n.circle[_ngcontent-%COMP%]{\r\n    display: inline-block;\r\n    position: relative;\r\n    width: 80px;\r\n    height: 80px;\r\n    overflow: hidden;\r\n    border-radius: 50%;\r\n    background: #111D5E;\r\n    margin-right: 10px;\r\n}\r\n.myRightCtn[_ngcontent-%COMP%]   header[_ngcontent-%COMP%]\r\n{\r\n    color: #ffffff;\r\n    margin-bottom: 50px;\r\n    text-align: initial;\r\n    height: 86px;\r\n}\r\n.myRightCtn[_ngcontent-%COMP%]   header[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]\r\n{\r\n    color: #ffffff;\r\n   \r\n}\r\n.myLeftCtn[_ngcontent-%COMP%]\r\n{\r\n   position: relative;\r\n    background: #fff;\r\n    border-radius: 25px;\r\n    height: 100%;\r\n    padding: 25px;\r\n    padding-left: 50px;\r\n}\r\n.myLeftCtn[_ngcontent-%COMP%]   header[_ngcontent-%COMP%]\r\n{\r\n    color: #696969;\r\n    margin-bottom: 50px;\r\n    height: 94px;\r\n    text-align: initial;\r\n    display: flex;\r\n    flex-direction: column;\r\n    align-items: center;\r\n    justify-content: center;\r\n}\r\n.myLeftCtn[_ngcontent-%COMP%]   header[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]\r\n{\r\n    color: #111D5E;\r\n   \r\n}\r\n.row[_ngcontent-%COMP%]\r\n{\r\n    height: 100%;\r\n \r\n}\r\n.myCard[_ngcontent-%COMP%]\r\n{\r\n    position: relative;\r\n    background: #fff;\r\n    height: 100%;\r\n    border-radius: 25px;\r\n    box-shadow: 0px 10px 40px -10px rgba(0,0,0,0.7);\r\n    width: 159%;\r\n\r\n}\r\n.box[_ngcontent-%COMP%]\r\n{\r\n    position: relative;\r\n    margin: 20px;\r\n    margin-bottom: 100px;\r\n   \r\n}\r\n.end[_ngcontent-%COMP%]{\r\n   margin-bottom: 3rem;\r\n   text-align: left;\r\n   margin-left: 15px;\r\n}\r\n.block[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: row;\r\n    justify-content: initial; \r\n    margin-bottom: 25px;\r\n}\r\n.myLeftCtn[_ngcontent-%COMP%]   .myInput[_ngcontent-%COMP%]\r\n{\r\n    width: 236px;\r\n    border-radius: 25px;\r\n    padding: 10px;\r\n    padding-left: 50px;\r\n    border: none;\r\n    box-shadow: 0px 10px 49px -14px rgba(0,0,0,0.7);\r\n}\r\n.myLeftCtn[_ngcontent-%COMP%]   .myInput[_ngcontent-%COMP%]:focus\r\n{\r\n    outline: none;\r\n}\r\n.myForm[_ngcontent-%COMP%]\r\n{\r\n    position: relative;\r\n    margin-top: 5px;\r\n    height: 20%;\r\n    width: 100%;\r\n    z-index: 1;\r\n}\r\n.myRightCtn[_ngcontent-%COMP%]   .radio[_ngcontent-%COMP%]{\r\n    display: flex;\r\njustify-content: space-evenly;\r\nflex-direction: row;\r\n}\r\n.myRightCtn[_ngcontent-%COMP%]   .form-group[_ngcontent-%COMP%]{\r\n    margin-bottom: 1rem;\r\ntext-align: initial;\r\nwidth: -moz-fit-content;\r\nwidth: fit-content;\r\n}\r\n.myRightCtn[_ngcontent-%COMP%]   .myInput[_ngcontent-%COMP%]\r\n{\r\n    width: 100%;\r\n    border-radius: 25px;\r\n    padding: 10px;\r\n    padding-left: 50px;\r\n    border: none;\r\n    box-shadow: 0px 10px 49px -14px rgba(0,0,0,0.7);\r\n}\r\n.myRightCtn[_ngcontent-%COMP%]   .myInput[_ngcontent-%COMP%]:focus\r\n{\r\n    outline: none;\r\n}\r\n.myLeftCtn[_ngcontent-%COMP%]   .butt[_ngcontent-%COMP%]\r\n{\r\n    background:  linear-gradient(45deg, #fd3636, #e80000 );\r\n    color: #fff;\r\n    width: 230px;\r\n    border: none;\r\n    border-radius: 25px;\r\n    padding: 10px;\r\nbox-shadow: 0px 10px 41px -11px rgba(0,0,0,0.7);\r\n}\r\n.myLeftCtn[_ngcontent-%COMP%]   .butt[_ngcontent-%COMP%]:hover\r\n{\r\n    background:  linear-gradient(45deg, #ff5b5b, #ff2626 );\r\n\r\n}\r\n.myLeftCtn[_ngcontent-%COMP%]   .butt[_ngcontent-%COMP%]:focus\r\n{\r\n    outline: none;\r\n}\r\n.myRightCtn[_ngcontent-%COMP%]   label[_ngcontent-%COMP%]{\r\n    margin-bottom: 8px;\r\n}\r\n.myLeftCtn[_ngcontent-%COMP%]   .fas[_ngcontent-%COMP%]\r\n{\r\n    position: relative;\r\n    color: #111D5E;\r\n    left: 36px;\r\n}\r\n.butt_out[_ngcontent-%COMP%]\r\n{\r\n    background:  transparent;\r\n    color: #fff;\r\n    width: 120px;\r\n    border: 2px solid#fff;\r\n    border-radius: 25px;\r\n    padding: 10px;\r\nbox-shadow: 0px 10px 49px -14px rgba(0,0,0,0.7);\r\n}\r\n.butt_out[_ngcontent-%COMP%]:hover\r\n{\r\n    border: 2px solid#eecbff;\r\n}\r\n.butt_out[_ngcontent-%COMP%]:focus\r\n{\r\n    outline: none;\r\n}\r\n.myRightCtn[_ngcontent-%COMP%]   .end[_ngcontent-%COMP%]\r\n{\r\n    margin-top: 26px;    \r\n}\r\n\r\n.rating[_ngcontent-%COMP%] {\r\n    position: relative;\r\n    width: 180px;\r\n    background: transparent;\r\n    display: flex;\r\n    justify-content: center;\r\n    align-items: center;\r\n    gap: .3em;\r\n    padding: 5px;\r\n    overflow: hidden;\r\n    border-radius: 20px;\r\n    box-shadow: 0 0 2px #b3acac;\r\n }\r\n.rating__result[_ngcontent-%COMP%] {\r\n    position: absolute;\r\n    top: 0;\r\n    left: 0;\r\n    transform: translateY(-10px) translateX(-5px);\r\n    z-index: -9;\r\n    font: 3em Arial, Helvetica, sans-serif;\r\n    color: #ebebeb8e;\r\n    pointer-events: none;\r\n }\r\n.rating__star[_ngcontent-%COMP%] {\r\n    font-size: 1.3em;\r\n    cursor: pointer;\r\n    color: #dabd18b2;\r\n    transition: filter linear .3s;\r\n }\r\n.rating__star[_ngcontent-%COMP%]:hover {\r\n    filter: drop-shadow(1px 1px 4px gold);\r\n }\r\n.starActive[_ngcontent-%COMP%]{\r\n    color: gold;\r\n    \r\n}\r\n\r\n.rate[_ngcontent-%COMP%] {\r\n    float: left;\r\n    height: 38px;\r\n    padding: 0 10px;\r\n    position: relative;\r\n    width: 180px;\r\n    background: transparent;\r\n    display: flex;\r\n    justify-content: center;\r\n    align-items: center;\r\n   \r\n    padding: 5px;\r\n      padding-bottom: 5px;\r\n      flex-flow: row-reverse;\r\n    overflow: hidden;\r\n    border-radius: 20px;\r\n    box-shadow: 0 0 2px #b3acac;\r\n    padding-bottom: 0px;\r\n}\r\n.rate[_ngcontent-%COMP%]:not(:checked)    > input[_ngcontent-%COMP%] {\r\n    position:absolute;\r\n    top:-9999px;\r\n}\r\n.rate[_ngcontent-%COMP%]:not(:checked)    > label[_ngcontent-%COMP%] {\r\n    float:right;\r\n    width:1em;\r\n    overflow:hidden;\r\n    white-space:nowrap;\r\n    cursor:pointer;\r\n    font-size:30px;\r\n    color:#ccc;\r\n}\r\n.rate[_ngcontent-%COMP%]:not(:checked)    > label[_ngcontent-%COMP%]:before {\r\n    content: '\u2605 ';\r\n}\r\n.rate[_ngcontent-%COMP%]    > input[_ngcontent-%COMP%]:checked    ~ label[_ngcontent-%COMP%] {\r\n    color: #ffc700;    \r\n}\r\n.rate[_ngcontent-%COMP%]:not(:checked)    > label[_ngcontent-%COMP%]:hover, .rate[_ngcontent-%COMP%]:not(:checked)    > label[_ngcontent-%COMP%]:hover    ~ label[_ngcontent-%COMP%] {\r\n    color: #deb217;  \r\n}\r\n.rate[_ngcontent-%COMP%]    > input[_ngcontent-%COMP%]:checked    + label[_ngcontent-%COMP%]:hover, .rate[_ngcontent-%COMP%]    > input[_ngcontent-%COMP%]:checked    + label[_ngcontent-%COMP%]:hover    ~ label[_ngcontent-%COMP%], .rate[_ngcontent-%COMP%]    > input[_ngcontent-%COMP%]:checked    ~ label[_ngcontent-%COMP%]:hover, .rate[_ngcontent-%COMP%]    > input[_ngcontent-%COMP%]:checked    ~ label[_ngcontent-%COMP%]:hover    ~ label[_ngcontent-%COMP%], .rate[_ngcontent-%COMP%]    > label[_ngcontent-%COMP%]:hover    ~ input[_ngcontent-%COMP%]:checked    ~ label[_ngcontent-%COMP%] {\r\n    color: #c59b08;\r\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXZpcy9hdmlzLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7O0lBRUksbUJBQW1CO0FBQ3ZCO0FBQ0E7SUFDSSxXQUFXO0lBQ1gsYUFBYTtJQUNiLHNCQUFzQjtJQUN0Qix5QkFBeUI7SUFDekIscUJBQXFCO0lBQ3JCLGVBQWU7SUFDZixVQUFVO0lBQ1YsV0FBVztBQUNmO0FBRUE7SUFDSSxjQUFjO0lBQ2Qsa0JBQWtCO0lBQ2xCLGlCQUFpQjtJQUNqQix1QkFBdUI7QUFDM0I7QUFFQTtJQUNJLGtCQUFrQjtJQUNsQixVQUFVO0lBQ1YsV0FBVztJQUNYLFVBQVU7SUFDVixZQUFZO0lBQ1osYUFBYTtJQUNiLGFBQWE7SUFDYix1QkFBdUI7SUFDdkIsbUJBQW1CO0lBQ25CLHNCQUFzQjtJQUN0QixrQkFBa0I7SUFDbEIsVUFBVTtJQUNWLFNBQVM7QUFDYjtBQUNBOztJQUVJLGtCQUFrQjtJQUNsQixnQkFBZ0I7SUFDaEIsYUFBYTtJQUNiLFlBQVk7SUFDWixRQUFRO0lBQ1IsU0FBUztJQUNULCtCQUErQjtBQUNuQztBQUVBOztJQUVJLGtCQUFrQjtJQUNsQiwyREFBMkQ7SUFDM0QsbUJBQW1CO0lBQ25CLFlBQVk7SUFDWixhQUFhO0lBQ2IseUJBQXlCO0lBQ3pCLGVBQWU7SUFDZixhQUFhO0VBQ2YsdUJBQXVCO0VBQ3ZCLG9CQUFvQjs7QUFFdEI7QUFDQTtJQUNJLFVBQVU7QUFDZCxhQUFhO0FBQ2Isc0JBQXNCO0FBQ3RCLHVCQUF1QjtBQUN2QixtQkFBbUI7QUFDbkIsU0FBUztBQUNULGtCQUFrQjtBQUNsQixVQUFVO0FBQ1Y7QUFFQTtJQUNJLHFCQUFxQjtJQUNyQixrQkFBa0I7SUFDbEIsV0FBVztJQUNYLFlBQVk7SUFDWixnQkFBZ0I7SUFDaEIsa0JBQWtCO0lBQ2xCLG1CQUFtQjtJQUNuQixrQkFBa0I7QUFDdEI7QUFFQTs7SUFFSSxjQUFjO0lBQ2QsbUJBQW1CO0lBQ25CLG1CQUFtQjtJQUNuQixZQUFZO0FBQ2hCO0FBRUE7O0lBRUksY0FBYzs7QUFFbEI7QUFFQTs7R0FFRyxrQkFBa0I7SUFDakIsZ0JBQWdCO0lBQ2hCLG1CQUFtQjtJQUNuQixZQUFZO0lBQ1osYUFBYTtJQUNiLGtCQUFrQjtBQUN0QjtBQUNBOztJQUVJLGNBQWM7SUFDZCxtQkFBbUI7SUFDbkIsWUFBWTtJQUNaLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLG1CQUFtQjtJQUNuQix1QkFBdUI7QUFDM0I7QUFFQTs7SUFFSSxjQUFjOztBQUVsQjtBQUVBOztJQUVJLFlBQVk7O0FBRWhCO0FBRUE7O0lBRUksa0JBQWtCO0lBQ2xCLGdCQUFnQjtJQUNoQixZQUFZO0lBQ1osbUJBQW1CO0lBR25CLCtDQUErQztJQUMvQyxXQUFXOztBQUVmO0FBR0E7O0lBRUksa0JBQWtCO0lBQ2xCLFlBQVk7SUFDWixvQkFBb0I7O0FBRXhCO0FBRUE7R0FDRyxtQkFBbUI7R0FDbkIsZ0JBQWdCO0dBQ2hCLGlCQUFpQjtBQUNwQjtBQUVBO0lBQ0ksYUFBYTtJQUNiLG1CQUFtQjtJQUNuQix3QkFBd0I7SUFDeEIsbUJBQW1CO0FBQ3ZCO0FBRUE7O0lBRUksWUFBWTtJQUNaLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2Isa0JBQWtCO0lBQ2xCLFlBQVk7SUFHWiwrQ0FBK0M7QUFDbkQ7QUFFQTs7SUFFSSxhQUFhO0FBQ2pCO0FBRUE7O0lBRUksa0JBQWtCO0lBQ2xCLGVBQWU7SUFDZixXQUFXO0lBQ1gsV0FBVztJQUNYLFVBQVU7QUFDZDtBQUNBO0lBQ0ksYUFBYTtBQUNqQiw2QkFBNkI7QUFDN0IsbUJBQW1CO0FBQ25CO0FBRUE7SUFDSSxtQkFBbUI7QUFDdkIsbUJBQW1CO0FBQ25CLHVCQUFrQjtBQUFsQixrQkFBa0I7QUFDbEI7QUFJQTs7SUFFSSxXQUFXO0lBQ1gsbUJBQW1CO0lBQ25CLGFBQWE7SUFDYixrQkFBa0I7SUFDbEIsWUFBWTtJQUdaLCtDQUErQztBQUNuRDtBQUVBOztJQUVJLGFBQWE7QUFDakI7QUFFQTs7SUFFSSxzREFBc0Q7SUFDdEQsV0FBVztJQUNYLFlBQVk7SUFDWixZQUFZO0lBQ1osbUJBQW1CO0lBQ25CLGFBQWE7QUFHakIsK0NBQStDO0FBQy9DO0FBRUE7O0lBRUksc0RBQXNEOztBQUUxRDtBQUVBOztJQUVJLGFBQWE7QUFDakI7QUFFQTtJQUNJLGtCQUFrQjtBQUN0QjtBQUVBOztJQUVJLGtCQUFrQjtJQUNsQixjQUFjO0lBQ2QsVUFBVTtBQUNkO0FBRUE7O0lBRUksd0JBQXdCO0lBQ3hCLFdBQVc7SUFDWCxZQUFZO0lBQ1oscUJBQXFCO0lBQ3JCLG1CQUFtQjtJQUNuQixhQUFhO0FBR2pCLCtDQUErQztBQUMvQztBQUVBOztJQUVJLHdCQUF3QjtBQUM1QjtBQUdBOztJQUVJLGFBQWE7QUFDakI7QUFFQTs7SUFFSSxnQkFBZ0I7QUFDcEI7QUFDQSxvREFBb0Q7QUFFcEQ7SUFDSSxrQkFBa0I7SUFDbEIsWUFBWTtJQUNaLHVCQUF1QjtJQUN2QixhQUFhO0lBQ2IsdUJBQXVCO0lBQ3ZCLG1CQUFtQjtJQUNuQixTQUFTO0lBQ1QsWUFBWTtJQUNaLGdCQUFnQjtJQUNoQixtQkFBbUI7SUFDbkIsMkJBQTJCO0NBQzlCO0FBRUE7SUFDRyxrQkFBa0I7SUFDbEIsTUFBTTtJQUNOLE9BQU87SUFDUCw2Q0FBNkM7SUFDN0MsV0FBVztJQUNYLHNDQUFzQztJQUN0QyxnQkFBZ0I7SUFDaEIsb0JBQW9CO0NBQ3ZCO0FBRUE7SUFDRyxnQkFBZ0I7SUFDaEIsZUFBZTtJQUNmLGdCQUFnQjtJQUNoQiw2QkFBNkI7Q0FDaEM7QUFFQTtJQUNHLHFDQUFxQztDQUN4QztBQUVEO0lBQ0ksV0FBVzs7QUFFZjtBQUVBLHNCQUFzQjtBQUV0QjtJQUNJLFdBQVc7SUFDWCxZQUFZO0lBQ1osZUFBZTtJQUNmLGtCQUFrQjtJQUNsQixZQUFZO0lBQ1osdUJBQXVCO0lBQ3ZCLGFBQWE7SUFDYix1QkFBdUI7SUFDdkIsbUJBQW1CO0dBQ3BCLGNBQWM7SUFDYixZQUFZO01BQ1YsbUJBQW1CO01BQ25CLHNCQUFzQjtJQUN4QixnQkFBZ0I7SUFDaEIsbUJBQW1CO0lBQ25CLDJCQUEyQjtJQUMzQixtQkFBbUI7QUFDdkI7QUFDQTtJQUNJLGlCQUFpQjtJQUNqQixXQUFXO0FBQ2Y7QUFDQTtJQUNJLFdBQVc7SUFDWCxTQUFTO0lBQ1QsZUFBZTtJQUNmLGtCQUFrQjtJQUNsQixjQUFjO0lBQ2QsY0FBYztJQUNkLFVBQVU7QUFDZDtBQUNBO0lBQ0ksYUFBYTtBQUNqQjtBQUNBO0lBQ0ksY0FBYztBQUNsQjtBQUNBOztJQUVJLGNBQWM7QUFDbEI7QUFDQTs7Ozs7SUFLSSxjQUFjO0FBQ2xCO0FBRUEsMkVBQTJFIiwiZmlsZSI6InNyYy9hcHAvYXZpcy9hdmlzLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJib2R5XHJcbntcclxuICAgIGJhY2tncm91bmQ6ICNmYmYzZmY7XHJcbn1cclxuLmxvZ297XHJcbiAgICB3aWR0aDogNjZweDtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcclxuICAgIGFsaWduLWl0ZW1zOiBmbGV4LWVuZDtcclxuICAgIHBvc2l0aW9uOiBmaXhlZDtcclxuICAgIHRvcDogNTQ3cHg7XHJcbiAgICBsZWZ0OiA1NDlweDs7XHJcbn1cclxuXHJcbi5mb3JtLWNoZWNrIHtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgbWluLWhlaWdodDogMS41cmVtO1xyXG4gICAgcGFkZGluZy1sZWZ0OiAyZW07XHJcbiAgICBtYXJnaW4tYm90dG9tOiAwLjEyNXJlbTtcclxufVxyXG5cclxuLmJhY2tncm91bmR7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB3aWR0aDogNzYlO1xyXG4gICAgaGVpZ2h0OiA2NSU7XHJcbiAgICB6LWluZGV4OiAxO1xyXG4gICAgb3BhY2l0eTogMXB4O1xyXG4gICAgb3BhY2l0eTogMC4xMztcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgbGVmdDogNzRweDtcclxuICAgIHRvcDogOTdweDtcclxufVxyXG4uY29udGFpbmVyXHJcbntcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIG1heC13aWR0aDogODAwcHg7XHJcbiAgICBoZWlnaHQ6IDYxOHB4O1xyXG4gICAgbWFyZ2luOiBhdXRvO1xyXG4gICAgdG9wOiA1MCU7XHJcbiAgICBsZWZ0OiAzNSU7XHJcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLC01MCUpO1xyXG59XHJcblxyXG4ubXlSaWdodEN0blxyXG57XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQoNDVkZWcsICM2MjQ2ZmYsICMxMTFENUUgKTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDI1cHg7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICBwYWRkaW5nOiAyNXB4O1xyXG4gICAgY29sb3I6IHJnYigxOTIsIDE5MiwgMTkyKTtcclxuICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgYWxpZ24taXRlbXM6IGluaXRpYWw7XHJcblxyXG59XHJcbi5sb2dvMXtcclxuICAgIHdpZHRoOiA1NSU7XHJcbmRpc3BsYXk6IGZsZXg7XHJcbmZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbmp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG5hbGlnbi1pdGVtczogY2VudGVyO1xyXG50b3A6IDI1cHg7XHJcbnBvc2l0aW9uOiBhYnNvbHV0ZTtcclxubGVmdDogMjBweDtcclxufVxyXG5cclxuLmNpcmNsZXtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIHdpZHRoOiA4MHB4O1xyXG4gICAgaGVpZ2h0OiA4MHB4O1xyXG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICAgIGJhY2tncm91bmQ6ICMxMTFENUU7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDEwcHg7XHJcbn1cclxuXHJcbi5teVJpZ2h0Q3RuIGhlYWRlclxyXG57XHJcbiAgICBjb2xvcjogI2ZmZmZmZjtcclxuICAgIG1hcmdpbi1ib3R0b206IDUwcHg7XHJcbiAgICB0ZXh0LWFsaWduOiBpbml0aWFsO1xyXG4gICAgaGVpZ2h0OiA4NnB4O1xyXG59XHJcblxyXG4ubXlSaWdodEN0biBoZWFkZXIgaDFcclxue1xyXG4gICAgY29sb3I6ICNmZmZmZmY7XHJcbiAgIFxyXG59XHJcblxyXG4ubXlMZWZ0Q3RuXHJcbntcclxuICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgYmFja2dyb3VuZDogI2ZmZjtcclxuICAgIGJvcmRlci1yYWRpdXM6IDI1cHg7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICBwYWRkaW5nOiAyNXB4O1xyXG4gICAgcGFkZGluZy1sZWZ0OiA1MHB4O1xyXG59XHJcbi5teUxlZnRDdG4gaGVhZGVyXHJcbntcclxuICAgIGNvbG9yOiAjNjk2OTY5O1xyXG4gICAgbWFyZ2luLWJvdHRvbTogNTBweDtcclxuICAgIGhlaWdodDogOTRweDtcclxuICAgIHRleHQtYWxpZ246IGluaXRpYWw7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxufVxyXG5cclxuLm15TGVmdEN0biBoZWFkZXIgaDFcclxue1xyXG4gICAgY29sb3I6ICMxMTFENUU7XHJcbiAgIFxyXG59XHJcblxyXG4ucm93XHJcbntcclxuICAgIGhlaWdodDogMTAwJTtcclxuIFxyXG59XHJcblxyXG4ubXlDYXJkXHJcbntcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGJhY2tncm91bmQ6ICNmZmY7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICBib3JkZXItcmFkaXVzOiAyNXB4O1xyXG4gICAgLXdlYmtpdC1ib3gtc2hhZG93OiAwcHggMTBweCA0MHB4IC0xMHB4IHJnYmEoMCwwLDAsMC43KTtcclxuICAgIC1tb3otYm94LXNoYWRvdzogMHB4IDEwcHggNDBweCAtMTBweCByZ2JhKDAsMCwwLDAuNyk7XHJcbiAgICBib3gtc2hhZG93OiAwcHggMTBweCA0MHB4IC0xMHB4IHJnYmEoMCwwLDAsMC43KTtcclxuICAgIHdpZHRoOiAxNTklO1xyXG5cclxufVxyXG5cclxuXHJcbi5ib3hcclxue1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgbWFyZ2luOiAyMHB4O1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMTAwcHg7XHJcbiAgIFxyXG59XHJcblxyXG4uZW5ke1xyXG4gICBtYXJnaW4tYm90dG9tOiAzcmVtO1xyXG4gICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG4gICBtYXJnaW4tbGVmdDogMTVweDtcclxufVxyXG5cclxuLmJsb2Nre1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGluaXRpYWw7IFxyXG4gICAgbWFyZ2luLWJvdHRvbTogMjVweDtcclxufVxyXG5cclxuLm15TGVmdEN0biAubXlJbnB1dFxyXG57XHJcbiAgICB3aWR0aDogMjM2cHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiAyNXB4O1xyXG4gICAgcGFkZGluZzogMTBweDtcclxuICAgIHBhZGRpbmctbGVmdDogNTBweDtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICAgIC13ZWJraXQtYm94LXNoYWRvdzogMHB4IDEwcHggNDlweCAtMTRweCByZ2JhKDAsMCwwLDAuNyk7XHJcbiAgICAtbW96LWJveC1zaGFkb3c6IDBweCAxMHB4IDQ5cHggLTE0cHggcmdiYSgwLDAsMCwwLjcpO1xyXG4gICAgYm94LXNoYWRvdzogMHB4IDEwcHggNDlweCAtMTRweCByZ2JhKDAsMCwwLDAuNyk7XHJcbn1cclxuXHJcbi5teUxlZnRDdG4gLm15SW5wdXQ6Zm9jdXNcclxue1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxufVxyXG5cclxuLm15Rm9ybVxyXG57XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBtYXJnaW4tdG9wOiA1cHg7XHJcbiAgICBoZWlnaHQ6IDIwJTtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgei1pbmRleDogMTtcclxufVxyXG4ubXlSaWdodEN0biAucmFkaW97XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG5qdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWV2ZW5seTtcclxuZmxleC1kaXJlY3Rpb246IHJvdztcclxufVxyXG5cclxuLm15UmlnaHRDdG4gLmZvcm0tZ3JvdXB7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAxcmVtO1xyXG50ZXh0LWFsaWduOiBpbml0aWFsO1xyXG53aWR0aDogZml0LWNvbnRlbnQ7XHJcbn1cclxuXHJcblxyXG5cclxuLm15UmlnaHRDdG4gLm15SW5wdXRcclxue1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBib3JkZXItcmFkaXVzOiAyNXB4O1xyXG4gICAgcGFkZGluZzogMTBweDtcclxuICAgIHBhZGRpbmctbGVmdDogNTBweDtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICAgIC13ZWJraXQtYm94LXNoYWRvdzogMHB4IDEwcHggNDlweCAtMTRweCByZ2JhKDAsMCwwLDAuNyk7XHJcbiAgICAtbW96LWJveC1zaGFkb3c6IDBweCAxMHB4IDQ5cHggLTE0cHggcmdiYSgwLDAsMCwwLjcpO1xyXG4gICAgYm94LXNoYWRvdzogMHB4IDEwcHggNDlweCAtMTRweCByZ2JhKDAsMCwwLDAuNyk7XHJcbn1cclxuXHJcbi5teVJpZ2h0Q3RuIC5teUlucHV0OmZvY3VzXHJcbntcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbn1cclxuXHJcbi5teUxlZnRDdG4gLmJ1dHRcclxue1xyXG4gICAgYmFja2dyb3VuZDogIGxpbmVhci1ncmFkaWVudCg0NWRlZywgI2ZkMzYzNiwgI2U4MDAwMCApO1xyXG4gICAgY29sb3I6ICNmZmY7XHJcbiAgICB3aWR0aDogMjMwcHg7XHJcbiAgICBib3JkZXI6IG5vbmU7XHJcbiAgICBib3JkZXItcmFkaXVzOiAyNXB4O1xyXG4gICAgcGFkZGluZzogMTBweDtcclxuICAgIC13ZWJraXQtYm94LXNoYWRvdzogMHB4IDEwcHggNDFweCAtMTFweCByZ2JhKDAsMCwwLDAuNyk7XHJcbi1tb3otYm94LXNoYWRvdzogMHB4IDEwcHggNDFweCAtMTFweCByZ2JhKDAsMCwwLDAuNyk7XHJcbmJveC1zaGFkb3c6IDBweCAxMHB4IDQxcHggLTExcHggcmdiYSgwLDAsMCwwLjcpO1xyXG59XHJcblxyXG4ubXlMZWZ0Q3RuIC5idXR0OmhvdmVyXHJcbntcclxuICAgIGJhY2tncm91bmQ6ICBsaW5lYXItZ3JhZGllbnQoNDVkZWcsICNmZjViNWIsICNmZjI2MjYgKTtcclxuXHJcbn1cclxuXHJcbi5teUxlZnRDdG4gLmJ1dHQ6Zm9jdXNcclxue1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxufVxyXG5cclxuLm15UmlnaHRDdG4gbGFiZWx7XHJcbiAgICBtYXJnaW4tYm90dG9tOiA4cHg7XHJcbn1cclxuXHJcbi5teUxlZnRDdG4gLmZhc1xyXG57XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBjb2xvcjogIzExMUQ1RTtcclxuICAgIGxlZnQ6IDM2cHg7XHJcbn1cclxuXHJcbi5idXR0X291dFxyXG57XHJcbiAgICBiYWNrZ3JvdW5kOiAgdHJhbnNwYXJlbnQ7XHJcbiAgICBjb2xvcjogI2ZmZjtcclxuICAgIHdpZHRoOiAxMjBweDtcclxuICAgIGJvcmRlcjogMnB4IHNvbGlkI2ZmZjtcclxuICAgIGJvcmRlci1yYWRpdXM6IDI1cHg7XHJcbiAgICBwYWRkaW5nOiAxMHB4O1xyXG4gICAgLXdlYmtpdC1ib3gtc2hhZG93OiAwcHggMTBweCA0OXB4IC0xNHB4IHJnYmEoMCwwLDAsMC43KTtcclxuLW1vei1ib3gtc2hhZG93OiAwcHggMTBweCA0OXB4IC0xNHB4IHJnYmEoMCwwLDAsMC43KTtcclxuYm94LXNoYWRvdzogMHB4IDEwcHggNDlweCAtMTRweCByZ2JhKDAsMCwwLDAuNyk7XHJcbn1cclxuXHJcbi5idXR0X291dDpob3ZlclxyXG57XHJcbiAgICBib3JkZXI6IDJweCBzb2xpZCNlZWNiZmY7XHJcbn1cclxuXHJcblxyXG4uYnV0dF9vdXQ6Zm9jdXNcclxue1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxufVxyXG5cclxuLm15UmlnaHRDdG4gLmVuZFxyXG57XHJcbiAgICBtYXJnaW4tdG9wOiAyNnB4OyAgICBcclxufVxyXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKipyYXRpbmcqKioqKioqKioqKioqKioqL1xyXG5cclxuLnJhdGluZyB7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICB3aWR0aDogMTgwcHg7XHJcbiAgICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBnYXA6IC4zZW07XHJcbiAgICBwYWRkaW5nOiA1cHg7XHJcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMjBweDtcclxuICAgIGJveC1zaGFkb3c6IDAgMCAycHggI2IzYWNhYztcclxuIH1cclxuIFxyXG4gLnJhdGluZ19fcmVzdWx0IHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRvcDogMDtcclxuICAgIGxlZnQ6IDA7XHJcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVkoLTEwcHgpIHRyYW5zbGF0ZVgoLTVweCk7XHJcbiAgICB6LWluZGV4OiAtOTtcclxuICAgIGZvbnQ6IDNlbSBBcmlhbCwgSGVsdmV0aWNhLCBzYW5zLXNlcmlmO1xyXG4gICAgY29sb3I6ICNlYmViZWI4ZTtcclxuICAgIHBvaW50ZXItZXZlbnRzOiBub25lO1xyXG4gfVxyXG4gXHJcbiAucmF0aW5nX19zdGFyIHtcclxuICAgIGZvbnQtc2l6ZTogMS4zZW07XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICBjb2xvcjogI2RhYmQxOGIyO1xyXG4gICAgdHJhbnNpdGlvbjogZmlsdGVyIGxpbmVhciAuM3M7XHJcbiB9XHJcbiBcclxuIC5yYXRpbmdfX3N0YXI6aG92ZXIge1xyXG4gICAgZmlsdGVyOiBkcm9wLXNoYWRvdygxcHggMXB4IDRweCBnb2xkKTtcclxuIH1cclxuXHJcbi5zdGFyQWN0aXZle1xyXG4gICAgY29sb3I6IGdvbGQ7XHJcbiAgICBcclxufVxyXG5cclxuLyoqKioqKioqKioqKioqKioqKioqKi9cclxuXHJcbi5yYXRlIHtcclxuICAgIGZsb2F0OiBsZWZ0O1xyXG4gICAgaGVpZ2h0OiAzOHB4O1xyXG4gICAgcGFkZGluZzogMCAxMHB4O1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgd2lkdGg6IDE4MHB4O1xyXG4gICAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAvKiBnYXA6IC4zZW07Ki9cclxuICAgIHBhZGRpbmc6IDVweDtcclxuICAgICAgcGFkZGluZy1ib3R0b206IDVweDtcclxuICAgICAgZmxleC1mbG93OiByb3ctcmV2ZXJzZTtcclxuICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICBib3JkZXItcmFkaXVzOiAyMHB4O1xyXG4gICAgYm94LXNoYWRvdzogMCAwIDJweCAjYjNhY2FjO1xyXG4gICAgcGFkZGluZy1ib3R0b206IDBweDtcclxufVxyXG4ucmF0ZTpub3QoOmNoZWNrZWQpID4gaW5wdXQge1xyXG4gICAgcG9zaXRpb246YWJzb2x1dGU7XHJcbiAgICB0b3A6LTk5OTlweDtcclxufVxyXG4ucmF0ZTpub3QoOmNoZWNrZWQpID4gbGFiZWwge1xyXG4gICAgZmxvYXQ6cmlnaHQ7XHJcbiAgICB3aWR0aDoxZW07XHJcbiAgICBvdmVyZmxvdzpoaWRkZW47XHJcbiAgICB3aGl0ZS1zcGFjZTpub3dyYXA7XHJcbiAgICBjdXJzb3I6cG9pbnRlcjtcclxuICAgIGZvbnQtc2l6ZTozMHB4O1xyXG4gICAgY29sb3I6I2NjYztcclxufVxyXG4ucmF0ZTpub3QoOmNoZWNrZWQpID4gbGFiZWw6YmVmb3JlIHtcclxuICAgIGNvbnRlbnQ6ICfimIUgJztcclxufVxyXG4ucmF0ZSA+IGlucHV0OmNoZWNrZWQgfiBsYWJlbCB7XHJcbiAgICBjb2xvcjogI2ZmYzcwMDsgICAgXHJcbn1cclxuLnJhdGU6bm90KDpjaGVja2VkKSA+IGxhYmVsOmhvdmVyLFxyXG4ucmF0ZTpub3QoOmNoZWNrZWQpID4gbGFiZWw6aG92ZXIgfiBsYWJlbCB7XHJcbiAgICBjb2xvcjogI2RlYjIxNzsgIFxyXG59XHJcbi5yYXRlID4gaW5wdXQ6Y2hlY2tlZCArIGxhYmVsOmhvdmVyLFxyXG4ucmF0ZSA+IGlucHV0OmNoZWNrZWQgKyBsYWJlbDpob3ZlciB+IGxhYmVsLFxyXG4ucmF0ZSA+IGlucHV0OmNoZWNrZWQgfiBsYWJlbDpob3ZlcixcclxuLnJhdGUgPiBpbnB1dDpjaGVja2VkIH4gbGFiZWw6aG92ZXIgfiBsYWJlbCxcclxuLnJhdGUgPiBsYWJlbDpob3ZlciB+IGlucHV0OmNoZWNrZWQgfiBsYWJlbCB7XHJcbiAgICBjb2xvcjogI2M1OWIwODtcclxufVxyXG5cclxuLyogTW9kaWZpZWQgZnJvbTogaHR0cHM6Ly9naXRodWIuY29tL211a3Vsa2FudC9TdGFyLXJhdGluZy11c2luZy1wdXJlLWNzcyAqLyJdfQ== */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AvisComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-avis',
                templateUrl: './avis.component.html',
                styleUrls: ['./avis.component.css']
            }]
    }], function () { return [{ type: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"] }, { type: src_app_services_cpn_avis_service__WEBPACK_IMPORTED_MODULE_3__["AvisService"] }]; }, null); })();


/***/ }),

/***/ "./src/app/baseUrl.ts":
/*!****************************!*\
  !*** ./src/app/baseUrl.ts ***!
  \****************************/
/*! exports provided: baseUrl */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "baseUrl", function() { return baseUrl; });
const baseUrl = "https://www.cpn-aide-aux-entreprise.jobid.fr";


/***/ }),

/***/ "./src/app/home/home.component.ts":
/*!****************************************!*\
  !*** ./src/app/home/home.component.ts ***!
  \****************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var src_app_services_token_storage_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/services/token-storage.service */ "./src/app/services/token-storage.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _services_cpn_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/cpn/auth.service */ "./src/app/services/cpn/auth.service.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var ng_lazyload_image__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ng-lazyload-image */ "./node_modules/ng-lazyload-image/__ivy_ngcc__/fesm2015/ng-lazyload-image.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");








function HomeComponent_a_11_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "a", 159);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, " Inscription");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "/cpn/Inscription");
} }
function HomeComponent_a_13_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "a", 160);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, " Connexion");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "/cpn/Connexion");
} }
function HomeComponent_li_14_Template(rf, ctx) { if (rf & 1) {
    const _r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "li", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 161);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "a", 162);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 163);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "a", 164);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "profile");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "a", 165);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function HomeComponent_li_14_Template_a_click_7_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r4); const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r3.logout(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "d\u00E9connexion");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx_r2.user.first_name, " ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "/cpn/profile");
} }
class HomeComponent {
    constructor(tokenStorage, route, auth) {
        this.tokenStorage = tokenStorage;
        this.route = route;
        this.auth = auth;
        this.token = "";
        this.user = null;
        this.connect = false;
        this.role = "logout";
    }
    ngOnInit() {
        this.auth.getFellower().subscribe(res => {
            this.follow = res;
        });
        if (this.tokenStorage.getUser() != false) {
            this.token = this.tokenStorage.getUser();
            this.user = JSON.parse(this.token);
            this.role = this.user.role;
            this.connect = true;
        }
        this.serachbar();
        this.compteur();
    }
    serachbar() {
        $(document).mousemove(function (e) {
            $('#info-box').css('top', e.pageY - $('#info-box').height() - 30);
            $('#info-box').css('left', e.pageX - ($('#info-box').width()) / 2);
        }).mouseover();
        $('.search').mouseenter(function () {
            $(this).addClass('search--show');
            $(this).removeClass('search--hide');
        });
        $('.search').mouseleave(function () {
            $(this).addClass('search--hide');
            $(this).removeClass('search--show');
        });
    }
    compteur() {
        $(document).ready(function () {
            $('.item_num').counterUp({
                time: 2000
            });
        });
    }
    logout() {
        this.tokenStorage.signOut();
        this.connect = false;
        this.role = "logout";
        location.href = '/home';
    }
}
HomeComponent.ɵfac = function HomeComponent_Factory(t) { return new (t || HomeComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_token_storage_service__WEBPACK_IMPORTED_MODULE_1__["TokenStorageService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_cpn_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"])); };
HomeComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: HomeComponent, selectors: [["app-home"]], decls: 336, vars: 11, consts: [[1, "navbar", "navbar-expand-lg", "nav_g"], [1, "navbar-brand", 3, "routerLink"], ["src", "assets/cpnimages/logo/logo-cpn-blanc.png", "alt", "Logo", 1, "brand_logo", "d-inline-block", "align-text-top", "nav_img"], ["type", "button", "data-toggle", "collapse", "data-target", "#navbarSupportedContent", "aria-controls", "navbarSupportedContent", "aria-expanded", "false", "aria-label", "Toggle navigation", 1, "navbar-toggler", 2, "box-shadow", "none"], [1, "far", "fa-bars", "navbar-toggler-icon", 2, "color", "white", "z-index", "1"], ["id", "navbarSupportedContent", 1, "collapse", "navbar-collapse"], [1, "navbar-nav", "ml-auto", "topnav"], [1, "nav-item"], ["href", "https://cpn-aide-aux-entreprises.com/elementor-hf/menu-de-navigation/?fbclid=IwAR21xUJ1IVljTge9wrx_wMpVfn4X_FYFoYWwBYSW_oXP0fCPkbRqxO-PKZc", 1, "nav-link"], ["class", "nav-link", 3, "routerLink", 4, "ngIf"], ["class", "nav-link con", 3, "routerLink", 4, "ngIf"], ["class", "nav-item", 4, "ngIf"], [1, "primary_body", "mb-5"], [1, "home_container"], [1, "section_heading", "mb-3"], [1, "heading_wrapper", "container-fluid", "g-0"], [1, "row", "g-0"], [1, "col-md-8", "col-12"], [1, "row", "g-0", "justify-content-center"], [1, "col-auto"], [1, "img_wrapper", "content"], ["alt", "", 1, "heading_img", 3, "defaultImage", "lazyLoad"], [1, "col-md-4", "p-3", "block0"], [1, "row", "g-0", "justify-content-start"], [1, "col-md-auto", "transtion"], [1, "title_heading"], [1, "desc_heading"], [1, "search_bloc"], [1, "search"], ["placeholder", "Quel type de subvention souhaitez vous", 1, "search__input"], [1, "carre"], [1, "row"], [1, "col", 2, "display", "initial", "flex-direction", "row", "padding", "8px", "text-align", "start"], ["src", "assets/cpnimages/home/I.png", "alt", "", 2, "width", "10%", "margin", "0 0 0 20px"], [2, "margin-left", "4px", "font-size", "13px"], [2, "text-align", "center", "margin", "0 90px 0 0"], [2, "text-align", "center", "font-size", "12px", "margin", "0 0px 0 -45px"], [2, "color", "#00ff00"], [1, "our_success", "mb-3"], [1, "success_wrapper", "container", "px-4", "g-0"], [1, "col-md-6", "py-2", "title"], [1, "success_txt"], [1, "success_desc"], [1, "col-md-6", "py-2", "chiffre"], [1, "success_list"], [1, "success_item"], [1, "k"], [1, "item_num"], [1, "item_desc"], [1, "actuality", "mb-5"], [1, "float_actions"], [1, "actions_content"], [1, "action_items"], ["href", "cpn/Home_tpe_pme", 1, "item_href"], [1, "ihref_logo"], ["width", "40%", "src", "assets/cpnimages/sidebar/Entreprise.png", "alt", ""], [1, "ihref_text"], [1, "testmegi", 3, "routerLink"], ["href", "/cpn/agence", 1, "item_href"], ["width", "40%", "src", "assets/cpnimages/sidebar/Agence.png"], ["href", "/cpn/Home_collectivite", 1, "item_href"], ["width", "40%", "src", "assets/cpnimages/sidebar/Collectivit\u00E9.png"], [1, "card-blog", "mb-5"], [1, "card_wrapper", "container", "px-4", "g-0"], [1, "row", "py-3"], [1, "col-md-4"], [1, "card", "d-flex", "flex-column", "align-items-center", "justify-content-center", "block2"], ["src", "assets/cpnimages/home/agent.png", "alt", "...", 1, "card-img-top", 2, "height", "50%", "width", "50%"], [1, "card-body", "py-0", "d-flex", "flex-column", "justify-content-center", "align-items-center"], [1, "card-title", "home"], [1, "card-text", "home"], [1, "card-footer", "py-0"], [1, "test-btn1", "btn", "btn-light", 3, "routerLink"], ["src", "assets/cpnimages/home/casque.png", "alt", "...", 1, "card-img-top", 2, "height", "50%", "width", "50%"], [1, "card-text", "home", 2, "text-align", "center"], ["href", "#", 1, "test-btn2", "btn", "btn-light"], ["src", "assets/cpnimages/home/money.png", "alt", "...", 1, "card-img-top", 2, "height", "50%", "width", "50%"], ["href", "#", 1, "test-btn3", "btn", "btn-light"], [1, "text-bloc", "g-0", "mb-5", "block3"], [1, "container", "px-4"], [1, "text_body"], [1, "text_content"], [1, "divider", "g-0", "mb-5"], [1, "divider_ligne"], [1, "block4"], [1, "container", "px-4", "block1"], [1, "wavy"], [1, "outer-div", "one"], [1, "inner-div"], [1, "front"], [1, "front__face-photo1"], ["src", "assets/cpnimages/home/icone-2.png", "alt", "", 2, "width", "100%"], [1, "front__text"], [1, "front__text-header"], [1, "front__text-para"], [1, "outer-div", "two"], [1, "front__face-photo2"], ["src", "assets/cpnimages/home/icone-1.png", "alt", "", 2, "width", "100%"], [1, "outer-div", "three"], [1, "front__face-photo3"], ["src", "assets/cpnimages/home/icone-3.png", "alt", "", 2, "width", "100%"], [1, "third-bloc", "g-0", "mb-5"], [1, "container", "px-4", "text-center"], [2, "color", "#111d5e"], [1, "subvention-text", "text-center", 2, "color", "gray"], [1, "col-6", "block5"], ["src", "assets/cpnimages/home/old.PNG", "alt", "..."], [1, "col-6", "block6"], [1, "third-bloc-border"], [1, "block7"], [1, "container", "px-4", "lastB"], [1, "text-center"], [1, "row", "row-cols-1", "row-cols-md-3", "g-4", "justify-content-center"], [1, "col-md-3", "card1"], [1, "card", "h-100", "box1"], [1, "card-body"], [1, "card-text"], [1, "image"], ["src", "assets/cpnimages/home/avis3.png", "alt", " avis 1", 1, "img-fluid"], [1, "col-md-3", "card2"], [1, "card", "h-100", "box2"], ["src", "assets/cpnimages/home/avis1.png", "alt", " avis 2", 1, "img-fluid"], [1, "col-md-3", "card3"], [1, "card", "h-100", "box3"], [1, "card-text", 2, "margin-right", "-43px"], ["src", "assets/cpnimages/home/avis2.png", "alt", "avis 3", 1, "img-fluid"], [1, "text-lg-start", "text-muted"], [1, "d-flex", "justify-content-center", "justify-content-lg-between"], [1, ""], [1, "container", "text-md-start", "mt-5", "footer", 2, "font-size", "12px"], [1, "row", "mt-3"], [1, "col-md-3", "col-lg-4", "col-xl-3", "mx-auto"], [1, "text-uppercase", "fw-bold", "mb-4"], ["src", "assets/cpnimages/logo/logo-cpn-blanc.png", "alt", "logo", "width", "50px", "height", "50px"], [2, "color", "white", "font-size", "12px"], [2, "font-size", "25px"], ["href", "https://www.instagram.com/cpn_aideauxentreprises/?hl=fr"], ["aria-hidden", "true", 1, "fab", "fa-instagram"], ["href", "https://www.youtube.com/channel/UC2KAUP-XzalYUGGPLEXBUBQ"], ["aria-hidden", "true", 1, "fab", "fa-youtube", 2, "margin-left", "5px"], ["href", "https://twitter.com/cpn_officiel"], ["aria-hidden", "true", 1, "fab", "fa-twitter", 2, "margin-left", "5px"], ["href", "https://www.linkedin.com/company/76078573/admin/"], ["aria-hidden", "true", 1, "fab", "fa-linkedin", 2, "margin-left", "5px"], ["href", "https://www.facebook.com/CPN.aideauxentreprises"], ["aria-hidden", "true", 1, "fab", "fa-facebook", 2, "margin-left", "5px"], [1, "col-md-2", "col-lg-2", "col-xl-2", "mx-auto"], [1, "text-uppercase", "fw-bold", "mb-4", 2, "color", "white"], ["href", "#!", 1, "text-reset", 2, "color", "white"], [1, "col-md-3", "col-lg-2", "col-xl-2", "mx-auto"], ["href", "#!", 1, "text-reset", "text-left", 2, "color", "white"], [1, "col-md-4", "col-lg-3", "col-xl-3", "mx-auto", "mb-md-0"], [2, "color", "white"], [1, "fas", "fa-phone", "me-3"], [2, "color", "white", "width", "-moz-available"], [1, "fas", "fa-envelope", "me-3"], ["href", "mailto:votreconseiller@cpn-aide-aux-entreprise.com", 2, "color", "#fff", "width", "-moz-available", "text-decoration", "none"], [1, "copyright"], ["href", "https://jobid.fr/", 1, "text-reset", "fw-bold", 2, "color", "white"], [1, "nav-link", 3, "routerLink"], [1, "nav-link", "con", 3, "routerLink"], [1, "dropdown"], ["role", "button", "id", "dropdownMenuLink", "data-toggle", "dropdown", "aria-haspopup", "true", "aria-expanded", "false", 1, "nav-link", "dropdown-toggle"], ["aria-labelledby", "dropdownMenuLink", 1, "dropdown-menu", "drop"], [1, "dropdown-item", 3, "routerLink"], [1, "dropdown-item", 3, "click"]], template: function HomeComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "nav", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "a", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "button", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "i", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "ul", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "li", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "a", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, "Accedez au site");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "li", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](11, HomeComponent_a_11_Template, 2, 1, "a", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "li", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](13, HomeComponent_a_13_Template, 2, 1, "a", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](14, HomeComponent_li_14_Template, 9, 2, "li", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "section", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "div", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "div", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "div", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "div", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "div", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](24, "img", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "div", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "div", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "div", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "h2", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](29, "Transition Num\u00E9rique");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "h4", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](31, "2021");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "div", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "form", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](34, "input", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "div", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "div", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "div", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](38, "img", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "span", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](40, "Followers");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "h4", 35);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](42);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "p", 36);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "a", 37);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](45, "2.1%");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](46, "vs last 7 days ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "section", 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "div", 39);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](49, "div", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "div", 40);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](51, "h5", 41);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](52, "Notre succ\u00E9s");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](53, "h3", 42);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](54, " Peut importe votre secteur d'acitivit\u00E9 nous vous offrons une subvention ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](55, "div", 43);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](56, "ul", 44);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](57, "li", 45);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](58, "h3", 46);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](59, "span", 47);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](60, "562");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](61, "K");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](62, "p", 48);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](63, "Entreprises");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](64, "li", 45);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](65, "h3", 46);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](66, "span", 47);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](67, "10");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](68, "K");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](69, "p", 48);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](70, "Subventions");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](71, "li", 45);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](72, "h3", 46);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](73, "span", 47);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](74, "200");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](75, "K+");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](76, "p", 48);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](77, "Adh\u00E9rants");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](78, "section", 49);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](79, "div", 50);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](80, "ul", 51);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](81, "li", 52);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](82, "a", 53);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](83, "i", 54);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](84, "img", 55);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](85, "p", 56);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](86, "Entreprise");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](87, "a", 57);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](88, "Testez mon \u00E9gibilit\u00E9");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](89, "li", 52);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](90, "a", 58);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](91, "i", 54);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](92, "img", 59);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](93, "p", 56);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](94, "Agence");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](95, "a", 57);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](96, "Testez mon \u00E9gibilit\u00E9");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](97, "li", 52);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](98, "a", 60);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](99, "i", 54);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](100, "img", 61);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](101, "p", 56);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](102, "Collectivites");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](103, "a", 57);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](104, "Testez mon \u00E9gibilit\u00E9");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](105, "section", 62);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](106, "div", 63);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](107, "div", 64);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](108, "div", 65);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](109, "div", 66);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](110, "img", 67);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](111, "div", 68);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](112, "h5", 69);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](113, "Subvention imm\u00E9diate");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](114, "p", 70);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](115, "Besoin d'un ch\u00E8que Num\u00E9rique");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](116, "div", 71);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](117, "a", 72);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](118, "Testez Votre \u00E9ligibilit\u00E9");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](119, "div", 65);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](120, "div", 66);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](121, "img", 73);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](122, "div", 68);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](123, "h5", 69);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](124, "Accompagnement");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](125, "p", 74);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](126, " Des Experts \u00E0 votre disposition pour digitaliser votre entreprise ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](127, "div", 71);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](128, "a", 75);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](129, "En savoir plus");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](130, "div", 65);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](131, "div", 66);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](132, "img", 76);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](133, "div", 68);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](134, "h5", 69);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](135, "Financement");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](136, "p", 74);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](137, " Obtenez le imm\u00E9diatement sur votre devis ou facture ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](138, "div", 71);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](139, "a", 77);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](140, "En savoir plus");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](141, "section", 78);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](142, "div", 79);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](143, "ul", 80);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](144, "li", 81);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](145, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](146, "Conseils et Accompagnement");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](147, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](148, " Le CPN est un acteur majeur dans la transition digital des entreprises nous subventionnons et accompagnons tous type de projet de d\u00E9veloppement informatique nous vous orientons au-pr\u00E8s d\u2019agence de d\u00E9veloppement v\u00E9rifier et d\u00E9sign\u00E9. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](149, "section", 82);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](150, "div", 79);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](151, "span", 83);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](152, "section", 84);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](153, "div", 85);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](154, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](155, "Facilit\u00E9 & Rapidit\u00E9");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](156, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](157, " Tous les services du CPN Aide aux entreprises sont destin\u00E9e aux petites et moyennes entreprises et Start-up. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](158, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](159, " Nous faisons un suivie et donnons acc\u00E8s a notre r\u00E9seau de partenaires et d\u2019entreprise pour favoris\u00E9 leurs croissance. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](160, "div", 86);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](161, "div", 87);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](162, "div", 88);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](163, "div", 89);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](164, "div", 90);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](165, "img", 91);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](166, "div", 92);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](167, "h3", 93);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](168, "Agence");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](169, "p", 94);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](170, " Grace au Cpn Soyez accompagn\u00E9 au pr\u00E9s d'agence garentie et referenc\u00E9 ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](171, "div", 95);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](172, "div", 88);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](173, "div", 89);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](174, "div", 96);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](175, "img", 97);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](176, "div", 92);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](177, "h3", 93);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](178, " Transformation ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](179, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](180, " digitale ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](181, "p", 94);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](182, " Grace au Cpn Soyez accompagn\u00E9 au pr\u00E9s d'agence garentie et referenc\u00E9 ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](183, "div", 98);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](184, "div", 88);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](185, "div", 89);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](186, "div", 99);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](187, "img", 100);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](188, "div", 92);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](189, "h3", 93);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](190, " Financement ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](191, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](192, " Im\u00E9diat ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](193, "p", 94);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](194, " Grace au Cpn Soyez accompagn\u00E9 au pr\u00E9s d'agence garentie et referenc\u00E9 ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](195, "section", 101);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](196, "div", 102);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](197, "h1", 103);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](198, "Pour obtenir votre subvention");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](199, "p", 104);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](200, " Le CPN s'engage \u00E0 vous mettre en relation avec une agence. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](201, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](202, " Le cabinet vous permet d'obtenir une subvention sur votre ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](203, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](204, " d\u00E9veloppement informatique, et vous met a disposition \u00E9galement ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](205, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](206, " sont r\u00E9seaux d'entreprises et de partenaire international. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](207, "div", 105);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](208, "img", 106);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](209, "div", 107);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](210, "div", 108);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](211, "h3");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](212, "Economiser");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](213, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](214, " nos subventions sont calcul\u00E9es par rapport \u00E0 votre investissement quel que soit le poids de votre projet digital. Nos aides varient de 1000 \u00E0 10 000 euro de subvention imm\u00E9diate. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](215, "div", 108);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](216, "h3");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](217, "Gagner du temps");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](218, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](219, " Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](220, "div", 108);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](221, "h3");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](222, "Service de qualit\u00E9");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](223, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](224, " Tous nos conseillers d\u00E9tiennent un domaine d\u2019expertise qui leurs est propre et pourront vous accompagner dans la r\u00E9alisation de vos projets. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](225, "section", 109);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](226, "div", 110);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](227, "div", 111);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](228, "h1", 103);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](229, "Avis D'entreprises");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](230, "p", 104);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](231, " Ils nous ont fait confiance , ont \u00E9t\u00E9 accompagn\u00E9s par le CPN et ont r\u00E9ussi \u00E0 avoir leur ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](232, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](233, " subventions ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](234, "div", 112);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](235, "div", 113);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](236, "div", 114);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](237, "div", 115);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](238, "p", 116);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](239, " \"J\u2019ai \u00E9t\u00E9 accompagn\u00E9 par le CPN et j\u2019ai eu ma subvention, excellent service.\" ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](240, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](241, "Justin Rhodes");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](242, "h6");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](243, "Marketing officer");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](244, "div", 117);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](245, "img", 118);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](246, "div", 119);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](247, "div", 120);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](248, "div", 115);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](249, "p", 116);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](250, " \"Rapidit\u00E9, efficacit\u00E9 et un tr\u00E8s bon service client, le CPN m\u2019a aid\u00E9 \u00E0 num\u00E9riser mon entreprise.\" ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](251, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](252, "Justin Rhodes");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](253, "h6");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](254, "Marketing officer");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](255, "div", 117);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](256, "img", 121);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](257, "div", 122);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](258, "div", 123);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](259, "div", 115);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](260, "p", 124);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](261, " \"Gr\u00E2ce \u00E0 la digitalisation de mon entreprise j\u2019ai pu augmenter mon chiffre d\u2019affaire, et c\u2019est gr\u00E2ce au CPN que j\u2019ai pu \u00EAtre bien accompagn\u00E9 et conseill\u00E9.\" ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](262, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](263, "Justin Rhodes");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](264, "h6");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](265, "Marketing officer");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](266, "div", 117);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](267, "img", 125);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](268, "footer", 126);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](269, "section", 127);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](270, "section", 128);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](271, "div", 129);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](272, "div", 130);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](273, "div", 131);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](274, "h6", 132);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](275, "img", 133);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](276, "p", 134);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](277, " Le Cabinet de Propulsion Num\u00E9rique aide les entreprises \u00E0 se propulser num\u00E9riquement et \u00E0 b\u00E9n\u00E9ficier de financement. CPN est un organisme de financement \u00E0 but non lucratif. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](278, "p", 135);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](279, "a", 136);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](280, "i", 137);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](281, "a", 138);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](282, "i", 139);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](283, "a", 140);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](284, "i", 141);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](285, "a", 142);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](286, "i", 143);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](287, "a", 144);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](288, "i", 145);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](289, "div", 146);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](290, "h6", 147);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](291, "Menu");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](292, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](293, "a", 148);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](294, "Acceuil");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](295, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](296, "a", 148);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](297, "Actualit\u00E9");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](298, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](299, "a", 148);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](300, "Agenda");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](301, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](302, "a", 148);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](303, "A propos");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](304, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](305, "a", 148);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](306, "Contactez-nous");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](307, "div", 149);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](308, "h6", 147);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](309, " Support ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](310, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](311, "a", 150);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](312, "FAQ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](313, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](314, "a", 150);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](315, "Inscription");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](316, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](317, "a", 150);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](318, "Actualit\u00E9");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](319, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](320, "a", 148);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](321, "Contact");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](322, "div", 151);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](323, "h6", 147);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](324, " Contact ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](325, "p", 152);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](326, "i", 153);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](327, "+33 0184142394 ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](328, "p", 154);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](329, "i", 155);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](330, "a", 156);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](331, "votreconseiller@cpn-aide-aux-entreprise.com");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](332, "div", 157);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](333, " \u00A9 2021 Copyright:Tous droits r\u00E9serv\u00E9s ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](334, "a", 158);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](335, "Jobid.fr");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "/home");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.connect);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.connect);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.connect);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("defaultImage", "assets/cpnimages/home/33.png")("lazyLoad", "assets/cpnimages/home/33.png");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx.follow, "K ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](45);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "/test");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "/test");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "/test");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "/test");
    } }, directives: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterLinkWithHref"], _angular_common__WEBPACK_IMPORTED_MODULE_4__["NgIf"], ng_lazyload_image__WEBPACK_IMPORTED_MODULE_5__["LazyLoadImageDirective"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["NgForm"]], styles: ["footer[_ngcontent-%COMP%]{\n  background: #111D5E !important;\n  color: white !important;\n\n}\nfooter[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]{\n  color: white;\n}\n.drop[_ngcontent-%COMP%]{\n  min-width: -moz-available;\n  margin-left: -70px;\n}\n.navbar-nav[_ngcontent-%COMP%]{\n  flex-direction: row;\n  justify-content: space-between;\n  display: flex;\n  font-size: 14px;\n  height: 40px\n  }\n.nav-link[_ngcontent-%COMP%]{\n      color: white !important;\n  }\n.navlinkwhit[_ngcontent-%COMP%]{\n      color: #111D5E !important;\n  }\n.con[_ngcontent-%COMP%]{\n      color: white !important;\n      border: none;\n      background: red;\n      border-radius: 25px;\n      width: 120px;\n      text-align: center;\n  height: 100%;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  }\n.topnav[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]:hover {\n      border-bottom: 0.1px solid red;\n\n  }\n.nav_t[_ngcontent-%COMP%]{\n      color: white !important;\n  }\n.nav_g[_ngcontent-%COMP%]{\n      background-color: #111D5E;\n  }\n.navwhit[_ngcontent-%COMP%]{\n      background-color: #EBECF0;\n\n  }\n.nav_img[_ngcontent-%COMP%]{\n      width: 80px;\n      margin-bottom: 10px;\n  }\n.navbar-brand[_ngcontent-%COMP%] {\n      display: inline-block;\n      padding-top: .3125rem;\n      padding-bottom: .3125rem;\n      margin-right: 1rem;\n      font-size: 1.25rem;\n      line-height: inherit;\n      white-space: nowrap;\n      margin-left: 75px;\n      z-index: 5;\n  }\n\n.float_actions[_ngcontent-%COMP%] {\nposition: fixed;\nbackground: red;\nborder-radius: 10px;\nleft: 0;\ntop: 35%;\npadding: 10px;\ndisplay: flex;\njustify-content: center;\nalign-items: center;\nwidth: 6%;\nz-index:5900;\n}\n.float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%] {\npadding: 0;\nmargin: 0 0 -30px 0;\ndisplay: flex;\nflex-direction: column;\njustify-content: space-between;\n}\n.float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%] {\npadding: 5px;\nwidth: 120px;\nheight: 120px;\ndisplay: flex;\njustify-content: center;\nalign-content: center;\nposition: relative;\n}\n.float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]   .item_href[_ngcontent-%COMP%] {\ntext-decoration: none;\ndisplay: block;\nflex-direction: column;\njustify-content: center;\nfont-size: 14px;\n}\n.float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]   .item_href[_ngcontent-%COMP%]   .ihref_logo[_ngcontent-%COMP%] {\nwidth: 100px;\nheight: 100px;\nmargin-left: 25px;\n}\n.float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]   .item_href[_ngcontent-%COMP%]   .ihref_text[_ngcontent-%COMP%] {\ntext-align: center;\nmargin: 0;\ncolor: white;\nmargin-top: 7px;\n}\n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]::before{\n content: \">\";\n position: absolute;\n right: -10px;\n top: 15%;\n color: white;\n font-size: 20px;\n width: 40%;\n font-weight: bold;\n }\n.float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]   .testmegi[_ngcontent-%COMP%]{\n  text-decoration: none;\n   position: absolute;\n   right: -180px;\n   top: 15%;\n   color: black;\n   font-size: 17px;\n   width: 40%;\n   background: white;\n   width: 180px;\n   border-radius: 25px;\n   text-align: center;\n   height: 40px;\n   display: none;\n   justify-content: center;\n   align-items: center;\n }\n.float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]:hover   .testmegi[_ngcontent-%COMP%]{\n display: flex;\n }\n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]   .item_href[_ngcontent-%COMP%] {\n  text-decoration: none;\n  display: block;\n  flex-direction: column;\n  justify-content: center;\n  font-size: 14px;\n }\n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]   .item_href[_ngcontent-%COMP%]   .ihref_logo[_ngcontent-%COMP%] {\n  width: 100px;\n  height: 100px;\n  margin-left: 25px;\n }\n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]   .item_href[_ngcontent-%COMP%]   .ihref_text[_ngcontent-%COMP%] {\n  text-align: center;\n  margin: 0;\n  color: white;\n  margin-top: 7px;\n }\n\n\n.primary_body[_ngcontent-%COMP%]   .divider[_ngcontent-%COMP%]   .divider_ligne[_ngcontent-%COMP%] {\n  width: 100%;\n  height: 5px;\n  background: #111d5e;\n  display: block;\n  position: relative;\n }\n.primary_body[_ngcontent-%COMP%]   .divider[_ngcontent-%COMP%]   .divider_ligne[_ngcontent-%COMP%]::before {\n  content: \"\";\n  position: absolute;\n  width: 30%;\n  top: 0;\n  left: 0;\n  height: 5px;\n  background: red;\n }\n.primary_body[_ngcontent-%COMP%]   .divider[_ngcontent-%COMP%]   .divider_ligne[_ngcontent-%COMP%]::after {\n  content: \"\";\n  position: absolute;\n  width: 30%;\n  top: 0;\n  right: 0;\n  height: 5px;\n  background: red;\n }\n\n.content[_ngcontent-%COMP%]{\n  width: max-content;\n  }\n.transtion[_ngcontent-%COMP%]{\n    display: flex;\n    position: absolute;\n    margin-left: -350px;\n    width: max-content;\n    flex-direction: column;\n    }\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%] {\n  background:#111d5e;\n  min-height: -moz-fit-content;\n  min-height: fit-content;\n  border-bottom-right-radius: 100px;\n }\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .img_wrapper[_ngcontent-%COMP%]   .heading_img[_ngcontent-%COMP%] {\n  height: 1000px;\n  max-height: 640px;\n  margin-top: 1;\n  width: 1050px;\n }\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .title_heading[_ngcontent-%COMP%] {\n  font-size: 60px;\n  font-weight: 800;\n  color:  #ffffff;\n  width: max-content;\n }\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .desc_heading[_ngcontent-%COMP%] {\n  font-size: 60px;\n  font-weight: 700;\n  color:  #ffffff;\n  width: max-content;\n }\n.carre[_ngcontent-%COMP%] {\n  width: 200px;\n  height: 90px;\n  background: white;\n  border-radius: 18px;\n  margin-left: -290px;\n  margin-top: 385px;\n  }\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   *[_ngcontent-%COMP%], .primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   *[_ngcontent-%COMP%]:before, .primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   *[_ngcontent-%COMP%]:after {\n    box-sizing: border-box;\n   }\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   body[_ngcontent-%COMP%] {\n   background: #f5f5f5;\n   }\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%] {\n   display: flex;\n   flex-direction: row;\n   justify-content: flex-start;\n   margin-left: unset;\n   width: 650px;\n   margin-left: 100px;\n   }\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   main[_ngcontent-%COMP%] {\n    left: 50%;\n    position: absolute;\n    top: 50%;\n    transform: translateX(-50%) translateY(-50%);\n    width: 300px;\n   }\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search[_ngcontent-%COMP%]:before, .primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search[_ngcontent-%COMP%]:after {\n    content: \"\";\n    display: block;\n    position: absolute;\n   }\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search[_ngcontent-%COMP%]:before {\n    border: 5px solid #ffffff ;\n    border-radius: 20px;\n    height: 40px;\n    transition: all 0.3s ease-out;\n    transition-delay: 0.3s;\n    width: 40px;\n   }\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search[_ngcontent-%COMP%]:after {\n    background: #ffffff;\n    border-radius: 3px;\n    height: 5px;\n    transform: rotate(-45deg);\n    transform-origin: 0% 100%;\n    transition: all 0.3s ease-out;\n    width: 15px;\n   }\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search__input[_ngcontent-%COMP%] {\n    background: transparent;\n    border: none;\n    border-radius: 20px;\n    display: block;\n    font-size: 20px;\n    height: 40px;\n    line-height: 40px;\n    opacity: 0;\n    outline: none;\n    padding: 0 15px;\n    position: relative;\n    transition: all 0.3s ease-out;\n    transition-delay: 0.6s;\n    width: 40px;\n    z-index: 1;\n    color: rgb(223, 223, 223);\n   }\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--hide[_ngcontent-%COMP%]:before {\n    transition-delay: 0.3s;\n   }\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--hide[_ngcontent-%COMP%]:after {\n    transition-delay: 0.6s;\n   }\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--hide[_ngcontent-%COMP%]   .search__input[_ngcontent-%COMP%] {\n    transition-delay: 0s;\n   }\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--show[_ngcontent-%COMP%]:after {\n    transform: rotate(45deg) translateX(15px) translateY(-2px);\n    width: 0;\n   }\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--show[_ngcontent-%COMP%]:before {\n    border: 5px solid #ffffff;\n    border-radius: 20px;\n    height: 40px;\n    width: 500px;\n   }\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--show[_ngcontent-%COMP%]   .search__input[_ngcontent-%COMP%] {\n    opacity: 1;\n    width: 500px;\n   }\ninput[type=\"text\"][_ngcontent-%COMP%] {\n  height: 50px;\n  font-size: 30px;\n  display: inline-block;\n  \n  font-weight: 100;\n  border: none;\n  outline: none;\n  color: white;\n  padding: 3px;\n  padding-right: 60px;\n  width: 0px;\n  position: absolute;\n  top: 0;\n  right: 0;\n  background: none;\n  z-index: 3;\n  transition: width 0.4s cubic-bezier(0, 0.795, 0, 1);\n  cursor: pointer;\n  }\ninput[type=\"text\"][_ngcontent-%COMP%]:focus:hover {\n  border-bottom: 1px solid white;\n  }\ninput[type=\"text\"][_ngcontent-%COMP%]:focus {\n  width: 700px;\n  z-index: 1;\n  border-bottom: 1px solid white;\n  cursor: text;\n  }\ninput[type=\"submit\"][_ngcontent-%COMP%] {\n  height: 50px;\n  width: 50px;\n  display: inline-block;\n  color: white;\n  float: right;\n  background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAMAAABg3Am1AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAADNQTFRFU1NT9fX1lJSUXl5e1dXVfn5+c3Nz6urqv7+/tLS0iYmJqampn5+fysrK39/faWlp////Vi4ZywAAABF0Uk5T/////////////////////wAlrZliAAABLklEQVR42rSWWRbDIAhFHeOUtN3/ags1zaA4cHrKZ8JFRHwoXkwTvwGP1Qo0bYObAPwiLmbNAHBWFBZlD9j0JxflDViIObNHG/Do8PRHTJk0TezAhv7qloK0JJEBh+F8+U/hopIELOWfiZUCDOZD1RADOQKA75oq4cvVkcT+OdHnqqpQCITWAjnWVgGQUWz12lJuGwGoaWgBKzRVBcCypgUkOAoWgBX/L0CmxN40u6xwcIJ1cOzWYDffp3axsQOyvdkXiH9FKRFwPRHYZUaXMgPLeiW7QhbDRciyLXJaKheCuLbiVoqx1DVRyH26yb0hsuoOFEPsoz+BVE0MRlZNjGZcRQyHYkmMp2hBTIzdkzCTc/pLqOnBrk7/yZdAOq/q5NPBH1f7x7fGP4C3AAMAQrhzX9zhcGsAAAAASUVORK5CYII=)\n    center center no-repeat;\n  text-indent: -10000px;\n  border: none;\n  position: absolute;\n  top: 0;\n  right: 0;\n  z-index: 2;\n  cursor: pointer;\n  opacity: 0.4;\n  cursor: pointer;\n  transition: opacity 0.4s ease;\n  }\ninput[type=\"submit\"][_ngcontent-%COMP%]:hover {\n  opacity: 0.8;\n  }\n\n.item_num[_ngcontent-%COMP%] {\n  margin: 0;\n  font-weight: 700;\n  color: #111d5e;\n}\n.success_item[_ngcontent-%COMP%] {\n  list-style: none;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n}\n.item_desc[_ngcontent-%COMP%] {\n  margin: 0;\n  color: #111d5e;\n}\n.success_list[_ngcontent-%COMP%] {\n  display: flex;\n  justify-content: space-between;\n  margin: 0;\n  padding: 0;\n}\n.success_txt[_ngcontent-%COMP%] {\n  text-transform: uppercase;\n  font-weight: 400;\n  font-size: 14px;\n  color: #111d5e;\n}\n.success_desc[_ngcontent-%COMP%] {\n  font-size: 28px;\n  color: #111d5e;\n}\n.container_box[_ngcontent-%COMP%] {\n  width: 100%;\n  height: auto;\n  display: flex;\n  flex-direction: row;\n\n}\n.k[_ngcontent-%COMP%]{\n  font-weight: bold;\n  margin: inherit;\n  }\n.chiffre[_ngcontent-%COMP%]{\n  display: flex;\n              flex-direction: column;\n              justify-content: center;\n              width: 40%;\n              align-items: inherit;\n  }\n\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .our_success[_ngcontent-%COMP%]   .success_wrapper[_ngcontent-%COMP%]   .success_txt[_ngcontent-%COMP%] {\n  text-transform: uppercase;\n  font-weight: 400;\n  font-size: 14px;\n  color: #111d5e;\n }\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .our_success[_ngcontent-%COMP%]   .success_wrapper[_ngcontent-%COMP%]   .success_desc[_ngcontent-%COMP%] {\n  font-size: 28px;\n  color: #111d5e;\n }\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .our_success[_ngcontent-%COMP%]   .success_wrapper[_ngcontent-%COMP%]   .success_list[_ngcontent-%COMP%] {\n  display: flex;\n  justify-content: space-between;\n  margin: 0;\n  padding: 0;\n }\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .our_success[_ngcontent-%COMP%]   .success_wrapper[_ngcontent-%COMP%]   .success_list[_ngcontent-%COMP%]   .success_item[_ngcontent-%COMP%] {\n  list-style: none;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n }\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .our_success[_ngcontent-%COMP%]   .success_wrapper[_ngcontent-%COMP%]   .success_list[_ngcontent-%COMP%]   .success_item[_ngcontent-%COMP%]   .item_num[_ngcontent-%COMP%] {\n  margin: 0;\n  font-weight: 700;\n  color: #111d5e;\n }\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .our_success[_ngcontent-%COMP%]   .success_wrapper[_ngcontent-%COMP%]   .success_list[_ngcontent-%COMP%]   .success_item[_ngcontent-%COMP%]   .item_desc[_ngcontent-%COMP%] {\n  margin: 0;\n  color: #111d5e;\n }\n\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .card-blog[_ngcontent-%COMP%]   .card_wrapper[_ngcontent-%COMP%]   .row[_ngcontent-%COMP%] {\n  background-color: #111d5e;\n  border-radius: 30px;\n }\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .card-blog[_ngcontent-%COMP%]   .card_wrapper[_ngcontent-%COMP%]   .row[_ngcontent-%COMP%]   .card-body[_ngcontent-%COMP%] {\n  margin-bottom: 14px;\n  margin-top: -22px;\n }\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .card-blog[_ngcontent-%COMP%]   .card_wrapper[_ngcontent-%COMP%]   .row[_ngcontent-%COMP%]   .card-body[_ngcontent-%COMP%]   .test-btn1[_ngcontent-%COMP%] {\n  border-radius: 20px;\n }\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .card-blog[_ngcontent-%COMP%]   .card_wrapper[_ngcontent-%COMP%]   .row[_ngcontent-%COMP%]   .card-body[_ngcontent-%COMP%]   .test-btn2[_ngcontent-%COMP%] {\n  border: none;\n  background: red;\n  border-radius: 25px;\n  color: white;\n  padding: 8px 30px;\n }\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .card-blog[_ngcontent-%COMP%]   .card_wrapper[_ngcontent-%COMP%]   .row[_ngcontent-%COMP%]   .card-body[_ngcontent-%COMP%]   .test-btn3[_ngcontent-%COMP%] {\n  border-radius: 20px;\n }\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .card-blog[_ngcontent-%COMP%]   .card_wrapper[_ngcontent-%COMP%]   .row[_ngcontent-%COMP%]   .card-group[_ngcontent-%COMP%] {\n  text-align: center;\n }\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .card-blog[_ngcontent-%COMP%]   .card_wrapper[_ngcontent-%COMP%]   .row[_ngcontent-%COMP%]   .card-group[_ngcontent-%COMP%]   .card[_ngcontent-%COMP%] {\n  border: none;\n  position: relative;\n  display: flex;\n  flex-direction: column;\n  min-width: 0;\n  word-wrap: break-word;\n  background-color: #111d5e;\n  border-radius: 12.25rem;\n  color: white;\n }\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .card-blog[_ngcontent-%COMP%]   .card_wrapper[_ngcontent-%COMP%]   .row[_ngcontent-%COMP%]   .card-group[_ngcontent-%COMP%]   .card[_ngcontent-%COMP%]   .card-img-top[_ngcontent-%COMP%] {\n  width: 50%;\n  margin-left: 91px;\n }\n.block2[_ngcontent-%COMP%]{\n  background: transparent;\n   color: white;\n   padding: 19px;\n  }\n.block2[_ngcontent-%COMP%]:hover{\n   border: 5px solid white;\n  }\n.block2[_ngcontent-%COMP%]   .test-btn1[_ngcontent-%COMP%] {\n    border-radius: 25px;\n    margin-top: 30px;\n  }\n.block2[_ngcontent-%COMP%]   .test-btn2[_ngcontent-%COMP%] {\n  border-radius: 25px;\n  }\n.block2[_ngcontent-%COMP%]   .test-btn3[_ngcontent-%COMP%] {\n    border-radius: 25px;\n  }\n.block2[_ngcontent-%COMP%]:hover   .test-btn1[_ngcontent-%COMP%] {\n    border: none;\n    margin-top: 30px;\n    color: white;\n    background-color:red; \n  }\n.block2[_ngcontent-%COMP%]:hover   .test-btn2[_ngcontent-%COMP%] {\n    color: white;\n    border: none;\n    background-color:red; \n  }\n.block2[_ngcontent-%COMP%]:hover   .test-btn3[_ngcontent-%COMP%] {\n    border: none;\n    color: white;\n    background-color:red; \n  }\n.home[_ngcontent-%COMP%]{\n    color: white;\n    font-size: 18px;\n    }\n\n.block3[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\n  padding-right: 25rem;\n  width: 800px;\n}\n.text-bloc[_ngcontent-%COMP%]   .text_body[_ngcontent-%COMP%]{\n  margin-left: 15px;\n}\n.text-bloc[_ngcontent-%COMP%]   .text_body[_ngcontent-%COMP%]   .text_content[_ngcontent-%COMP%]{\n  font-size: 70px;\n  color:#111d5e\n}\n.text-bloc[_ngcontent-%COMP%]   .text_body[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\n  margin-top: -75px;\n  margin-left: -17px;\n  color:#111d5e\n}\n\n.block4[_ngcontent-%COMP%]{\n  background-image: url('sin.png');\n  background-repeat: no-repeat;\n  background-position:658px 234px;\n  margin-top:100px;\n}\n.block4[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\n  color:#111d5e;\n  font-weight: bold;\n}\n.block4[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{padding-right: 48rem;}\n.outer-div[_ngcontent-%COMP%], .inner-div[_ngcontent-%COMP%] {\n  height: 378px;\n  max-width: 300px;\n  margin: 0 auto;\n  position: relative;\n}\n.outer-div[_ngcontent-%COMP%] {\n  perspective: 900px;\n  perspective-origin: 50% calc(50% - 18em);\n}\n.one[_ngcontent-%COMP%]{\nmargin: 11px 0px 0px 556px\n}\n.two[_ngcontent-%COMP%]{\nmargin: -580px 0 0 1065px\n}\n.three[_ngcontent-%COMP%]{\nmargin: -12px 0 0 1059px\n}\n.inner-div[_ngcontent-%COMP%] {\n  margin: 0 auto;\n  border-radius: 5px;\n  font-weight: 400;\n  color: black;\n  font-size: 1rem;\n  text-align: center;\n \n}\n.front[_ngcontent-%COMP%] {\n  cursor: pointer;\n  height: 85%;\n  background: white;\n  -webkit-backface-visibility: hidden;\n          backface-visibility: hidden;\n  box-shadow: 0 0 40px rgba(0, 0, 0, 0.1) inset;\n  box-shadow: 0px 1px 15px grey;\n  border-radius: 25px;\n  position: relative;\n  top: 0;\n  left: 0;\n}\n.front__face-photo1[_ngcontent-%COMP%] {\n  position: relative;\n  top: 10px;\n  height: 120px;\n  width: 120px;\n  margin: 0 auto;\n  border-radius: 50%;\n\n  background-size: contain;\n  overflow: hidden;\n  \n}\n.front__face-photo2[_ngcontent-%COMP%] {\n  position: relative;\n  top: 10px;\n  height: 120px;\n  width: 120px;\n  margin: 0 auto;\n  border-radius: 50%;\n  background-size: contain;\n  overflow: hidden;\n  \n}\n.front__face-photo3[_ngcontent-%COMP%] {\n  position: relative;\n  top: 10px;\n  height: 120px;\n  width: 120px;\n  margin: 0 auto;\n  border-radius: 50%;\n\n  background-size: contain;\n  overflow: hidden;\n  \n}\n.front__text[_ngcontent-%COMP%] {\n  position: relative;\n  top: 35px;\n  margin: 0 auto;\n  font-family: \"Montserrat\";\n  font-size: 18px;\n  -webkit-backface-visibility: hidden;\n          backface-visibility: hidden;\n}\n.front__text-header[_ngcontent-%COMP%] {\n  font-weight: 700;\n  font-family: \"Oswald\";\n  text-transform: uppercase;\n  font-size: 20px;\n}\n.front__text-para[_ngcontent-%COMP%] {\n  position: relative;\n  top: -5px;\n  color: #000;\n  font-size: 14px;\n  letter-spacing: 0.4px;\n  font-weight: 400;\n  font-family: \"Montserrat\", sans-serif;\n}\n.front-icons[_ngcontent-%COMP%] {\n  position: relative;\n  top: 0;\n  font-size: 14px;\n  margin-right: 6px;\n  color: gray;\n}\n.front__text-hover[_ngcontent-%COMP%] {\n  position: relative;\n  top: 10px;\n  font-size: 10px;\n  color: red;\n  -webkit-backface-visibility: hidden;\n          backface-visibility: hidden;\n\n  font-weight: 700;\n  text-transform: uppercase;\n  letter-spacing: .4px;\n\n  border: 2px solid red;\n  padding: 8px 15px;\n  border-radius: 30px;\n\n  background: red;\n  color: white;\n}\n\n.block5[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\n  margin-left: 190px;\n  height:80%;\n  width:80%\n}\n\n.block6[_ngcontent-%COMP%]{\n  padding-top: 30px; \n  padding-left: 110px;\n  margin-top: -501px; \n  float:right\n}\n.block6[_ngcontent-%COMP%]   .third-bloc-border[_ngcontent-%COMP%]{\n  border-radius: 20px;\n   border: 2px solid #C5C5C5;\n    margin-top: 20px; \n    width: 90%; \n  padding: 20px;\n}\n.block6[_ngcontent-%COMP%]   .third-bloc-border[_ngcontent-%COMP%]   h3[_ngcontent-%COMP%]{\n  font-size: 26px;\n  font-weight: 700;\n}\n.block6[_ngcontent-%COMP%]   .third-bloc-border[_ngcontent-%COMP%]:hover{\nborder: 2px solid blue;\n}\n.block6[_ngcontent-%COMP%]   .third-bloc-border[_ngcontent-%COMP%]:hover   h3[_ngcontent-%COMP%]{\n  color: blue;  \n}\n\n.lastB[_ngcontent-%COMP%]{\n  width: 100%;\n \n}\n.block7[_ngcontent-%COMP%]{\n  padding-top: 100px\n}\n.card1[_ngcontent-%COMP%]{\n  margin-right: 50px;height: 345px;width: 315px;\n}\n.card1[_ngcontent-%COMP%]   .box1[_ngcontent-%COMP%]{\n  box-shadow: 0px 1px 15px grey;\n  border: none;\n  border-radius: 71px 14px 71px 14px;\n  background-color: #ffffff;\n  padding: 40px;\n}\n.card2[_ngcontent-%COMP%]{\n  margin-right: 50px;\n  height: 345px;\n  width: 315px;\n}\n.card2[_ngcontent-%COMP%]   .box2[_ngcontent-%COMP%]{\n  box-shadow: 0px 1px 15px grey;\n  border: none;\n  border-radius: 71px 14px 71px 14px;\n  background-color: #ffffff;\n  padding: 40px;\n}\n.card3[_ngcontent-%COMP%]{\n  margin-right: 50px;\n  height: 345px;\n  width: 315px;\n}\n.card3[_ngcontent-%COMP%]   .box3[_ngcontent-%COMP%]{\n  box-shadow: 0px 1px 15px grey;\n  border: none;\n  border-radius: 71px 14px 71px 14px;\n  background-color: #ffffff;\n  padding: 40px;\n}\n.card1[_ngcontent-%COMP%]   .box1[_ngcontent-%COMP%]   h4[_ngcontent-%COMP%]{\n  padding: 69px 0 0 26px;\n  font-size: 20px;\n  color:#00BFFF\n}\n.card1[_ngcontent-%COMP%]   .box1[_ngcontent-%COMP%]   h6[_ngcontent-%COMP%]{\n  padding: 0px 0 0px 54px;\n  font-size: 10px;\n  color:#c7c7c7\n}\n.card1[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\n  position: relative;\n  top: -132px;\n  left: -116px;\n  height: 100px;\n  width: 100px;\n  margin: 0 auto;\n  border-radius: 50%;\n  background-size: contain;\n  overflow: hidden;\n}\n.card1[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\n  width: 120%;\n  height: 120%\n}\n.card2[_ngcontent-%COMP%]   .box2[_ngcontent-%COMP%]   h4[_ngcontent-%COMP%]{\n  padding: 54px 0 0 26px;\n  font-size: 20px;\n   color:#00BFFF\n}\n.card2[_ngcontent-%COMP%]   .box2[_ngcontent-%COMP%]   h6[_ngcontent-%COMP%]{\n  padding: 0px 0 0px 54px;\n  font-size: 10px;\n  color:#c7c7c7\n}\n.card2[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\n  position: relative;\n   top: -132px;\n  left: -116px;\n  height: 100px;\n  width: 100px;\n  margin: 0 auto;\n  border-radius: 50%;\n   background-size: contain;\n   overflow: hidden;\n}\n.card2[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\n  width: 120%;\n  height: 120%\n}\n.card3[_ngcontent-%COMP%]   .box3[_ngcontent-%COMP%]   h4[_ngcontent-%COMP%]{\n  padding: 44px 0 0 26px;\n  font-size: 20px;\n   color:#00BFFF\n}\n.card3[_ngcontent-%COMP%]   .box3[_ngcontent-%COMP%]   h6[_ngcontent-%COMP%]{\n  padding: 0px 0 0px 54px;\n  font-size: 10px;\n  color:#c7c7c7\n}\n.card3[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\n  position: relative;\n  top: -132px;\n  left: -116px;\n  height: 100px;\n  width: 100px;\n  margin: 0 auto;\n  border-radius: 50%;\n  background-size: contain;\n  overflow: hidden;\n}\n.card3[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\n  width: 120%;\n  height: 120%\n}\n\nh1[_ngcontent-%COMP%]{\nfont-weight: bold;\ncolor: #111d5e;\nmargin: 50px 0 50px 0;\n}\nh5[_ngcontent-%COMP%]{\ncolor: #111d5e;\nfont-size: 15px;\n}\np[_ngcontent-%COMP%]{\nfont-size: 12px;\ncolor: #111d5e;\n\n}\n.footer[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\n  font-size: 12px;\ncolor: #ffffff;\n}\n.copyright[_ngcontent-%COMP%]{\n  background-color: #0c133a;\n  color:#fff;\n  font-size:13px;\n  text-align: center;\n}\n\n\n@media only screen and (min-width : 320px) and (max-width : 480px)  {\n     \n  \n    .navbar-brand[_ngcontent-%COMP%] {\n        display: inline-block;\n        padding-top: .3125rem;\n        padding-bottom: .3125rem;\n        margin-right: 1rem;\n        font-size: 1.25rem;\n        line-height: inherit;\n        white-space: nowrap;\n        margin-left: 0;\n        z-index: 5;\n  }\n  \n  .drop[_ngcontent-%COMP%]{\n    min-width: -moz-available;\n    margin-left: 0px;\n  }\n  \n.navbar-nav[_ngcontent-%COMP%]{\n  flex-direction: column;\n  justify-content: space-between;\n  display: flex;\n  font-size: 14px;\n  height:max-content;\n  }\n  \n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]{\n  position: fixed;\nbackground: red;\nborder-radius: 10px;\nleft: 0;\ntop: 35%;\npadding: 10px;\ndisplay: flex;\njustify-content: center;\nalign-items: center;\nwidth: 3%;\nheight: 5%;\nz-index:5900;\n}\n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]::after{\n  content: \">\";\n  color: white;\n  position: fixed;\nbackground: red;\nborder-radius: 10px;\nleft: 0;\ntop: 35%;\npadding: 0px;\ndisplay: flex;\njustify-content: center;\nalign-items: center;\nwidth: 5%;\nheight: 5%;\nfont-weight: bold;\nfont-size: 20px;\n\n}\n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]:hover:after{\n  content: \">\";\n  color: white;\n  position: fixed;\nbackground: red;\nborder-radius: 10px;\nleft: 0;\ntop: 35%;\npadding: 10px;\ndisplay: none;\njustify-content: center;\nalign-items: center;\nwidth: 3%;\nheight: 5%;\n}\n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%] {\n  padding: 0;\n  margin: 0 0 -30px 0;\n  display: none;\n  flex-direction: column;\n  justify-content: space-between;\n}\n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]:hover{\n  position: fixed;\n  background: red;\n  border-radius: 10px;\n  left: 0;\n  top: 35%;\n  padding: 10px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  width: 25%;\n  height: auto;\n  z-index: 99999;\n}\n\n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]:hover   .actions_content[_ngcontent-%COMP%] {\npadding: 0;\nmargin: 0 0 -30px 0;\ndisplay: flex;\nflex-direction: column;\njustify-content: space-between;\n}\n    \n  .primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]{\n    border-bottom-right-radius:0;\n  }\n  .primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--show[_ngcontent-%COMP%]:before {\n    border: 5px solid #111d5e;\n    border-radius: 20px;\n    height: 40px;\n    width: 300px;\n  }\n  .primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--show[_ngcontent-%COMP%]   .search__input[_ngcontent-%COMP%] {\n    opacity: 1;\n    width: 300px;\n  }\n\n  .primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .img_wrapper[_ngcontent-%COMP%]   .heading_img[_ngcontent-%COMP%] {\n    font-size: 40px;\n    font-weight: 800;\n    color: #ffffff;\n    width: max-content;\n    text-align: center;\n    margin-top: auto;\n    height: auto;\n }\n\n\n .primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%] {\n  display: flex;\n  width: 300px;\n\n  flex-direction: row;\n  justify-content: center;\n  margin-left: unset;\n  width: -moz-available;\n  align-items: center;\n  text-align: center;\n}\n.transtion[_ngcontent-%COMP%]{\n  display: flex;\n  width: -moz-available;\n  width: -moz-fit-content;\n  width: fit-content;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n  margin: 0;\n}\n.carre[_ngcontent-%COMP%] {\n  width: 200px;\n  height: 90px;\n  background: white;\n  border-radius: 18px;\n  margin-left: auto;\n  margin-top: 250px;\n  align-items: center;\n}\n.content[_ngcontent-%COMP%]{\n  width: -moz-fit-content;\n  width: fit-content;\n}\n\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .title_heading[_ngcontent-%COMP%] {\n  font-size: 30px;\n  font-weight: 800;\n  color: #ffffff;\n  width: -moz-available;\n  text-align: center;\n}\n\n\n\n\n\n\n.block2[_ngcontent-%COMP%]   .test-btn1[_ngcontent-%COMP%] {\n  border-radius: 25px;\n  margin-top: 30px;\n  border: 0;\n}\n.block2[_ngcontent-%COMP%]   .test-btn2[_ngcontent-%COMP%] {\nborder-radius: 25px;\nbackground-color: white;\ncolor: black;\nborder: 0;\n}\n.block2[_ngcontent-%COMP%]   .test-btn3[_ngcontent-%COMP%] {\n  border-radius: 25px;\n  border: 0;\n}\n.block2[_ngcontent-%COMP%]:hover   .test-btn1[_ngcontent-%COMP%] {\n  border: none;\n  margin-top: 30px;\n  color: white;\n  background-color:red; \n}\n.block2[_ngcontent-%COMP%]:hover   .test-btn2[_ngcontent-%COMP%] {\n  color: white;\n  border: none;\n  background-color:red; \n}\n.block2[_ngcontent-%COMP%]:hover   .test-btn3[_ngcontent-%COMP%] {\n  border: none;\n  color: white;\n  background-color:red; \n}\n\n\n\n\n.chiffre[_ngcontent-%COMP%]{\n  display: flex;\nflex-direction: column;\njustify-content: center;\nwidth: 100%;\nalign-items: center;\n}\n\n.k[_ngcontent-%COMP%]{\n  margin: initial;\n}\n.success_item[_ngcontent-%COMP%]{\n  margin-left: 20px;\n  list-style: none;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n}\n \n .block3[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\n  padding-right: 0;\n  width: 250px;\n  }\n\n  \n\n.block4[_ngcontent-%COMP%]{\n  background-image: url('sin.png');\n  background-repeat: no-repeat;\n  background-position:658px 234px;\n  margin-top:100px;\n  }\n  .block4[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]{\n  display: flex;\n  flex-direction: column;\n  align-items: flex-start;\n  }\n  .block4[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\n  padding-right: 0;\n  }\n  \n   .one[_ngcontent-%COMP%]{\n  margin: 40px 0 0 0 ;\n  }\n   .two[_ngcontent-%COMP%]{\n  margin: 0;\n  }\n   .three[_ngcontent-%COMP%]{\n  margin: 0;\n  }\n  .outer-div[_ngcontent-%COMP%]{\n    display: contents;\n  }\n  \n  \n  .block5[_ngcontent-%COMP%]{\n  max-width: 100%;\n  width: 100%;\n  }\n  .block5[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\n  margin-left: 0;\n  height:100%;\n  width:100%\n  }\n  \n  .block6[_ngcontent-%COMP%]{\n  padding-top: 30px; \n  padding-left:0;\n  margin-top: 0; \n  max-width: 100%;\n  display: contents;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n  }\n  \n  .block6[_ngcontent-%COMP%]   .third-bloc-border[_ngcontent-%COMP%]{\n    margin: 20px auto 0 auto;\n  }\n  \n  \n \n .block7[_ngcontent-%COMP%]{\n  padding-top: 10px;\n}\n\n    .card1[_ngcontent-%COMP%]{\n      margin-right: 0px;\n      height: 345px;\n      width: 315px;\n      }\n      .card1[_ngcontent-%COMP%]   .box1[_ngcontent-%COMP%]{\n      box-shadow: 0px 1px 15px grey;\n      border: none;\n      border-radius: 71px 14px 71px 14px;\n      background-color: #ffffff;\n      padding: 40px;\n      }\n      .card2[_ngcontent-%COMP%]{\n        margin-right: 0px;\n        height: 345px;\n        width: 315px;\n      }\n      .card2[_ngcontent-%COMP%]   .box2[_ngcontent-%COMP%]{\n        box-shadow: 0px 1px 15px grey;\n        border: none;\n        border-radius: 71px 14px 71px 14px;\n        background-color: #ffffff;\n        padding: 40px;\n        }\n      .card3[_ngcontent-%COMP%]{\n        margin-right: 0px;\n        height: 345px;\n        width: 315px;\n      }\n      .card3[_ngcontent-%COMP%]   .box3[_ngcontent-%COMP%]{\n        box-shadow: 0px 1px 15px grey;\n        border: none;\n        border-radius: 71px 14px 71px 14px;\n        background-color: #ffffff;\n        padding: 40px;\n        }\n        \n      .lastB[_ngcontent-%COMP%]{\n      width: 80%;\n      margin: 50px;\n      }\n\n\n      .text-center[_ngcontent-%COMP%]{\n      margin-bottom: 50px;\n      }\n\n    }\n\n@media only screen and (min-width : 480px) and (max-width : 768px)  {\n            \n    \n  \n  .navbar-brand[_ngcontent-%COMP%] {\n    display: inline-block;\n    padding-top: .3125rem;\n    padding-bottom: .3125rem;\n    margin-right: 1rem;\n    font-size: 1.25rem;\n    line-height: inherit;\n    white-space: nowrap;\n    margin-left: 0;\n    z-index: 5;\n}\n.nav_img[_ngcontent-%COMP%] {\n  width: 80px;\n  margin-bottom: 10px;\n  margin-left: 38px;\n}\n\n.drop[_ngcontent-%COMP%]{\n  min-width: -moz-available;\n  margin-left: 0px;\n}\n.navbar-nav[_ngcontent-%COMP%]{\n  flex-direction: column;\n  justify-content: space-between;\n  display: flex;\n  font-size: 14px;\n  height:max-content;\n  }\n\n  \n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]{\nposition: fixed;\nbackground: red;\nborder-radius: 10px;\nleft: 0;\ntop: 35%;\npadding: 10px;\ndisplay: flex;\njustify-content: center;\nalign-items: center;\nwidth: 3%;\nheight: 5%;\nz-index:5900;\n\n}\n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]::after{\ncontent: \">\";\ncolor: white;\nposition: fixed;\nbackground: red;\nborder-radius: 10px;\nleft: 0;\ntop: 35%;\npadding: 0px;\ndisplay: flex;\njustify-content: center;\nalign-items: center;\nwidth: 5%;\nheight: 5%;\nfont-weight: bold;\nfont-size: 20px;\n\n}\n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]:hover:after{\ncontent: \">\";\ncolor: white;\nposition: fixed;\nbackground: red;\nborder-radius: 10px;\nleft: 0;\ntop: 35%;\npadding: 10px;\ndisplay: none;\njustify-content: center;\nalign-items: center;\nwidth: 3%;\nheight: 5%;\n}\n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%] {\npadding: 0;\nmargin: 0 0 -30px 0;\ndisplay: none;\nflex-direction: column;\njustify-content: space-between;\n}\n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]:hover{\nposition: fixed;\nbackground: red;\nborder-radius: 10px;\nleft: 0;\ntop: 35%;\npadding: 10px;\ndisplay: flex;\njustify-content: center;\nalign-items: center;\nwidth: 18%;\nheight: auto;\nz-index: 99999;\n}\n\n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]:hover   .actions_content[_ngcontent-%COMP%] {\npadding: 0;\nmargin: 0 0 -30px 0;\ndisplay: flex;\nflex-direction: column;\njustify-content: space-between;\n}\n  \n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]{\nborder-bottom-right-radius:0;\n}\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--show[_ngcontent-%COMP%]:before {\nborder: 5px solid #111d5e;\nborder-radius: 20px;\nheight: 40px;\nwidth: 300px;\n}\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--show[_ngcontent-%COMP%]   .search__input[_ngcontent-%COMP%] {\nopacity: 1;\nwidth: 300px;\n}\n\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .img_wrapper[_ngcontent-%COMP%] {\n  width: auto;\n}\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .img_wrapper[_ngcontent-%COMP%]   .heading_img[_ngcontent-%COMP%] {\n  height: 100%;\n  max-height: 585px;\n  margin-top: 0;\n  width: 750px;\n}\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%] {\ndisplay: flex;\nflex-direction: row;\njustify-content: center;\nmargin-left: unset;\nwidth: -moz-available;\nalign-items: center;\ntext-align: center;\n}\n.transtion[_ngcontent-%COMP%]{\ndisplay: flex;\nwidth: -moz-available;\nflex-direction: column;\njustify-content: center;\nalign-items: center;\nmargin: 0;\n}\n.carre[_ngcontent-%COMP%] {\nwidth: 200px;\nheight: 90px;\nbackground: white;\nborder-radius: 18px;\nmargin-left: auto;\nmargin-top: 250px;\nalign-items: center;\n}\n.content[_ngcontent-%COMP%]{\nwidth: -moz-fit-content;\nwidth: fit-content;\n}\n\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .title_heading[_ngcontent-%COMP%] {\nfont-size: 30px;\nfont-weight: 800;\ncolor: #ffffff;\nwidth: -moz-available;\ntext-align: center;\n}\n\n\n\n\n\n.block2[_ngcontent-%COMP%]   .test-btn1[_ngcontent-%COMP%] {\nborder-radius: 25px;\nmargin-top: 30px;\nborder: 0;\n}\n.block2[_ngcontent-%COMP%]   .test-btn2[_ngcontent-%COMP%] {\nborder-radius: 25px;\nbackground-color: white;\ncolor: black;\nborder: 0;\n}\n.block2[_ngcontent-%COMP%]   .test-btn3[_ngcontent-%COMP%] {\nborder-radius: 25px;\nborder: 0;\n}\n.block2[_ngcontent-%COMP%]:hover   .test-btn1[_ngcontent-%COMP%] {\nborder: none;\nmargin-top: 30px;\ncolor: white;\nbackground-color:red; \n}\n.block2[_ngcontent-%COMP%]:hover   .test-btn2[_ngcontent-%COMP%] {\ncolor: white;\nborder: none;\nbackground-color:red; \n}\n.block2[_ngcontent-%COMP%]:hover   .test-btn3[_ngcontent-%COMP%] {\nborder: none;\ncolor: white;\nbackground-color:red; \n}\n\n\n\n\n.chiffre[_ngcontent-%COMP%]{\ndisplay: flex;\nflex-direction: column;\njustify-content: center;\nwidth: 100%;\nalign-items: center;\n}\n\n.k[_ngcontent-%COMP%]{\nmargin: initial;\n}\n.success_item[_ngcontent-%COMP%]{\nmargin-left: 20px;\nlist-style: none;\ndisplay: flex;\nflex-direction: column;\njustify-content: center;\nalign-items: center;\n}\n\n.block3[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\npadding-right: 0;\nwidth: 250px;\n}\n\n\n\n.block4[_ngcontent-%COMP%]{\nbackground-image: url('sin.png');\nbackground-repeat: no-repeat;\nbackground-position:0px 576px;\nmargin-top:100px;\n}\n.block4[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]{\ndisplay: flex;\nflex-direction: column;\nalign-items: flex-start;\n}\n.block4[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\npadding-right: 0;\n}\n\n.one[_ngcontent-%COMP%]{\nmargin: 40px 0 0 0 ;\n}\n.two[_ngcontent-%COMP%]{\nmargin: 0;\n}\n.three[_ngcontent-%COMP%]{\nmargin: 0;\n}\n.outer-div[_ngcontent-%COMP%]{\ndisplay: contents;\n}\n\n\n.block5[_ngcontent-%COMP%]{\nmax-width: 100%;\nwidth: 100%;\n}\n.block5[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\nmargin-left: 0;\nheight:100%;\nwidth:100%\n}\n\n.block6[_ngcontent-%COMP%]{\npadding-top: 30px; \npadding-left:0;\nmargin-top: 0; \nmax-width: 100%;\ndisplay: contents;\nflex-direction: column;\njustify-content: center;\nalign-items: center;\n}\n\n.block6[_ngcontent-%COMP%]   .third-bloc-border[_ngcontent-%COMP%]{\nmargin: 20px auto 0 auto;\n}\n\n\n\n.block7[_ngcontent-%COMP%]{\npadding-top: 10px;\n}\n\n.card1[_ngcontent-%COMP%]{\n  margin-right: 0px;\n  height: 345px;\n  width: 315px;\n  }\n  .card1[_ngcontent-%COMP%]   .box1[_ngcontent-%COMP%]{\n  box-shadow: 0px 1px 15px grey;\n  border: none;\n  border-radius: 71px 14px 71px 14px;\n  background-color: #ffffff;\n  padding: 40px;\n  }\n  .card2[_ngcontent-%COMP%]{\n    margin-right: 0px;\n    height: 345px;\n    width: 315px;\n  }\n  .card2[_ngcontent-%COMP%]   .box2[_ngcontent-%COMP%]{\n    box-shadow: 0px 1px 15px grey;\n    border: none;\n    border-radius: 71px 14px 71px 14px;\n    background-color: #ffffff;\n    padding: 40px;\n    }\n  .card3[_ngcontent-%COMP%]{\n    margin-right: 0px;\n    height: 345px;\n    width: 315px;\n  }\n  .card3[_ngcontent-%COMP%]   .box3[_ngcontent-%COMP%]{\n    box-shadow: 0px 1px 15px grey;\n    border: none;\n    border-radius: 71px 14px 71px 14px;\n    background-color: #ffffff;\n    padding: 40px;\n    }\n    \n  .lastB[_ngcontent-%COMP%]{\n    width: 100%;\n    margin: none;\n  }\n\n\n  .text-center[_ngcontent-%COMP%]{\n  margin-bottom: 50px;\n  }\n\n\n    }\n\n@media only screen and (min-width : 768px) and (max-width : 992px)  {\n    \n       \n  .navbar-brand[_ngcontent-%COMP%] {\n    display: inline-block;\n    padding-top: .3125rem;\n    padding-bottom: .3125rem;\n    margin-right: 1rem;\n    font-size: 1.25rem;\n    line-height: inherit;\n    white-space: nowrap;\n    margin-left: 0;\n    z-index: 5;\n}\n.nav_img[_ngcontent-%COMP%] {\n  width: 80px;\n  margin-bottom: 10px;\n  margin-left: 38px;\n}\n.navbar-nav[_ngcontent-%COMP%]{\n  flex-direction: column;\n  justify-content: space-between;\n  display: flex;\n  font-size: 14px;\n  height:max-content;\n\n  }\n.drop[_ngcontent-%COMP%]{\n  min-width: -moz-available;\n  margin-left: 0px;\n}\n  \n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]{\nposition: fixed;\nbackground: red;\nborder-radius: 10px;\nleft: 0;\ntop: 35%;\npadding: 10px;\ndisplay: flex;\njustify-content: center;\nalign-items: center;\nwidth: 3%;\nheight: 5%;\nz-index:5900;\n}\n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]::after{\ncontent: \">\";\ncolor: white;\nposition: fixed;\nbackground: red;\nborder-radius: 10px;\nleft: 0;\ntop: 35%;\npadding: 0px;\ndisplay: flex;\njustify-content: center;\nalign-items: center;\nwidth: 5%;\nheight: 5%;\nfont-weight: bold;\nfont-size: 20px;\n\n}\n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]:hover:after{\ncontent: \">\";\ncolor: white;\nposition: fixed;\nbackground: red;\nborder-radius: 10px;\nleft: 0;\ntop: 35%;\npadding: 10px;\ndisplay: none;\njustify-content: center;\nalign-items: center;\nwidth: 3%;\nheight: 5%;\n}\n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%] {\npadding: 0;\nmargin: 0 0 -30px 0;\ndisplay: none;\nflex-direction: column;\njustify-content: space-between;\n}\n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]:hover{\nposition: fixed;\nbackground: red;\nborder-radius: 10px;\nleft: 0;\ntop: 35%;\npadding: 10px;\ndisplay: flex;\njustify-content: center;\nalign-items: center;\nwidth: 12%;\nheight:auto;\nz-index: 99999;\n}\n\n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]:hover   .actions_content[_ngcontent-%COMP%] {\npadding: 0;\nmargin: 0 0 -30px 0;\ndisplay: flex;\nflex-direction: column;\njustify-content: space-between;\n}\n  \n.block0[_ngcontent-%COMP%]{\n  flex: 0 0 auto;\nwidth: -moz-available;\n}\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]{\nborder-bottom-right-radius:0;\n}\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--show[_ngcontent-%COMP%]:before {\nborder: 5px solid #111d5e;\nborder-radius: 20px;\nheight: 40px;\nwidth: 300px;\n}\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--show[_ngcontent-%COMP%]   .search__input[_ngcontent-%COMP%] {\nopacity: 1;\nwidth: 300px;\n}\n\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .img_wrapper[_ngcontent-%COMP%] {\n  width: auto;\n}\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .img_wrapper[_ngcontent-%COMP%]   .heading_img[_ngcontent-%COMP%] {\n  height: 100%;\n  max-height: 585px;\n  margin-top: 0;\n  width: 750px;\n}\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%] {\ndisplay: flex;\nflex-direction: row;\njustify-content: center;\nmargin-left: unset;\nwidth: -moz-available;\nalign-items: center;\ntext-align: center;\n}\n.transtion[_ngcontent-%COMP%]{\ndisplay: flex;\nwidth: -moz-available;\nflex-direction: column;\njustify-content: center;\nalign-items: center;\nmargin: 0;\n}\n.carre[_ngcontent-%COMP%] {\nwidth: 200px;\nheight: 90px;\nbackground: white;\nborder-radius: 18px;\nmargin-left: auto;\nmargin-top: 250px;\nalign-items: center;\n}\n.content[_ngcontent-%COMP%]{\nwidth: -moz-fit-content;\nwidth: fit-content;\n}\n\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .title_heading[_ngcontent-%COMP%] {\nfont-size: 30px;\nfont-weight: 800;\ncolor: #ffffff;\nwidth: -moz-available;\ntext-align: center;\n}\n\n\n\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .card-blog[_ngcontent-%COMP%]   .card_wrapper[_ngcontent-%COMP%]   .row[_ngcontent-%COMP%]   .card-body[_ngcontent-%COMP%] {\n  margin-bottom: 14px;\n  margin-top: -22px;\n  text-align: center;\n }\n\n.block2[_ngcontent-%COMP%]   .test-btn1[_ngcontent-%COMP%] {\nborder-radius: 25px;\nmargin-top: 0px;\nborder: 0;\n}\n.block2[_ngcontent-%COMP%]   .test-btn2[_ngcontent-%COMP%] {\nborder-radius: 25px;\nbackground-color: white;\ncolor: black;\nborder: 0;\n}\n.block2[_ngcontent-%COMP%]   .test-btn3[_ngcontent-%COMP%] {\nborder-radius: 25px;\nborder: 0;\n}\n.block2[_ngcontent-%COMP%]:hover   .test-btn1[_ngcontent-%COMP%] {\nborder: none;\nmargin-top: 30px;\ncolor: white;\nbackground-color:red; \n}\n.block2[_ngcontent-%COMP%]:hover   .test-btn2[_ngcontent-%COMP%] {\ncolor: white;\nborder: none;\nbackground-color:red; \n}\n.block2[_ngcontent-%COMP%]:hover   .test-btn3[_ngcontent-%COMP%] {\nborder: none;\ncolor: white;\nbackground-color:red; \n}\n\n\n\n\n.chiffre[_ngcontent-%COMP%]{\ndisplay: flex;\nwidth: 100%;\n}\n\n.k[_ngcontent-%COMP%]{\nmargin: initial;\n}\n.success_item[_ngcontent-%COMP%]{\nmargin-left: 20px;\nlist-style: none;\ndisplay: flex;\nflex-direction: column;\njustify-content: center;\nalign-items: center;\n}\n.our_success[_ngcontent-%COMP%]   .success_wrapper[_ngcontent-%COMP%]   .title[_ngcontent-%COMP%]{\n  width: -moz-available;\n}\n\n.block3[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\npadding-right: 0;\nwidth: -moz-available;\n}\n\n\n\n.block4[_ngcontent-%COMP%]{\nbackground-image: url('sin.png');\nbackground-repeat: no-repeat;\nbackground-position:658px 234px;\nmargin-top:0px;\n}\n.block4[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]{\ndisplay: flex;\nflex-direction: column;\nalign-items: flex-start;\n}\n.block4[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\npadding-right: 0px;\n}\n\n.one[_ngcontent-%COMP%]{\nmargin: 40px 0 0 0 ;\n}\n.two[_ngcontent-%COMP%]{\nmargin: 0;\n}\n.three[_ngcontent-%COMP%]{\nmargin: 0;\n}\n.outer-div[_ngcontent-%COMP%]{\ndisplay: contents;\n}\n\n\n.block5[_ngcontent-%COMP%]{\nmax-width: 100%;\nwidth: 100%;\n}\n.block5[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\nmargin-left: 0;\nheight:100%;\nwidth:100%\n}\n\n.block6[_ngcontent-%COMP%]{\npadding-top: 30px; \npadding-left:0;\nmargin-top: 0; \nmax-width: 100%;\ndisplay: contents;\nflex-direction: column;\njustify-content: center;\nalign-items: center;\n}\n\n.block6[_ngcontent-%COMP%]   .third-bloc-border[_ngcontent-%COMP%]{\nmargin: 20px auto 0 auto;\n}\n\n\n\n.block7[_ngcontent-%COMP%]{\npadding-top: 10px;\n}\n\n.card1[_ngcontent-%COMP%]{\n  margin-right: 0px;\n  height: 345px;\n  width: 315px;\n  }\n  .card1[_ngcontent-%COMP%]   .box1[_ngcontent-%COMP%]{\n  box-shadow: 0px 1px 15px grey;\n  border: none;\n  border-radius: 71px 14px 71px 14px;\n  background-color: #ffffff;\n  padding: 40px;\n  }\n  .card2[_ngcontent-%COMP%]{\n    margin-right: 0px;\n    height: 345px;\n    width: 315px;\n  }\n  .card2[_ngcontent-%COMP%]   .box2[_ngcontent-%COMP%]{\n    box-shadow: 0px 1px 15px grey;\n    border: none;\n    border-radius: 71px 14px 71px 14px;\n    background-color: #ffffff;\n    padding: 40px;\n    }\n  .card3[_ngcontent-%COMP%]{\n    margin-right: 0px;\n    height: 345px;\n    width: 315px;\n  }\n  .card3[_ngcontent-%COMP%]   .box3[_ngcontent-%COMP%]{\n    box-shadow: 0px 1px 15px grey;\n    border: none;\n    border-radius: 71px 14px 71px 14px;\n    background-color: #ffffff;\n    padding: 40px;\n    }\n    \n  .lastB[_ngcontent-%COMP%]{\n    width: 100%;\n    margin: none;\n  }\n\n\n  .text-center[_ngcontent-%COMP%]{\n  margin-bottom: 50px;\n  }\n\n    }\n\n@media only screen and (min-width : 992px) and (max-width : 1200px)  {\n \n .drop[_ngcontent-%COMP%]{\n  min-width: -moz-available;\n  margin-left: -70px;\n }     \n\n.float_actions[_ngcontent-%COMP%] {\n  position: fixed;\n  background: red;\n  border-radius: 10px;\n  left: 0;\n  top: 35%;\n  padding: 10px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  width: 8%;\n  z-index:5900;\n  }\n  \n  .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%] {\n  padding: 0;\n  margin: 0 0 -30px 0;\n  display: flex;\n  flex-direction: column;\n  justify-content: space-between;\n  }\n  .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%] {\n  padding: 5px;\n  width: 120px;\n  height: 120px;\n  display: flex;\n  justify-content: center;\n  align-content: center;\n  position: relative;\n  }\n  \n  .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]   .item_href[_ngcontent-%COMP%] {\n  text-decoration: none;\n  display: block;\n  flex-direction: column;\n  justify-content: center;\n  font-size: 14px;\n  }\n  .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]   .item_href[_ngcontent-%COMP%]   .ihref_logo[_ngcontent-%COMP%] {\n  width: 100px;\n  height: 100px;\n  margin-left: 25px;\n  }\n  .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]   .item_href[_ngcontent-%COMP%]   .ihref_text[_ngcontent-%COMP%] {\n  text-align: center;\n  margin: 0;\n  color: white;\n  margin-top: 7px;\n  }\n  \n  \n   .primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]::before{\n   content: \">\";\n   position: absolute;\n   right: -10px;\n   top: 15%;\n   color: white;\n   font-size: 20px;\n   width: 40%;\n   font-weight: bold;\n   }\n   \n   .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]   .testmegi[_ngcontent-%COMP%]{\n    text-decoration: none;\n     position: absolute;\n     right: -180px;\n     top: 15%;\n     color: black;\n     font-size: 17px;\n     width: 40%;\n     background: white;\n     width: 180px;\n     border-radius: 25px;\n     text-align: center;\n     height: 40px;\n     display: none;\n     justify-content: center;\n     align-items: center;\n   }\n    .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]:hover   .testmegi[_ngcontent-%COMP%]{\n   display: flex;\n   }\n   .primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]   .item_href[_ngcontent-%COMP%] {\n    text-decoration: none;\n    display: block;\n    flex-direction: column;\n    justify-content: center;\n    font-size: 14px;\n   }\n   .primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]   .item_href[_ngcontent-%COMP%]   .ihref_logo[_ngcontent-%COMP%] {\n    width: 100px;\n    height: 100px;\n    margin-left: 25px;\n   }\n   .primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]   .item_href[_ngcontent-%COMP%]   .ihref_text[_ngcontent-%COMP%] {\n    text-align: center;\n    margin: 0;\n    color: white;\n    margin-top: 7px;\n   }\n   \n      \n\n.content[_ngcontent-%COMP%]{\n  width: max-content;\n  }\n  .transtion[_ngcontent-%COMP%]{\n    display: flex;\n    position: absolute;\n    margin-left: -140px;\n    width: max-content;\n    flex-direction: column;\n    }\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%] {\n  background:#111d5e;\n  min-height: -moz-fit-content;\n  min-height: fit-content;\n  border-bottom-right-radius: 100px;\n }\n .primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .img_wrapper[_ngcontent-%COMP%]   .heading_img[_ngcontent-%COMP%] {\n  height: 100%;\n  max-height: 585px;\n  margin-top: 1;\n  width: 993px;\n }\n .primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .title_heading[_ngcontent-%COMP%] {\n  font-size: 40px;\n  font-weight: 800;\n  color:  #ffffff;\n  width: max-content;\n }\n .primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .desc_heading[_ngcontent-%COMP%] {\n  font-size: 40px;\n  font-weight: 700;\n  color:  #ffffff;\n  width: max-content;\n }\n .carre[_ngcontent-%COMP%] {\n  width: 200px;\n  height: 90px;\n  background: white;\n  border-radius: 18px;\n  margin-left: 80px;\n  margin-top: 335px;\n  }\n  \n  .primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%] {\n    display: flex;\n    flex-direction: row;\n    justify-content: flex-start;\n    margin-left: unset;\n    width: -moz-available;\n    margin-left: 0px;\n    }\n    \n   .primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--show[_ngcontent-%COMP%]:before {\n    border: 5px solid #ffffff;\n    border-radius: 20px;\n    height: 40px;\n    width: 400px;\n   }\n   .primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--show[_ngcontent-%COMP%]   .search__input[_ngcontent-%COMP%] {\n    opacity: 1;\n    width: -moz-available;\n   }\n\n\n   \n   .home[_ngcontent-%COMP%]{\n    color: white;\n    font-size: 18px;\n    text-align: center;\n    }\n\n    .block2[_ngcontent-%COMP%]{\n      background: transparent;\n       color: white;\n       padding: 22px;\n      }\n      .block2[_ngcontent-%COMP%]:hover{\n       border: 5px solid white;\n       padding: 18px;\n      }\n\n       \n \n       .block4[_ngcontent-%COMP%]{\n        background-image: url('sin.png');\n        background-repeat: no-repeat;\n        background-position:258px 220px;\n        margin-top:0px;\n        }\n        .block4[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]{\n        display: flex;\n        flex-direction: column;\n        align-items: flex-start;\n        }\n        .block4[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\n          padding-right: 50%;\n           }\n\n        .one[_ngcontent-%COMP%]{\n          margin: 11px 0px 0px 263px\n          }\n           .two[_ngcontent-%COMP%]{\n          margin: -580px 0 0 650px\n          }\n          .three[_ngcontent-%COMP%]{\n          margin: -12px 0 0 650px\n          }\n\n          \n          .block5[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\n            margin-left: 60px;\n            height:100%;\n            width:100%\n          }\n\n          \n \n          .block6[_ngcontent-%COMP%]{\n            padding-top: 30px; \n            padding-left: 110px;\n            margin-top: -490px; \n            float:right\n          }\n\n    }\n\n@media only screen and (min-width : 1200px) and (max-width : 1500px) {\n      \n\n.content[_ngcontent-%COMP%]{\n  width: max-content;\n  }\n  .transtion[_ngcontent-%COMP%]{\n    display: flex;\n    position: absolute;\n   padding-left: 115px;\n    width: max-content;\n    flex-direction: column;\n    }\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%] {\n  background:#111d5e;\n  min-height: -moz-fit-content;\n  min-height: fit-content;\n  border-bottom-right-radius: 100px;\n }\n .primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .img_wrapper[_ngcontent-%COMP%]   .heading_img[_ngcontent-%COMP%] {\n  height: 100%;\n  max-height: 585px;\n  margin-top: 1;\n  width: 993px;\n }\n .primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .title_heading[_ngcontent-%COMP%] {\n  font-size: 40px;\n  font-weight: 800;\n  color:  #ffffff;\n  width: max-content;\n }\n .primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .desc_heading[_ngcontent-%COMP%] {\n  font-size: 40px;\n  font-weight: 700;\n  color:  #ffffff;\n  width: max-content;\n }\n .carre[_ngcontent-%COMP%] {\n  width: 200px;\n  height: 90px;\n  background: white;\n  border-radius: 18px;\n  margin-left: 80px;\n  margin-top: 335px;\n  }\n  \n  .primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%] {\n    display: flex;\n    flex-direction: row;\n    justify-content: flex-start;\n    margin-left: unset;\n    width: -moz-available;\n    margin-left: 0px;\n    }\n    \n   .primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--show[_ngcontent-%COMP%]:before {\n    border: 5px solid #ffffff;\n    border-radius: 20px;\n    height: 40px;\n    width: 400px;\n   }\n   .primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--show[_ngcontent-%COMP%]   .search__input[_ngcontent-%COMP%] {\n    opacity: 1;\n    width: -moz-available;\n   }\n\n       \n \n        .block4[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]{\n        display: flex;\n        flex-direction: column;\n        align-items: flex-start;\n        }\n     \n\n        .one[_ngcontent-%COMP%]{\n          margin: 11px 0px 0px 456px\n          }\n           .two[_ngcontent-%COMP%]{\n          margin: -580px 0 0 792px\n          }\n          .three[_ngcontent-%COMP%]{\n          margin: -12px 0 0 792px\n          }\n          \n    }\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaG9tZS9ob21lLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUNBO0VBQ0UsOEJBQThCO0VBQzlCLHVCQUF1Qjs7QUFFekI7QUFDQTtFQUNFLFlBQVk7QUFDZDtBQUVBO0VBQ0UseUJBQXlCO0VBQ3pCLGtCQUFrQjtBQUNwQjtBQUVBO0VBQ0UsbUJBQW1CO0VBQ25CLDhCQUE4QjtFQUM5QixhQUFhO0VBQ2IsZUFBZTtFQUNmO0VBQ0E7QUFDQTtNQUNJLHVCQUF1QjtFQUMzQjtBQUNBO01BQ0kseUJBQXlCO0VBQzdCO0FBQ0Y7TUFDTSx1QkFBdUI7TUFDdkIsWUFBWTtNQUNaLGVBQWU7TUFDZixtQkFBbUI7TUFDbkIsWUFBWTtNQUNaLGtCQUFrQjtFQUN0QixZQUFZO0VBQ1osYUFBYTtFQUNiLHVCQUF1QjtFQUN2QixtQkFBbUI7RUFDbkI7QUFDQTtNQUNJLDhCQUE4Qjs7RUFFbEM7QUFDQTtNQUNJLHVCQUF1QjtFQUMzQjtBQUNBO01BQ0kseUJBQXlCO0VBQzdCO0FBQ0E7TUFDSSx5QkFBeUI7O0VBRTdCO0FBQ0E7TUFDSSxXQUFXO01BQ1gsbUJBQW1CO0VBQ3ZCO0FBQ0c7TUFDQyxxQkFBcUI7TUFDckIscUJBQXFCO01BQ3JCLHdCQUF3QjtNQUN4QixrQkFBa0I7TUFDbEIsa0JBQWtCO01BQ2xCLG9CQUFvQjtNQUNwQixtQkFBbUI7TUFDbkIsaUJBQWlCO01BQ2pCLFVBQVU7RUFDZDtBQUdGLDRHQUE0RztBQUM1RztBQUNBLGVBQWU7QUFDZixlQUFlO0FBQ2YsbUJBQW1CO0FBQ25CLE9BQU87QUFDUCxRQUFRO0FBQ1IsYUFBYTtBQUNiLGFBQWE7QUFDYix1QkFBdUI7QUFDdkIsbUJBQW1CO0FBQ25CLFNBQVM7QUFDVCxZQUFZO0FBQ1o7QUFFQTtBQUNBLFVBQVU7QUFDVixtQkFBbUI7QUFDbkIsYUFBYTtBQUNiLHNCQUFzQjtBQUN0Qiw4QkFBOEI7QUFDOUI7QUFDQTtBQUNBLFlBQVk7QUFDWixZQUFZO0FBQ1osYUFBYTtBQUNiLGFBQWE7QUFDYix1QkFBdUI7QUFDdkIscUJBQXFCO0FBQ3JCLGtCQUFrQjtBQUNsQjtBQUVBO0FBQ0EscUJBQXFCO0FBQ3JCLGNBQWM7QUFDZCxzQkFBc0I7QUFDdEIsdUJBQXVCO0FBQ3ZCLGVBQWU7QUFDZjtBQUNBO0FBQ0EsWUFBWTtBQUNaLGFBQWE7QUFDYixpQkFBaUI7QUFDakI7QUFDQTtBQUNBLGtCQUFrQjtBQUNsQixTQUFTO0FBQ1QsWUFBWTtBQUNaLGVBQWU7QUFDZjtBQUdDO0NBQ0EsWUFBWTtDQUNaLGtCQUFrQjtDQUNsQixZQUFZO0NBQ1osUUFBUTtDQUNSLFlBQVk7Q0FDWixlQUFlO0NBQ2YsVUFBVTtDQUNWLGlCQUFpQjtDQUNqQjtBQUNBO0VBQ0MscUJBQXFCO0dBQ3BCLGtCQUFrQjtHQUNsQixhQUFhO0dBQ2IsUUFBUTtHQUNSLFlBQVk7R0FDWixlQUFlO0dBQ2YsVUFBVTtHQUNWLGlCQUFpQjtHQUNqQixZQUFZO0dBQ1osbUJBQW1CO0dBQ25CLGtCQUFrQjtHQUNsQixZQUFZO0dBQ1osYUFBYTtHQUNiLHVCQUF1QjtHQUN2QixtQkFBbUI7Q0FDckI7QUFDQztDQUNELGFBQWE7Q0FDYjtBQUVBO0VBQ0MscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCxzQkFBc0I7RUFDdEIsdUJBQXVCO0VBQ3ZCLGVBQWU7Q0FDaEI7QUFDQTtFQUNDLFlBQVk7RUFDWixhQUFhO0VBQ2IsaUJBQWlCO0NBQ2xCO0FBQ0E7RUFDQyxrQkFBa0I7RUFDbEIsU0FBUztFQUNULFlBQVk7RUFDWixlQUFlO0NBQ2hCO0FBSUQsZ0hBQWdIO0FBQ2hILG1IQUFtSDtBQUVuSDtFQUNFLFdBQVc7RUFDWCxXQUFXO0VBQ1gsbUJBQW1CO0VBQ25CLGNBQWM7RUFDZCxrQkFBa0I7Q0FDbkI7QUFDQTtFQUNDLFdBQVc7RUFDWCxrQkFBa0I7RUFDbEIsVUFBVTtFQUNWLE1BQU07RUFDTixPQUFPO0VBQ1AsV0FBVztFQUNYLGVBQWU7Q0FDaEI7QUFDQTtFQUNDLFdBQVc7RUFDWCxrQkFBa0I7RUFDbEIsVUFBVTtFQUNWLE1BQU07RUFDTixRQUFRO0VBQ1IsV0FBVztFQUNYLGVBQWU7Q0FDaEI7QUFFRCxrSEFBa0g7QUFDbEg7RUFDRSxrQkFBa0I7RUFDbEI7QUFDQTtJQUNFLGFBQWE7SUFDYixrQkFBa0I7SUFDbEIsbUJBQW1CO0lBQ25CLGtCQUFrQjtJQUNsQixzQkFBc0I7SUFDdEI7QUFDSjtFQUNFLGtCQUFrQjtFQUNsQiw0QkFBdUI7RUFBdkIsdUJBQXVCO0VBQ3ZCLGlDQUFpQztDQUNsQztBQUNBO0VBQ0MsY0FBYztFQUNkLGlCQUFpQjtFQUNqQixhQUFhO0VBQ2IsYUFBYTtDQUNkO0FBQ0E7RUFDQyxlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLGVBQWU7RUFDZixrQkFBa0I7Q0FDbkI7QUFDQTtFQUNDLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsZUFBZTtFQUNmLGtCQUFrQjtDQUNuQjtBQUNBO0VBQ0MsWUFBWTtFQUNaLFlBQVk7RUFDWixpQkFBaUI7RUFDakIsbUJBQW1CO0VBQ25CLG1CQUFtQjtFQUNuQixpQkFBaUI7RUFDakI7QUFHQTtJQUNFLHNCQUFzQjtHQUN2QjtBQUNBO0dBQ0EsbUJBQW1CO0dBQ25CO0FBQ0E7R0FDQSxhQUFhO0dBQ2IsbUJBQW1CO0dBQ25CLDJCQUEyQjtHQUMzQixrQkFBa0I7R0FDbEIsWUFBWTtHQUNaLGtCQUFrQjtHQUNsQjtBQUNBO0lBQ0MsU0FBUztJQUNULGtCQUFrQjtJQUNsQixRQUFRO0lBQ1IsNENBQTRDO0lBQzVDLFlBQVk7R0FDYjtBQUNBO0lBQ0MsV0FBVztJQUNYLGNBQWM7SUFDZCxrQkFBa0I7R0FDbkI7QUFDQTtJQUNDLDBCQUEwQjtJQUMxQixtQkFBbUI7SUFDbkIsWUFBWTtJQUNaLDZCQUE2QjtJQUM3QixzQkFBc0I7SUFDdEIsV0FBVztHQUNaO0FBQ0E7SUFDQyxtQkFBbUI7SUFDbkIsa0JBQWtCO0lBQ2xCLFdBQVc7SUFDWCx5QkFBeUI7SUFDekIseUJBQXlCO0lBQ3pCLDZCQUE2QjtJQUM3QixXQUFXO0dBQ1o7QUFDQTtJQUNDLHVCQUF1QjtJQUN2QixZQUFZO0lBQ1osbUJBQW1CO0lBQ25CLGNBQWM7SUFDZCxlQUFlO0lBQ2YsWUFBWTtJQUNaLGlCQUFpQjtJQUNqQixVQUFVO0lBQ1YsYUFBYTtJQUNiLGVBQWU7SUFDZixrQkFBa0I7SUFDbEIsNkJBQTZCO0lBQzdCLHNCQUFzQjtJQUN0QixXQUFXO0lBQ1gsVUFBVTtJQUNWLHlCQUF5QjtHQUMxQjtBQUNBO0lBQ0Msc0JBQXNCO0dBQ3ZCO0FBQ0E7SUFDQyxzQkFBc0I7R0FDdkI7QUFDQTtJQUNDLG9CQUFvQjtHQUNyQjtBQUNBO0lBQ0MsMERBQTBEO0lBQzFELFFBQVE7R0FDVDtBQUNBO0lBQ0MseUJBQXlCO0lBQ3pCLG1CQUFtQjtJQUNuQixZQUFZO0lBQ1osWUFBWTtHQUNiO0FBQ0E7SUFDQyxVQUFVO0lBQ1YsWUFBWTtHQUNiO0FBSUg7RUFDRSxZQUFZO0VBQ1osZUFBZTtFQUNmLHFCQUFxQjs7RUFFckIsZ0JBQWdCO0VBQ2hCLFlBQVk7RUFDWixhQUFhO0VBQ2IsWUFBWTtFQUNaLFlBQVk7RUFDWixtQkFBbUI7RUFDbkIsVUFBVTtFQUNWLGtCQUFrQjtFQUNsQixNQUFNO0VBQ04sUUFBUTtFQUNSLGdCQUFnQjtFQUNoQixVQUFVO0VBQ1YsbURBQW1EO0VBQ25ELGVBQWU7RUFDZjtBQUVBO0VBQ0EsOEJBQThCO0VBQzlCO0FBRUE7RUFDQSxZQUFZO0VBQ1osVUFBVTtFQUNWLDhCQUE4QjtFQUM5QixZQUFZO0VBQ1o7QUFDQTtFQUNBLFlBQVk7RUFDWixXQUFXO0VBQ1gscUJBQXFCO0VBQ3JCLFlBQVk7RUFDWixZQUFZO0VBQ1o7MkJBQ3lCO0VBQ3pCLHFCQUFxQjtFQUNyQixZQUFZO0VBQ1osa0JBQWtCO0VBQ2xCLE1BQU07RUFDTixRQUFRO0VBQ1IsVUFBVTtFQUNWLGVBQWU7RUFDZixZQUFZO0VBQ1osZUFBZTtFQUNmLDZCQUE2QjtFQUM3QjtBQUVBO0VBQ0EsWUFBWTtFQUNaO0FBSUQsd0hBQXdIO0FBRXpIO0VBQ0UsU0FBUztFQUNULGdCQUFnQjtFQUNoQixjQUFjO0FBQ2hCO0FBQ0E7RUFDRSxnQkFBZ0I7RUFDaEIsYUFBYTtFQUNiLHNCQUFzQjtFQUN0Qix1QkFBdUI7RUFDdkIsbUJBQW1CO0FBQ3JCO0FBQ0E7RUFDRSxTQUFTO0VBQ1QsY0FBYztBQUNoQjtBQUNBO0VBQ0UsYUFBYTtFQUNiLDhCQUE4QjtFQUM5QixTQUFTO0VBQ1QsVUFBVTtBQUNaO0FBQ0E7RUFDRSx5QkFBeUI7RUFDekIsZ0JBQWdCO0VBQ2hCLGVBQWU7RUFDZixjQUFjO0FBQ2hCO0FBQ0E7RUFDRSxlQUFlO0VBQ2YsY0FBYztBQUNoQjtBQUNBO0VBQ0UsV0FBVztFQUNYLFlBQVk7RUFDWixhQUFhO0VBQ2IsbUJBQW1COztBQUVyQjtBQUdBO0VBQ0UsaUJBQWlCO0VBQ2pCLGVBQWU7RUFDZjtBQUdGO0VBQ0UsYUFBYTtjQUNELHNCQUFzQjtjQUN0Qix1QkFBdUI7Y0FDdkIsVUFBVTtjQUNWLG9CQUFvQjtFQUNoQztBQUVELGtIQUFrSDtBQUVuSDtFQUNFLHlCQUF5QjtFQUN6QixnQkFBZ0I7RUFDaEIsZUFBZTtFQUNmLGNBQWM7Q0FDZjtBQUNBO0VBQ0MsZUFBZTtFQUNmLGNBQWM7Q0FDZjtBQUNBO0VBQ0MsYUFBYTtFQUNiLDhCQUE4QjtFQUM5QixTQUFTO0VBQ1QsVUFBVTtDQUNYO0FBQ0E7RUFDQyxnQkFBZ0I7RUFDaEIsYUFBYTtFQUNiLHNCQUFzQjtFQUN0Qix1QkFBdUI7RUFDdkIsbUJBQW1CO0NBQ3BCO0FBQ0E7RUFDQyxTQUFTO0VBQ1QsZ0JBQWdCO0VBQ2hCLGNBQWM7Q0FDZjtBQUNBO0VBQ0MsU0FBUztFQUNULGNBQWM7Q0FDZjtBQUVBLGtIQUFrSDtBQUNsSDtFQUNDLHlCQUF5QjtFQUN6QixtQkFBbUI7Q0FDcEI7QUFDQTtFQUNDLG1CQUFtQjtFQUNuQixpQkFBaUI7Q0FDbEI7QUFDQTtFQUNDLG1CQUFtQjtDQUNwQjtBQUNBO0VBQ0MsWUFBWTtFQUNaLGVBQWU7RUFDZixtQkFBbUI7RUFDbkIsWUFBWTtFQUNaLGlCQUFpQjtDQUNsQjtBQUNBO0VBQ0MsbUJBQW1CO0NBQ3BCO0FBQ0E7RUFDQyxrQkFBa0I7Q0FDbkI7QUFDQTtFQUNDLFlBQVk7RUFDWixrQkFBa0I7RUFDbEIsYUFBYTtFQUNiLHNCQUFzQjtFQUN0QixZQUFZO0VBQ1oscUJBQXFCO0VBQ3JCLHlCQUF5QjtFQUN6Qix1QkFBdUI7RUFDdkIsWUFBWTtDQUNiO0FBQ0E7RUFDQyxVQUFVO0VBQ1YsaUJBQWlCO0NBQ2xCO0FBRUE7RUFDQyx1QkFBdUI7R0FDdEIsWUFBWTtHQUNaLGFBQWE7RUFDZDtBQUNBO0dBQ0MsdUJBQXVCO0VBQ3hCO0FBRUE7SUFDRSxtQkFBbUI7SUFDbkIsZ0JBQWdCO0VBQ2xCO0FBQ0E7RUFDQSxtQkFBbUI7RUFDbkI7QUFDQTtJQUNFLG1CQUFtQjtFQUNyQjtBQUNBO0lBQ0UsWUFBWTtJQUNaLGdCQUFnQjtJQUNoQixZQUFZO0lBQ1osb0JBQW9CO0VBQ3RCO0FBQ0E7SUFDRSxZQUFZO0lBQ1osWUFBWTtJQUNaLG9CQUFvQjtFQUN0QjtBQUNBO0lBQ0UsWUFBWTtJQUNaLFlBQVk7SUFDWixvQkFBb0I7RUFDdEI7QUFDQTtJQUNFLFlBQVk7SUFDWixlQUFlO0lBQ2Y7QUFDSCxrSEFBa0g7QUFFbkg7RUFDRSxvQkFBb0I7RUFDcEIsWUFBWTtBQUNkO0FBQ0E7RUFDRSxpQkFBaUI7QUFDbkI7QUFDQTtFQUNFLGVBQWU7RUFDZjtBQUNGO0FBQ0E7RUFDRSxpQkFBaUI7RUFDakIsa0JBQWtCO0VBQ2xCO0FBQ0Y7QUFFQyxrSEFBa0g7QUFFbkg7RUFDRSxnQ0FBMEQ7RUFDMUQsNEJBQTRCO0VBQzVCLCtCQUErQjtFQUMvQixnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGFBQWE7RUFDYixpQkFBaUI7QUFDbkI7QUFFQSxrQkFBa0Isb0JBQW9CLENBQUM7QUFHdkM7O0VBRUUsYUFBYTtFQUNiLGdCQUFnQjtFQUNoQixjQUFjO0VBQ2Qsa0JBQWtCO0FBQ3BCO0FBRUE7RUFDRSxrQkFBa0I7RUFDbEIsd0NBQXdDO0FBQzFDO0FBQ0M7QUFDRDtBQUNBO0FBQ0M7QUFDRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7RUFDRSxjQUFjO0VBQ2Qsa0JBQWtCO0VBQ2xCLGdCQUFnQjtFQUNoQixZQUFZO0VBQ1osZUFBZTtFQUNmLGtCQUFrQjs7QUFFcEI7QUFJQTtFQUNFLGVBQWU7RUFDZixXQUFXO0VBQ1gsaUJBQWlCO0VBQ2pCLG1DQUEyQjtVQUEzQiwyQkFBMkI7RUFDM0IsNkNBQTZDO0VBQzdDLDZCQUE2QjtFQUM3QixtQkFBbUI7RUFDbkIsa0JBQWtCO0VBQ2xCLE1BQU07RUFDTixPQUFPO0FBQ1Q7QUFHQTtFQUNFLGtCQUFrQjtFQUNsQixTQUFTO0VBQ1QsYUFBYTtFQUNiLFlBQVk7RUFDWixjQUFjO0VBQ2Qsa0JBQWtCOztFQUVsQix3QkFBd0I7RUFDeEIsZ0JBQWdCO0VBQ2hCOzttQkFFaUI7QUFDbkI7QUFHQTtFQUNFLGtCQUFrQjtFQUNsQixTQUFTO0VBQ1QsYUFBYTtFQUNiLFlBQVk7RUFDWixjQUFjO0VBQ2Qsa0JBQWtCO0VBQ2xCLHdCQUF3QjtFQUN4QixnQkFBZ0I7RUFDaEI7O21CQUVpQjtBQUNuQjtBQUdBO0VBQ0Usa0JBQWtCO0VBQ2xCLFNBQVM7RUFDVCxhQUFhO0VBQ2IsWUFBWTtFQUNaLGNBQWM7RUFDZCxrQkFBa0I7O0VBRWxCLHdCQUF3QjtFQUN4QixnQkFBZ0I7RUFDaEI7O21CQUVpQjtBQUNuQjtBQUVBO0VBQ0Usa0JBQWtCO0VBQ2xCLFNBQVM7RUFDVCxjQUFjO0VBQ2QseUJBQXlCO0VBQ3pCLGVBQWU7RUFDZixtQ0FBMkI7VUFBM0IsMkJBQTJCO0FBQzdCO0FBRUE7RUFDRSxnQkFBZ0I7RUFDaEIscUJBQXFCO0VBQ3JCLHlCQUF5QjtFQUN6QixlQUFlO0FBQ2pCO0FBRUE7RUFDRSxrQkFBa0I7RUFDbEIsU0FBUztFQUNULFdBQVc7RUFDWCxlQUFlO0VBQ2YscUJBQXFCO0VBQ3JCLGdCQUFnQjtFQUNoQixxQ0FBcUM7QUFDdkM7QUFFQTtFQUNFLGtCQUFrQjtFQUNsQixNQUFNO0VBQ04sZUFBZTtFQUNmLGlCQUFpQjtFQUNqQixXQUFXO0FBQ2I7QUFFQTtFQUNFLGtCQUFrQjtFQUNsQixTQUFTO0VBQ1QsZUFBZTtFQUNmLFVBQVU7RUFDVixtQ0FBMkI7VUFBM0IsMkJBQTJCOztFQUUzQixnQkFBZ0I7RUFDaEIseUJBQXlCO0VBQ3pCLG9CQUFvQjs7RUFFcEIscUJBQXFCO0VBQ3JCLGlCQUFpQjtFQUNqQixtQkFBbUI7O0VBRW5CLGVBQWU7RUFDZixZQUFZO0FBQ2Q7QUFPQyxrSEFBa0g7QUFDbEg7RUFDQyxrQkFBa0I7RUFDbEIsVUFBVTtFQUNWO0FBQ0Y7QUFDQyxrSEFBa0g7QUFFbkg7RUFDRSxpQkFBaUI7RUFDakIsbUJBQW1CO0VBQ25CLGtCQUFrQjtFQUNsQjtBQUNGO0FBQ0E7RUFDRSxtQkFBbUI7R0FDbEIseUJBQXlCO0lBQ3hCLGdCQUFnQjtJQUNoQixVQUFVO0VBQ1osYUFBYTtBQUNmO0FBQ0E7RUFDRSxlQUFlO0VBQ2YsZ0JBQWdCO0FBQ2xCO0FBQ0E7QUFDQSxzQkFBc0I7QUFDdEI7QUFDQTtFQUNFLFdBQVc7QUFDYjtBQUdDLGtIQUFrSDtBQUNsSDtFQUNDLFdBQVc7O0FBRWI7QUFDQTtFQUNFO0FBQ0Y7QUFFQTtFQUNFLGtCQUFrQixDQUFDLGFBQWEsQ0FBQyxZQUFZO0FBQy9DO0FBQ0E7RUFDRSw2QkFBNkI7RUFDN0IsWUFBWTtFQUNaLGtDQUFrQztFQUNsQyx5QkFBeUI7RUFDekIsYUFBYTtBQUNmO0FBQ0E7RUFDRSxrQkFBa0I7RUFDbEIsYUFBYTtFQUNiLFlBQVk7QUFDZDtBQUNBO0VBQ0UsNkJBQTZCO0VBQzdCLFlBQVk7RUFDWixrQ0FBa0M7RUFDbEMseUJBQXlCO0VBQ3pCLGFBQWE7QUFDZjtBQUNBO0VBQ0Usa0JBQWtCO0VBQ2xCLGFBQWE7RUFDYixZQUFZO0FBQ2Q7QUFDQTtFQUNFLDZCQUE2QjtFQUM3QixZQUFZO0VBQ1osa0NBQWtDO0VBQ2xDLHlCQUF5QjtFQUN6QixhQUFhO0FBQ2Y7QUFDQTtFQUNFLHNCQUFzQjtFQUN0QixlQUFlO0VBQ2Y7QUFDRjtBQUNBO0VBQ0UsdUJBQXVCO0VBQ3ZCLGVBQWU7RUFDZjtBQUNGO0FBQ0E7RUFDRSxrQkFBa0I7RUFDbEIsV0FBVztFQUNYLFlBQVk7RUFDWixhQUFhO0VBQ2IsWUFBWTtFQUNaLGNBQWM7RUFDZCxrQkFBa0I7RUFDbEIsd0JBQXdCO0VBQ3hCLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsV0FBVztFQUNYO0FBQ0Y7QUFFQTtFQUNFLHNCQUFzQjtFQUN0QixlQUFlO0dBQ2Q7QUFDSDtBQUNBO0VBQ0UsdUJBQXVCO0VBQ3ZCLGVBQWU7RUFDZjtBQUNGO0FBQ0E7RUFDRSxrQkFBa0I7R0FDakIsV0FBVztFQUNaLFlBQVk7RUFDWixhQUFhO0VBQ2IsWUFBWTtFQUNaLGNBQWM7RUFDZCxrQkFBa0I7R0FDakIsd0JBQXdCO0dBQ3hCLGdCQUFnQjtBQUNuQjtBQUNBO0VBQ0UsV0FBVztFQUNYO0FBQ0Y7QUFFQTtFQUNFLHNCQUFzQjtFQUN0QixlQUFlO0dBQ2Q7QUFDSDtBQUNBO0VBQ0UsdUJBQXVCO0VBQ3ZCLGVBQWU7RUFDZjtBQUNGO0FBQ0E7RUFDRSxrQkFBa0I7RUFDbEIsV0FBVztFQUNYLFlBQVk7RUFDWixhQUFhO0VBQ2IsWUFBWTtFQUNaLGNBQWM7RUFDZCxrQkFBa0I7RUFDbEIsd0JBQXdCO0VBQ3hCLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsV0FBVztFQUNYO0FBQ0Y7QUFDQSwrR0FBK0c7QUFHL0c7QUFDQSxpQkFBaUI7QUFDakIsY0FBYztBQUNkLHFCQUFxQjtBQUNyQjtBQUVBO0FBQ0EsY0FBYztBQUNkLGVBQWU7QUFDZjtBQUNBO0FBQ0EsZUFBZTtBQUNmLGNBQWM7O0FBRWQ7QUFDQTtFQUNFLGVBQWU7QUFDakIsY0FBYztBQUNkO0FBR0E7RUFDRSx5QkFBeUI7RUFDekIsVUFBVTtFQUNWLGNBQWM7RUFDZCxrQkFBa0I7QUFDcEI7QUFFRSwwR0FBMEc7QUFHMUcsMkJBQTJCO0FBQ3pCOztFQUVGLHNHQUFzRztJQUNwRztRQUNJLHFCQUFxQjtRQUNyQixxQkFBcUI7UUFDckIsd0JBQXdCO1FBQ3hCLGtCQUFrQjtRQUNsQixrQkFBa0I7UUFDbEIsb0JBQW9CO1FBQ3BCLG1CQUFtQjtRQUNuQixjQUFjO1FBQ2QsVUFBVTtFQUNoQjs7RUFFQTtJQUNFLHlCQUF5QjtJQUN6QixnQkFBZ0I7RUFDbEI7O0FBRUY7RUFDRSxzQkFBc0I7RUFDdEIsOEJBQThCO0VBQzlCLGFBQWE7RUFDYixlQUFlO0VBQ2Ysa0JBQWtCO0VBQ2xCO0FBQ0YsdUhBQXVIO0FBQ3ZIO0VBQ0UsZUFBZTtBQUNqQixlQUFlO0FBQ2YsbUJBQW1CO0FBQ25CLE9BQU87QUFDUCxRQUFRO0FBQ1IsYUFBYTtBQUNiLGFBQWE7QUFDYix1QkFBdUI7QUFDdkIsbUJBQW1CO0FBQ25CLFNBQVM7QUFDVCxVQUFVO0FBQ1YsWUFBWTtBQUNaO0FBQ0E7RUFDRSxZQUFZO0VBQ1osWUFBWTtFQUNaLGVBQWU7QUFDakIsZUFBZTtBQUNmLG1CQUFtQjtBQUNuQixPQUFPO0FBQ1AsUUFBUTtBQUNSLFlBQVk7QUFDWixhQUFhO0FBQ2IsdUJBQXVCO0FBQ3ZCLG1CQUFtQjtBQUNuQixTQUFTO0FBQ1QsVUFBVTtBQUNWLGlCQUFpQjtBQUNqQixlQUFlOztBQUVmO0FBQ0E7RUFDRSxZQUFZO0VBQ1osWUFBWTtFQUNaLGVBQWU7QUFDakIsZUFBZTtBQUNmLG1CQUFtQjtBQUNuQixPQUFPO0FBQ1AsUUFBUTtBQUNSLGFBQWE7QUFDYixhQUFhO0FBQ2IsdUJBQXVCO0FBQ3ZCLG1CQUFtQjtBQUNuQixTQUFTO0FBQ1QsVUFBVTtBQUNWO0FBQ0E7RUFDRSxVQUFVO0VBQ1YsbUJBQW1CO0VBQ25CLGFBQWE7RUFDYixzQkFBc0I7RUFDdEIsOEJBQThCO0FBQ2hDO0FBQ0E7RUFDRSxlQUFlO0VBQ2YsZUFBZTtFQUNmLG1CQUFtQjtFQUNuQixPQUFPO0VBQ1AsUUFBUTtFQUNSLGFBQWE7RUFDYixhQUFhO0VBQ2IsdUJBQXVCO0VBQ3ZCLG1CQUFtQjtFQUNuQixVQUFVO0VBQ1YsWUFBWTtFQUNaLGNBQWM7QUFDaEI7O0FBRUE7QUFDQSxVQUFVO0FBQ1YsbUJBQW1CO0FBQ25CLGFBQWE7QUFDYixzQkFBc0I7QUFDdEIsOEJBQThCO0FBQzlCO0VBQ0UsaUdBQWlHO0VBQ2pHO0lBQ0UsNEJBQTRCO0VBQzlCO0VBQ0E7SUFDRSx5QkFBeUI7SUFDekIsbUJBQW1CO0lBQ25CLFlBQVk7SUFDWixZQUFZO0VBQ2Q7RUFDQTtJQUNFLFVBQVU7SUFDVixZQUFZO0VBQ2Q7O0VBRUE7SUFDRSxlQUFlO0lBQ2YsZ0JBQWdCO0lBQ2hCLGNBQWM7SUFDZCxrQkFBa0I7SUFDbEIsa0JBQWtCO0lBQ2xCLGdCQUFnQjtJQUNoQixZQUFZO0NBQ2Y7OztDQUdBO0VBQ0MsYUFBYTtFQUNiLFlBQVk7O0VBRVosbUJBQW1CO0VBQ25CLHVCQUF1QjtFQUN2QixrQkFBa0I7RUFDbEIscUJBQXFCO0VBQ3JCLG1CQUFtQjtFQUNuQixrQkFBa0I7QUFDcEI7QUFDQTtFQUNFLGFBQWE7RUFDYixxQkFBcUI7RUFDckIsdUJBQWtCO0VBQWxCLGtCQUFrQjtFQUNsQixzQkFBc0I7RUFDdEIsdUJBQXVCO0VBQ3ZCLG1CQUFtQjtFQUNuQixTQUFTO0FBQ1g7QUFDQTtFQUNFLFlBQVk7RUFDWixZQUFZO0VBQ1osaUJBQWlCO0VBQ2pCLG1CQUFtQjtFQUNuQixpQkFBaUI7RUFDakIsaUJBQWlCO0VBQ2pCLG1CQUFtQjtBQUNyQjtBQUNBO0VBQ0UsdUJBQWtCO0VBQWxCLGtCQUFrQjtBQUNwQjs7QUFFQTtFQUNFLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsY0FBYztFQUNkLHFCQUFxQjtFQUNyQixrQkFBa0I7QUFDcEI7OztBQUdBLHlHQUF5Rzs7OztBQUl6RztFQUNFLG1CQUFtQjtFQUNuQixnQkFBZ0I7RUFDaEIsU0FBUztBQUNYO0FBQ0E7QUFDQSxtQkFBbUI7QUFDbkIsdUJBQXVCO0FBQ3ZCLFlBQVk7QUFDWixTQUFTO0FBQ1Q7QUFDQTtFQUNFLG1CQUFtQjtFQUNuQixTQUFTO0FBQ1g7QUFDQTtFQUNFLFlBQVk7RUFDWixnQkFBZ0I7RUFDaEIsWUFBWTtFQUNaLG9CQUFvQjtBQUN0QjtBQUNBO0VBQ0UsWUFBWTtFQUNaLFlBQVk7RUFDWixvQkFBb0I7QUFDdEI7QUFDQTtFQUNFLFlBQVk7RUFDWixZQUFZO0VBQ1osb0JBQW9CO0FBQ3RCOzs7O0FBSUEsZ0hBQWdIO0FBQ2hIO0VBQ0UsYUFBYTtBQUNmLHNCQUFzQjtBQUN0Qix1QkFBdUI7QUFDdkIsV0FBVztBQUNYLG1CQUFtQjtBQUNuQjs7QUFFQTtFQUNFLGVBQWU7QUFDakI7QUFDQTtFQUNFLGlCQUFpQjtFQUNqQixnQkFBZ0I7RUFDaEIsYUFBYTtFQUNiLHNCQUFzQjtFQUN0Qix1QkFBdUI7RUFDdkIsbUJBQW1CO0FBQ3JCO0NBQ0MsOEdBQThHO0NBQzlHO0VBQ0MsZ0JBQWdCO0VBQ2hCLFlBQVk7RUFDWjs7RUFFQSwrRkFBK0Y7O0FBRWpHO0VBQ0UsZ0NBQTBEO0VBQzFELDRCQUE0QjtFQUM1QiwrQkFBK0I7RUFDL0IsZ0JBQWdCO0VBQ2hCO0VBQ0E7RUFDQSxhQUFhO0VBQ2Isc0JBQXNCO0VBQ3RCLHVCQUF1QjtFQUN2QjtFQUNBO0VBQ0EsZ0JBQWdCO0VBQ2hCOztHQUVDO0VBQ0QsbUJBQW1CO0VBQ25CO0dBQ0M7RUFDRCxTQUFTO0VBQ1Q7R0FDQztFQUNELFNBQVM7RUFDVDtFQUNBO0lBQ0UsaUJBQWlCO0VBQ25COztFQUVBLGtHQUFrRztFQUNsRztFQUNBLGVBQWU7RUFDZixXQUFXO0VBQ1g7RUFDQTtFQUNBLGNBQWM7RUFDZCxXQUFXO0VBQ1g7RUFDQTtFQUNBLGtHQUFrRztFQUNsRztFQUNBLGlCQUFpQjtFQUNqQixjQUFjO0VBQ2QsYUFBYTtFQUNiLGVBQWU7RUFDZixpQkFBaUI7RUFDakIsc0JBQXNCO0VBQ3RCLHVCQUF1QjtFQUN2QixtQkFBbUI7RUFDbkI7O0VBRUE7SUFDRSx3QkFBd0I7RUFDMUI7OztDQUdELGtIQUFrSDtDQUNsSDtFQUNDLGlCQUFpQjtBQUNuQjs7SUFFSTtNQUNFLGlCQUFpQjtNQUNqQixhQUFhO01BQ2IsWUFBWTtNQUNaO01BQ0E7TUFDQSw2QkFBNkI7TUFDN0IsWUFBWTtNQUNaLGtDQUFrQztNQUNsQyx5QkFBeUI7TUFDekIsYUFBYTtNQUNiO01BQ0E7UUFDRSxpQkFBaUI7UUFDakIsYUFBYTtRQUNiLFlBQVk7TUFDZDtNQUNBO1FBQ0UsNkJBQTZCO1FBQzdCLFlBQVk7UUFDWixrQ0FBa0M7UUFDbEMseUJBQXlCO1FBQ3pCLGFBQWE7UUFDYjtNQUNGO1FBQ0UsaUJBQWlCO1FBQ2pCLGFBQWE7UUFDYixZQUFZO01BQ2Q7TUFDQTtRQUNFLDZCQUE2QjtRQUM3QixZQUFZO1FBQ1osa0NBQWtDO1FBQ2xDLHlCQUF5QjtRQUN6QixhQUFhO1FBQ2I7O01BRUY7TUFDQSxVQUFVO01BQ1YsWUFBWTtNQUNaOzs7TUFHQTtNQUNBLG1CQUFtQjtNQUNuQjs7SUFFRjtBQUVILGdDQUFnQztBQUM3Qjs7O0VBR0Ysc0dBQXNHO0VBQ3RHO0lBQ0UscUJBQXFCO0lBQ3JCLHFCQUFxQjtJQUNyQix3QkFBd0I7SUFDeEIsa0JBQWtCO0lBQ2xCLGtCQUFrQjtJQUNsQixvQkFBb0I7SUFDcEIsbUJBQW1CO0lBQ25CLGNBQWM7SUFDZCxVQUFVO0FBQ2Q7QUFDQTtFQUNFLFdBQVc7RUFDWCxtQkFBbUI7RUFDbkIsaUJBQWlCO0FBQ25COztBQUVBO0VBQ0UseUJBQXlCO0VBQ3pCLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0Usc0JBQXNCO0VBQ3RCLDhCQUE4QjtFQUM5QixhQUFhO0VBQ2IsZUFBZTtFQUNmLGtCQUFrQjtFQUNsQjs7QUFFRix1SEFBdUg7QUFDdkg7QUFDQSxlQUFlO0FBQ2YsZUFBZTtBQUNmLG1CQUFtQjtBQUNuQixPQUFPO0FBQ1AsUUFBUTtBQUNSLGFBQWE7QUFDYixhQUFhO0FBQ2IsdUJBQXVCO0FBQ3ZCLG1CQUFtQjtBQUNuQixTQUFTO0FBQ1QsVUFBVTtBQUNWLFlBQVk7O0FBRVo7QUFDQTtBQUNBLFlBQVk7QUFDWixZQUFZO0FBQ1osZUFBZTtBQUNmLGVBQWU7QUFDZixtQkFBbUI7QUFDbkIsT0FBTztBQUNQLFFBQVE7QUFDUixZQUFZO0FBQ1osYUFBYTtBQUNiLHVCQUF1QjtBQUN2QixtQkFBbUI7QUFDbkIsU0FBUztBQUNULFVBQVU7QUFDVixpQkFBaUI7QUFDakIsZUFBZTs7QUFFZjtBQUNBO0FBQ0EsWUFBWTtBQUNaLFlBQVk7QUFDWixlQUFlO0FBQ2YsZUFBZTtBQUNmLG1CQUFtQjtBQUNuQixPQUFPO0FBQ1AsUUFBUTtBQUNSLGFBQWE7QUFDYixhQUFhO0FBQ2IsdUJBQXVCO0FBQ3ZCLG1CQUFtQjtBQUNuQixTQUFTO0FBQ1QsVUFBVTtBQUNWO0FBQ0E7QUFDQSxVQUFVO0FBQ1YsbUJBQW1CO0FBQ25CLGFBQWE7QUFDYixzQkFBc0I7QUFDdEIsOEJBQThCO0FBQzlCO0FBQ0E7QUFDQSxlQUFlO0FBQ2YsZUFBZTtBQUNmLG1CQUFtQjtBQUNuQixPQUFPO0FBQ1AsUUFBUTtBQUNSLGFBQWE7QUFDYixhQUFhO0FBQ2IsdUJBQXVCO0FBQ3ZCLG1CQUFtQjtBQUNuQixVQUFVO0FBQ1YsWUFBWTtBQUNaLGNBQWM7QUFDZDs7QUFFQTtBQUNBLFVBQVU7QUFDVixtQkFBbUI7QUFDbkIsYUFBYTtBQUNiLHNCQUFzQjtBQUN0Qiw4QkFBOEI7QUFDOUI7QUFDQSxpR0FBaUc7QUFDakc7QUFDQSw0QkFBNEI7QUFDNUI7QUFDQTtBQUNBLHlCQUF5QjtBQUN6QixtQkFBbUI7QUFDbkIsWUFBWTtBQUNaLFlBQVk7QUFDWjtBQUNBO0FBQ0EsVUFBVTtBQUNWLFlBQVk7QUFDWjs7QUFFQTtFQUNFLFdBQVc7QUFDYjtBQUNBO0VBQ0UsWUFBWTtFQUNaLGlCQUFpQjtFQUNqQixhQUFhO0VBQ2IsWUFBWTtBQUNkO0FBQ0E7QUFDQSxhQUFhO0FBQ2IsbUJBQW1CO0FBQ25CLHVCQUF1QjtBQUN2QixrQkFBa0I7QUFDbEIscUJBQXFCO0FBQ3JCLG1CQUFtQjtBQUNuQixrQkFBa0I7QUFDbEI7QUFDQTtBQUNBLGFBQWE7QUFDYixxQkFBcUI7QUFDckIsc0JBQXNCO0FBQ3RCLHVCQUF1QjtBQUN2QixtQkFBbUI7QUFDbkIsU0FBUztBQUNUO0FBQ0E7QUFDQSxZQUFZO0FBQ1osWUFBWTtBQUNaLGlCQUFpQjtBQUNqQixtQkFBbUI7QUFDbkIsaUJBQWlCO0FBQ2pCLGlCQUFpQjtBQUNqQixtQkFBbUI7QUFDbkI7QUFDQTtBQUNBLHVCQUFrQjtBQUFsQixrQkFBa0I7QUFDbEI7O0FBRUE7QUFDQSxlQUFlO0FBQ2YsZ0JBQWdCO0FBQ2hCLGNBQWM7QUFDZCxxQkFBcUI7QUFDckIsa0JBQWtCO0FBQ2xCOztBQUVBLHlHQUF5Rzs7OztBQUl6RztBQUNBLG1CQUFtQjtBQUNuQixnQkFBZ0I7QUFDaEIsU0FBUztBQUNUO0FBQ0E7QUFDQSxtQkFBbUI7QUFDbkIsdUJBQXVCO0FBQ3ZCLFlBQVk7QUFDWixTQUFTO0FBQ1Q7QUFDQTtBQUNBLG1CQUFtQjtBQUNuQixTQUFTO0FBQ1Q7QUFDQTtBQUNBLFlBQVk7QUFDWixnQkFBZ0I7QUFDaEIsWUFBWTtBQUNaLG9CQUFvQjtBQUNwQjtBQUNBO0FBQ0EsWUFBWTtBQUNaLFlBQVk7QUFDWixvQkFBb0I7QUFDcEI7QUFDQTtBQUNBLFlBQVk7QUFDWixZQUFZO0FBQ1osb0JBQW9CO0FBQ3BCOzs7O0FBSUEsZ0hBQWdIO0FBQ2hIO0FBQ0EsYUFBYTtBQUNiLHNCQUFzQjtBQUN0Qix1QkFBdUI7QUFDdkIsV0FBVztBQUNYLG1CQUFtQjtBQUNuQjs7QUFFQTtBQUNBLGVBQWU7QUFDZjtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCLGdCQUFnQjtBQUNoQixhQUFhO0FBQ2Isc0JBQXNCO0FBQ3RCLHVCQUF1QjtBQUN2QixtQkFBbUI7QUFDbkI7QUFDQSw4R0FBOEc7QUFDOUc7QUFDQSxnQkFBZ0I7QUFDaEIsWUFBWTtBQUNaOztBQUVBLCtGQUErRjs7QUFFL0Y7QUFDQSxnQ0FBMEQ7QUFDMUQsNEJBQTRCO0FBQzVCLDZCQUE2QjtBQUM3QixnQkFBZ0I7QUFDaEI7QUFDQTtBQUNBLGFBQWE7QUFDYixzQkFBc0I7QUFDdEIsdUJBQXVCO0FBQ3ZCO0FBQ0E7QUFDQSxnQkFBZ0I7QUFDaEI7O0FBRUE7QUFDQSxtQkFBbUI7QUFDbkI7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7O0FBRUEsa0dBQWtHO0FBQ2xHO0FBQ0EsZUFBZTtBQUNmLFdBQVc7QUFDWDtBQUNBO0FBQ0EsY0FBYztBQUNkLFdBQVc7QUFDWDtBQUNBO0FBQ0Esa0dBQWtHO0FBQ2xHO0FBQ0EsaUJBQWlCO0FBQ2pCLGNBQWM7QUFDZCxhQUFhO0FBQ2IsZUFBZTtBQUNmLGlCQUFpQjtBQUNqQixzQkFBc0I7QUFDdEIsdUJBQXVCO0FBQ3ZCLG1CQUFtQjtBQUNuQjs7QUFFQTtBQUNBLHdCQUF3QjtBQUN4Qjs7O0FBR0Esa0hBQWtIO0FBQ2xIO0FBQ0EsaUJBQWlCO0FBQ2pCOztBQUVBO0VBQ0UsaUJBQWlCO0VBQ2pCLGFBQWE7RUFDYixZQUFZO0VBQ1o7RUFDQTtFQUNBLDZCQUE2QjtFQUM3QixZQUFZO0VBQ1osa0NBQWtDO0VBQ2xDLHlCQUF5QjtFQUN6QixhQUFhO0VBQ2I7RUFDQTtJQUNFLGlCQUFpQjtJQUNqQixhQUFhO0lBQ2IsWUFBWTtFQUNkO0VBQ0E7SUFDRSw2QkFBNkI7SUFDN0IsWUFBWTtJQUNaLGtDQUFrQztJQUNsQyx5QkFBeUI7SUFDekIsYUFBYTtJQUNiO0VBQ0Y7SUFDRSxpQkFBaUI7SUFDakIsYUFBYTtJQUNiLFlBQVk7RUFDZDtFQUNBO0lBQ0UsNkJBQTZCO0lBQzdCLFlBQVk7SUFDWixrQ0FBa0M7SUFDbEMseUJBQXlCO0lBQ3pCLGFBQWE7SUFDYjs7RUFFRjtJQUNFLFdBQVc7SUFDWCxZQUFZO0VBQ2Q7OztFQUdBO0VBQ0EsbUJBQW1CO0VBQ25COzs7SUFHRTtBQUdILDBCQUEwQjtBQUN2Qjs7T0FFRyxzR0FBc0c7RUFDM0c7SUFDRSxxQkFBcUI7SUFDckIscUJBQXFCO0lBQ3JCLHdCQUF3QjtJQUN4QixrQkFBa0I7SUFDbEIsa0JBQWtCO0lBQ2xCLG9CQUFvQjtJQUNwQixtQkFBbUI7SUFDbkIsY0FBYztJQUNkLFVBQVU7QUFDZDtBQUNBO0VBQ0UsV0FBVztFQUNYLG1CQUFtQjtFQUNuQixpQkFBaUI7QUFDbkI7QUFDQTtFQUNFLHNCQUFzQjtFQUN0Qiw4QkFBOEI7RUFDOUIsYUFBYTtFQUNiLGVBQWU7RUFDZixrQkFBa0I7O0VBRWxCO0FBQ0Y7RUFDRSx5QkFBeUI7RUFDekIsZ0JBQWdCO0FBQ2xCO0FBQ0EsdUhBQXVIO0FBQ3ZIO0FBQ0EsZUFBZTtBQUNmLGVBQWU7QUFDZixtQkFBbUI7QUFDbkIsT0FBTztBQUNQLFFBQVE7QUFDUixhQUFhO0FBQ2IsYUFBYTtBQUNiLHVCQUF1QjtBQUN2QixtQkFBbUI7QUFDbkIsU0FBUztBQUNULFVBQVU7QUFDVixZQUFZO0FBQ1o7QUFDQTtBQUNBLFlBQVk7QUFDWixZQUFZO0FBQ1osZUFBZTtBQUNmLGVBQWU7QUFDZixtQkFBbUI7QUFDbkIsT0FBTztBQUNQLFFBQVE7QUFDUixZQUFZO0FBQ1osYUFBYTtBQUNiLHVCQUF1QjtBQUN2QixtQkFBbUI7QUFDbkIsU0FBUztBQUNULFVBQVU7QUFDVixpQkFBaUI7QUFDakIsZUFBZTs7QUFFZjtBQUNBO0FBQ0EsWUFBWTtBQUNaLFlBQVk7QUFDWixlQUFlO0FBQ2YsZUFBZTtBQUNmLG1CQUFtQjtBQUNuQixPQUFPO0FBQ1AsUUFBUTtBQUNSLGFBQWE7QUFDYixhQUFhO0FBQ2IsdUJBQXVCO0FBQ3ZCLG1CQUFtQjtBQUNuQixTQUFTO0FBQ1QsVUFBVTtBQUNWO0FBQ0E7QUFDQSxVQUFVO0FBQ1YsbUJBQW1CO0FBQ25CLGFBQWE7QUFDYixzQkFBc0I7QUFDdEIsOEJBQThCO0FBQzlCO0FBQ0E7QUFDQSxlQUFlO0FBQ2YsZUFBZTtBQUNmLG1CQUFtQjtBQUNuQixPQUFPO0FBQ1AsUUFBUTtBQUNSLGFBQWE7QUFDYixhQUFhO0FBQ2IsdUJBQXVCO0FBQ3ZCLG1CQUFtQjtBQUNuQixVQUFVO0FBQ1YsV0FBVztBQUNYLGNBQWM7QUFDZDs7QUFFQTtBQUNBLFVBQVU7QUFDVixtQkFBbUI7QUFDbkIsYUFBYTtBQUNiLHNCQUFzQjtBQUN0Qiw4QkFBOEI7QUFDOUI7QUFDQSxpR0FBaUc7QUFDakc7RUFDRSxjQUFjO0FBQ2hCLHFCQUFxQjtBQUNyQjtBQUNBO0FBQ0EsNEJBQTRCO0FBQzVCO0FBQ0E7QUFDQSx5QkFBeUI7QUFDekIsbUJBQW1CO0FBQ25CLFlBQVk7QUFDWixZQUFZO0FBQ1o7QUFDQTtBQUNBLFVBQVU7QUFDVixZQUFZO0FBQ1o7O0FBRUE7RUFDRSxXQUFXO0FBQ2I7QUFDQTtFQUNFLFlBQVk7RUFDWixpQkFBaUI7RUFDakIsYUFBYTtFQUNiLFlBQVk7QUFDZDtBQUNBO0FBQ0EsYUFBYTtBQUNiLG1CQUFtQjtBQUNuQix1QkFBdUI7QUFDdkIsa0JBQWtCO0FBQ2xCLHFCQUFxQjtBQUNyQixtQkFBbUI7QUFDbkIsa0JBQWtCO0FBQ2xCO0FBQ0E7QUFDQSxhQUFhO0FBQ2IscUJBQXFCO0FBQ3JCLHNCQUFzQjtBQUN0Qix1QkFBdUI7QUFDdkIsbUJBQW1CO0FBQ25CLFNBQVM7QUFDVDtBQUNBO0FBQ0EsWUFBWTtBQUNaLFlBQVk7QUFDWixpQkFBaUI7QUFDakIsbUJBQW1CO0FBQ25CLGlCQUFpQjtBQUNqQixpQkFBaUI7QUFDakIsbUJBQW1CO0FBQ25CO0FBQ0E7QUFDQSx1QkFBa0I7QUFBbEIsa0JBQWtCO0FBQ2xCOztBQUVBO0FBQ0EsZUFBZTtBQUNmLGdCQUFnQjtBQUNoQixjQUFjO0FBQ2QscUJBQXFCO0FBQ3JCLGtCQUFrQjtBQUNsQjs7QUFFQSx5R0FBeUc7O0FBRXpHO0VBQ0UsbUJBQW1CO0VBQ25CLGlCQUFpQjtFQUNqQixrQkFBa0I7Q0FDbkI7O0FBRUQ7QUFDQSxtQkFBbUI7QUFDbkIsZUFBZTtBQUNmLFNBQVM7QUFDVDtBQUNBO0FBQ0EsbUJBQW1CO0FBQ25CLHVCQUF1QjtBQUN2QixZQUFZO0FBQ1osU0FBUztBQUNUO0FBQ0E7QUFDQSxtQkFBbUI7QUFDbkIsU0FBUztBQUNUO0FBQ0E7QUFDQSxZQUFZO0FBQ1osZ0JBQWdCO0FBQ2hCLFlBQVk7QUFDWixvQkFBb0I7QUFDcEI7QUFDQTtBQUNBLFlBQVk7QUFDWixZQUFZO0FBQ1osb0JBQW9CO0FBQ3BCO0FBQ0E7QUFDQSxZQUFZO0FBQ1osWUFBWTtBQUNaLG9CQUFvQjtBQUNwQjs7OztBQUlBLGdIQUFnSDtBQUNoSDtBQUNBLGFBQWE7QUFDYixXQUFXO0FBQ1g7O0FBRUE7QUFDQSxlQUFlO0FBQ2Y7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQixnQkFBZ0I7QUFDaEIsYUFBYTtBQUNiLHNCQUFzQjtBQUN0Qix1QkFBdUI7QUFDdkIsbUJBQW1CO0FBQ25CO0FBQ0E7RUFDRSxxQkFBcUI7QUFDdkI7QUFDQSw4R0FBOEc7QUFDOUc7QUFDQSxnQkFBZ0I7QUFDaEIscUJBQXFCO0FBQ3JCOztBQUVBLCtGQUErRjs7QUFFL0Y7QUFDQSxnQ0FBMEQ7QUFDMUQsNEJBQTRCO0FBQzVCLCtCQUErQjtBQUMvQixjQUFjO0FBQ2Q7QUFDQTtBQUNBLGFBQWE7QUFDYixzQkFBc0I7QUFDdEIsdUJBQXVCO0FBQ3ZCO0FBQ0E7QUFDQSxrQkFBa0I7QUFDbEI7O0FBRUE7QUFDQSxtQkFBbUI7QUFDbkI7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7O0FBRUEsa0dBQWtHO0FBQ2xHO0FBQ0EsZUFBZTtBQUNmLFdBQVc7QUFDWDtBQUNBO0FBQ0EsY0FBYztBQUNkLFdBQVc7QUFDWDtBQUNBO0FBQ0Esa0dBQWtHO0FBQ2xHO0FBQ0EsaUJBQWlCO0FBQ2pCLGNBQWM7QUFDZCxhQUFhO0FBQ2IsZUFBZTtBQUNmLGlCQUFpQjtBQUNqQixzQkFBc0I7QUFDdEIsdUJBQXVCO0FBQ3ZCLG1CQUFtQjtBQUNuQjs7QUFFQTtBQUNBLHdCQUF3QjtBQUN4Qjs7O0FBR0Esa0hBQWtIO0FBQ2xIO0FBQ0EsaUJBQWlCO0FBQ2pCOztBQUVBO0VBQ0UsaUJBQWlCO0VBQ2pCLGFBQWE7RUFDYixZQUFZO0VBQ1o7RUFDQTtFQUNBLDZCQUE2QjtFQUM3QixZQUFZO0VBQ1osa0NBQWtDO0VBQ2xDLHlCQUF5QjtFQUN6QixhQUFhO0VBQ2I7RUFDQTtJQUNFLGlCQUFpQjtJQUNqQixhQUFhO0lBQ2IsWUFBWTtFQUNkO0VBQ0E7SUFDRSw2QkFBNkI7SUFDN0IsWUFBWTtJQUNaLGtDQUFrQztJQUNsQyx5QkFBeUI7SUFDekIsYUFBYTtJQUNiO0VBQ0Y7SUFDRSxpQkFBaUI7SUFDakIsYUFBYTtJQUNiLFlBQVk7RUFDZDtFQUNBO0lBQ0UsNkJBQTZCO0lBQzdCLFlBQVk7SUFDWixrQ0FBa0M7SUFDbEMseUJBQXlCO0lBQ3pCLGFBQWE7SUFDYjs7RUFFRjtJQUNFLFdBQVc7SUFDWCxZQUFZO0VBQ2Q7OztFQUdBO0VBQ0EsbUJBQW1CO0VBQ25COztJQUVFO0FBR0gsNkJBQTZCO0FBQzFCO0NBQ0gsbUVBQW1FO0NBQ25FO0VBQ0MseUJBQXlCO0VBQ3pCLGtCQUFrQjtDQUNuQjtBQUNELDRHQUE0RztBQUM1RztFQUNFLGVBQWU7RUFDZixlQUFlO0VBQ2YsbUJBQW1CO0VBQ25CLE9BQU87RUFDUCxRQUFRO0VBQ1IsYUFBYTtFQUNiLGFBQWE7RUFDYix1QkFBdUI7RUFDdkIsbUJBQW1CO0VBQ25CLFNBQVM7RUFDVCxZQUFZO0VBQ1o7O0VBRUE7RUFDQSxVQUFVO0VBQ1YsbUJBQW1CO0VBQ25CLGFBQWE7RUFDYixzQkFBc0I7RUFDdEIsOEJBQThCO0VBQzlCO0VBQ0E7RUFDQSxZQUFZO0VBQ1osWUFBWTtFQUNaLGFBQWE7RUFDYixhQUFhO0VBQ2IsdUJBQXVCO0VBQ3ZCLHFCQUFxQjtFQUNyQixrQkFBa0I7RUFDbEI7O0VBRUE7RUFDQSxxQkFBcUI7RUFDckIsY0FBYztFQUNkLHNCQUFzQjtFQUN0Qix1QkFBdUI7RUFDdkIsZUFBZTtFQUNmO0VBQ0E7RUFDQSxZQUFZO0VBQ1osYUFBYTtFQUNiLGlCQUFpQjtFQUNqQjtFQUNBO0VBQ0Esa0JBQWtCO0VBQ2xCLFNBQVM7RUFDVCxZQUFZO0VBQ1osZUFBZTtFQUNmOzs7R0FHQztHQUNBLFlBQVk7R0FDWixrQkFBa0I7R0FDbEIsWUFBWTtHQUNaLFFBQVE7R0FDUixZQUFZO0dBQ1osZUFBZTtHQUNmLFVBQVU7R0FDVixpQkFBaUI7R0FDakI7O0dBRUE7SUFDQyxxQkFBcUI7S0FDcEIsa0JBQWtCO0tBQ2xCLGFBQWE7S0FDYixRQUFRO0tBQ1IsWUFBWTtLQUNaLGVBQWU7S0FDZixVQUFVO0tBQ1YsaUJBQWlCO0tBQ2pCLFlBQVk7S0FDWixtQkFBbUI7S0FDbkIsa0JBQWtCO0tBQ2xCLFlBQVk7S0FDWixhQUFhO0tBQ2IsdUJBQXVCO0tBQ3ZCLG1CQUFtQjtHQUNyQjtJQUNDO0dBQ0QsYUFBYTtHQUNiO0dBQ0E7SUFDQyxxQkFBcUI7SUFDckIsY0FBYztJQUNkLHNCQUFzQjtJQUN0Qix1QkFBdUI7SUFDdkIsZUFBZTtHQUNoQjtHQUNBO0lBQ0MsWUFBWTtJQUNaLGFBQWE7SUFDYixpQkFBaUI7R0FDbEI7R0FDQTtJQUNDLGtCQUFrQjtJQUNsQixTQUFTO0lBQ1QsWUFBWTtJQUNaLGVBQWU7R0FDaEI7OztBQUdILGtIQUFrSDtBQUNsSDtFQUNFLGtCQUFrQjtFQUNsQjtFQUNBO0lBQ0UsYUFBYTtJQUNiLGtCQUFrQjtJQUNsQixtQkFBbUI7SUFDbkIsa0JBQWtCO0lBQ2xCLHNCQUFzQjtJQUN0QjtBQUNKO0VBQ0Usa0JBQWtCO0VBQ2xCLDRCQUF1QjtFQUF2Qix1QkFBdUI7RUFDdkIsaUNBQWlDO0NBQ2xDO0NBQ0E7RUFDQyxZQUFZO0VBQ1osaUJBQWlCO0VBQ2pCLGFBQWE7RUFDYixZQUFZO0NBQ2I7Q0FDQTtFQUNDLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsZUFBZTtFQUNmLGtCQUFrQjtDQUNuQjtDQUNBO0VBQ0MsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixlQUFlO0VBQ2Ysa0JBQWtCO0NBQ25CO0NBQ0E7RUFDQyxZQUFZO0VBQ1osWUFBWTtFQUNaLGlCQUFpQjtFQUNqQixtQkFBbUI7RUFDbkIsaUJBQWlCO0VBQ2pCLGlCQUFpQjtFQUNqQjs7RUFFQTtJQUNFLGFBQWE7SUFDYixtQkFBbUI7SUFDbkIsMkJBQTJCO0lBQzNCLGtCQUFrQjtJQUNsQixxQkFBcUI7SUFDckIsZ0JBQWdCO0lBQ2hCOztHQUVEO0lBQ0MseUJBQXlCO0lBQ3pCLG1CQUFtQjtJQUNuQixZQUFZO0lBQ1osWUFBWTtHQUNiO0dBQ0E7SUFDQyxVQUFVO0lBQ1YscUJBQXFCO0dBQ3RCOzs7R0FHQSwyRkFBMkY7R0FDM0Y7SUFDQyxZQUFZO0lBQ1osZUFBZTtJQUNmLGtCQUFrQjtJQUNsQjs7SUFFQTtNQUNFLHVCQUF1QjtPQUN0QixZQUFZO09BQ1osYUFBYTtNQUNkO01BQ0E7T0FDQyx1QkFBdUI7T0FDdkIsYUFBYTtNQUNkOztPQUVDLGtIQUFrSDs7T0FFbEg7UUFDQyxnQ0FBMEQ7UUFDMUQsNEJBQTRCO1FBQzVCLCtCQUErQjtRQUMvQixjQUFjO1FBQ2Q7UUFDQTtRQUNBLGFBQWE7UUFDYixzQkFBc0I7UUFDdEIsdUJBQXVCO1FBQ3ZCO1FBQ0E7VUFDRSxrQkFBa0I7V0FDakI7O1FBRUg7VUFDRTtVQUNBO1dBQ0M7VUFDRDtVQUNBO1VBQ0E7VUFDQTtVQUNBOztVQUVBLGtIQUFrSDtVQUNsSDtZQUNFLGlCQUFpQjtZQUNqQixXQUFXO1lBQ1g7VUFDRjs7VUFFQSxrSEFBa0g7O1VBRWxIO1lBQ0UsaUJBQWlCO1lBQ2pCLG1CQUFtQjtZQUNuQixrQkFBa0I7WUFDbEI7VUFDRjs7SUFFTjtBQUdDLDhCQUE4QjtBQUMvQjs7QUFFSixrSEFBa0g7QUFDbEg7RUFDRSxrQkFBa0I7RUFDbEI7RUFDQTtJQUNFLGFBQWE7SUFDYixrQkFBa0I7R0FDbkIsbUJBQW1CO0lBQ2xCLGtCQUFrQjtJQUNsQixzQkFBc0I7SUFDdEI7QUFDSjtFQUNFLGtCQUFrQjtFQUNsQiw0QkFBdUI7RUFBdkIsdUJBQXVCO0VBQ3ZCLGlDQUFpQztDQUNsQztDQUNBO0VBQ0MsWUFBWTtFQUNaLGlCQUFpQjtFQUNqQixhQUFhO0VBQ2IsWUFBWTtDQUNiO0NBQ0E7RUFDQyxlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLGVBQWU7RUFDZixrQkFBa0I7Q0FDbkI7Q0FDQTtFQUNDLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsZUFBZTtFQUNmLGtCQUFrQjtDQUNuQjtDQUNBO0VBQ0MsWUFBWTtFQUNaLFlBQVk7RUFDWixpQkFBaUI7RUFDakIsbUJBQW1CO0VBQ25CLGlCQUFpQjtFQUNqQixpQkFBaUI7RUFDakI7O0VBRUE7SUFDRSxhQUFhO0lBQ2IsbUJBQW1CO0lBQ25CLDJCQUEyQjtJQUMzQixrQkFBa0I7SUFDbEIscUJBQXFCO0lBQ3JCLGdCQUFnQjtJQUNoQjs7R0FFRDtJQUNDLHlCQUF5QjtJQUN6QixtQkFBbUI7SUFDbkIsWUFBWTtJQUNaLFlBQVk7R0FDYjtHQUNBO0lBQ0MsVUFBVTtJQUNWLHFCQUFxQjtHQUN0Qjs7T0FFSSxrSEFBa0g7O1FBRWpIO1FBQ0EsYUFBYTtRQUNiLHNCQUFzQjtRQUN0Qix1QkFBdUI7UUFDdkI7OztRQUdBO1VBQ0U7VUFDQTtXQUNDO1VBQ0Q7VUFDQTtVQUNBO1VBQ0E7VUFDQTs7SUFFTiIsImZpbGUiOiJzcmMvYXBwL2hvbWUvaG9tZS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiXG5mb290ZXJ7XG4gIGJhY2tncm91bmQ6ICMxMTFENUUgIWltcG9ydGFudDtcbiAgY29sb3I6IHdoaXRlICFpbXBvcnRhbnQ7XG5cbn1cbmZvb3RlciBhe1xuICBjb2xvcjogd2hpdGU7XG59XG5cbi5kcm9we1xuICBtaW4td2lkdGg6IC1tb3otYXZhaWxhYmxlO1xuICBtYXJnaW4tbGVmdDogLTcwcHg7XG59XG5cbi5uYXZiYXItbmF2e1xuICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgaGVpZ2h0OiA0MHB4XG4gIH1cbiAgLm5hdi1saW5re1xuICAgICAgY29sb3I6IHdoaXRlICFpbXBvcnRhbnQ7XG4gIH1cbiAgLm5hdmxpbmt3aGl0e1xuICAgICAgY29sb3I6ICMxMTFENUUgIWltcG9ydGFudDtcbiAgfVxuLmNvbntcbiAgICAgIGNvbG9yOiB3aGl0ZSAhaW1wb3J0YW50O1xuICAgICAgYm9yZGVyOiBub25lO1xuICAgICAgYmFja2dyb3VuZDogcmVkO1xuICAgICAgYm9yZGVyLXJhZGl1czogMjVweDtcbiAgICAgIHdpZHRoOiAxMjBweDtcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgaGVpZ2h0OiAxMDAlO1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgfVxuICAudG9wbmF2IGxpIGE6aG92ZXIge1xuICAgICAgYm9yZGVyLWJvdHRvbTogMC4xcHggc29saWQgcmVkO1xuXG4gIH1cbiAgLm5hdl90e1xuICAgICAgY29sb3I6IHdoaXRlICFpbXBvcnRhbnQ7XG4gIH1cbiAgLm5hdl9ne1xuICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzExMUQ1RTtcbiAgfVxuICAubmF2d2hpdHtcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICNFQkVDRjA7XG5cbiAgfVxuICAubmF2X2ltZ3tcbiAgICAgIHdpZHRoOiA4MHB4O1xuICAgICAgbWFyZ2luLWJvdHRvbTogMTBweDtcbiAgfVxuICAgICAubmF2YmFyLWJyYW5kIHtcbiAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICAgIHBhZGRpbmctdG9wOiAuMzEyNXJlbTtcbiAgICAgIHBhZGRpbmctYm90dG9tOiAuMzEyNXJlbTtcbiAgICAgIG1hcmdpbi1yaWdodDogMXJlbTtcbiAgICAgIGZvbnQtc2l6ZTogMS4yNXJlbTtcbiAgICAgIGxpbmUtaGVpZ2h0OiBpbmhlcml0O1xuICAgICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgICAgIG1hcmdpbi1sZWZ0OiA3NXB4O1xuICAgICAgei1pbmRleDogNTtcbiAgfVxuXG5cbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiogc2lkZSBiYXIgKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbi5mbG9hdF9hY3Rpb25zIHtcbnBvc2l0aW9uOiBmaXhlZDtcbmJhY2tncm91bmQ6IHJlZDtcbmJvcmRlci1yYWRpdXM6IDEwcHg7XG5sZWZ0OiAwO1xudG9wOiAzNSU7XG5wYWRkaW5nOiAxMHB4O1xuZGlzcGxheTogZmxleDtcbmp1c3RpZnktY29udGVudDogY2VudGVyO1xuYWxpZ24taXRlbXM6IGNlbnRlcjtcbndpZHRoOiA2JTtcbnotaW5kZXg6NTkwMDtcbn1cblxuLmZsb2F0X2FjdGlvbnMgLmFjdGlvbnNfY29udGVudCB7XG5wYWRkaW5nOiAwO1xubWFyZ2luOiAwIDAgLTMwcHggMDtcbmRpc3BsYXk6IGZsZXg7XG5mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuanVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xufVxuLmZsb2F0X2FjdGlvbnMgLmFjdGlvbnNfY29udGVudCAuYWN0aW9uX2l0ZW1zIHtcbnBhZGRpbmc6IDVweDtcbndpZHRoOiAxMjBweDtcbmhlaWdodDogMTIwcHg7XG5kaXNwbGF5OiBmbGV4O1xuanVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG5hbGlnbi1jb250ZW50OiBjZW50ZXI7XG5wb3NpdGlvbjogcmVsYXRpdmU7XG59XG5cbi5mbG9hdF9hY3Rpb25zIC5hY3Rpb25zX2NvbnRlbnQgLmFjdGlvbl9pdGVtcyAuaXRlbV9ocmVmIHtcbnRleHQtZGVjb3JhdGlvbjogbm9uZTtcbmRpc3BsYXk6IGJsb2NrO1xuZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbmp1c3RpZnktY29udGVudDogY2VudGVyO1xuZm9udC1zaXplOiAxNHB4O1xufVxuLmZsb2F0X2FjdGlvbnMgLmFjdGlvbnNfY29udGVudCAuYWN0aW9uX2l0ZW1zIC5pdGVtX2hyZWYgLmlocmVmX2xvZ28ge1xud2lkdGg6IDEwMHB4O1xuaGVpZ2h0OiAxMDBweDtcbm1hcmdpbi1sZWZ0OiAyNXB4O1xufVxuLmZsb2F0X2FjdGlvbnMgLmFjdGlvbnNfY29udGVudCAuYWN0aW9uX2l0ZW1zIC5pdGVtX2hyZWYgLmlocmVmX3RleHQge1xudGV4dC1hbGlnbjogY2VudGVyO1xubWFyZ2luOiAwO1xuY29sb3I6IHdoaXRlO1xubWFyZ2luLXRvcDogN3B4O1xufVxuXG5cbiAucHJpbWFyeV9ib2R5IC5mbG9hdF9hY3Rpb25zIC5hY3Rpb25zX2NvbnRlbnQgLmFjdGlvbl9pdGVtczo6YmVmb3Jle1xuIGNvbnRlbnQ6IFwiPlwiO1xuIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiByaWdodDogLTEwcHg7XG4gdG9wOiAxNSU7XG4gY29sb3I6IHdoaXRlO1xuIGZvbnQtc2l6ZTogMjBweDtcbiB3aWR0aDogNDAlO1xuIGZvbnQtd2VpZ2h0OiBib2xkO1xuIH1cbiAuZmxvYXRfYWN0aW9ucyAuYWN0aW9uc19jb250ZW50IC5hY3Rpb25faXRlbXMgLnRlc3RtZWdpe1xuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICByaWdodDogLTE4MHB4O1xuICAgdG9wOiAxNSU7XG4gICBjb2xvcjogYmxhY2s7XG4gICBmb250LXNpemU6IDE3cHg7XG4gICB3aWR0aDogNDAlO1xuICAgYmFja2dyb3VuZDogd2hpdGU7XG4gICB3aWR0aDogMTgwcHg7XG4gICBib3JkZXItcmFkaXVzOiAyNXB4O1xuICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgaGVpZ2h0OiA0MHB4O1xuICAgZGlzcGxheTogbm9uZTtcbiAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiB9XG4gIC5mbG9hdF9hY3Rpb25zIC5hY3Rpb25zX2NvbnRlbnQgLmFjdGlvbl9pdGVtczpob3ZlciAudGVzdG1lZ2l7XG4gZGlzcGxheTogZmxleDtcbiB9XG4gXG4gLnByaW1hcnlfYm9keSAuZmxvYXRfYWN0aW9ucyAuYWN0aW9uc19jb250ZW50IC5hY3Rpb25faXRlbXMgLml0ZW1faHJlZiB7XG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBmb250LXNpemU6IDE0cHg7XG4gfVxuIC5wcmltYXJ5X2JvZHkgLmZsb2F0X2FjdGlvbnMgLmFjdGlvbnNfY29udGVudCAuYWN0aW9uX2l0ZW1zIC5pdGVtX2hyZWYgLmlocmVmX2xvZ28ge1xuICB3aWR0aDogMTAwcHg7XG4gIGhlaWdodDogMTAwcHg7XG4gIG1hcmdpbi1sZWZ0OiAyNXB4O1xuIH1cbiAucHJpbWFyeV9ib2R5IC5mbG9hdF9hY3Rpb25zIC5hY3Rpb25zX2NvbnRlbnQgLmFjdGlvbl9pdGVtcyAuaXRlbV9ocmVmIC5paHJlZl90ZXh0IHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW46IDA7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgbWFyZ2luLXRvcDogN3B4O1xuIH1cbiBcblxuXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKmhvbWUgKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipkaXZpZGVyICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cblxuLnByaW1hcnlfYm9keSAuZGl2aWRlciAuZGl2aWRlcl9saWduZSB7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDVweDtcbiAgYmFja2dyb3VuZDogIzExMWQ1ZTtcbiAgZGlzcGxheTogYmxvY2s7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiB9XG4gLnByaW1hcnlfYm9keSAuZGl2aWRlciAuZGl2aWRlcl9saWduZTo6YmVmb3JlIHtcbiAgY29udGVudDogXCJcIjtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB3aWR0aDogMzAlO1xuICB0b3A6IDA7XG4gIGxlZnQ6IDA7XG4gIGhlaWdodDogNXB4O1xuICBiYWNrZ3JvdW5kOiByZWQ7XG4gfVxuIC5wcmltYXJ5X2JvZHkgLmRpdmlkZXIgLmRpdmlkZXJfbGlnbmU6OmFmdGVyIHtcbiAgY29udGVudDogXCJcIjtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB3aWR0aDogMzAlO1xuICB0b3A6IDA7XG4gIHJpZ2h0OiAwO1xuICBoZWlnaHQ6IDVweDtcbiAgYmFja2dyb3VuZDogcmVkO1xuIH1cbiBcbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqYmxvY2swICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbi5jb250ZW50e1xuICB3aWR0aDogbWF4LWNvbnRlbnQ7XG4gIH1cbiAgLnRyYW5zdGlvbntcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBtYXJnaW4tbGVmdDogLTM1MHB4O1xuICAgIHdpZHRoOiBtYXgtY29udGVudDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIH1cbi5wcmltYXJ5X2JvZHkgLmhvbWVfY29udGFpbmVyIC5zZWN0aW9uX2hlYWRpbmcge1xuICBiYWNrZ3JvdW5kOiMxMTFkNWU7XG4gIG1pbi1oZWlnaHQ6IGZpdC1jb250ZW50O1xuICBib3JkZXItYm90dG9tLXJpZ2h0LXJhZGl1czogMTAwcHg7XG4gfVxuIC5wcmltYXJ5X2JvZHkgLmhvbWVfY29udGFpbmVyIC5zZWN0aW9uX2hlYWRpbmcgLmhlYWRpbmdfd3JhcHBlciAuaW1nX3dyYXBwZXIgLmhlYWRpbmdfaW1nIHtcbiAgaGVpZ2h0OiAxMDAwcHg7XG4gIG1heC1oZWlnaHQ6IDY0MHB4O1xuICBtYXJnaW4tdG9wOiAxO1xuICB3aWR0aDogMTA1MHB4O1xuIH1cbiAucHJpbWFyeV9ib2R5IC5ob21lX2NvbnRhaW5lciAuc2VjdGlvbl9oZWFkaW5nIC5oZWFkaW5nX3dyYXBwZXIgLnRpdGxlX2hlYWRpbmcge1xuICBmb250LXNpemU6IDYwcHg7XG4gIGZvbnQtd2VpZ2h0OiA4MDA7XG4gIGNvbG9yOiAgI2ZmZmZmZjtcbiAgd2lkdGg6IG1heC1jb250ZW50O1xuIH1cbiAucHJpbWFyeV9ib2R5IC5ob21lX2NvbnRhaW5lciAuc2VjdGlvbl9oZWFkaW5nIC5oZWFkaW5nX3dyYXBwZXIgLmRlc2NfaGVhZGluZyB7XG4gIGZvbnQtc2l6ZTogNjBweDtcbiAgZm9udC13ZWlnaHQ6IDcwMDtcbiAgY29sb3I6ICAjZmZmZmZmO1xuICB3aWR0aDogbWF4LWNvbnRlbnQ7XG4gfVxuIC5jYXJyZSB7XG4gIHdpZHRoOiAyMDBweDtcbiAgaGVpZ2h0OiA5MHB4O1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgYm9yZGVyLXJhZGl1czogMThweDtcbiAgbWFyZ2luLWxlZnQ6IC0yOTBweDtcbiAgbWFyZ2luLXRvcDogMzg1cHg7XG4gIH1cblxuXG4gIC5wcmltYXJ5X2JvZHkgLnNlYXJjaF9ibG9jICosIC5wcmltYXJ5X2JvZHkgLnNlYXJjaF9ibG9jICo6YmVmb3JlLCAucHJpbWFyeV9ib2R5IC5zZWFyY2hfYmxvYyAqOmFmdGVyIHtcbiAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICAgfVxuICAgLnByaW1hcnlfYm9keSAuc2VhcmNoX2Jsb2MgYm9keSB7XG4gICBiYWNrZ3JvdW5kOiAjZjVmNWY1O1xuICAgfVxuICAgLnByaW1hcnlfYm9keSAuc2VhcmNoX2Jsb2Mge1xuICAgZGlzcGxheTogZmxleDtcbiAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XG4gICBtYXJnaW4tbGVmdDogdW5zZXQ7XG4gICB3aWR0aDogNjUwcHg7XG4gICBtYXJnaW4tbGVmdDogMTAwcHg7XG4gICB9XG4gICAucHJpbWFyeV9ib2R5IC5zZWFyY2hfYmxvYyBtYWluIHtcbiAgICBsZWZ0OiA1MCU7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHRvcDogNTAlO1xuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWCgtNTAlKSB0cmFuc2xhdGVZKC01MCUpO1xuICAgIHdpZHRoOiAzMDBweDtcbiAgIH1cbiAgIC5wcmltYXJ5X2JvZHkgLnNlYXJjaF9ibG9jIC5zZWFyY2g6YmVmb3JlLCAucHJpbWFyeV9ib2R5IC5zZWFyY2hfYmxvYyAuc2VhcmNoOmFmdGVyIHtcbiAgICBjb250ZW50OiBcIlwiO1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgIH1cbiAgIC5wcmltYXJ5X2JvZHkgLnNlYXJjaF9ibG9jIC5zZWFyY2g6YmVmb3JlIHtcbiAgICBib3JkZXI6IDVweCBzb2xpZCAjZmZmZmZmIDtcbiAgICBib3JkZXItcmFkaXVzOiAyMHB4O1xuICAgIGhlaWdodDogNDBweDtcbiAgICB0cmFuc2l0aW9uOiBhbGwgMC4zcyBlYXNlLW91dDtcbiAgICB0cmFuc2l0aW9uLWRlbGF5OiAwLjNzO1xuICAgIHdpZHRoOiA0MHB4O1xuICAgfVxuICAgLnByaW1hcnlfYm9keSAuc2VhcmNoX2Jsb2MgLnNlYXJjaDphZnRlciB7XG4gICAgYmFja2dyb3VuZDogI2ZmZmZmZjtcbiAgICBib3JkZXItcmFkaXVzOiAzcHg7XG4gICAgaGVpZ2h0OiA1cHg7XG4gICAgdHJhbnNmb3JtOiByb3RhdGUoLTQ1ZGVnKTtcbiAgICB0cmFuc2Zvcm0tb3JpZ2luOiAwJSAxMDAlO1xuICAgIHRyYW5zaXRpb246IGFsbCAwLjNzIGVhc2Utb3V0O1xuICAgIHdpZHRoOiAxNXB4O1xuICAgfVxuICAgLnByaW1hcnlfYm9keSAuc2VhcmNoX2Jsb2MgLnNlYXJjaF9faW5wdXQge1xuICAgIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICAgIGJvcmRlcjogbm9uZTtcbiAgICBib3JkZXItcmFkaXVzOiAyMHB4O1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICBoZWlnaHQ6IDQwcHg7XG4gICAgbGluZS1oZWlnaHQ6IDQwcHg7XG4gICAgb3BhY2l0eTogMDtcbiAgICBvdXRsaW5lOiBub25lO1xuICAgIHBhZGRpbmc6IDAgMTVweDtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgdHJhbnNpdGlvbjogYWxsIDAuM3MgZWFzZS1vdXQ7XG4gICAgdHJhbnNpdGlvbi1kZWxheTogMC42cztcbiAgICB3aWR0aDogNDBweDtcbiAgICB6LWluZGV4OiAxO1xuICAgIGNvbG9yOiByZ2IoMjIzLCAyMjMsIDIyMyk7XG4gICB9XG4gICAucHJpbWFyeV9ib2R5IC5zZWFyY2hfYmxvYyAuc2VhcmNoLS1oaWRlOmJlZm9yZSB7XG4gICAgdHJhbnNpdGlvbi1kZWxheTogMC4zcztcbiAgIH1cbiAgIC5wcmltYXJ5X2JvZHkgLnNlYXJjaF9ibG9jIC5zZWFyY2gtLWhpZGU6YWZ0ZXIge1xuICAgIHRyYW5zaXRpb24tZGVsYXk6IDAuNnM7XG4gICB9XG4gICAucHJpbWFyeV9ib2R5IC5zZWFyY2hfYmxvYyAuc2VhcmNoLS1oaWRlIC5zZWFyY2hfX2lucHV0IHtcbiAgICB0cmFuc2l0aW9uLWRlbGF5OiAwcztcbiAgIH1cbiAgIC5wcmltYXJ5X2JvZHkgLnNlYXJjaF9ibG9jIC5zZWFyY2gtLXNob3c6YWZ0ZXIge1xuICAgIHRyYW5zZm9ybTogcm90YXRlKDQ1ZGVnKSB0cmFuc2xhdGVYKDE1cHgpIHRyYW5zbGF0ZVkoLTJweCk7XG4gICAgd2lkdGg6IDA7XG4gICB9XG4gICAucHJpbWFyeV9ib2R5IC5zZWFyY2hfYmxvYyAuc2VhcmNoLS1zaG93OmJlZm9yZSB7XG4gICAgYm9yZGVyOiA1cHggc29saWQgI2ZmZmZmZjtcbiAgICBib3JkZXItcmFkaXVzOiAyMHB4O1xuICAgIGhlaWdodDogNDBweDtcbiAgICB3aWR0aDogNTAwcHg7XG4gICB9XG4gICAucHJpbWFyeV9ib2R5IC5zZWFyY2hfYmxvYyAuc2VhcmNoLS1zaG93IC5zZWFyY2hfX2lucHV0IHtcbiAgICBvcGFjaXR5OiAxO1xuICAgIHdpZHRoOiA1MDBweDtcbiAgIH1cblxuICAgXG5cbmlucHV0W3R5cGU9XCJ0ZXh0XCJdIHtcbiAgaGVpZ2h0OiA1MHB4O1xuICBmb250LXNpemU6IDMwcHg7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgXG4gIGZvbnQtd2VpZ2h0OiAxMDA7XG4gIGJvcmRlcjogbm9uZTtcbiAgb3V0bGluZTogbm9uZTtcbiAgY29sb3I6IHdoaXRlO1xuICBwYWRkaW5nOiAzcHg7XG4gIHBhZGRpbmctcmlnaHQ6IDYwcHg7XG4gIHdpZHRoOiAwcHg7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiAwO1xuICByaWdodDogMDtcbiAgYmFja2dyb3VuZDogbm9uZTtcbiAgei1pbmRleDogMztcbiAgdHJhbnNpdGlvbjogd2lkdGggMC40cyBjdWJpYy1iZXppZXIoMCwgMC43OTUsIDAsIDEpO1xuICBjdXJzb3I6IHBvaW50ZXI7XG4gIH1cbiAgXG4gIGlucHV0W3R5cGU9XCJ0ZXh0XCJdOmZvY3VzOmhvdmVyIHtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIHdoaXRlO1xuICB9XG4gIFxuICBpbnB1dFt0eXBlPVwidGV4dFwiXTpmb2N1cyB7XG4gIHdpZHRoOiA3MDBweDtcbiAgei1pbmRleDogMTtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIHdoaXRlO1xuICBjdXJzb3I6IHRleHQ7XG4gIH1cbiAgaW5wdXRbdHlwZT1cInN1Ym1pdFwiXSB7XG4gIGhlaWdodDogNTBweDtcbiAgd2lkdGg6IDUwcHg7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgY29sb3I6IHdoaXRlO1xuICBmbG9hdDogcmlnaHQ7XG4gIGJhY2tncm91bmQ6IHVybChkYXRhOmltYWdlL3BuZztiYXNlNjQsaVZCT1J3MEtHZ29BQUFBTlNVaEVVZ0FBQURBQUFBQXdDQU1BQUFCZzNBbTFBQUFBR1hSRldIUlRiMlowZDJGeVpRQkJaRzlpWlNCSmJXRm5aVkpsWVdSNWNjbGxQQUFBQUROUVRGUkZVMU5UOWZYMWxKU1VYbDVlMWRYVmZuNStjM056NnVycXY3Ky90TFMwaVltSnFhbXBuNStmeXNySzM5L2ZhV2xwLy8vL1ZpNFp5d0FBQUJGMFVrNVQvLy8vLy8vLy8vLy8vLy8vLy8vLy93QWxyWmxpQUFBQkxrbEVRVlI0MnJTV1dSYkRJQWhGSGVPVXROMy9hZ3MxemFBNGNIcktaOEpGUkh3b1hrd1R2d0dQMVFvMGJZT2JBUHdpTG1iTkFIQldGQlpsRDlqMEp4ZmxEVmlJT2JOSEcvRG84UFJIVEprMFRlekFodjdxbG9LMEpKRUJoK0Y4K1UvaG9wSUVMT1dmaVpVQ0RPWkQxUkFET1FLQTc1b3E0Y3ZWa2NUK09kSG5xcXBRQ0lUV0FqbldWZ0dRVVd6MTJsSnVHd0dvYVdnQkt6UlZCY0N5cGdVa09Bb1dnQlgvTDBDbXhONDB1Nnh3Y0lKMWNPeldZRGZmcDNheHNRT3l2ZGtYaUg5RktSRndQUkhZWlVhWE1nUExlaVc3UWhiRFJjaXlMWEphS2hlQ3VMYmlWb3F4MURWUnlIMjZ5YjBoc3VvT0ZFUHNveitCVkUwTVJsWk5qR1pjUlF5SFlrbU1wMmhCVEl6ZGt6Q1RjL3BMcU9uQnJrNy95WmRBT3EvcTVOUEJIMWY3eDdmR1A0QzNBQU1BUXJoelg5emhjR3NBQUFBQVNVVk9SSzVDWUlJPSlcbiAgICBjZW50ZXIgY2VudGVyIG5vLXJlcGVhdDtcbiAgdGV4dC1pbmRlbnQ6IC0xMDAwMHB4O1xuICBib3JkZXI6IG5vbmU7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiAwO1xuICByaWdodDogMDtcbiAgei1pbmRleDogMjtcbiAgY3Vyc29yOiBwb2ludGVyO1xuICBvcGFjaXR5OiAwLjQ7XG4gIGN1cnNvcjogcG9pbnRlcjtcbiAgdHJhbnNpdGlvbjogb3BhY2l0eSAwLjRzIGVhc2U7XG4gIH1cbiAgXG4gIGlucHV0W3R5cGU9XCJzdWJtaXRcIl06aG92ZXIge1xuICBvcGFjaXR5OiAwLjg7XG4gIH1cbiAgXG5cblxuIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqbm90cmUgc3VjY2VzICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbiAgXG4uaXRlbV9udW0ge1xuICBtYXJnaW46IDA7XG4gIGZvbnQtd2VpZ2h0OiA3MDA7XG4gIGNvbG9yOiAjMTExZDVlO1xufVxuLnN1Y2Nlc3NfaXRlbSB7XG4gIGxpc3Qtc3R5bGU6IG5vbmU7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuLml0ZW1fZGVzYyB7XG4gIG1hcmdpbjogMDtcbiAgY29sb3I6ICMxMTFkNWU7XG59XG4uc3VjY2Vzc19saXN0IHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICBtYXJnaW46IDA7XG4gIHBhZGRpbmc6IDA7XG59XG4uc3VjY2Vzc190eHQge1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICBmb250LXdlaWdodDogNDAwO1xuICBmb250LXNpemU6IDE0cHg7XG4gIGNvbG9yOiAjMTExZDVlO1xufVxuLnN1Y2Nlc3NfZGVzYyB7XG4gIGZvbnQtc2l6ZTogMjhweDtcbiAgY29sb3I6ICMxMTFkNWU7XG59XG4uY29udGFpbmVyX2JveCB7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IGF1dG87XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XG5cbn1cblxuXG4ua3tcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIG1hcmdpbjogaW5oZXJpdDtcbiAgfVxuICBcbiAgIFxuLmNoaWZmcmV7XG4gIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgICAgICAgICAgICB3aWR0aDogNDAlO1xuICAgICAgICAgICAgICBhbGlnbi1pdGVtczogaW5oZXJpdDtcbiAgfVxuXG4gLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipibG9jazEgKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuIFxuLnByaW1hcnlfYm9keSAuaG9tZV9jb250YWluZXIgLm91cl9zdWNjZXNzIC5zdWNjZXNzX3dyYXBwZXIgLnN1Y2Nlc3NfdHh0IHtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgZm9udC13ZWlnaHQ6IDQwMDtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBjb2xvcjogIzExMWQ1ZTtcbiB9XG4gLnByaW1hcnlfYm9keSAuaG9tZV9jb250YWluZXIgLm91cl9zdWNjZXNzIC5zdWNjZXNzX3dyYXBwZXIgLnN1Y2Nlc3NfZGVzYyB7XG4gIGZvbnQtc2l6ZTogMjhweDtcbiAgY29sb3I6ICMxMTFkNWU7XG4gfVxuIC5wcmltYXJ5X2JvZHkgLmhvbWVfY29udGFpbmVyIC5vdXJfc3VjY2VzcyAuc3VjY2Vzc193cmFwcGVyIC5zdWNjZXNzX2xpc3Qge1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gIG1hcmdpbjogMDtcbiAgcGFkZGluZzogMDtcbiB9XG4gLnByaW1hcnlfYm9keSAuaG9tZV9jb250YWluZXIgLm91cl9zdWNjZXNzIC5zdWNjZXNzX3dyYXBwZXIgLnN1Y2Nlc3NfbGlzdCAuc3VjY2Vzc19pdGVtIHtcbiAgbGlzdC1zdHlsZTogbm9uZTtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gfVxuIC5wcmltYXJ5X2JvZHkgLmhvbWVfY29udGFpbmVyIC5vdXJfc3VjY2VzcyAuc3VjY2Vzc193cmFwcGVyIC5zdWNjZXNzX2xpc3QgLnN1Y2Nlc3NfaXRlbSAuaXRlbV9udW0ge1xuICBtYXJnaW46IDA7XG4gIGZvbnQtd2VpZ2h0OiA3MDA7XG4gIGNvbG9yOiAjMTExZDVlO1xuIH1cbiAucHJpbWFyeV9ib2R5IC5ob21lX2NvbnRhaW5lciAub3VyX3N1Y2Nlc3MgLnN1Y2Nlc3Nfd3JhcHBlciAuc3VjY2Vzc19saXN0IC5zdWNjZXNzX2l0ZW0gLml0ZW1fZGVzYyB7XG4gIG1hcmdpbjogMDtcbiAgY29sb3I6ICMxMTFkNWU7XG4gfVxuIFxuIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqYmxvY2syICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbiAucHJpbWFyeV9ib2R5IC5ob21lX2NvbnRhaW5lciAuY2FyZC1ibG9nIC5jYXJkX3dyYXBwZXIgLnJvdyB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMxMTFkNWU7XG4gIGJvcmRlci1yYWRpdXM6IDMwcHg7XG4gfVxuIC5wcmltYXJ5X2JvZHkgLmhvbWVfY29udGFpbmVyIC5jYXJkLWJsb2cgLmNhcmRfd3JhcHBlciAucm93IC5jYXJkLWJvZHkge1xuICBtYXJnaW4tYm90dG9tOiAxNHB4O1xuICBtYXJnaW4tdG9wOiAtMjJweDtcbiB9XG4gLnByaW1hcnlfYm9keSAuaG9tZV9jb250YWluZXIgLmNhcmQtYmxvZyAuY2FyZF93cmFwcGVyIC5yb3cgLmNhcmQtYm9keSAudGVzdC1idG4xIHtcbiAgYm9yZGVyLXJhZGl1czogMjBweDtcbiB9XG4gLnByaW1hcnlfYm9keSAuaG9tZV9jb250YWluZXIgLmNhcmQtYmxvZyAuY2FyZF93cmFwcGVyIC5yb3cgLmNhcmQtYm9keSAudGVzdC1idG4yIHtcbiAgYm9yZGVyOiBub25lO1xuICBiYWNrZ3JvdW5kOiByZWQ7XG4gIGJvcmRlci1yYWRpdXM6IDI1cHg7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgcGFkZGluZzogOHB4IDMwcHg7XG4gfVxuIC5wcmltYXJ5X2JvZHkgLmhvbWVfY29udGFpbmVyIC5jYXJkLWJsb2cgLmNhcmRfd3JhcHBlciAucm93IC5jYXJkLWJvZHkgLnRlc3QtYnRuMyB7XG4gIGJvcmRlci1yYWRpdXM6IDIwcHg7XG4gfVxuIC5wcmltYXJ5X2JvZHkgLmhvbWVfY29udGFpbmVyIC5jYXJkLWJsb2cgLmNhcmRfd3JhcHBlciAucm93IC5jYXJkLWdyb3VwIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuIH1cbiAucHJpbWFyeV9ib2R5IC5ob21lX2NvbnRhaW5lciAuY2FyZC1ibG9nIC5jYXJkX3dyYXBwZXIgLnJvdyAuY2FyZC1ncm91cCAuY2FyZCB7XG4gIGJvcmRlcjogbm9uZTtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICBtaW4td2lkdGg6IDA7XG4gIHdvcmQtd3JhcDogYnJlYWstd29yZDtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzExMWQ1ZTtcbiAgYm9yZGVyLXJhZGl1czogMTIuMjVyZW07XG4gIGNvbG9yOiB3aGl0ZTtcbiB9XG4gLnByaW1hcnlfYm9keSAuaG9tZV9jb250YWluZXIgLmNhcmQtYmxvZyAuY2FyZF93cmFwcGVyIC5yb3cgLmNhcmQtZ3JvdXAgLmNhcmQgLmNhcmQtaW1nLXRvcCB7XG4gIHdpZHRoOiA1MCU7XG4gIG1hcmdpbi1sZWZ0OiA5MXB4O1xuIH1cblxuIC5ibG9jazJ7XG4gIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICAgY29sb3I6IHdoaXRlO1xuICAgcGFkZGluZzogMTlweDtcbiAgfVxuICAuYmxvY2syOmhvdmVye1xuICAgYm9yZGVyOiA1cHggc29saWQgd2hpdGU7XG4gIH1cbiBcbiAgLmJsb2NrMiAudGVzdC1idG4xIHtcbiAgICBib3JkZXItcmFkaXVzOiAyNXB4O1xuICAgIG1hcmdpbi10b3A6IDMwcHg7XG4gIH1cbiAgLmJsb2NrMiAudGVzdC1idG4yIHtcbiAgYm9yZGVyLXJhZGl1czogMjVweDtcbiAgfVxuICAuYmxvY2syIC50ZXN0LWJ0bjMge1xuICAgIGJvcmRlci1yYWRpdXM6IDI1cHg7XG4gIH1cbiAgLmJsb2NrMjpob3ZlciAudGVzdC1idG4xIHtcbiAgICBib3JkZXI6IG5vbmU7XG4gICAgbWFyZ2luLXRvcDogMzBweDtcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgYmFja2dyb3VuZC1jb2xvcjpyZWQ7IFxuICB9XG4gIC5ibG9jazI6aG92ZXIgLnRlc3QtYnRuMiB7XG4gICAgY29sb3I6IHdoaXRlO1xuICAgIGJvcmRlcjogbm9uZTtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOnJlZDsgXG4gIH1cbiAgLmJsb2NrMjpob3ZlciAudGVzdC1idG4zIHtcbiAgICBib3JkZXI6IG5vbmU7XG4gICAgY29sb3I6IHdoaXRlO1xuICAgIGJhY2tncm91bmQtY29sb3I6cmVkOyBcbiAgfVxuICAuaG9tZXtcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgZm9udC1zaXplOiAxOHB4O1xuICAgIH1cbiAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKmJsb2NrMyAqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG4gXG4uYmxvY2szIHB7XG4gIHBhZGRpbmctcmlnaHQ6IDI1cmVtO1xuICB3aWR0aDogODAwcHg7XG59XG4udGV4dC1ibG9jIC50ZXh0X2JvZHl7XG4gIG1hcmdpbi1sZWZ0OiAxNXB4O1xufVxuLnRleHQtYmxvYyAudGV4dF9ib2R5IC50ZXh0X2NvbnRlbnR7XG4gIGZvbnQtc2l6ZTogNzBweDtcbiAgY29sb3I6IzExMWQ1ZVxufVxuLnRleHQtYmxvYyAudGV4dF9ib2R5ICBoMXtcbiAgbWFyZ2luLXRvcDogLTc1cHg7XG4gIG1hcmdpbi1sZWZ0OiAtMTdweDtcbiAgY29sb3I6IzExMWQ1ZVxufVxuXG4gLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipibG9jazQgKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuIFxuLmJsb2NrNHtcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKC4uLy4uL2Fzc2V0cy9jcG5pbWFnZXMvaG9tZS9zaW4ucG5nKTtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjo2NThweCAyMzRweDtcbiAgbWFyZ2luLXRvcDoxMDBweDtcbn1cbi5ibG9jazQgaDF7XG4gIGNvbG9yOiMxMTFkNWU7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuXG4uYmxvY2s0IC5ibG9jazEgcHtwYWRkaW5nLXJpZ2h0OiA0OHJlbTt9XG5cblxuLm91dGVyLWRpdixcbi5pbm5lci1kaXYge1xuICBoZWlnaHQ6IDM3OHB4O1xuICBtYXgtd2lkdGg6IDMwMHB4O1xuICBtYXJnaW46IDAgYXV0bztcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuXG4ub3V0ZXItZGl2IHtcbiAgcGVyc3BlY3RpdmU6IDkwMHB4O1xuICBwZXJzcGVjdGl2ZS1vcmlnaW46IDUwJSBjYWxjKDUwJSAtIDE4ZW0pO1xufVxuIC5vbmV7XG5tYXJnaW46IDExcHggMHB4IDBweCA1NTZweFxufVxuIC50d297XG5tYXJnaW46IC01ODBweCAwIDAgMTA2NXB4XG59XG4udGhyZWV7XG5tYXJnaW46IC0xMnB4IDAgMCAxMDU5cHhcbn1cblxuLmlubmVyLWRpdiB7XG4gIG1hcmdpbjogMCBhdXRvO1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG4gIGZvbnQtd2VpZ2h0OiA0MDA7XG4gIGNvbG9yOiBibGFjaztcbiAgZm9udC1zaXplOiAxcmVtO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gXG59XG5cblxuXG4uZnJvbnQge1xuICBjdXJzb3I6IHBvaW50ZXI7XG4gIGhlaWdodDogODUlO1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgYmFja2ZhY2UtdmlzaWJpbGl0eTogaGlkZGVuO1xuICBib3gtc2hhZG93OiAwIDAgNDBweCByZ2JhKDAsIDAsIDAsIDAuMSkgaW5zZXQ7XG4gIGJveC1zaGFkb3c6IDBweCAxcHggMTVweCBncmV5O1xuICBib3JkZXItcmFkaXVzOiAyNXB4O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIHRvcDogMDtcbiAgbGVmdDogMDtcbn1cblxuXG4uZnJvbnRfX2ZhY2UtcGhvdG8xIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICB0b3A6IDEwcHg7XG4gIGhlaWdodDogMTIwcHg7XG4gIHdpZHRoOiAxMjBweDtcbiAgbWFyZ2luOiAwIGF1dG87XG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcblxuICBiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIC8qIGJhY2tmYWNlLXZpc2liaWxpdHk6IGhpZGRlbjtcbiAgICAgICB0cmFuc2l0aW9uOiBhbGwgMC42cyBjdWJpYy1iZXppZXIoMC44LCAtMC40LCAwLjIsIDEuNyk7XG4gICAgICAgei1pbmRleDogMzsqL1xufVxuXG5cbi5mcm9udF9fZmFjZS1waG90bzIge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIHRvcDogMTBweDtcbiAgaGVpZ2h0OiAxMjBweDtcbiAgd2lkdGg6IDEyMHB4O1xuICBtYXJnaW46IDAgYXV0bztcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIC8qIGJhY2tmYWNlLXZpc2liaWxpdHk6IGhpZGRlbjtcbiAgICAgICB0cmFuc2l0aW9uOiBhbGwgMC42cyBjdWJpYy1iZXppZXIoMC44LCAtMC40LCAwLjIsIDEuNyk7XG4gICAgICAgei1pbmRleDogMzsqL1xufVxuXG5cbi5mcm9udF9fZmFjZS1waG90bzMge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIHRvcDogMTBweDtcbiAgaGVpZ2h0OiAxMjBweDtcbiAgd2lkdGg6IDEyMHB4O1xuICBtYXJnaW46IDAgYXV0bztcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xuXG4gIGJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgLyogYmFja2ZhY2UtdmlzaWJpbGl0eTogaGlkZGVuO1xuICAgICAgIHRyYW5zaXRpb246IGFsbCAwLjZzIGN1YmljLWJlemllcigwLjgsIC0wLjQsIDAuMiwgMS43KTtcbiAgICAgICB6LWluZGV4OiAzOyovXG59XG5cbi5mcm9udF9fdGV4dCB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgdG9wOiAzNXB4O1xuICBtYXJnaW46IDAgYXV0bztcbiAgZm9udC1mYW1pbHk6IFwiTW9udHNlcnJhdFwiO1xuICBmb250LXNpemU6IDE4cHg7XG4gIGJhY2tmYWNlLXZpc2liaWxpdHk6IGhpZGRlbjtcbn1cblxuLmZyb250X190ZXh0LWhlYWRlciB7XG4gIGZvbnQtd2VpZ2h0OiA3MDA7XG4gIGZvbnQtZmFtaWx5OiBcIk9zd2FsZFwiO1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICBmb250LXNpemU6IDIwcHg7XG59XG5cbi5mcm9udF9fdGV4dC1wYXJhIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICB0b3A6IC01cHg7XG4gIGNvbG9yOiAjMDAwO1xuICBmb250LXNpemU6IDE0cHg7XG4gIGxldHRlci1zcGFjaW5nOiAwLjRweDtcbiAgZm9udC13ZWlnaHQ6IDQwMDtcbiAgZm9udC1mYW1pbHk6IFwiTW9udHNlcnJhdFwiLCBzYW5zLXNlcmlmO1xufVxuXG4uZnJvbnQtaWNvbnMge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIHRvcDogMDtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBtYXJnaW4tcmlnaHQ6IDZweDtcbiAgY29sb3I6IGdyYXk7XG59XG5cbi5mcm9udF9fdGV4dC1ob3ZlciB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgdG9wOiAxMHB4O1xuICBmb250LXNpemU6IDEwcHg7XG4gIGNvbG9yOiByZWQ7XG4gIGJhY2tmYWNlLXZpc2liaWxpdHk6IGhpZGRlbjtcblxuICBmb250LXdlaWdodDogNzAwO1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICBsZXR0ZXItc3BhY2luZzogLjRweDtcblxuICBib3JkZXI6IDJweCBzb2xpZCByZWQ7XG4gIHBhZGRpbmc6IDhweCAxNXB4O1xuICBib3JkZXItcmFkaXVzOiAzMHB4O1xuXG4gIGJhY2tncm91bmQ6IHJlZDtcbiAgY29sb3I6IHdoaXRlO1xufVxuXG5cblxuXG5cblxuIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqYmxvY2s1ICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbiAuYmxvY2s1IGltZ3tcbiAgbWFyZ2luLWxlZnQ6IDE5MHB4O1xuICBoZWlnaHQ6ODAlO1xuICB3aWR0aDo4MCVcbn1cbiAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKmJsb2NrNiAqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG4gXG4uYmxvY2s2e1xuICBwYWRkaW5nLXRvcDogMzBweDsgXG4gIHBhZGRpbmctbGVmdDogMTEwcHg7XG4gIG1hcmdpbi10b3A6IC01MDFweDsgXG4gIGZsb2F0OnJpZ2h0XG59XG4uYmxvY2s2IC50aGlyZC1ibG9jLWJvcmRlcntcbiAgYm9yZGVyLXJhZGl1czogMjBweDtcbiAgIGJvcmRlcjogMnB4IHNvbGlkICNDNUM1QzU7XG4gICAgbWFyZ2luLXRvcDogMjBweDsgXG4gICAgd2lkdGg6IDkwJTsgXG4gIHBhZGRpbmc6IDIwcHg7XG59XG4uYmxvY2s2IC50aGlyZC1ibG9jLWJvcmRlciBoM3tcbiAgZm9udC1zaXplOiAyNnB4O1xuICBmb250LXdlaWdodDogNzAwO1xufVxuLmJsb2NrNiAudGhpcmQtYmxvYy1ib3JkZXI6aG92ZXJ7XG5ib3JkZXI6IDJweCBzb2xpZCBibHVlO1xufVxuLmJsb2NrNiAudGhpcmQtYmxvYy1ib3JkZXI6aG92ZXIgaDN7XG4gIGNvbG9yOiBibHVlOyAgXG59XG5cblxuIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqYmxvY2s3ICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbiAubGFzdEJ7XG4gIHdpZHRoOiAxMDAlO1xuIFxufVxuLmJsb2NrN3tcbiAgcGFkZGluZy10b3A6IDEwMHB4XG59XG5cbi5jYXJkMXtcbiAgbWFyZ2luLXJpZ2h0OiA1MHB4O2hlaWdodDogMzQ1cHg7d2lkdGg6IDMxNXB4O1xufVxuLmNhcmQxIC5ib3gxe1xuICBib3gtc2hhZG93OiAwcHggMXB4IDE1cHggZ3JleTtcbiAgYm9yZGVyOiBub25lO1xuICBib3JkZXItcmFkaXVzOiA3MXB4IDE0cHggNzFweCAxNHB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmO1xuICBwYWRkaW5nOiA0MHB4O1xufVxuLmNhcmQye1xuICBtYXJnaW4tcmlnaHQ6IDUwcHg7XG4gIGhlaWdodDogMzQ1cHg7XG4gIHdpZHRoOiAzMTVweDtcbn1cbi5jYXJkMiAuYm94MntcbiAgYm94LXNoYWRvdzogMHB4IDFweCAxNXB4IGdyZXk7XG4gIGJvcmRlcjogbm9uZTtcbiAgYm9yZGVyLXJhZGl1czogNzFweCAxNHB4IDcxcHggMTRweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcbiAgcGFkZGluZzogNDBweDtcbn1cbi5jYXJkM3tcbiAgbWFyZ2luLXJpZ2h0OiA1MHB4O1xuICBoZWlnaHQ6IDM0NXB4O1xuICB3aWR0aDogMzE1cHg7XG59XG4uY2FyZDMgLmJveDN7XG4gIGJveC1zaGFkb3c6IDBweCAxcHggMTVweCBncmV5O1xuICBib3JkZXI6IG5vbmU7XG4gIGJvcmRlci1yYWRpdXM6IDcxcHggMTRweCA3MXB4IDE0cHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmZmZmY7XG4gIHBhZGRpbmc6IDQwcHg7XG59XG4uY2FyZDEgLmJveDEgaDR7XG4gIHBhZGRpbmc6IDY5cHggMCAwIDI2cHg7XG4gIGZvbnQtc2l6ZTogMjBweDtcbiAgY29sb3I6IzAwQkZGRlxufVxuLmNhcmQxIC5ib3gxIGg2e1xuICBwYWRkaW5nOiAwcHggMCAwcHggNTRweDtcbiAgZm9udC1zaXplOiAxMHB4O1xuICBjb2xvcjojYzdjN2M3XG59XG4uY2FyZDEgLmltYWdle1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIHRvcDogLTEzMnB4O1xuICBsZWZ0OiAtMTE2cHg7XG4gIGhlaWdodDogMTAwcHg7XG4gIHdpZHRoOiAxMDBweDtcbiAgbWFyZ2luOiAwIGF1dG87XG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xuICBvdmVyZmxvdzogaGlkZGVuO1xufVxuLmNhcmQxIC5pbWFnZSBpbWd7XG4gIHdpZHRoOiAxMjAlO1xuICBoZWlnaHQ6IDEyMCVcbn1cblxuLmNhcmQyIC5ib3gyIGg0e1xuICBwYWRkaW5nOiA1NHB4IDAgMCAyNnB4O1xuICBmb250LXNpemU6IDIwcHg7XG4gICBjb2xvcjojMDBCRkZGXG59XG4uY2FyZDIgLmJveDIgaDZ7XG4gIHBhZGRpbmc6IDBweCAwIDBweCA1NHB4O1xuICBmb250LXNpemU6IDEwcHg7XG4gIGNvbG9yOiNjN2M3Yzdcbn1cbi5jYXJkMiAuaW1hZ2V7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgIHRvcDogLTEzMnB4O1xuICBsZWZ0OiAtMTE2cHg7XG4gIGhlaWdodDogMTAwcHg7XG4gIHdpZHRoOiAxMDBweDtcbiAgbWFyZ2luOiAwIGF1dG87XG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgIGJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcbiAgIG92ZXJmbG93OiBoaWRkZW47XG59XG4uY2FyZDIgLmltYWdlIGltZ3tcbiAgd2lkdGg6IDEyMCU7XG4gIGhlaWdodDogMTIwJVxufVxuXG4uY2FyZDMgLmJveDMgaDR7XG4gIHBhZGRpbmc6IDQ0cHggMCAwIDI2cHg7XG4gIGZvbnQtc2l6ZTogMjBweDtcbiAgIGNvbG9yOiMwMEJGRkZcbn1cbi5jYXJkMyAuYm94MyBoNntcbiAgcGFkZGluZzogMHB4IDAgMHB4IDU0cHg7XG4gIGZvbnQtc2l6ZTogMTBweDtcbiAgY29sb3I6I2M3YzdjN1xufVxuLmNhcmQzIC5pbWFnZXtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICB0b3A6IC0xMzJweDtcbiAgbGVmdDogLTExNnB4O1xuICBoZWlnaHQ6IDEwMHB4O1xuICB3aWR0aDogMTAwcHg7XG4gIG1hcmdpbjogMCBhdXRvO1xuICBib3JkZXItcmFkaXVzOiA1MCU7XG4gIGJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbn1cbi5jYXJkMyAuaW1hZ2UgaW1ne1xuICB3aWR0aDogMTIwJTtcbiAgaGVpZ2h0OiAxMjAlXG59XG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG5cblxuaDF7XG5mb250LXdlaWdodDogYm9sZDtcbmNvbG9yOiAjMTExZDVlO1xubWFyZ2luOiA1MHB4IDAgNTBweCAwO1xufVxuXG5oNXtcbmNvbG9yOiAjMTExZDVlO1xuZm9udC1zaXplOiAxNXB4O1xufVxucHtcbmZvbnQtc2l6ZTogMTJweDtcbmNvbG9yOiAjMTExZDVlO1xuXG59XG4uZm9vdGVyIHB7XG4gIGZvbnQtc2l6ZTogMTJweDtcbmNvbG9yOiAjZmZmZmZmO1xufVxuXG5cbi5jb3B5cmlnaHR7XG4gIGJhY2tncm91bmQtY29sb3I6ICMwYzEzM2E7XG4gIGNvbG9yOiNmZmY7XG4gIGZvbnQtc2l6ZToxM3B4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbiAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqcmVzcG9uc2l2ZSBjc3MgKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cblxuXG4gIC8qIEN1c3RvbSwgaVBob25lIFJldGluYSAgKi9cbiAgICBAbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4td2lkdGggOiAzMjBweCkgYW5kIChtYXgtd2lkdGggOiA0ODBweCkgIHtcbiAgICAgXG4gIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKm5hdiBiYXIqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbiAgICAubmF2YmFyLWJyYW5kIHtcbiAgICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgICAgICBwYWRkaW5nLXRvcDogLjMxMjVyZW07XG4gICAgICAgIHBhZGRpbmctYm90dG9tOiAuMzEyNXJlbTtcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiAxcmVtO1xuICAgICAgICBmb250LXNpemU6IDEuMjVyZW07XG4gICAgICAgIGxpbmUtaGVpZ2h0OiBpbmhlcml0O1xuICAgICAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICAgICAgICBtYXJnaW4tbGVmdDogMDtcbiAgICAgICAgei1pbmRleDogNTtcbiAgfVxuICBcbiAgLmRyb3B7XG4gICAgbWluLXdpZHRoOiAtbW96LWF2YWlsYWJsZTtcbiAgICBtYXJnaW4tbGVmdDogMHB4O1xuICB9XG4gIFxuLm5hdmJhci1uYXZ7XG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgZGlzcGxheTogZmxleDtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBoZWlnaHQ6bWF4LWNvbnRlbnQ7XG4gIH1cbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnNpZGUgYmFyKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqLyAgXG4ucHJpbWFyeV9ib2R5ICAuZmxvYXRfYWN0aW9uc3tcbiAgcG9zaXRpb246IGZpeGVkO1xuYmFja2dyb3VuZDogcmVkO1xuYm9yZGVyLXJhZGl1czogMTBweDtcbmxlZnQ6IDA7XG50b3A6IDM1JTtcbnBhZGRpbmc6IDEwcHg7XG5kaXNwbGF5OiBmbGV4O1xuanVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG5hbGlnbi1pdGVtczogY2VudGVyO1xud2lkdGg6IDMlO1xuaGVpZ2h0OiA1JTtcbnotaW5kZXg6NTkwMDtcbn1cbi5wcmltYXJ5X2JvZHkgIC5mbG9hdF9hY3Rpb25zOjphZnRlcntcbiAgY29udGVudDogXCI+XCI7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgcG9zaXRpb246IGZpeGVkO1xuYmFja2dyb3VuZDogcmVkO1xuYm9yZGVyLXJhZGl1czogMTBweDtcbmxlZnQ6IDA7XG50b3A6IDM1JTtcbnBhZGRpbmc6IDBweDtcbmRpc3BsYXk6IGZsZXg7XG5qdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbmFsaWduLWl0ZW1zOiBjZW50ZXI7XG53aWR0aDogNSU7XG5oZWlnaHQ6IDUlO1xuZm9udC13ZWlnaHQ6IGJvbGQ7XG5mb250LXNpemU6IDIwcHg7XG5cbn1cbi5wcmltYXJ5X2JvZHkgIC5mbG9hdF9hY3Rpb25zOmhvdmVyOmFmdGVye1xuICBjb250ZW50OiBcIj5cIjtcbiAgY29sb3I6IHdoaXRlO1xuICBwb3NpdGlvbjogZml4ZWQ7XG5iYWNrZ3JvdW5kOiByZWQ7XG5ib3JkZXItcmFkaXVzOiAxMHB4O1xubGVmdDogMDtcbnRvcDogMzUlO1xucGFkZGluZzogMTBweDtcbmRpc3BsYXk6IG5vbmU7XG5qdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbmFsaWduLWl0ZW1zOiBjZW50ZXI7XG53aWR0aDogMyU7XG5oZWlnaHQ6IDUlO1xufVxuLnByaW1hcnlfYm9keSAuZmxvYXRfYWN0aW9ucyAuYWN0aW9uc19jb250ZW50IHtcbiAgcGFkZGluZzogMDtcbiAgbWFyZ2luOiAwIDAgLTMwcHggMDtcbiAgZGlzcGxheTogbm9uZTtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xufVxuLnByaW1hcnlfYm9keSAgLmZsb2F0X2FjdGlvbnM6aG92ZXJ7XG4gIHBvc2l0aW9uOiBmaXhlZDtcbiAgYmFja2dyb3VuZDogcmVkO1xuICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICBsZWZ0OiAwO1xuICB0b3A6IDM1JTtcbiAgcGFkZGluZzogMTBweDtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIHdpZHRoOiAyNSU7XG4gIGhlaWdodDogYXV0bztcbiAgei1pbmRleDogOTk5OTk7XG59XG5cbi5wcmltYXJ5X2JvZHkgLmZsb2F0X2FjdGlvbnM6aG92ZXIgLmFjdGlvbnNfY29udGVudCB7XG5wYWRkaW5nOiAwO1xubWFyZ2luOiAwIDAgLTMwcHggMDtcbmRpc3BsYXk6IGZsZXg7XG5mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuanVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xufVxuICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKmJsb2NrMCoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqLyAgXG4gIC5wcmltYXJ5X2JvZHkgLmhvbWVfY29udGFpbmVyIC5zZWN0aW9uX2hlYWRpbmd7XG4gICAgYm9yZGVyLWJvdHRvbS1yaWdodC1yYWRpdXM6MDtcbiAgfVxuICAucHJpbWFyeV9ib2R5IC5zZWFyY2hfYmxvYyAuc2VhcmNoLS1zaG93OmJlZm9yZSB7XG4gICAgYm9yZGVyOiA1cHggc29saWQgIzExMWQ1ZTtcbiAgICBib3JkZXItcmFkaXVzOiAyMHB4O1xuICAgIGhlaWdodDogNDBweDtcbiAgICB3aWR0aDogMzAwcHg7XG4gIH1cbiAgLnByaW1hcnlfYm9keSAuc2VhcmNoX2Jsb2MgLnNlYXJjaC0tc2hvdyAuc2VhcmNoX19pbnB1dCB7XG4gICAgb3BhY2l0eTogMTtcbiAgICB3aWR0aDogMzAwcHg7XG4gIH1cblxuICAucHJpbWFyeV9ib2R5IC5ob21lX2NvbnRhaW5lciAuc2VjdGlvbl9oZWFkaW5nIC5oZWFkaW5nX3dyYXBwZXIgLmltZ193cmFwcGVyIC5oZWFkaW5nX2ltZyB7XG4gICAgZm9udC1zaXplOiA0MHB4O1xuICAgIGZvbnQtd2VpZ2h0OiA4MDA7XG4gICAgY29sb3I6ICNmZmZmZmY7XG4gICAgd2lkdGg6IG1heC1jb250ZW50O1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBtYXJnaW4tdG9wOiBhdXRvO1xuICAgIGhlaWdodDogYXV0bztcbiB9XG5cblxuIC5wcmltYXJ5X2JvZHkgLnNlYXJjaF9ibG9jIHtcbiAgZGlzcGxheTogZmxleDtcbiAgd2lkdGg6IDMwMHB4O1xuXG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBtYXJnaW4tbGVmdDogdW5zZXQ7XG4gIHdpZHRoOiAtbW96LWF2YWlsYWJsZTtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuLnRyYW5zdGlvbntcbiAgZGlzcGxheTogZmxleDtcbiAgd2lkdGg6IC1tb3otYXZhaWxhYmxlO1xuICB3aWR0aDogZml0LWNvbnRlbnQ7XG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBtYXJnaW46IDA7XG59XG4uY2FycmUge1xuICB3aWR0aDogMjAwcHg7XG4gIGhlaWdodDogOTBweDtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIGJvcmRlci1yYWRpdXM6IDE4cHg7XG4gIG1hcmdpbi1sZWZ0OiBhdXRvO1xuICBtYXJnaW4tdG9wOiAyNTBweDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cbi5jb250ZW50e1xuICB3aWR0aDogZml0LWNvbnRlbnQ7XG59XG5cbi5wcmltYXJ5X2JvZHkgLmhvbWVfY29udGFpbmVyIC5zZWN0aW9uX2hlYWRpbmcgLmhlYWRpbmdfd3JhcHBlciAudGl0bGVfaGVhZGluZyB7XG4gIGZvbnQtc2l6ZTogMzBweDtcbiAgZm9udC13ZWlnaHQ6IDgwMDtcbiAgY29sb3I6ICNmZmZmZmY7XG4gIHdpZHRoOiAtbW96LWF2YWlsYWJsZTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG5cbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqYmxvY2syKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cblxuXG5cbi5ibG9jazIgLnRlc3QtYnRuMSB7XG4gIGJvcmRlci1yYWRpdXM6IDI1cHg7XG4gIG1hcmdpbi10b3A6IDMwcHg7XG4gIGJvcmRlcjogMDtcbn1cbi5ibG9jazIgLnRlc3QtYnRuMiB7XG5ib3JkZXItcmFkaXVzOiAyNXB4O1xuYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG5jb2xvcjogYmxhY2s7XG5ib3JkZXI6IDA7XG59XG4uYmxvY2syIC50ZXN0LWJ0bjMge1xuICBib3JkZXItcmFkaXVzOiAyNXB4O1xuICBib3JkZXI6IDA7XG59XG4uYmxvY2syOmhvdmVyIC50ZXN0LWJ0bjEge1xuICBib3JkZXI6IG5vbmU7XG4gIG1hcmdpbi10b3A6IDMwcHg7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgYmFja2dyb3VuZC1jb2xvcjpyZWQ7IFxufVxuLmJsb2NrMjpob3ZlciAudGVzdC1idG4yIHtcbiAgY29sb3I6IHdoaXRlO1xuICBib3JkZXI6IG5vbmU7XG4gIGJhY2tncm91bmQtY29sb3I6cmVkOyBcbn1cbi5ibG9jazI6aG92ZXIgLnRlc3QtYnRuMyB7XG4gIGJvcmRlcjogbm9uZTtcbiAgY29sb3I6IHdoaXRlO1xuICBiYWNrZ3JvdW5kLWNvbG9yOnJlZDsgXG59XG5cblxuXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKm5vdHJlIHN1Y2NlcyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuLmNoaWZmcmV7XG4gIGRpc3BsYXk6IGZsZXg7XG5mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuanVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG53aWR0aDogMTAwJTtcbmFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG5cbi5re1xuICBtYXJnaW46IGluaXRpYWw7XG59XG4uc3VjY2Vzc19pdGVte1xuICBtYXJnaW4tbGVmdDogMjBweDtcbiAgbGlzdC1zdHlsZTogbm9uZTtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG4gLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipibG9jayAzKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG4gLmJsb2NrMyBwe1xuICBwYWRkaW5nLXJpZ2h0OiAwO1xuICB3aWR0aDogMjUwcHg7XG4gIH1cblxuICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipibG9jayA0KioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cblxuLmJsb2NrNHtcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKC4uLy4uL2Fzc2V0cy9jcG5pbWFnZXMvaG9tZS9zaW4ucG5nKTtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjo2NThweCAyMzRweDtcbiAgbWFyZ2luLXRvcDoxMDBweDtcbiAgfVxuICAuYmxvY2s0IC5ibG9jazF7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xuICB9XG4gIC5ibG9jazQgLmJsb2NrMSBwe1xuICBwYWRkaW5nLXJpZ2h0OiAwO1xuICB9XG4gIFxuICAgLm9uZXtcbiAgbWFyZ2luOiA0MHB4IDAgMCAwIDtcbiAgfVxuICAgLnR3b3tcbiAgbWFyZ2luOiAwO1xuICB9XG4gICAudGhyZWV7XG4gIG1hcmdpbjogMDtcbiAgfVxuICAub3V0ZXItZGl2e1xuICAgIGRpc3BsYXk6IGNvbnRlbnRzO1xuICB9XG4gIFxuICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqYmxvY2s1KioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbiAgLmJsb2NrNXtcbiAgbWF4LXdpZHRoOiAxMDAlO1xuICB3aWR0aDogMTAwJTtcbiAgfVxuICAuYmxvY2s1IGltZ3tcbiAgbWFyZ2luLWxlZnQ6IDA7XG4gIGhlaWdodDoxMDAlO1xuICB3aWR0aDoxMDAlXG4gIH1cbiAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqYmxvY2s2KioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG4gIC5ibG9jazZ7XG4gIHBhZGRpbmctdG9wOiAzMHB4OyBcbiAgcGFkZGluZy1sZWZ0OjA7XG4gIG1hcmdpbi10b3A6IDA7IFxuICBtYXgtd2lkdGg6IDEwMCU7XG4gIGRpc3BsYXk6IGNvbnRlbnRzO1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgfVxuICBcbiAgLmJsb2NrNiAudGhpcmQtYmxvYy1ib3JkZXJ7XG4gICAgbWFyZ2luOiAyMHB4IGF1dG8gMCBhdXRvO1xuICB9XG4gIFxuICBcbiAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKmJsb2NrNyAqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG4gLmJsb2NrN3tcbiAgcGFkZGluZy10b3A6IDEwcHg7XG59XG5cbiAgICAuY2FyZDF7XG4gICAgICBtYXJnaW4tcmlnaHQ6IDBweDtcbiAgICAgIGhlaWdodDogMzQ1cHg7XG4gICAgICB3aWR0aDogMzE1cHg7XG4gICAgICB9XG4gICAgICAuY2FyZDEgLmJveDF7XG4gICAgICBib3gtc2hhZG93OiAwcHggMXB4IDE1cHggZ3JleTtcbiAgICAgIGJvcmRlcjogbm9uZTtcbiAgICAgIGJvcmRlci1yYWRpdXM6IDcxcHggMTRweCA3MXB4IDE0cHg7XG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmO1xuICAgICAgcGFkZGluZzogNDBweDtcbiAgICAgIH1cbiAgICAgIC5jYXJkMntcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiAwcHg7XG4gICAgICAgIGhlaWdodDogMzQ1cHg7XG4gICAgICAgIHdpZHRoOiAzMTVweDtcbiAgICAgIH1cbiAgICAgIC5jYXJkMiAuYm94MntcbiAgICAgICAgYm94LXNoYWRvdzogMHB4IDFweCAxNXB4IGdyZXk7XG4gICAgICAgIGJvcmRlcjogbm9uZTtcbiAgICAgICAgYm9yZGVyLXJhZGl1czogNzFweCAxNHB4IDcxcHggMTRweDtcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcbiAgICAgICAgcGFkZGluZzogNDBweDtcbiAgICAgICAgfVxuICAgICAgLmNhcmQze1xuICAgICAgICBtYXJnaW4tcmlnaHQ6IDBweDtcbiAgICAgICAgaGVpZ2h0OiAzNDVweDtcbiAgICAgICAgd2lkdGg6IDMxNXB4O1xuICAgICAgfVxuICAgICAgLmNhcmQzIC5ib3gze1xuICAgICAgICBib3gtc2hhZG93OiAwcHggMXB4IDE1cHggZ3JleTtcbiAgICAgICAgYm9yZGVyOiBub25lO1xuICAgICAgICBib3JkZXItcmFkaXVzOiA3MXB4IDE0cHggNzFweCAxNHB4O1xuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmO1xuICAgICAgICBwYWRkaW5nOiA0MHB4O1xuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgLmxhc3RCe1xuICAgICAgd2lkdGg6IDgwJTtcbiAgICAgIG1hcmdpbjogNTBweDtcbiAgICAgIH1cblxuXG4gICAgICAudGV4dC1jZW50ZXJ7XG4gICAgICBtYXJnaW4tYm90dG9tOiA1MHB4O1xuICAgICAgfVxuXG4gICAgfVxuXG4gLyogRXh0cmEgU21hbGwgRGV2aWNlcywgUGhvbmVzICovXG4gICAgQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoIDogNDgwcHgpIGFuZCAobWF4LXdpZHRoIDogNzY4cHgpICB7XG4gICAgICAgICAgICBcbiAgICBcbiAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqbmF2IGJhcioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuICAubmF2YmFyLWJyYW5kIHtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgcGFkZGluZy10b3A6IC4zMTI1cmVtO1xuICAgIHBhZGRpbmctYm90dG9tOiAuMzEyNXJlbTtcbiAgICBtYXJnaW4tcmlnaHQ6IDFyZW07XG4gICAgZm9udC1zaXplOiAxLjI1cmVtO1xuICAgIGxpbmUtaGVpZ2h0OiBpbmhlcml0O1xuICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gICAgbWFyZ2luLWxlZnQ6IDA7XG4gICAgei1pbmRleDogNTtcbn1cbi5uYXZfaW1nIHtcbiAgd2lkdGg6IDgwcHg7XG4gIG1hcmdpbi1ib3R0b206IDEwcHg7XG4gIG1hcmdpbi1sZWZ0OiAzOHB4O1xufVxuXG4uZHJvcHtcbiAgbWluLXdpZHRoOiAtbW96LWF2YWlsYWJsZTtcbiAgbWFyZ2luLWxlZnQ6IDBweDtcbn1cbi5uYXZiYXItbmF2e1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgaGVpZ2h0Om1heC1jb250ZW50O1xuICB9XG5cbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnNpZGUgYmFyKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqLyAgXG4ucHJpbWFyeV9ib2R5ICAuZmxvYXRfYWN0aW9uc3tcbnBvc2l0aW9uOiBmaXhlZDtcbmJhY2tncm91bmQ6IHJlZDtcbmJvcmRlci1yYWRpdXM6IDEwcHg7XG5sZWZ0OiAwO1xudG9wOiAzNSU7XG5wYWRkaW5nOiAxMHB4O1xuZGlzcGxheTogZmxleDtcbmp1c3RpZnktY29udGVudDogY2VudGVyO1xuYWxpZ24taXRlbXM6IGNlbnRlcjtcbndpZHRoOiAzJTtcbmhlaWdodDogNSU7XG56LWluZGV4OjU5MDA7XG5cbn1cbi5wcmltYXJ5X2JvZHkgIC5mbG9hdF9hY3Rpb25zOjphZnRlcntcbmNvbnRlbnQ6IFwiPlwiO1xuY29sb3I6IHdoaXRlO1xucG9zaXRpb246IGZpeGVkO1xuYmFja2dyb3VuZDogcmVkO1xuYm9yZGVyLXJhZGl1czogMTBweDtcbmxlZnQ6IDA7XG50b3A6IDM1JTtcbnBhZGRpbmc6IDBweDtcbmRpc3BsYXk6IGZsZXg7XG5qdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbmFsaWduLWl0ZW1zOiBjZW50ZXI7XG53aWR0aDogNSU7XG5oZWlnaHQ6IDUlO1xuZm9udC13ZWlnaHQ6IGJvbGQ7XG5mb250LXNpemU6IDIwcHg7XG5cbn1cbi5wcmltYXJ5X2JvZHkgIC5mbG9hdF9hY3Rpb25zOmhvdmVyOmFmdGVye1xuY29udGVudDogXCI+XCI7XG5jb2xvcjogd2hpdGU7XG5wb3NpdGlvbjogZml4ZWQ7XG5iYWNrZ3JvdW5kOiByZWQ7XG5ib3JkZXItcmFkaXVzOiAxMHB4O1xubGVmdDogMDtcbnRvcDogMzUlO1xucGFkZGluZzogMTBweDtcbmRpc3BsYXk6IG5vbmU7XG5qdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbmFsaWduLWl0ZW1zOiBjZW50ZXI7XG53aWR0aDogMyU7XG5oZWlnaHQ6IDUlO1xufVxuLnByaW1hcnlfYm9keSAuZmxvYXRfYWN0aW9ucyAuYWN0aW9uc19jb250ZW50IHtcbnBhZGRpbmc6IDA7XG5tYXJnaW46IDAgMCAtMzBweCAwO1xuZGlzcGxheTogbm9uZTtcbmZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG5qdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG59XG4ucHJpbWFyeV9ib2R5ICAuZmxvYXRfYWN0aW9uczpob3ZlcntcbnBvc2l0aW9uOiBmaXhlZDtcbmJhY2tncm91bmQ6IHJlZDtcbmJvcmRlci1yYWRpdXM6IDEwcHg7XG5sZWZ0OiAwO1xudG9wOiAzNSU7XG5wYWRkaW5nOiAxMHB4O1xuZGlzcGxheTogZmxleDtcbmp1c3RpZnktY29udGVudDogY2VudGVyO1xuYWxpZ24taXRlbXM6IGNlbnRlcjtcbndpZHRoOiAxOCU7XG5oZWlnaHQ6IGF1dG87XG56LWluZGV4OiA5OTk5OTtcbn1cblxuLnByaW1hcnlfYm9keSAuZmxvYXRfYWN0aW9uczpob3ZlciAuYWN0aW9uc19jb250ZW50IHtcbnBhZGRpbmc6IDA7XG5tYXJnaW46IDAgMCAtMzBweCAwO1xuZGlzcGxheTogZmxleDtcbmZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG5qdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG59XG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKmJsb2NrMCoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqLyAgXG4ucHJpbWFyeV9ib2R5IC5ob21lX2NvbnRhaW5lciAuc2VjdGlvbl9oZWFkaW5ne1xuYm9yZGVyLWJvdHRvbS1yaWdodC1yYWRpdXM6MDtcbn1cbi5wcmltYXJ5X2JvZHkgLnNlYXJjaF9ibG9jIC5zZWFyY2gtLXNob3c6YmVmb3JlIHtcbmJvcmRlcjogNXB4IHNvbGlkICMxMTFkNWU7XG5ib3JkZXItcmFkaXVzOiAyMHB4O1xuaGVpZ2h0OiA0MHB4O1xud2lkdGg6IDMwMHB4O1xufVxuLnByaW1hcnlfYm9keSAuc2VhcmNoX2Jsb2MgLnNlYXJjaC0tc2hvdyAuc2VhcmNoX19pbnB1dCB7XG5vcGFjaXR5OiAxO1xud2lkdGg6IDMwMHB4O1xufVxuXG4ucHJpbWFyeV9ib2R5IC5ob21lX2NvbnRhaW5lciAuc2VjdGlvbl9oZWFkaW5nIC5oZWFkaW5nX3dyYXBwZXIgLmltZ193cmFwcGVyIHtcbiAgd2lkdGg6IGF1dG87XG59XG4ucHJpbWFyeV9ib2R5IC5ob21lX2NvbnRhaW5lciAuc2VjdGlvbl9oZWFkaW5nIC5oZWFkaW5nX3dyYXBwZXIgLmltZ193cmFwcGVyIC5oZWFkaW5nX2ltZyB7XG4gIGhlaWdodDogMTAwJTtcbiAgbWF4LWhlaWdodDogNTg1cHg7XG4gIG1hcmdpbi10b3A6IDA7XG4gIHdpZHRoOiA3NTBweDtcbn1cbi5wcmltYXJ5X2JvZHkgLnNlYXJjaF9ibG9jIHtcbmRpc3BsYXk6IGZsZXg7XG5mbGV4LWRpcmVjdGlvbjogcm93O1xuanVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG5tYXJnaW4tbGVmdDogdW5zZXQ7XG53aWR0aDogLW1vei1hdmFpbGFibGU7XG5hbGlnbi1pdGVtczogY2VudGVyO1xudGV4dC1hbGlnbjogY2VudGVyO1xufVxuLnRyYW5zdGlvbntcbmRpc3BsYXk6IGZsZXg7XG53aWR0aDogLW1vei1hdmFpbGFibGU7XG5mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuanVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG5hbGlnbi1pdGVtczogY2VudGVyO1xubWFyZ2luOiAwO1xufVxuLmNhcnJlIHtcbndpZHRoOiAyMDBweDtcbmhlaWdodDogOTBweDtcbmJhY2tncm91bmQ6IHdoaXRlO1xuYm9yZGVyLXJhZGl1czogMThweDtcbm1hcmdpbi1sZWZ0OiBhdXRvO1xubWFyZ2luLXRvcDogMjUwcHg7XG5hbGlnbi1pdGVtczogY2VudGVyO1xufVxuLmNvbnRlbnR7XG53aWR0aDogZml0LWNvbnRlbnQ7XG59XG5cbi5wcmltYXJ5X2JvZHkgLmhvbWVfY29udGFpbmVyIC5zZWN0aW9uX2hlYWRpbmcgLmhlYWRpbmdfd3JhcHBlciAudGl0bGVfaGVhZGluZyB7XG5mb250LXNpemU6IDMwcHg7XG5mb250LXdlaWdodDogODAwO1xuY29sb3I6ICNmZmZmZmY7XG53aWR0aDogLW1vei1hdmFpbGFibGU7XG50ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqYmxvY2syKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cblxuXG5cbi5ibG9jazIgLnRlc3QtYnRuMSB7XG5ib3JkZXItcmFkaXVzOiAyNXB4O1xubWFyZ2luLXRvcDogMzBweDtcbmJvcmRlcjogMDtcbn1cbi5ibG9jazIgLnRlc3QtYnRuMiB7XG5ib3JkZXItcmFkaXVzOiAyNXB4O1xuYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG5jb2xvcjogYmxhY2s7XG5ib3JkZXI6IDA7XG59XG4uYmxvY2syIC50ZXN0LWJ0bjMge1xuYm9yZGVyLXJhZGl1czogMjVweDtcbmJvcmRlcjogMDtcbn1cbi5ibG9jazI6aG92ZXIgLnRlc3QtYnRuMSB7XG5ib3JkZXI6IG5vbmU7XG5tYXJnaW4tdG9wOiAzMHB4O1xuY29sb3I6IHdoaXRlO1xuYmFja2dyb3VuZC1jb2xvcjpyZWQ7IFxufVxuLmJsb2NrMjpob3ZlciAudGVzdC1idG4yIHtcbmNvbG9yOiB3aGl0ZTtcbmJvcmRlcjogbm9uZTtcbmJhY2tncm91bmQtY29sb3I6cmVkOyBcbn1cbi5ibG9jazI6aG92ZXIgLnRlc3QtYnRuMyB7XG5ib3JkZXI6IG5vbmU7XG5jb2xvcjogd2hpdGU7XG5iYWNrZ3JvdW5kLWNvbG9yOnJlZDsgXG59XG5cblxuXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKm5vdHJlIHN1Y2NlcyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuLmNoaWZmcmV7XG5kaXNwbGF5OiBmbGV4O1xuZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbmp1c3RpZnktY29udGVudDogY2VudGVyO1xud2lkdGg6IDEwMCU7XG5hbGlnbi1pdGVtczogY2VudGVyO1xufVxuXG4ua3tcbm1hcmdpbjogaW5pdGlhbDtcbn1cbi5zdWNjZXNzX2l0ZW17XG5tYXJnaW4tbGVmdDogMjBweDtcbmxpc3Qtc3R5bGU6IG5vbmU7XG5kaXNwbGF5OiBmbGV4O1xuZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbmp1c3RpZnktY29udGVudDogY2VudGVyO1xuYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqYmxvY2sgMyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuLmJsb2NrMyBwe1xucGFkZGluZy1yaWdodDogMDtcbndpZHRoOiAyNTBweDtcbn1cblxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqYmxvY2sgNCoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG5cbi5ibG9jazR7XG5iYWNrZ3JvdW5kLWltYWdlOiB1cmwoLi4vLi4vYXNzZXRzL2NwbmltYWdlcy9ob21lL3Npbi5wbmcpO1xuYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbmJhY2tncm91bmQtcG9zaXRpb246MHB4IDU3NnB4O1xubWFyZ2luLXRvcDoxMDBweDtcbn1cbi5ibG9jazQgLmJsb2NrMXtcbmRpc3BsYXk6IGZsZXg7XG5mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XG59XG4uYmxvY2s0IC5ibG9jazEgcHtcbnBhZGRpbmctcmlnaHQ6IDA7XG59XG5cbi5vbmV7XG5tYXJnaW46IDQwcHggMCAwIDAgO1xufVxuLnR3b3tcbm1hcmdpbjogMDtcbn1cbi50aHJlZXtcbm1hcmdpbjogMDtcbn1cbi5vdXRlci1kaXZ7XG5kaXNwbGF5OiBjb250ZW50cztcbn1cblxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKmJsb2NrNSoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG4uYmxvY2s1e1xubWF4LXdpZHRoOiAxMDAlO1xud2lkdGg6IDEwMCU7XG59XG4uYmxvY2s1IGltZ3tcbm1hcmdpbi1sZWZ0OiAwO1xuaGVpZ2h0OjEwMCU7XG53aWR0aDoxMDAlXG59XG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipibG9jazYqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbi5ibG9jazZ7XG5wYWRkaW5nLXRvcDogMzBweDsgXG5wYWRkaW5nLWxlZnQ6MDtcbm1hcmdpbi10b3A6IDA7IFxubWF4LXdpZHRoOiAxMDAlO1xuZGlzcGxheTogY29udGVudHM7XG5mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuanVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG5hbGlnbi1pdGVtczogY2VudGVyO1xufVxuXG4uYmxvY2s2IC50aGlyZC1ibG9jLWJvcmRlcntcbm1hcmdpbjogMjBweCBhdXRvIDAgYXV0bztcbn1cblxuXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKmJsb2NrNyAqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG4uYmxvY2s3e1xucGFkZGluZy10b3A6IDEwcHg7XG59XG5cbi5jYXJkMXtcbiAgbWFyZ2luLXJpZ2h0OiAwcHg7XG4gIGhlaWdodDogMzQ1cHg7XG4gIHdpZHRoOiAzMTVweDtcbiAgfVxuICAuY2FyZDEgLmJveDF7XG4gIGJveC1zaGFkb3c6IDBweCAxcHggMTVweCBncmV5O1xuICBib3JkZXI6IG5vbmU7XG4gIGJvcmRlci1yYWRpdXM6IDcxcHggMTRweCA3MXB4IDE0cHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmZmZmY7XG4gIHBhZGRpbmc6IDQwcHg7XG4gIH1cbiAgLmNhcmQye1xuICAgIG1hcmdpbi1yaWdodDogMHB4O1xuICAgIGhlaWdodDogMzQ1cHg7XG4gICAgd2lkdGg6IDMxNXB4O1xuICB9XG4gIC5jYXJkMiAuYm94MntcbiAgICBib3gtc2hhZG93OiAwcHggMXB4IDE1cHggZ3JleTtcbiAgICBib3JkZXI6IG5vbmU7XG4gICAgYm9yZGVyLXJhZGl1czogNzFweCAxNHB4IDcxcHggMTRweDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmO1xuICAgIHBhZGRpbmc6IDQwcHg7XG4gICAgfVxuICAuY2FyZDN7XG4gICAgbWFyZ2luLXJpZ2h0OiAwcHg7XG4gICAgaGVpZ2h0OiAzNDVweDtcbiAgICB3aWR0aDogMzE1cHg7XG4gIH1cbiAgLmNhcmQzIC5ib3gze1xuICAgIGJveC1zaGFkb3c6IDBweCAxcHggMTVweCBncmV5O1xuICAgIGJvcmRlcjogbm9uZTtcbiAgICBib3JkZXItcmFkaXVzOiA3MXB4IDE0cHggNzFweCAxNHB4O1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmZmZmY7XG4gICAgcGFkZGluZzogNDBweDtcbiAgICB9XG4gICAgXG4gIC5sYXN0QntcbiAgICB3aWR0aDogMTAwJTtcbiAgICBtYXJnaW46IG5vbmU7XG4gIH1cblxuXG4gIC50ZXh0LWNlbnRlcntcbiAgbWFyZ2luLWJvdHRvbTogNTBweDtcbiAgfVxuXG5cbiAgICB9XG5cblxuIC8qIFNtYWxsIERldmljZXMsIFRhYmxldHMqL1xuICAgIEBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi13aWR0aCA6IDc2OHB4KSBhbmQgKG1heC13aWR0aCA6IDk5MnB4KSAge1xuICAgIFxuICAgICAgIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKm5hdiBiYXIqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbiAgLm5hdmJhci1icmFuZCB7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIHBhZGRpbmctdG9wOiAuMzEyNXJlbTtcbiAgICBwYWRkaW5nLWJvdHRvbTogLjMxMjVyZW07XG4gICAgbWFyZ2luLXJpZ2h0OiAxcmVtO1xuICAgIGZvbnQtc2l6ZTogMS4yNXJlbTtcbiAgICBsaW5lLWhlaWdodDogaW5oZXJpdDtcbiAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICAgIG1hcmdpbi1sZWZ0OiAwO1xuICAgIHotaW5kZXg6IDU7XG59XG4ubmF2X2ltZyB7XG4gIHdpZHRoOiA4MHB4O1xuICBtYXJnaW4tYm90dG9tOiAxMHB4O1xuICBtYXJnaW4tbGVmdDogMzhweDtcbn1cbi5uYXZiYXItbmF2e1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgaGVpZ2h0Om1heC1jb250ZW50O1xuXG4gIH1cbi5kcm9we1xuICBtaW4td2lkdGg6IC1tb3otYXZhaWxhYmxlO1xuICBtYXJnaW4tbGVmdDogMHB4O1xufVxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqc2lkZSBiYXIqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovICBcbi5wcmltYXJ5X2JvZHkgIC5mbG9hdF9hY3Rpb25ze1xucG9zaXRpb246IGZpeGVkO1xuYmFja2dyb3VuZDogcmVkO1xuYm9yZGVyLXJhZGl1czogMTBweDtcbmxlZnQ6IDA7XG50b3A6IDM1JTtcbnBhZGRpbmc6IDEwcHg7XG5kaXNwbGF5OiBmbGV4O1xuanVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG5hbGlnbi1pdGVtczogY2VudGVyO1xud2lkdGg6IDMlO1xuaGVpZ2h0OiA1JTtcbnotaW5kZXg6NTkwMDtcbn1cbi5wcmltYXJ5X2JvZHkgIC5mbG9hdF9hY3Rpb25zOjphZnRlcntcbmNvbnRlbnQ6IFwiPlwiO1xuY29sb3I6IHdoaXRlO1xucG9zaXRpb246IGZpeGVkO1xuYmFja2dyb3VuZDogcmVkO1xuYm9yZGVyLXJhZGl1czogMTBweDtcbmxlZnQ6IDA7XG50b3A6IDM1JTtcbnBhZGRpbmc6IDBweDtcbmRpc3BsYXk6IGZsZXg7XG5qdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbmFsaWduLWl0ZW1zOiBjZW50ZXI7XG53aWR0aDogNSU7XG5oZWlnaHQ6IDUlO1xuZm9udC13ZWlnaHQ6IGJvbGQ7XG5mb250LXNpemU6IDIwcHg7XG5cbn1cbi5wcmltYXJ5X2JvZHkgIC5mbG9hdF9hY3Rpb25zOmhvdmVyOmFmdGVye1xuY29udGVudDogXCI+XCI7XG5jb2xvcjogd2hpdGU7XG5wb3NpdGlvbjogZml4ZWQ7XG5iYWNrZ3JvdW5kOiByZWQ7XG5ib3JkZXItcmFkaXVzOiAxMHB4O1xubGVmdDogMDtcbnRvcDogMzUlO1xucGFkZGluZzogMTBweDtcbmRpc3BsYXk6IG5vbmU7XG5qdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbmFsaWduLWl0ZW1zOiBjZW50ZXI7XG53aWR0aDogMyU7XG5oZWlnaHQ6IDUlO1xufVxuLnByaW1hcnlfYm9keSAuZmxvYXRfYWN0aW9ucyAuYWN0aW9uc19jb250ZW50IHtcbnBhZGRpbmc6IDA7XG5tYXJnaW46IDAgMCAtMzBweCAwO1xuZGlzcGxheTogbm9uZTtcbmZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG5qdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG59XG4ucHJpbWFyeV9ib2R5ICAuZmxvYXRfYWN0aW9uczpob3ZlcntcbnBvc2l0aW9uOiBmaXhlZDtcbmJhY2tncm91bmQ6IHJlZDtcbmJvcmRlci1yYWRpdXM6IDEwcHg7XG5sZWZ0OiAwO1xudG9wOiAzNSU7XG5wYWRkaW5nOiAxMHB4O1xuZGlzcGxheTogZmxleDtcbmp1c3RpZnktY29udGVudDogY2VudGVyO1xuYWxpZ24taXRlbXM6IGNlbnRlcjtcbndpZHRoOiAxMiU7XG5oZWlnaHQ6YXV0bztcbnotaW5kZXg6IDk5OTk5O1xufVxuXG4ucHJpbWFyeV9ib2R5IC5mbG9hdF9hY3Rpb25zOmhvdmVyIC5hY3Rpb25zX2NvbnRlbnQge1xucGFkZGluZzogMDtcbm1hcmdpbjogMCAwIC0zMHB4IDA7XG5kaXNwbGF5OiBmbGV4O1xuZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbmp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2Vlbjtcbn1cbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqYmxvY2swKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovICBcbi5ibG9jazB7XG4gIGZsZXg6IDAgMCBhdXRvO1xud2lkdGg6IC1tb3otYXZhaWxhYmxlO1xufVxuLnByaW1hcnlfYm9keSAuaG9tZV9jb250YWluZXIgLnNlY3Rpb25faGVhZGluZ3tcbmJvcmRlci1ib3R0b20tcmlnaHQtcmFkaXVzOjA7XG59XG4ucHJpbWFyeV9ib2R5IC5zZWFyY2hfYmxvYyAuc2VhcmNoLS1zaG93OmJlZm9yZSB7XG5ib3JkZXI6IDVweCBzb2xpZCAjMTExZDVlO1xuYm9yZGVyLXJhZGl1czogMjBweDtcbmhlaWdodDogNDBweDtcbndpZHRoOiAzMDBweDtcbn1cbi5wcmltYXJ5X2JvZHkgLnNlYXJjaF9ibG9jIC5zZWFyY2gtLXNob3cgLnNlYXJjaF9faW5wdXQge1xub3BhY2l0eTogMTtcbndpZHRoOiAzMDBweDtcbn1cblxuLnByaW1hcnlfYm9keSAuaG9tZV9jb250YWluZXIgLnNlY3Rpb25faGVhZGluZyAuaGVhZGluZ193cmFwcGVyIC5pbWdfd3JhcHBlciB7XG4gIHdpZHRoOiBhdXRvO1xufVxuLnByaW1hcnlfYm9keSAuaG9tZV9jb250YWluZXIgLnNlY3Rpb25faGVhZGluZyAuaGVhZGluZ193cmFwcGVyIC5pbWdfd3JhcHBlciAuaGVhZGluZ19pbWcge1xuICBoZWlnaHQ6IDEwMCU7XG4gIG1heC1oZWlnaHQ6IDU4NXB4O1xuICBtYXJnaW4tdG9wOiAwO1xuICB3aWR0aDogNzUwcHg7XG59XG4ucHJpbWFyeV9ib2R5IC5zZWFyY2hfYmxvYyB7XG5kaXNwbGF5OiBmbGV4O1xuZmxleC1kaXJlY3Rpb246IHJvdztcbmp1c3RpZnktY29udGVudDogY2VudGVyO1xubWFyZ2luLWxlZnQ6IHVuc2V0O1xud2lkdGg6IC1tb3otYXZhaWxhYmxlO1xuYWxpZ24taXRlbXM6IGNlbnRlcjtcbnRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi50cmFuc3Rpb257XG5kaXNwbGF5OiBmbGV4O1xud2lkdGg6IC1tb3otYXZhaWxhYmxlO1xuZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbmp1c3RpZnktY29udGVudDogY2VudGVyO1xuYWxpZ24taXRlbXM6IGNlbnRlcjtcbm1hcmdpbjogMDtcbn1cbi5jYXJyZSB7XG53aWR0aDogMjAwcHg7XG5oZWlnaHQ6IDkwcHg7XG5iYWNrZ3JvdW5kOiB3aGl0ZTtcbmJvcmRlci1yYWRpdXM6IDE4cHg7XG5tYXJnaW4tbGVmdDogYXV0bztcbm1hcmdpbi10b3A6IDI1MHB4O1xuYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cbi5jb250ZW50e1xud2lkdGg6IGZpdC1jb250ZW50O1xufVxuXG4ucHJpbWFyeV9ib2R5IC5ob21lX2NvbnRhaW5lciAuc2VjdGlvbl9oZWFkaW5nIC5oZWFkaW5nX3dyYXBwZXIgLnRpdGxlX2hlYWRpbmcge1xuZm9udC1zaXplOiAzMHB4O1xuZm9udC13ZWlnaHQ6IDgwMDtcbmNvbG9yOiAjZmZmZmZmO1xud2lkdGg6IC1tb3otYXZhaWxhYmxlO1xudGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKmJsb2NrMioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG5cbi5wcmltYXJ5X2JvZHkgLmhvbWVfY29udGFpbmVyIC5jYXJkLWJsb2cgLmNhcmRfd3JhcHBlciAucm93IC5jYXJkLWJvZHkge1xuICBtYXJnaW4tYm90dG9tOiAxNHB4O1xuICBtYXJnaW4tdG9wOiAtMjJweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuIH1cblxuLmJsb2NrMiAudGVzdC1idG4xIHtcbmJvcmRlci1yYWRpdXM6IDI1cHg7XG5tYXJnaW4tdG9wOiAwcHg7XG5ib3JkZXI6IDA7XG59XG4uYmxvY2syIC50ZXN0LWJ0bjIge1xuYm9yZGVyLXJhZGl1czogMjVweDtcbmJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xuY29sb3I6IGJsYWNrO1xuYm9yZGVyOiAwO1xufVxuLmJsb2NrMiAudGVzdC1idG4zIHtcbmJvcmRlci1yYWRpdXM6IDI1cHg7XG5ib3JkZXI6IDA7XG59XG4uYmxvY2syOmhvdmVyIC50ZXN0LWJ0bjEge1xuYm9yZGVyOiBub25lO1xubWFyZ2luLXRvcDogMzBweDtcbmNvbG9yOiB3aGl0ZTtcbmJhY2tncm91bmQtY29sb3I6cmVkOyBcbn1cbi5ibG9jazI6aG92ZXIgLnRlc3QtYnRuMiB7XG5jb2xvcjogd2hpdGU7XG5ib3JkZXI6IG5vbmU7XG5iYWNrZ3JvdW5kLWNvbG9yOnJlZDsgXG59XG4uYmxvY2syOmhvdmVyIC50ZXN0LWJ0bjMge1xuYm9yZGVyOiBub25lO1xuY29sb3I6IHdoaXRlO1xuYmFja2dyb3VuZC1jb2xvcjpyZWQ7IFxufVxuXG5cblxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipub3RyZSBzdWNjZXMqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbi5jaGlmZnJle1xuZGlzcGxheTogZmxleDtcbndpZHRoOiAxMDAlO1xufVxuXG4ua3tcbm1hcmdpbjogaW5pdGlhbDtcbn1cbi5zdWNjZXNzX2l0ZW17XG5tYXJnaW4tbGVmdDogMjBweDtcbmxpc3Qtc3R5bGU6IG5vbmU7XG5kaXNwbGF5OiBmbGV4O1xuZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbmp1c3RpZnktY29udGVudDogY2VudGVyO1xuYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cbi5vdXJfc3VjY2VzcyAuc3VjY2Vzc193cmFwcGVyIC50aXRsZXtcbiAgd2lkdGg6IC1tb3otYXZhaWxhYmxlO1xufVxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipibG9jayAzKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG4uYmxvY2szIHB7XG5wYWRkaW5nLXJpZ2h0OiAwO1xud2lkdGg6IC1tb3otYXZhaWxhYmxlO1xufVxuXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipibG9jayA0KioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cblxuLmJsb2NrNHtcbmJhY2tncm91bmQtaW1hZ2U6IHVybCguLi8uLi9hc3NldHMvY3BuaW1hZ2VzL2hvbWUvc2luLnBuZyk7XG5iYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuYmFja2dyb3VuZC1wb3NpdGlvbjo2NThweCAyMzRweDtcbm1hcmdpbi10b3A6MHB4O1xufVxuLmJsb2NrNCAuYmxvY2sxe1xuZGlzcGxheTogZmxleDtcbmZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG5hbGlnbi1pdGVtczogZmxleC1zdGFydDtcbn1cbi5ibG9jazQgLmJsb2NrMSBwe1xucGFkZGluZy1yaWdodDogMHB4O1xufVxuXG4ub25le1xubWFyZ2luOiA0MHB4IDAgMCAwIDtcbn1cbi50d297XG5tYXJnaW46IDA7XG59XG4udGhyZWV7XG5tYXJnaW46IDA7XG59XG4ub3V0ZXItZGl2e1xuZGlzcGxheTogY29udGVudHM7XG59XG5cbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipibG9jazUqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuLmJsb2NrNXtcbm1heC13aWR0aDogMTAwJTtcbndpZHRoOiAxMDAlO1xufVxuLmJsb2NrNSBpbWd7XG5tYXJnaW4tbGVmdDogMDtcbmhlaWdodDoxMDAlO1xud2lkdGg6MTAwJVxufVxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqYmxvY2s2KioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG4uYmxvY2s2e1xucGFkZGluZy10b3A6IDMwcHg7IFxucGFkZGluZy1sZWZ0OjA7XG5tYXJnaW4tdG9wOiAwOyBcbm1heC13aWR0aDogMTAwJTtcbmRpc3BsYXk6IGNvbnRlbnRzO1xuZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbmp1c3RpZnktY29udGVudDogY2VudGVyO1xuYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cblxuLmJsb2NrNiAudGhpcmQtYmxvYy1ib3JkZXJ7XG5tYXJnaW46IDIwcHggYXV0byAwIGF1dG87XG59XG5cblxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipibG9jazcgKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuLmJsb2NrN3tcbnBhZGRpbmctdG9wOiAxMHB4O1xufVxuXG4uY2FyZDF7XG4gIG1hcmdpbi1yaWdodDogMHB4O1xuICBoZWlnaHQ6IDM0NXB4O1xuICB3aWR0aDogMzE1cHg7XG4gIH1cbiAgLmNhcmQxIC5ib3gxe1xuICBib3gtc2hhZG93OiAwcHggMXB4IDE1cHggZ3JleTtcbiAgYm9yZGVyOiBub25lO1xuICBib3JkZXItcmFkaXVzOiA3MXB4IDE0cHggNzFweCAxNHB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmO1xuICBwYWRkaW5nOiA0MHB4O1xuICB9XG4gIC5jYXJkMntcbiAgICBtYXJnaW4tcmlnaHQ6IDBweDtcbiAgICBoZWlnaHQ6IDM0NXB4O1xuICAgIHdpZHRoOiAzMTVweDtcbiAgfVxuICAuY2FyZDIgLmJveDJ7XG4gICAgYm94LXNoYWRvdzogMHB4IDFweCAxNXB4IGdyZXk7XG4gICAgYm9yZGVyOiBub25lO1xuICAgIGJvcmRlci1yYWRpdXM6IDcxcHggMTRweCA3MXB4IDE0cHg7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcbiAgICBwYWRkaW5nOiA0MHB4O1xuICAgIH1cbiAgLmNhcmQze1xuICAgIG1hcmdpbi1yaWdodDogMHB4O1xuICAgIGhlaWdodDogMzQ1cHg7XG4gICAgd2lkdGg6IDMxNXB4O1xuICB9XG4gIC5jYXJkMyAuYm94M3tcbiAgICBib3gtc2hhZG93OiAwcHggMXB4IDE1cHggZ3JleTtcbiAgICBib3JkZXI6IG5vbmU7XG4gICAgYm9yZGVyLXJhZGl1czogNzFweCAxNHB4IDcxcHggMTRweDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmO1xuICAgIHBhZGRpbmc6IDQwcHg7XG4gICAgfVxuICAgIFxuICAubGFzdEJ7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgbWFyZ2luOiBub25lO1xuICB9XG5cblxuICAudGV4dC1jZW50ZXJ7XG4gIG1hcmdpbi1ib3R0b206IDUwcHg7XG4gIH1cblxuICAgIH1cblxuICBcbiAvKiBNZWRpdW0gRGV2aWNlcywgRGVza3RvcHMgKi9cbiAgICBAbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4td2lkdGggOiA5OTJweCkgYW5kIChtYXgtd2lkdGggOiAxMjAwcHgpICB7XG4gLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKm5hdiBiYXIgKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbiAuZHJvcHtcbiAgbWluLXdpZHRoOiAtbW96LWF2YWlsYWJsZTtcbiAgbWFyZ2luLWxlZnQ6IC03MHB4O1xuIH0gICAgIFxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiBzaWRlIGJhciAqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuLmZsb2F0X2FjdGlvbnMge1xuICBwb3NpdGlvbjogZml4ZWQ7XG4gIGJhY2tncm91bmQ6IHJlZDtcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgbGVmdDogMDtcbiAgdG9wOiAzNSU7XG4gIHBhZGRpbmc6IDEwcHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICB3aWR0aDogOCU7XG4gIHotaW5kZXg6NTkwMDtcbiAgfVxuICBcbiAgLmZsb2F0X2FjdGlvbnMgLmFjdGlvbnNfY29udGVudCB7XG4gIHBhZGRpbmc6IDA7XG4gIG1hcmdpbjogMCAwIC0zMHB4IDA7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgfVxuICAuZmxvYXRfYWN0aW9ucyAuYWN0aW9uc19jb250ZW50IC5hY3Rpb25faXRlbXMge1xuICBwYWRkaW5nOiA1cHg7XG4gIHdpZHRoOiAxMjBweDtcbiAgaGVpZ2h0OiAxMjBweDtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWNvbnRlbnQ6IGNlbnRlcjtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICB9XG4gIFxuICAuZmxvYXRfYWN0aW9ucyAuYWN0aW9uc19jb250ZW50IC5hY3Rpb25faXRlbXMgLml0ZW1faHJlZiB7XG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBmb250LXNpemU6IDE0cHg7XG4gIH1cbiAgLmZsb2F0X2FjdGlvbnMgLmFjdGlvbnNfY29udGVudCAuYWN0aW9uX2l0ZW1zIC5pdGVtX2hyZWYgLmlocmVmX2xvZ28ge1xuICB3aWR0aDogMTAwcHg7XG4gIGhlaWdodDogMTAwcHg7XG4gIG1hcmdpbi1sZWZ0OiAyNXB4O1xuICB9XG4gIC5mbG9hdF9hY3Rpb25zIC5hY3Rpb25zX2NvbnRlbnQgLmFjdGlvbl9pdGVtcyAuaXRlbV9ocmVmIC5paHJlZl90ZXh0IHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW46IDA7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgbWFyZ2luLXRvcDogN3B4O1xuICB9XG4gIFxuICBcbiAgIC5wcmltYXJ5X2JvZHkgLmZsb2F0X2FjdGlvbnMgLmFjdGlvbnNfY29udGVudCAuYWN0aW9uX2l0ZW1zOjpiZWZvcmV7XG4gICBjb250ZW50OiBcIj5cIjtcbiAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgIHJpZ2h0OiAtMTBweDtcbiAgIHRvcDogMTUlO1xuICAgY29sb3I6IHdoaXRlO1xuICAgZm9udC1zaXplOiAyMHB4O1xuICAgd2lkdGg6IDQwJTtcbiAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgfVxuICAgXG4gICAuZmxvYXRfYWN0aW9ucyAuYWN0aW9uc19jb250ZW50IC5hY3Rpb25faXRlbXMgLnRlc3RtZWdpe1xuICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICByaWdodDogLTE4MHB4O1xuICAgICB0b3A6IDE1JTtcbiAgICAgY29sb3I6IGJsYWNrO1xuICAgICBmb250LXNpemU6IDE3cHg7XG4gICAgIHdpZHRoOiA0MCU7XG4gICAgIGJhY2tncm91bmQ6IHdoaXRlO1xuICAgICB3aWR0aDogMTgwcHg7XG4gICAgIGJvcmRlci1yYWRpdXM6IDI1cHg7XG4gICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgaGVpZ2h0OiA0MHB4O1xuICAgICBkaXNwbGF5OiBub25lO1xuICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgIH1cbiAgICAuZmxvYXRfYWN0aW9ucyAuYWN0aW9uc19jb250ZW50IC5hY3Rpb25faXRlbXM6aG92ZXIgLnRlc3RtZWdpe1xuICAgZGlzcGxheTogZmxleDtcbiAgIH1cbiAgIC5wcmltYXJ5X2JvZHkgLmZsb2F0X2FjdGlvbnMgLmFjdGlvbnNfY29udGVudCAuYWN0aW9uX2l0ZW1zIC5pdGVtX2hyZWYge1xuICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgIH1cbiAgIC5wcmltYXJ5X2JvZHkgLmZsb2F0X2FjdGlvbnMgLmFjdGlvbnNfY29udGVudCAuYWN0aW9uX2l0ZW1zIC5pdGVtX2hyZWYgLmlocmVmX2xvZ28ge1xuICAgIHdpZHRoOiAxMDBweDtcbiAgICBoZWlnaHQ6IDEwMHB4O1xuICAgIG1hcmdpbi1sZWZ0OiAyNXB4O1xuICAgfVxuICAgLnByaW1hcnlfYm9keSAuZmxvYXRfYWN0aW9ucyAuYWN0aW9uc19jb250ZW50IC5hY3Rpb25faXRlbXMgLml0ZW1faHJlZiAuaWhyZWZfdGV4dCB7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIG1hcmdpbjogMDtcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgbWFyZ2luLXRvcDogN3B4O1xuICAgfVxuICAgXG4gICAgICBcbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqYmxvY2swICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbi5jb250ZW50e1xuICB3aWR0aDogbWF4LWNvbnRlbnQ7XG4gIH1cbiAgLnRyYW5zdGlvbntcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBtYXJnaW4tbGVmdDogLTE0MHB4O1xuICAgIHdpZHRoOiBtYXgtY29udGVudDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIH1cbi5wcmltYXJ5X2JvZHkgLmhvbWVfY29udGFpbmVyIC5zZWN0aW9uX2hlYWRpbmcge1xuICBiYWNrZ3JvdW5kOiMxMTFkNWU7XG4gIG1pbi1oZWlnaHQ6IGZpdC1jb250ZW50O1xuICBib3JkZXItYm90dG9tLXJpZ2h0LXJhZGl1czogMTAwcHg7XG4gfVxuIC5wcmltYXJ5X2JvZHkgLmhvbWVfY29udGFpbmVyIC5zZWN0aW9uX2hlYWRpbmcgLmhlYWRpbmdfd3JhcHBlciAuaW1nX3dyYXBwZXIgLmhlYWRpbmdfaW1nIHtcbiAgaGVpZ2h0OiAxMDAlO1xuICBtYXgtaGVpZ2h0OiA1ODVweDtcbiAgbWFyZ2luLXRvcDogMTtcbiAgd2lkdGg6IDk5M3B4O1xuIH1cbiAucHJpbWFyeV9ib2R5IC5ob21lX2NvbnRhaW5lciAuc2VjdGlvbl9oZWFkaW5nIC5oZWFkaW5nX3dyYXBwZXIgLnRpdGxlX2hlYWRpbmcge1xuICBmb250LXNpemU6IDQwcHg7XG4gIGZvbnQtd2VpZ2h0OiA4MDA7XG4gIGNvbG9yOiAgI2ZmZmZmZjtcbiAgd2lkdGg6IG1heC1jb250ZW50O1xuIH1cbiAucHJpbWFyeV9ib2R5IC5ob21lX2NvbnRhaW5lciAuc2VjdGlvbl9oZWFkaW5nIC5oZWFkaW5nX3dyYXBwZXIgLmRlc2NfaGVhZGluZyB7XG4gIGZvbnQtc2l6ZTogNDBweDtcbiAgZm9udC13ZWlnaHQ6IDcwMDtcbiAgY29sb3I6ICAjZmZmZmZmO1xuICB3aWR0aDogbWF4LWNvbnRlbnQ7XG4gfVxuIC5jYXJyZSB7XG4gIHdpZHRoOiAyMDBweDtcbiAgaGVpZ2h0OiA5MHB4O1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgYm9yZGVyLXJhZGl1czogMThweDtcbiAgbWFyZ2luLWxlZnQ6IDgwcHg7XG4gIG1hcmdpbi10b3A6IDMzNXB4O1xuICB9XG4gIFxuICAucHJpbWFyeV9ib2R5IC5zZWFyY2hfYmxvYyB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcbiAgICBtYXJnaW4tbGVmdDogdW5zZXQ7XG4gICAgd2lkdGg6IC1tb3otYXZhaWxhYmxlO1xuICAgIG1hcmdpbi1sZWZ0OiAwcHg7XG4gICAgfVxuICAgIFxuICAgLnByaW1hcnlfYm9keSAuc2VhcmNoX2Jsb2MgLnNlYXJjaC0tc2hvdzpiZWZvcmUge1xuICAgIGJvcmRlcjogNXB4IHNvbGlkICNmZmZmZmY7XG4gICAgYm9yZGVyLXJhZGl1czogMjBweDtcbiAgICBoZWlnaHQ6IDQwcHg7XG4gICAgd2lkdGg6IDQwMHB4O1xuICAgfVxuICAgLnByaW1hcnlfYm9keSAuc2VhcmNoX2Jsb2MgLnNlYXJjaC0tc2hvdyAuc2VhcmNoX19pbnB1dCB7XG4gICAgb3BhY2l0eTogMTtcbiAgICB3aWR0aDogLW1vei1hdmFpbGFibGU7XG4gICB9XG5cblxuICAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqYmxvY2syKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbiAgIC5ob21le1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICBmb250LXNpemU6IDE4cHg7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIH1cblxuICAgIC5ibG9jazJ7XG4gICAgICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgICAgICBjb2xvcjogd2hpdGU7XG4gICAgICAgcGFkZGluZzogMjJweDtcbiAgICAgIH1cbiAgICAgIC5ibG9jazI6aG92ZXJ7XG4gICAgICAgYm9yZGVyOiA1cHggc29saWQgd2hpdGU7XG4gICAgICAgcGFkZGluZzogMThweDtcbiAgICAgIH1cblxuICAgICAgIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqYmxvY2s0ICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbiBcbiAgICAgICAuYmxvY2s0e1xuICAgICAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoLi4vLi4vYXNzZXRzL2NwbmltYWdlcy9ob21lL3Npbi5wbmcpO1xuICAgICAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICAgICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOjI1OHB4IDIyMHB4O1xuICAgICAgICBtYXJnaW4tdG9wOjBweDtcbiAgICAgICAgfVxuICAgICAgICAuYmxvY2s0IC5ibG9jazF7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgICAgIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xuICAgICAgICB9XG4gICAgICAgIC5ibG9jazQgLmJsb2NrMSBwe1xuICAgICAgICAgIHBhZGRpbmctcmlnaHQ6IDUwJTtcbiAgICAgICAgICAgfVxuXG4gICAgICAgIC5vbmV7XG4gICAgICAgICAgbWFyZ2luOiAxMXB4IDBweCAwcHggMjYzcHhcbiAgICAgICAgICB9XG4gICAgICAgICAgIC50d297XG4gICAgICAgICAgbWFyZ2luOiAtNTgwcHggMCAwIDY1MHB4XG4gICAgICAgICAgfVxuICAgICAgICAgIC50aHJlZXtcbiAgICAgICAgICBtYXJnaW46IC0xMnB4IDAgMCA2NTBweFxuICAgICAgICAgIH1cblxuICAgICAgICAgIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqYmxvY2s1ICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbiAgICAgICAgICAuYmxvY2s1IGltZ3tcbiAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiA2MHB4O1xuICAgICAgICAgICAgaGVpZ2h0OjEwMCU7XG4gICAgICAgICAgICB3aWR0aDoxMDAlXG4gICAgICAgICAgfVxuXG4gICAgICAgICAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipibG9jazYgKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuIFxuICAgICAgICAgIC5ibG9jazZ7XG4gICAgICAgICAgICBwYWRkaW5nLXRvcDogMzBweDsgXG4gICAgICAgICAgICBwYWRkaW5nLWxlZnQ6IDExMHB4O1xuICAgICAgICAgICAgbWFyZ2luLXRvcDogLTQ5MHB4OyBcbiAgICAgICAgICAgIGZsb2F0OnJpZ2h0XG4gICAgICAgICAgfVxuXG4gICAgfVxuXG5cbiAgICAgLypMYXJnZSBEZXZpY2VzLCBXaWRlIFNjcmVlbnMqL1xuICAgIEBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi13aWR0aCA6IDEyMDBweCkgYW5kIChtYXgtd2lkdGggOiAxNTAwcHgpIHtcbiAgICAgIFxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipibG9jazAgKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuLmNvbnRlbnR7XG4gIHdpZHRoOiBtYXgtY29udGVudDtcbiAgfVxuICAudHJhbnN0aW9ue1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgcGFkZGluZy1sZWZ0OiAxMTVweDtcbiAgICB3aWR0aDogbWF4LWNvbnRlbnQ7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICB9XG4ucHJpbWFyeV9ib2R5IC5ob21lX2NvbnRhaW5lciAuc2VjdGlvbl9oZWFkaW5nIHtcbiAgYmFja2dyb3VuZDojMTExZDVlO1xuICBtaW4taGVpZ2h0OiBmaXQtY29udGVudDtcbiAgYm9yZGVyLWJvdHRvbS1yaWdodC1yYWRpdXM6IDEwMHB4O1xuIH1cbiAucHJpbWFyeV9ib2R5IC5ob21lX2NvbnRhaW5lciAuc2VjdGlvbl9oZWFkaW5nIC5oZWFkaW5nX3dyYXBwZXIgLmltZ193cmFwcGVyIC5oZWFkaW5nX2ltZyB7XG4gIGhlaWdodDogMTAwJTtcbiAgbWF4LWhlaWdodDogNTg1cHg7XG4gIG1hcmdpbi10b3A6IDE7XG4gIHdpZHRoOiA5OTNweDtcbiB9XG4gLnByaW1hcnlfYm9keSAuaG9tZV9jb250YWluZXIgLnNlY3Rpb25faGVhZGluZyAuaGVhZGluZ193cmFwcGVyIC50aXRsZV9oZWFkaW5nIHtcbiAgZm9udC1zaXplOiA0MHB4O1xuICBmb250LXdlaWdodDogODAwO1xuICBjb2xvcjogICNmZmZmZmY7XG4gIHdpZHRoOiBtYXgtY29udGVudDtcbiB9XG4gLnByaW1hcnlfYm9keSAuaG9tZV9jb250YWluZXIgLnNlY3Rpb25faGVhZGluZyAuaGVhZGluZ193cmFwcGVyIC5kZXNjX2hlYWRpbmcge1xuICBmb250LXNpemU6IDQwcHg7XG4gIGZvbnQtd2VpZ2h0OiA3MDA7XG4gIGNvbG9yOiAgI2ZmZmZmZjtcbiAgd2lkdGg6IG1heC1jb250ZW50O1xuIH1cbiAuY2FycmUge1xuICB3aWR0aDogMjAwcHg7XG4gIGhlaWdodDogOTBweDtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIGJvcmRlci1yYWRpdXM6IDE4cHg7XG4gIG1hcmdpbi1sZWZ0OiA4MHB4O1xuICBtYXJnaW4tdG9wOiAzMzVweDtcbiAgfVxuICBcbiAgLnByaW1hcnlfYm9keSAuc2VhcmNoX2Jsb2Mge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XG4gICAgbWFyZ2luLWxlZnQ6IHVuc2V0O1xuICAgIHdpZHRoOiAtbW96LWF2YWlsYWJsZTtcbiAgICBtYXJnaW4tbGVmdDogMHB4O1xuICAgIH1cbiAgICBcbiAgIC5wcmltYXJ5X2JvZHkgLnNlYXJjaF9ibG9jIC5zZWFyY2gtLXNob3c6YmVmb3JlIHtcbiAgICBib3JkZXI6IDVweCBzb2xpZCAjZmZmZmZmO1xuICAgIGJvcmRlci1yYWRpdXM6IDIwcHg7XG4gICAgaGVpZ2h0OiA0MHB4O1xuICAgIHdpZHRoOiA0MDBweDtcbiAgIH1cbiAgIC5wcmltYXJ5X2JvZHkgLnNlYXJjaF9ibG9jIC5zZWFyY2gtLXNob3cgLnNlYXJjaF9faW5wdXQge1xuICAgIG9wYWNpdHk6IDE7XG4gICAgd2lkdGg6IC1tb3otYXZhaWxhYmxlO1xuICAgfVxuXG4gICAgICAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipibG9jazQgKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuIFxuICAgICAgICAuYmxvY2s0IC5ibG9jazF7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgICAgIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xuICAgICAgICB9XG4gICAgIFxuXG4gICAgICAgIC5vbmV7XG4gICAgICAgICAgbWFyZ2luOiAxMXB4IDBweCAwcHggNDU2cHhcbiAgICAgICAgICB9XG4gICAgICAgICAgIC50d297XG4gICAgICAgICAgbWFyZ2luOiAtNTgwcHggMCAwIDc5MnB4XG4gICAgICAgICAgfVxuICAgICAgICAgIC50aHJlZXtcbiAgICAgICAgICBtYXJnaW46IC0xMnB4IDAgMCA3OTJweFxuICAgICAgICAgIH1cbiAgICAgICAgICBcbiAgICB9ICJdfQ== */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](HomeComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-home',
                templateUrl: './home.component.html',
                styleUrls: ['./home.component.css']
            }]
    }], function () { return [{ type: src_app_services_token_storage_service__WEBPACK_IMPORTED_MODULE_1__["TokenStorageService"] }, { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] }, { type: _services_cpn_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"] }]; }, null); })();


/***/ }),

/***/ "./src/app/map-french/map-french.component.ts":
/*!****************************************************!*\
  !*** ./src/app/map-french/map-french.component.ts ***!
  \****************************************************/
/*! exports provided: MapFrenchComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MapFrenchComponent", function() { return MapFrenchComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");



const _c0 = function (a0) { return { choix: a0 }; };
class MapFrenchComponent {
    constructor() {
        this.myOutput = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.message = " Info about the action";
    }
    /*************************cahnge input *************/
    ngOnChanges(changes) {
        this.dept = changes.myinputDep.currentValue;
        this.selectDept(changes.myinputDep.currentValue);
    }
    ngOnInit() {
        $("path, circle").hover(function (e) {
            $('#info-box').css('display', 'block');
            $('#info-box').html($(this).data('department') + "  " + $(this).data('name'));
            // console.log('hover', $( '#info-box').html($(this).data('info')))
        });
        $("path, circle").mouseleave(function (e) {
            $('#info-box').css('display', 'none');
        });
        $(document).mousemove(function (e) {
            $('#info-box').css('top', e.pageY - $('#info-box').height() - 30);
            $('#info-box').css('left', e.pageX - ($('#info-box').width()) / 2);
        }).mouseover();
    }
    selectDept(val) {
        let mapCityName = document.getElementById("placeName");
        let map = document.getElementById("svgContent");
        let region = map.querySelectorAll(".region");
        region.forEach((regionItem) => {
            let departement = regionItem.querySelectorAll('.departement');
            departement.forEach((departementItem) => {
                var _a, _b, _c, _d, _e;
                if (((_a = departementItem.attributes[2]) === null || _a === void 0 ? void 0 : _a.nodeValue) == val || ((_b = departementItem.attributes[2]) === null || _b === void 0 ? void 0 : _b.nodeValue) == val) {
                    // mapCityName.innerHTML ="<b>"+regionItem.dataset.name+"</b> : "+departementItem.dataset.name+" <sup>("+departementItem.attributes[3]?.nodeValue+")</sup>";
                    this.map = {
                        region: (_c = regionItem === null || regionItem === void 0 ? void 0 : regionItem.attributes[1]) === null || _c === void 0 ? void 0 : _c.nodeValue,
                        departement: (_d = departementItem === null || departementItem === void 0 ? void 0 : departementItem.attributes[1]) === null || _d === void 0 ? void 0 : _d.nodeValue,
                        zipCode: (_e = departementItem === null || departementItem === void 0 ? void 0 : departementItem.attributes[2]) === null || _e === void 0 ? void 0 : _e.nodeValue,
                    };
                    this.myOutput.emit(this.map);
                }
            });
        });
    }
}
MapFrenchComponent.ɵfac = function MapFrenchComponent_Factory(t) { return new (t || MapFrenchComponent)(); };
MapFrenchComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: MapFrenchComponent, selectors: [["app-map-french"]], inputs: { myinputDep: "myinputDep" }, outputs: { myOutput: "myOutput" }, features: [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵNgOnChangesFeature"]], decls: 123, vars: 303, consts: [["id", "info-box", 1, "round", "right-in"], [1, "mapfrench_container"], [1, "mapfrench_wrapper"], ["id", "svgContent", "version", "1.1", "xmlns", "http://www.w3.org/2000/svg", 0, "xmlns", "xlink", "http://www.w3.org/1999/xlink", "x", "0px", "y", "0px", "viewBox", "0 0 700 590", 0, "xml", "space", "preserve", 1, "map_content"], ["data-name", "Guadeloupe", "data-department", "971", "data-code_insee", "01", 1, "region"], ["data-name", "Guadeloupe", "data-department", "971", "d", "M35.87,487.13l0.7,7.2l-4.5-1.1l-2,1.7l-5.8-0.6l-1.7-1.2l4.9,0.5l3.2-4.4L35.87,487.13z M104.87,553.63 l-4.4-1.8l-1.9,0.8l0.2,2.1l-1.9,0.3l-2.2,4.9l0.7,2.4l1.7,2.9l3.4,1.2l3.4-0.5l5.3-5l-0.4-2.5L104.87,553.63z M110.27,525.53 l-6.7-2.2l-2.4-4.2l-11.1-2.5l-2.7-5.7l-0.7-7.7l-6.2-4.7l-5.9,5.5l-0.8,2.9l1.2,4.5l3.1,1.2l-1,3.4l-2.6,1.2l-2.5,5.1l-1.9-0.2 l-1,1.9l-4.3-0.7l1.8-0.7l-3.5-3.7l-10.4-4.1l-3.4,1.6l-2.4,4.8l-0.5,3.5l3.1,9.7l0.6,12l6.3,9l0.6,2.7c3-1.2,6-2.5,9.1-3.7l5.9-6.9 l-0.4-8.7l-2.8-5.3l0.2-5.5l3.6,0.2l0.9-1.7l1.4,3.1l6.8,2l13.8-4.9L110.27,525.53z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Martinique", "data-department", "972", "data-code_insee", "02", 1, "region"], ["data-name", "Martinique", "data-department", "972", "d", "m44.23,433.5l1.4-4.1l-6.2-7.5l0.3-5.8l4.8-4 l4.9-0.9l17,9.9l7,8.8l9.4-5.2l1.8,2.2l-2.8,0.8l0.7,2.6l-2.9,1l-2.2-2.4l-1.9,1.7l0.6,2.5l5.1,1.6l-5.3,4.9l1.6,2.3l4.5-1.5 l-0.8,5.6l3.7,0.2l7.6,19l-1.8,5.5l-4.1,5.1h-2.6l-2-3l3.7-5.7l-4.3,1.7l-2.5-2.5l-2.4,1.2l-6-2.8l-5.5,0.1l-5.4,3.5l-2.4-2.1 l0.2-2.7l-2-2l2.5-4.9l3.4-2.5l4.9,3.4l3.2-1.9l-4.4-4.7l0.2-2.4l-1.8,1.2l-7.2-1.1l-7.6-7L44.23,433.5z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Guyane", "data-department", "973", "data-code_insee", "03", 1, "region"], ["data-name", "Guyane", "data-department", "973", "d", "m95.2,348.97l-11.7,16.4l0.3,2.4l-7.3,14.9 l-4.4,3.9l-2.6,1.3l-2.3-1.7l-4.4,0.8l0.7-1.8l-10.6-0.3l-4.3,0.8l-4.1,4.1l-9.1-4.4l6.6-11.8l0.3-6l4.2-10.8l-8.3-9.6l-2.7-8 l-0.6-11.4l3.8-7.5l5.9-5.4l1-4l4.2,0.5l-2.3-2l24.7,8.6l9.2,8.8l3.1,0.3l-0.7,1.2l6.1,4l1.4,4.1l-2.4,3.1l2.6-1.6l0.1-5.5l4,3.5 l2.4,7L95.2,348.97z", 1, "departement", 3, "ngClass", "click"], ["data-name", "La R\u00E9union", "data-department", "974", "data-code_insee", "04", 1, "region"], ["data-name", "La R\u00E9union", "data-department", "974", "d", "m41.33,265.3l-6.7-8.5l1.3-6l4.1-2.4l0.7-7.9 l3.3,0.4l7.6-6.1l5.7-0.8l21,4l5,5.3v4.1l7.3,10.1l6.7,4.5l1,3.6l-3.3,7.9l0.9,9.6l-3.4,3.5l-17.3,2.9l-19.6-6.5l-3.8-3.6l-4.7-1.2 l-0.9-2.5l-3.6-2.3L41.33,265.3z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Mayotte", "data-department", "976", "data-code_insee", "06", 1, "region"], ["data-name", "Mayotte", "data-department", "976", "d", "m57.79,157.13l11.32,5.82l-3.24,7.46l-5.66,7.52l5.66,8.37l-4.04,5.7l-5.66,8.01l5.66,4.37l-7.28,4.37l-8.09-2.73l-4.04-5.04v-4.85l-3.24-6.55l7.28,3.88l4.04,1.13v-7.14l-4.85-8.43v-14.8l-8.09-2.61l-3.24-2.67v-5.76l8.9-6.79l7.28,10.19L57.79,157.13z M78.07,164.38l-5.56,3.42l4.81,5.59l3.93-4.79L78.07,164.38z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Ile-de-France", "data-code_insee", "11", 1, "region"], ["data-name", "Paris", "data-department", "75", "d", "M641.8,78.3l-0.2,3.8l-1,2.6l-8.3-1.7l-6-0.6l-5.2,3h-4l-2.5-0.3l-0.4-0.1l-13.5-5l-3-3.8l-4.3-1.9l-0.5-0.2 l0.4-1.9l1.3-3.1l2.7-2.1l2.9-1.1l3.9,0.5h0.1l0.9-2.2l7.1-4.6l14-0.1l1.8,3.6l1.8,2.4l0.6,0.9l0.1,0.4L631,68l0.4,5.4l0.4,1.8v0.1 l-0.3,0.8l0.1,3.6l0.6-0.5l1.6-1.6l2-0.5l2-0.5L641.8,78.3z M396.8,154.7l-3.2-0.5l-2.5,1.7l3,3.5l5.3-0.1l-1.8-1.9L396.8,154.7z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Seine-et-Marne", "data-department", "77", "d", "m441.1,176.1l-2.9,0.8l0.4,8.5l-15.4,3 l-0.2,5.8l-3.9,5.4l-11.2,2.7l-9.2-0.7l2.6-1.5l0.6-2.7l-4.2-4.3L397,190l3.4-4.8l4-17.2l-0.5-1l1.1-4.1l-0.3-2.9v-0.1l-1.3-4.7 l1.3-2.5l-1.7-5.1l0.1-0.1l1.7-2.3l-0.2-2l6.9,1l2-2.2l2.5,1.6l8.1-2.9l2.6,0.7l1.8,2.5l-0.7,2.8l3.9,4.2l9.3,6l-0.4,2l-2.6,2.2 l3.5,8.3l2.6,1.7L441.1,176.1z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Yvelines", "data-department", "78", "d", "m364.1,158.1l-3.6-6.6l-1.8-5.8l2.3-2.6 l3.8,0.1l9.5,0.8l9,3.6l5.5,6.1l-2,3.1l3.2,5.2l-7.1,5.4l-1.6,2.6l0.7,2.9l-4.6,8.6l-3.1,0.7L372,180l-1.2-5.6l-6.2-5.4L364.1,158.1z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Essonne", "data-department", "91", "d", "m401.6,164.8l2.3,2.2l0.5,1l-4,17.2L397,190 l-3.7-0.6l-2.8,1.8l-1.5-2.7l-1.9,2.9l-6.9,0.7l-2.8-10.6l4.6-8.6l-0.7-2.9l1.6-2.6l7.1-5.4v-0.1l3.7,1.6l5.1,2.1L401.6,164.8z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Hauts-de-Seine", "data-department", "92", "d", "M391.1,155.9l3,3.5l-0.4,4.1l-3.7-1.6v0.1l-3.2-5.2l2-3.1l3.6-2.6l1.3,2l-0.1,1.1L391.1,155.9z M612.6,54.1 l1.6-0.7l0.7-1.9l0.5-1.8l-0.1-1.1l-0.2-1.4l-4.6-1.9l-4.6-0.9l-4,1.3l-7.6,5.6l-6.1,5.8l-5.3,3l-1,1l-3.75,7.4l1.79,7.17 l-0.06,0.07l0.01,0.06l-2.74,3.23l0.68,2.44l2.5,4.8l3.3-0.5l1,5.2l3.9-0.3l1.4,3.5l3.4,1.6l0.5,2.1l5.3,4.2l4.3,1.3l-0.1,4.9 l5.7,3.5l3.15-5.91l-0.7-5.46l0.72-1.2l0.4-1.3l0.7-2.1l-1.4-1.9l0.3-1.2l0.8-2.8l-1-2.6l0.5-0.3l0.5-0.3l0.9-0.5l0.7-1.1l-0.4-0.1 l-13.5-5l-3-3.8l-4.3-1.9l-0.5-0.2l0.3-1.9l1.4-3.1l2.7-2.1l2.8-1.1h0.1l3.9,0.5l0.9-2.2l7.2-4.6l-0.7-2l-0.6-2l1.4-0.7L612.6,54.1z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Seine-Saint-Denis", "data-department", "93", "d", "M404.7,152.7l-1.3,2.5l1.3,4.7v0.1l-7.1-2.6l-0.8-2.7l-3.2-0.5l0.1-1.1l-1.3-2l3.3-1.3l2.6,1.1 c1.6-1.1,3.2-2.2,4.7-3.3L404.7,152.7z M663.2,73.89l0.06-0.08l-0.02-0.04l2.61-3.38l-3.95-0.3l-1.6-5.9l0.06-0.06l-0.02-0.06 l6.36-6.56l0.1-5.42l1.1-4l-1.2-3.4l-5.1-8l0.07-0.08l-0.03-0.04l2.65-3.33l-0.89-4.04l-4.5-2.9l-4.1,1.7l-6.4,8.8l-8.2,6.2 l-0.7-0.2l-7.8-1.1l-1.9,1l-5.1-4.6l-1.3-0.2l-1.9-0.7l-5.1,3l-1.6,2.7l-1-1.2l-5.9-2.1l-1.96,2.25v0.2l0.66,2.45l3.9,0.8l4.7,1.9 l0.1,1.4l0.1,1.1l-0.2,0.9l-0.3,0.9l-0.7,1.9l-1.6,0.7l-0.3,0.8l-1.4,0.7l0.6,2l0.7,2l13.9-0.2l0.1,0.1l1.8,3.6l1.8,2.4l0.6,0.8 l0.1,0.5L631,68l0.4,5.4l0.4,1.8l5.9-0.5l0.5-0.3c0.1,0,0.1,0,0.2,0l6.3-2.8l2.9,0.4l0.7,1.3l3,1.5l4,2.9c0,0.1,0.1,0.2,0.2,0.2 l0.7,0.5l6,6.2l0.8,0.6c0.1,0,0.2,0.1,0.3,0.1l3.6,2.6l0.04-0.13l0.43-1.3l0.23-0.68l-1.8-6L663.2,73.89z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Val-de-Marne", "data-department", "94", "d", "M404.7,160l0.3,2.9l-1.1,4.1l-2.3-2.2l-2.8,0.8l-5.1-2.1l0.4-4.1l5.3-0.1l-1.8-1.9L404.7,160z M668.09,102.2 h0.06l-0.02-0.12l3.31-0.19l-1.55-3.58l-3.69-2.41l0.8-8h-0.1l-3.6-2.6c-0.1,0-0.2-0.1-0.3-0.1l-0.8-0.6l-6-6.2l-0.7-0.5 c-0.1,0-0.2-0.1-0.2-0.2l-4-2.9l-3-1.5l-0.7-1.3l-2.9-0.4l-6.3,2.8c-0.1,0-0.1,0-0.2,0l-0.5,0.3l-5.9,0.5v0.1l-0.3,0.8l0.1,3.6 l0.6-0.5l1.6-1.7l2-0.4l2-0.5l4,1.7l-0.2,3.8l-1,2.6l-8.3-1.7l-6-0.6l-5.2,3h-4l-2.5-0.3l-0.6,1.1h-0.1l-0.9,0.5l-0.5,0.3l-0.5,0.3 l1,2.5v0.1l-0.8,2.8l-0.3,1.2l1.4,1.9l-0.7,2.1l-0.4,1.3l-0.7,1.2l0.78,5.38h0.06l2.1,0.2l4.7,2.8l3.1-2.2l0.1,5.5l3.3,2.4l4.9-1.8 l0.7,2.5l5.2-2.3l0.5,1.3l1.7,1.7l4.6-3.6l2.1-0.5l5.2-1.8l1.9,6.8l1.7,2.5l3.3,1.8l5.44,1.88l-0.68-5.05l0.05-0.08l-0.01-0.04 l2.5-4.2l2.73-2.74l-1.38-3.64l0.07-0.06l-0.03-0.07l2.35-1.96L668.09,102.2z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Val-d\u2019Oise", "data-department", "95", "d", "m374.3,144l-9.5-0.8l4-9.5l1.6,3.2l5.6,1.1 l6.3-1.8l9.2,2.2l2.2-1.6l10.9,6.4l0.2,2l-1.7,2.3l-0.1,0.1c-1.5,1.1-3.1,2.2-4.7,3.3l-2.6-1.1l-3.3,1.3l-3.6,2.6l-5.5-6.1 L374.3,144z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Centre-Val de Loire", "data-code_insee", "24", 1, "region"], ["data-name", "Cher", "data-department", "18", "d", "m385.3,235.4l5-2.4l13.5,3.1l3.9,4.8l9-1.7l2,6.5l-1.7,5.8l2.7,2.1 l3.1,7.6l0.3,5.9l2.2,2l-0.2,5.8l-1.3,8.9h-0.1h-4l-4.8,3.7l-8.4,2.9l-2.3,1.9l1.7,5.3l-1.7,2.4l-8.7,1l-3.5,5.9v0.1l-4.9-0.2 l1.5-3.5l-0.9-8.9l-4.7-7.9l1.4-2.7l-2.3-2.2l2.5-5.1l-2.3-11.7l-11.6-1.6l2.8-5.5l2.8,0.1l0.6-2.8l9.7-2l-2.1-5.9l5.9-4.1 L385.3,235.4z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Eure-et-Loir", "data-department", "28", "d", "m333.1,200.9l-2.1-3.8l-1.1-7.5l7.5-5.1 l-0.5-4.6l0.2-4.5l-4.8-4.4l-0.1-3.2l2.4-2.6l6-1.1l5.3-3.2l2.8,1.6l6-1.3l-0.2-2.8l6-6.9l3.6,6.6l0.5,10.9l6.2,5.4l1.2,5.6l2.3,2.2 l3.1-0.7l2.8,10.6l-0.5,1.5l-4.8,10.8l-8.5,0.6l-6,2.8l0.2,2.8l-3.3-1.9l-5.5,3.5L339,201.4l-6.3,1.3L333.1,200.9z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Indre", "data-department", "36", "d", "m357.8,308.5l-2.8,2.9l-1.7-2.5l-5.8,1.1 l-2.6-1.1l1.5-2.8l-2.5-1.3l-2.6-5.4h-2.9l-4.6-4.4l0.8-5.8l-2.1-3l5.6-0.5l-1-2.7l3.3-11.9l5.1-2.7l2.3,1.7l2.6-3.5l2.5-2.1l-1-4.9 l6-3.2l2.5,1.3l1.5-2.6l6.4-0.9l5.2,3.5l-2.8,5.5l11.6,1.6l2.3,11.7l-2.5,5.1l2.3,2.2l-1.4,2.7l4.7,7.9l0.9,8.9l-1.5,3.5l-2.7,0.8 l-13.2-2.7l-1.9,2.5L357.8,308.5z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Indre-et-Loire", "data-department", "37", "d", "m303.9,263l-5.5-3.2v-0.1l5.8-15.3l1.7-9.3 l0.7-2.4l6.1,2.6l-0.5-3.3l2.8,0.3l7.7-4.5l10.5,0.5l-0.2,5.5l2.2-1.8l6,3.4l-0.7,2.7l3.4,5.1l-1.2,9.1l2.4,1.9l2.6-1.3l4.2,6.7 l1,4.9l-2.5,2.1l-2.6,3.5l-2.3-1.7l-5.1,2.7l-3.3,11.9l1,2.7l-5.6,0.5l-7.1-10l-0.3-3.1l-5.3-3l1.4,2.9l-10,0.4l-2.8-1.4l-1.3-6.1 l-2.9,0.3L303.9,263z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Loir-et-Cher", "data-department", "41", "d", "m357.9,256.4l-6,3.2l-4.2-6.7l-2.6,1.3 l-2.4-1.9l1.2-9.1l-3.4-5.1l0.7-2.7l-6-3.4l-2.2,1.8l0.2-5.5l-10.5-0.5l0.6-3.5l3.2-1.1l6.3-10.6l-0.4-5.5l-1.7-2.2l2-2.1v-0.1 l6.3-1.3l12.8,10.8l5.5-3.5l3.3,1.9l2.5,7.1l-1.8,3.2l1.7,5.6l3-1.3l2.4,1.5l1.1,3.8l2.9,0.6l1.9-2.3l15.2,1.6l0.8,2.6l-5,2.4 l5.1,7.6l-5.9,4.1l2.1,5.9l-9.7,2l-0.6,2.8l-2.8-0.1l-5.2-3.5l-6.4,0.9l-1.5,2.6L357.9,256.4z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Loiret", "data-department", "45", "d", "m393.3,189.4l3.7,0.6l0.7,3.1l4.2,4.3l-0.6,2.7 l-2.6,1.5l9.2,0.7l11.2-2.7l6.7,7.5l0.4,5.8l-4.6,4.9l1.1,2.9l-1.6,2.4l-5.3,3.3l3,2.8l2.2,6.9l-2.8,0.7l-1.5,2.4l-9,1.7l-3.9-4.8 l-13.5-3.1l-0.8-2.6l-15.2-1.6l-1.9,2.3l-2.9-0.6l-1.1-3.8l-2.4-1.5l-3,1.3l-1.7-5.6l1.8-3.2l-2.5-7.1l-0.2-2.8l6-2.8l8.5-0.6 l4.8-10.8l0.5-1.5l6.9-0.7l1.9-2.9l1.5,2.7L393.3,189.4z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Bourgogne-Franche-Comt\u00E9", "data-code_insee", "27", 1, "region"], ["data-name", "Cote-d\u2019Or", "data-department", "21", "d", "m523.6,241.7l3.9,8.2l-1.2,1.3l-1.8,8.2 l-6.2,6.8l-1.1,4.1v-0.1l-15,1.5l-8.8,4.2l-5.6-6.3l-5.5-1.9l-1.3-2.6l-5.7-1.7l-2.4-2.6V260l0.4-3.2l-3.7-1.2l-1.3-6h0.1l-1.3-2.7 l1.3-8.1l6.7-10.4l-1.7-2.3l2.8-2.1l0.3-3.7l-3.1-3.9l1.9-3.1l2.2-2l6.1-0.9l4.7-3.9l3.9,0.5l3.5,0.7l0.5,2.7l2.6,1l-0.3,2.9 l2.9,0.3l1.8,2.2l1,3.1l-2.8,2.4l2.3,4.8l9.2,2l3,1.6v2.8l4.8-1.9h0.1l2.7-1.6l2,3l0.1,3.2l-4.6,4.1L523.6,241.7z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Doubs", "data-department", "25", "d", "m590.1,245.2l-2.4,2.2l0.4,3l-4.8,6.2l-4.8,4 l-0.4,2.9l-2.5,2.7l-5.7,1.7l-0.3,0.3l-1.7,2.3l0.9,2.7l-0.7,4.5l0.5,2.5l-9.5,8.8l-2.9,5.2l-0.22,0.69l-3.68-3.49l3.6-7.4l2.1-2.3 l-4.2-4.1l-2.9-0.5l-5.8-10.1l-3,0.8l-1.5-2.5l-2,2.1l-1.2-2.5l3-5.1l-5.2-7.8l22.3-10.2l3-4.7l5.6-1.9l2.8,0.9l1.8-2.2l3.2-0.4 l0.5-2.8l5.9,0.8l0.2-0.1h0.1l5.9,2.7l-1.4,2.5l1.4,2.4l0.41-0.46l-0.11,0.16l-2.2,4.9l7-0.7L590.1,245.2z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Jura", "data-department", "39", "d", "m552.3,291.4l3.68,3.49L553.4,303l-5.3,7.2 l-5.5,3.2l-3.8,0.2l-0.4-2.8l-3.4-1.6l-4,4.4l-2.9,0.1l-0.1-3h-2.9l-4.3-7.7l2.8-1.1l-0.8-5.3l2.8-5l-2.2-8.7l-2.5-1.6l5-3.7 l-8.3-4.4l-0.4-2.9l1.1-4.1l6.2-6.8l1.8-8.2l1.2-1.3l2.3,2l5.4,0.1l5.2,7.8l-3,5.1l1.2,2.5l2-2.1l1.5,2.5l3-0.8l5.8,10.1l2.9,0.5 l4.2,4.1l-2.1,2.3L552.3,291.4z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Ni\u00E8vre", "data-department", "58", "d", "m462.8,250l5.5-0.4l1.3,6l3.7,1.2l-0.4,3.2v0.8 l-1.1,0.3l-2.7,0.4v1.3l-2.8,1l0.3,5.9l-2.1,1.7l4,7l-1.9,2.1l0.7,2.9l-11.3,5.7l-7-2.8l-5.9,6l-4.4-3.7l-2.8,1.7l-6.4-0.2l-5.7-6.3 l1.3-8.9l0.2-5.8l-2.2-2l-0.3-5.9l-3.1-7.6l-2.7-2.1l1.7-5.8l-2-6.5l1.5-2.4l2.8-0.7v0.1h3.4l7.4,4.8h6l4.6-4.3l3.9,5.6l5.5,3 l5.8-0.9l0.9,3.7l2.8-0.9L462.8,250z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Haute-Saone", "data-department", "70", "d", "m579.1,225.9l1.4,5.5l-0.2,0.1l-5.9-0.8 l-0.5,2.8l-3.2,0.4l-1.8,2.2l-2.8-0.9l-5.6,1.9l-3,4.7L535.2,252l-5.4-0.1l-2.3-2l-3.9-8.2l-2.6-1.4l4.6-4.1l-0.1-3.2l-2-3l-2.7,1.6 h-0.1l1.2-2.5l6.6-3.9l2.1,1.8l3.2-1l0.3-8.3l2-2.4l2.9,0.3l2.3-3.2l-0.2-1.4l8-5.8l7,4.3l5.8-1.6l4.9,3.6l5.1-2.2l8.4,6.6l-2.3,5.7 L579.1,225.9z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Saone-et-Loire", "data-department", "71", "d", "m517.2,270.2v0.1l0.4,2.9l8.3,4.4l-5,3.7 l2.5,1.6l2.2,8.7l-2.8,5l0.8,5.3l-2.8,1.1l-4.8-3.3l-5.4,1.3l-5.9-1.5l-5.9,20.9l-5.7-7.7l-1.6,2.3l-2.5-1.5l-2.2,1.6l-2.2-1.7 l-2.3,1.9l-0.29,2.91L482,318.2v0.1l-5.7,3.8l-2.1-2.1l-8,1.5l-5.2-3.3v-3l3.7-4.6l0.5-5.5l-1.6-2.4l-7.9-2.9l-6.7-13.5l7,2.8 l11.3-5.7l-0.7-2.9l1.9-2.1l-4-7l2.1-1.7l-0.3-5.9l2.8-1l2.7-1.7l1.1-0.3l2.4,2.6l5.7,1.7l1.3,2.6l5.5,1.9l5.6,6.3l8.8-4.2 L517.2,270.2z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Yonne", "data-department", "89", "d", "m425.8,207.1l-6.7-7.5l3.9-5.4l0.2-5.8l15.4-3 l3.6,1.5l4.5,5.5l2.5,8.3l2-2.2l3.6,4.1l5,10.9l12.6-1.6l2.9,1.4l-1.9,3.1l3.1,3.9l-0.3,3.7l-2.8,2.1l1.7,2.3l-6.7,10.4l-1.3,8.1 l1.3,2.7h-0.1l-5.5,0.4l-1.5-2.8l-2.8,0.9l-0.9-3.7l-5.8,0.9l-5.5-3l-3.9-5.6l-4.6,4.3h-6l-7.4-4.8H421v-0.1l-2.2-6.9l-3-2.8 l5.3-3.3l1.6-2.4l-1.1-2.9l4.6-4.9L425.8,207.1z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Territoire de Belfort", "data-department", "90", "d", "m580.3,215.9l0.9-0.6l7.6,5l0.5,9l2.8-0.2l2,5 l-0.1,0.1l-2.79,0.39l-1.11-0.39l-3.19,4.34L586.5,239l-1.4-2.4l1.4-2.5l-5.9-2.7h-0.1l-1.4-5.5l-1.1-4.3L580.3,215.9z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Normandie", "data-code_insee", "28", 1, "region"], ["data-name", "Calvados", "data-department", "14", "d", "m316.9,148l-0.7,2.2l-5.6-1l-7,1.7l-7.2,5.4 l-2.9,0.3l-5.7-1.1l-2.6,1.7l-4.9-3l-6.4,2.3l-2.7-1.3l-0.9,2.7l-5.4,2.9l-9.7-2.1l-1.8-2.4l4.5-5.3l-1.6-2.3l8.1-4.9l-2.2-8.2 l2-2.6l-8.4-3.1l-0.5-6.6v-0.1l0.1-0.7l1.8,0.8l1.9-2.1l3.4-0.3l9.4,3.3l13.9,1.5l6.9,3.4l5.7-0.7l4.7-2.5l4.1-3.7l5.1-1.1l0.3,8.3 h2.9l-2.3,2.1l2.8,9.4l-1.4,3L316.9,148z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Eure", "data-department", "27", "d", "m316.4,153.4l-0.2-3.2l0.7-2.2l-2.3-4.1l1.4-3l-2.8-9.4l2.3-2.1h-2.9 l-0.3-8.3l1.7-0.4l0.28-0.1h1.52l-0.9-0.2l0.8-0.3l-1.29-0.3l5.89-2.4l7.6,5l3.4-0.7l4.9,3l-1.9,2.4l2.1,2.1l5.4,2.4l1.4-2.7 l8.2-2.5l4.8-7l13.1,3.3l3.5,8.4l-4,2.6l-4,9.5l-3.8-0.1l-2.3,2.6l1.8,5.8l-6,6.9l0.2,2.8l-6,1.3l-2.8-1.6l-5.3,3.2l-6,1.1l-2.4,2.6 l-3.4-2.1l1.7-2.3l-7.8-9.5L316.4,153.4z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Manche", "data-department", "50", "d", "m255.2,158.7l9.7,2.1l4.1,4.2l-1.8,6.7 l-3.6,4.5h-0.1l-8.6-0.8l-5.4-2.3l-7.1,4.8l-2.7-1l-4.7-9.6l1.9-0.2l4.8,0.4l2.5-1.1l0.5-2.2l-2.4,1.3l-5.1-5.6l-0.3-5.3l2-6.1 l-0.3-4.9l-1.8-3.6l0.4-7.4l1.5-2l-2.5,0.3l-2-5l0.3-2.2l-2.4-1.2l-2.9-4.1l-0.7-5.9l-1.4-1.9l1.8-1.8l0.1-2.8l-0.5-2.3l-2.2-1.1 l-1-2.5l2.1-0.2l11.9,4.2h2.4l4-2.6l5.1,0.6l1.8,1.7l0.9,2.7l-3.2,5.2l4,6.5l1.1,4.3l-0.1,0.7v0.1l0.5,6.6l8.4,3.1l-2,2.6l2.2,8.2 l-8.1,4.9l1.6,2.3l-4.5,5.3L255.2,158.7z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Orne", "data-department", "61", "d", "m266.9,179.9l-3.3-3.7l3.6-4.5l1.8-6.7 l-4.1-4.2l5.4-2.9l0.9-2.7l2.7,1.3l6.4-2.3l4.9,3l2.6-1.7l5.7,1.1l2.9-0.3l7.2-5.4l7-1.7l5.6,1l0.2,3.2l6.3,0.5l7.8,9.5l-1.7,2.3 l3.4,2.1l0.1,3.2l4.8,4.4l-0.2,4.5l0.5,4.6l-7.5,5.1l1.1,7.5l-3.2-0.7l-3.1-3.5l-2.9,1l-7.2-5l-1.6-8.4l-2.8-1.5l-11,5.9l-3-0.1 v-0.1v-2.9l-3.3-1.6l-1.9-6l-2.7-0.2l-0.7,2.7h-9.1l-6.7,3.3l-2.5-1.7L266.9,179.9z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Seine-Maritime", "data-department", "76", "d", "m314.41,119.8l-7.61-1.8l-1.2-2l-0.1-2.3 l4.4-9.7l13.8-7.4L326,95l10.3-2.1l4.8-1.8l2.4,0.3L352,87l5.11-4.09l11.79,9.99l3.4,8.4l-3.1,4.7l1.4,8.7l-1.3,8l-13.1-3.3l-4.8,7 l-8.2,2.5l-1.4,2.7l-5.4-2.4l-2.1-2.1l1.9-2.4l-4.9-3l-3.4,0.7l-7.6-5L314.41,119.8z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Hauts-de-France", "data-code_insee", "32", 1, "region"], ["data-name", "Aisne", "data-department", "02", "d", "m450.3,82.6l16.7,4.6l2.91,0.94L470.6,94l-1.3,3.5l1.3,3.1l-5,7.2 l-2.7,0.3l0.3,14.3l-1,2.8l-5.3-1.8l-8,4l-1.2,2.6l3.2,8l-5.5,2.3l1.6,2.4l-0.8,2.7l2.5,1.3l-7.7,10.2l-9.3-6l-3.9-4.2l0.7-2.8 l-1.8-2.5l-2.6-0.7l2.1-1.7l-0.5-2.8l-2.9-1.1l-2.4,1.5l-0.7-2.9l3,0.2l-2.9-4.5l2.6-1.7l2.4-5.7l2.6-1.1l-2.2-1.8l0.8-4.5 l-0.4-10.2l-2.3-7l3.9-8.1l0.4-3.8l12.6-0.6l2.6-2.2l2.3,1.7L450.3,82.6z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Nord", "data-department", "59", "d", "m384.33,25.06l0.87-0.26l2,0.8l1.1-2.1l7.9-2.1 l2.9,0.3l4.4-1.9v-0.1l1.2,4.8l2.3,3.7l-1.6,1.9l0.6,0.8l1.2,5.8h3.4l2.7,5.1l3.1,1.5h2.1l0.6-2.4l8.1-3l3.8,7.5l0.1,1l1.3,5.2 l2,3.5h0.1l2.8,0.6l2.1-1.4l2.4-0.2l-0.5,2.2l2.2-0.7l2.8,1l1.8,4.4l-0.6,2.3l0.7,2.3l1.4,1.9l1.1-2.6l4.6-0.3l2.4,1.1L462,64l5.5,6 l2.3,0.2l-2.1,2.4l-1.4,4.7l2.6,0.2l1.4,3.3l-3.5,3.9l0.2,2.5l-16.7-4.6l-5.2,1.8l-2.3-1.7l-2.6,2.2l-12.6,0.6l-3.3-2.6l3.5-10.6 l-1.8-2.4l-3-0.4l0.7-2.7l-3.9-5.2l3.1-1.6l-3.8-5.3l-5.9-1l1-6.1l-1.3-2.5l-1.7,2.2l-11.6-0.5l-4.1-4.2l0.6-2.8l-5.5-2.6 L384.33,25.06z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Oise", "data-department", "60", "d", "m372.8,131.1l-3.5-8.4l1.3-8l-1.4-8.7l3.1-4.7 l4.1,3.7l3.1-1.2l14.4,2.2l12.8,6.7l8.6-6.8l10.3-1.5l0.4,10.2l-0.8,4.5l2.2,1.8l-2.6,1.1l-2.4,5.7l-2.6,1.7l2.9,4.5l-3-0.2l0.7,2.9 l2.4-1.5l2.9,1.1l0.5,2.8l-2.1,1.7l-8.1,2.9l-2.5-1.6l-2,2.2l-6.9-1l-10.9-6.4l-2.2,1.6l-9.2-2.2L376,138l-5.6-1.1l-1.6-3.2 L372.8,131.1z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Pas-de-Calais", "data-department", "62", "d", "m379.8,68.9l7.1,5.8l12-2.5l-2.6,5.7L398,81 l2.5-3.1l8.4,3.5l0.8-2.8l2.8,4.6l2.4-1.7l0.8,3.2l8.6-1.8l3.5-10.6l-1.8-2.4l-3-0.4l0.7-2.7l-3.9-5.2l3.1-1.6l-3.8-5.3l-5.9-1 l1-6.1l-1.3-2.5l-1.7,2.2l-11.6-0.5l-4.1-4.2l0.6-2.8l-5.5-2.6l-6.27-12.14L372.6,28.5l-6.4,5.4l0.9,5.6l-1.7,4.6l0.6,6.7l2,4.2 l-1.7-1.4l-0.3,9.7l2.27,1.58l10.53,1.02L379.8,68.9z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Somme", "data-department", "80", "d", "m424.3,82.9l3.3,2.6l-0.4,3.8l-3.9,8.1l2.3,7 l-10.3,1.5l-8.6,6.8l-12.8-6.7l-14.4-2.2l-3.1,1.2l-4.1-3.7l-3.4-8.4l-11.79-9.99L359.5,81l3.4-6.6l1.9-1.1l0.1-0.1l1.4,1.8l3.5,0.3 l-5.6-6l1.2-5.1l2.9,0.7l-0.03-0.02l10.53,1.02l1,3l7.1,5.8l12-2.5l-2.6,5.7L398,81l2.5-3.1l8.4,3.5l0.8-2.8l2.8,4.6l2.4-1.7 l0.8,3.2L424.3,82.9z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Grand Est", "data-code_insee", "44", 1, "region"], ["data-name", "Ardennes", "data-department", "08", "d", "m469.91,88.14l0.79,0.26l9.8,0.4l7.3-3.2l1.1-6 l4-3.8l2.8-0.2v3.8L494,81l-0.6,5.2l3.3,4.5l-1,2.4l0.6,3.1l1.4,1.9l3.3-0.9l4.3,2.4l2.8,3.8l4.9,0.6l2,1.7l-0.9,2.4l2.1-0.13 l-1.6,1.13l-2,2.7l-5.7-2.1l-1.9,2l0.8,8.8l-3.2,5.1l1.4,2.5l-4.2,3.6v0.1l-20.1-1.9l-9.8-6.6l-6.7-0.9l-0.3-14.3l2.7-0.3l5-7.2 l-1.3-3.1l1.3-3.5L469.91,88.14z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Aube", "data-department", "10", "d", "m442.2,186.9l-3.6-1.5l-0.4-8.5l2.9-0.8l3-5 l3.2,4.5l9,1.2v-3.3l9.5-7.6l6.5-0.9l3.1,0.5l0.4,6.1l2.6,2c1.9,0.8,3.8,1.5,5.6,2.3l2.5-1.5l3.3,1.1l-0.6,3.4l2.4,5.2l5.6,3 l0.5,9.9l-0.1,2.7l-5.6,2.5l0.2,4.8l-3.9-0.5l-4.7,3.9l-6.1,0.9l-2.2,2l-2.9-1.4l-12.6,1.6l-5-10.9l-3.6-4.1l-2,2.2l-2.5-8.3 L442.2,186.9z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Marne", "data-department", "51", "d", "m440.6,158.9l0.4-2l7.7-10.2l-2.5-1.3l0.8-2.7 l-1.6-2.4l5.5-2.3l-3.2-8l1.2-2.6l8-4l5.3,1.8l1-2.8l6.7,0.9l9.8,6.6l20.1,1.9l2.2,9l-1,4.1l2.6,1.3l-0.6,3.9l-3.1,1.1l-1.1,5.8 l3.2,4.6l0.5,4.1l-8.6,2.2l2.2,2.5l-2.3,2.2l0.7,2.9h-4.7l-3.3-1.1l-2.5,1.5c-1.8-0.8-3.7-1.5-5.6-2.3l-2.6-2l-0.4-6.1l-3.1-0.5 l-6.5,0.9l-9.5,7.6v3.3l-9-1.2l-3.2-4.5l-2.6-1.7l-3.5-8.3L440.6,158.9z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Haute-Marne", "data-department", "52", "d", "m493.9,167.9l8.6-2.2l3.4,5.2l16.9,10.4 l-2.4,2.3l12.7,9.5l-1.7,8.6l5.5,4.7l0.2,3.1l2.7-1.1l1.3,2.5v0.1l0.2,1.4l-2.3,3.2l-2.9-0.3l-2,2.4l-0.3,8.3l-3.2,1l-2.1-1.8 l-6.6,3.9l-1.2,2.5l-4.8,1.9v-2.8l-3-1.6l-9.2-2l-2.3-4.8l2.8-2.4l-1-3.1l-1.8-2.2l-2.9-0.3l0.3-2.9l-2.6-1l-0.5-2.7l-3.5-0.7 l-0.2-4.8l5.6-2.5l0.1-2.7l-0.5-9.9l-5.6-3l-2.4-5.2l0.6-3.4h4.7l-0.7-2.9l2.3-2.2L493.9,167.9z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Meurthe-et-Moselle", "data-department", "54", "d", "m588.2,170.9l1.9,1.3l-1.5,0.4l-10.6,7.6l-6.1-1.6l-1.6-2.7l-5.3,3.8 l-6,1l-2.4-1.8l-5.4,2l-1.1,2.8l-5.7,0.7l-4.1-4.8l0.1-2.9l-5.8-0.6l0.2-2.9l-2.5-2l1.7-2.8l-1.3-8.6l2.2-13.8l0.9-2.7l-4.9-11.5 l1.5-5.9l-1.2-2.7l-4.4-4.8l-5.3,2l-0.7-5.3l4.8-1.7l2-1.9h6.8l2.54,2.31L539.6,124l2.5,1.6l1.2,3.6l-1.7,3.1l1,5.6l-2.8,0.1 l4.3,7.5l11.5,4l-0.3,2.9l2.7,5.1l8.5,1.5l5.3,3.9l14.4,5.3L588.2,170.9z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Meuse", "data-department", "55", "d", "m516.2,107.97l1.2-0.07l1.5,1.6l1.9,5.6 l0.7,5.3l5.3-2l4.4,4.8l1.2,2.7l-1.5,5.9l4.9,11.5l-0.9,2.7l-2.2,13.8l1.3,8.6l-1.7,2.8l2.5,2l-0.2,2.9l-1.9,2.3l-3-0.5l-6.9,3.4 l-16.9-10.4l-3.4-5.2l-0.5-4.1l-3.2-4.6l1.1-5.8l3.1-1.1l0.6-3.9l-2.6-1.3l1-4.1l-2.2-9v-0.1l4.2-3.6l-1.4-2.5l3.2-5.1l-0.8-8.8 l1.9-2l5.7,2.1l2-2.7L516.2,107.97z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Moselle", "data-department", "57", "d", "m539.6,124l-2.65-10.19l0.65,0.59h2.4l1.5,2.1 l2.3,0.7l2.3-0.5l1-2.3l2-1.2l2.2-0.2l4.5,2.3l4.9-0.1l3.1,3.8l2.3,1.9l-0.5,2l3.7,3.2l2.8,4.5v2.3l4.2,0.7l1.2-1.9l-0.3-2.4 l2.6-0.2l3.8,1.8l1.4,3.5l2.1-1.5l2.5,1.9l5.8-0.4l5.3-4.2l2.2,1.4l0.5,2.1l2.4,2.4l3.2,1.5h0.03l-1.73,4.4l-1.4,2.6l-8.9,0.3 l-9.1-4.6l-0.8-2.8l-5,10.8l5.5,2.4l-1.6,2.5l2.3,1.7l1.3-2.5l3,0.3l4.3,3.4l-3,13.3l-2.3,1.8l-3.4-0.3l-2-2.7l-14.4-5.3l-5.3-3.9 l-8.5-1.5l-2.7-5.1l0.3-2.9l-11.5-4l-4.3-7.5l2.8-0.1l-1-5.6l1.7-3.1l-1.2-3.6L539.6,124z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Bas-Rhin", "data-department", "67", "d", "m631.8,140.7l-2.8,9.4l-7.8,10.5l-2,1.5l-1.4,3.3l0.3,4.9l-2.4,7.2 l0.7,3.6l-1.5,2l-1.2,5.5l-3.16,6.23L605.9,193l-0.3-2.8l-8.5-5.6l-3.1-0.2l-5.2-2.2l1.3-10l-1.9-1.3l3.4,0.3l2.3-1.8l3-13.3 l-4.3-3.4l-3-0.3l-1.3,2.5l-2.3-1.7l1.6-2.5l-5.5-2.4l5-10.8l0.8,2.8l9.1,4.6l8.9-0.3l1.4-2.6l1.73-4.4l8.87,0.6l2.4-0.6 L631.8,140.7z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Haut-Rhin", "data-department", "68", "d", "m605.9,193l4.64,1.83l-0.04,0.07v5.3l1.6,1.9 l0.2,3.4l-2.2,11.1l0.1,6.7l1.8,1.5l0.6,3.5l-2.2,2l-0.2,2.3l-3.1,0.9l0.5,2.2l-1.5,1.6h-2.7l-3.8,1.4l-3-1.1l0.3-2.5l-2.4-1.1 l-0.4,0.1l-2-5l-2.8,0.2l-0.5-9l-7.6-5l2.8-2.4v-6.2l4.8-7.8l4.1-13.5l1.1-1l3.1,0.2l8.5,5.6L605.9,193z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Vosges", "data-department", "88", "d", "m520.4,183.6l2.4-2.3l6.9-3.4l3,0.5l1.9-2.3 l5.8,0.6l-0.1,2.9l4.1,4.8l5.7-0.7l1.1-2.8l5.4-2l2.4,1.8l6-1l5.3-3.8l1.6,2.7l6.1,1.6l10.6-7.6l1.5-0.4l-1.3,10l5.2,2.2l-1.1,1 l-4.1,13.5l-4.8,7.8v6.2l-2.8,2.4l-0.9,0.6l-8.4-6.6l-5.1,2.2l-4.9-3.6l-5.8,1.6l-7-4.3l-8,5.8v-0.1l-1.3-2.5l-2.7,1.1l-0.2-3.1 l-5.5-4.7l1.7-8.6L520.4,183.6z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Pays de la Loire", "data-code_insee", "52", 1, "region"], ["data-name", "Loire-Atlantique", "data-department", "44", "d", "m213.1,265.2l1.8-1l-2.8-4.1l-7.8-3l3-1.3 l0.6-2.2l-0.5-2.5l1.4-2.1l5.8-1.1l-5.5-0.7l-6.6,3.7l-4.1-3.2l-2.2,1l-2.2-1.2l-0.5-4.9l0.9-2.5l3-0.5l-0.9-2.2l-0.18-0.31 l13.18-3.89l0.4-6l5.2-3.4l13.2-0.4l1.6-2.9l9-3.9l6.8,3.6l7.2,13.3l-2.7-0.4l-1.9,2.4l8.5,3.3l0.3,5.9l-14.3,2.1l-2.9,2.2l3,0.8 l3.6,4.7l0.8,2.8l-2.8,4.5l2.8,1.4l0.4,3l-4.8-3.5l-1.5,2.4l-3.2,0.7l0.5,3l-2.4,2.1l-2.3-1.7v-3.1l-3.4,0.2l-0.2,9.5l-11.7-5 L213.1,265.2z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Maine-et-Loire", "data-department", "49", "d", "m270.6,269.2l-12.3,0.8l-10.6-3.8l-0.4-3 l-2.8-1.4l2.8-4.5l-0.8-2.8l-3.6-4.7l-3-0.8l2.9-2.2l14.3-2.1l-0.3-5.9l-8.5-3.3l1.9-2.4l2.7,0.4l-7.2-13.3l0.4-2.2l10.5,3.5 l2.1-1.9l8.7,3.6l3,0.4l5.9-2.7l5.1,1.7l0.6,2.7l6.7-0.2l0.2,3.5l2,2l3.1-1.3l5.2,3.3l7.4,0.1l-0.7,2.4l-1.7,9.3l-5.8,15.3v0.1 l-6.6,5.9l-2.3-2.3l-9.6,0.2l-5.6,0.8L270.6,269.2z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Mayenne", "data-department", "53", "d", "m256.6,221.5l-10.5-3.5l3.6-8.6l5.5-2.2 l-1.9-17.3l1.5-2.4l0.1-12.1l8.6,0.8h0.1l3.3,3.7l2.4-1.6l2.5,1.7l6.7-3.3h9.1l0.7-2.7l2.7,0.2l1.9,6l3.3,1.6v2.9v0.1l-4.3,2.7 l0.3,6.9l-4.4,4l1.2,2.9l-5,4.6l1.4,3.4l-5.5,7.7l1.5,5.6l-5.1-1.7l-5.9,2.7l-3-0.4l-8.7-3.6L256.6,221.5z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Sarthe", "data-department", "72", "d", "m312.7,235.3l-6.1-2.6l-7.4-0.1l-5.2-3.3 l-3.1,1.3l-2-2l-0.2-3.5l-6.7,0.2l-0.6-2.7l-1.5-5.6l5.5-7.7l-1.4-3.4l5-4.6l-1.2-2.9l4.4-4l-0.3-6.9l4.3-2.7l3,0.1l11-5.9l2.8,1.5 l1.6,8.4l7.2,5l2.9-1l3.1,3.5l3.2,0.7l2.1,3.8l-0.4,1.8v0.1l-2,2.1l1.7,2.2l0.4,5.5l-6.3,10.6l-3.2,1.1l-0.6,3.5l-7.7,4.5l-2.8-0.3 L312.7,235.3z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Vend\u00E9e", "data-department", "85", "d", "m269.3,305.1l0.2-7.4l-4.7-17.9l-4.2-4.1l-2.3-5.7l-10.6-3.8l-4.8-3.5l-1.5,2.4l-3.2,0.7 l0.5,3l-2.4,2.1l-2.3-1.7v-3.1l-3.4,0.2l-0.2,9.5l-11.7-5l-5.6-5.6l-0.3,0.1l-0.8,2.6l-3.4,4.3l-1.2,2.3l0.2,2.4l8.7,9.5l2.7,5.6 l1.2,5.3l8,5.4l3.4,0.5l3.9,4.3l2.9-0.1l2,1.2l1.8,2.5l-0.9-2.1l3.9,3.3l0.5-2.7l2.4,0.3l7.1-2.7l-1.4,2.9l6.5-0.3l2.4,1.8l9.1-4.5 L269.3,305.1z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Bretagne", "data-code_insee", "53", 1, "region", "region-53"], ["data-name", "Cotes-d\u2019Armor", "data-department", "22", "d", "m208.7,188.9l-4.9,7.1l-2.9,1.1l-1.5-2.7 l-3.5-0.9l-6.2,7.5l-1.8-6l-3,0.9l-12.9-6.5l-7.9,3l-12.46-3.29l2.06-4.11l-2.5-9.3l2.5-8.3l-3.6-4.7l1.1-4.3l1.2,1.4l3.2-0.4 l1.1-7.7l1.5-1.6l2.2-0.6l1.9,1.4h2.5l2.1-1l2.2,0.3l1.5-1.8l0.9,2L170,153l3-3.6l2.9-0.8l-0.1,2.3l-1.2,4.4l1.7-3.1l2.6-0.5l-1.1,2 l7.2,7.8l2.2,5.4l3,2l0.8,3.7l0.7-2.2l3-1l2.4-2.7l8.1-3.3l2.7-0.2l-2,2.5l2.9-1.1l1.8,4.4l1.3-1.9l2.5,0.2v-0.09l1.6,3.99h-0.3h0.3 l2.5,0.3l0.7,0.2l0.4,1.7l-1.9,13L208.7,188.9z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Finist\u00E8re", "data-department", "29", "d", "m151.6,210.1l2,3.4l-0.8,1.4l-5.5-1.2l-1.2-1.9 l2.2-0.7l-3,0.8l-0.3-2.7v2.7l-2.5,0.7l-2.2-1l-4.2-6.1l-0.8,2.5l-2.3,0.2l-3.5-3.1l1.6-4.6l-2.4,4.3l1.3,1.9l-2.2,1l-1,2.8 l-5.9-0.2l-2.1-1.6l1.5-1.6l-1.5-5.5l-2.4-3.1l-2.8-1.8l1.6-1.7l-2.1,1.4l-7.5-2.2l2.2-1.3l12.5-1.8l1.8,1.8l2-1.3l0.7-2.5l-1.6-3.6 l-6.8-2.5l-1.5,2.6l-2.6-4.2l1.3-1.8l-0.3-2.2l1.7,2.3l4.9,1l4.6-0.8l2.1,3.1l5.4,1l-3.7-0.9l-2.8-2l2.2-0.5l-4.2-2l2-1.5l-2.6-0.2 l-2.7,0.8l-0.8-2.2l7.1-4.5l-4.4,2.2l-2.3,0.1l-7.5,2.9l-2.7-1.2l-2.7,1.2l-1.5-1.8l0.6-5.3l2.5-1.6l-2.2-0.9l0.8-2.6l1.8-1.6 l2.1-0.8l5.1,1.5l-1.9-1.1l2.5-1.2l1.6,1.4l-1.9-1.7l1.2-1.9l2.9-0.1l3.8-2l2.3,2.6l6.7-3.1l3,1.6l1-2.2l2.9-0.5l0.4,5l2.2-1.5 l1.3,2.5l1.2-4.5l4.7,0.3l1.2,1.7l-1.1,4.3l3.6,4.7l-2.5,8.3l2.5,9.3l-2.06,4.11l-0.04-0.01v0.1l-6.8,3.2l0.5,3.5l3.4,5.5l8.1,1.3 l0.1,5.4l-2.5,2.8L151.6,210.1z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Ille-et-Vilaine", "data-department", "35", "d", "m255.2,207.2l-5.5,2.2l-3.6,8.6l-0.4,2.2 l-6.8-3.6l-9,3.9l-1.6,2.9l-13.2,0.4l-5.2,3.4l-1-5.8l3-0.7l-2.8-1.5l2.4-2.2l1-3.2l-2.4-1.7l1.6-2.6l-1.2-2.5l-5.1-2.8l-0.5-2.8 l3.5-0.9l-3.6-0.1l-1-4.4l4.9-7.1l9-2.5l1.9-13l-0.4-1.7l-0.7-0.2l-2.5-0.3l-1.6-3.99l0.05-0.86l0.05-0.85l0.7-0.1h2.1v0.1l1.7,4.4 l1.3,2l-0.5,2.1l1.4-2.1l-2.3-5.1l0.7-2.5l2.2-1.5l2.3-0.6l2.2,1l-1.5,2.3l2.9,2.4l7.3-0.6l4.7,9.6l2.7,1l7.1-4.8l5.4,2.3l-0.1,12.1 l-1.5,2.4L255.2,207.2z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Morbihan", "data-department", "56", "d", "M167.7,242.6l2.9,1.2l-1.1,2.1l-5.1-1.2l-1.3-2.7l0.4-3l2.1,1.4L167.7,242.6z M209.1,219.2l2.4-2.2l1-3.2 l-2.4-1.7l1.6-2.6l-1.2-2.5l-5.1-2.8l-0.5-2.8l3.5-0.9l-3.6-0.1l-1-4.4l-2.9,1.1l-1.5-2.7l-3.5-0.9l-6.2,7.5l-1.8-6l-3,0.9 l-12.9-6.5l-7.9,3l-12.46-3.29l-0.04,0.09l-6.8,3.2l0.5,3.5l3.4,5.5l8.1,1.3l0.1,5.4l-2.5,2.8l-2.8-0.8l2,3.4l0.1,1.5l2.9,4.4 l2.3-0.2l1.5-1.7l-0.8-5.1l0.6,2.4l1.7,1.7l1.9-1.7l-2.5,4.2l2.2,1.4l-2.3-0.6l3.2,1.9l0.1,0.1l1.6,1l1.7-2.5l-1.6,3.1l2.1,2.6 l0.6,3.5l-0.9,2.8l2.1,1.1l-1.2-3l0.5-3.8l2.2,1.6l5.1,0.1l-0.7-5l1.4,2l2.1,1.5l4.8-0.5l2.1,2.4l-1,2.2l-2.1-0.6l-4.8,0.4l3.8,3.3 l12.9-0.9l3.1,1.5l-3.4,0.1l1.42,2.39l13.18-3.89l0.4-6l-1-5.8l3-0.7L209.1,219.2z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Nouvelle-Aquitaine", "data-code_insee", "75", 1, "region"], ["data-name", "Charente", "data-department", "16", "d", "m294.8,379.2l-2,2v-0.1l-6.3-6.3l-6-1.2l1.7-3l-2.3-2l2.4-1.7l-1.5-2.6 l1.7-2.6l-2.4-1.7l-0.3-3l-5-3.1l2.2-2.1l-3.2-5.6l8.1-3.3l2.3,2l2.7-0.1l2.7-11.6l2.7-1.6l0.3-3l5.8-2.5l3.5,0.4l0.8-0.8h0.1l9.1,3 l2.9-0.8l-1.4-2.4l2.2-1.8l4.1,3.9l3.8-1.4l1.3-2.5l4.8,0.6l-0.2,5.1l4.7,3.6l-0.6,3.2l-2.6,1.1l-4,8l-2.8,0.6l-3.4,3.8h0.1 l-5.7,6.1l-2.1,5.3l-7.9,5.9l-0.7,5.7l-4.1,5.8L294.8,379.2z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Charente-Maritime", "data-department", "17", "d", "M242.8,341.1l-1.4-5l-3.5-3l-1.3-2.3l1.5-3.6l1.7,1.8l2.9,0.5l1.4,8.4L242.8,341.1z M241.9,318.9l-5.8-4.5 l-4.4-1.5l-0.6,2.9l2.7,0.1l4.8,3.3L241.9,318.9z M286.5,374.8l-6-1.2l1.7-3l-2.3-2l2.4-1.7l-1.5-2.6l1.7-2.6l-2.4-1.7l-0.3-3 l-5-3.1l2.2-2.1l-3.2-5.6l8.1-3.3l2.3,2l2.7-0.1l2.7-11.6l-3.6-4.7l-17.4-6.7l-5.9-6.5v-3.7l-2.4-1.8l-6.5,0.3l1.4-2.9l-7.1,2.7 l0.5,0.1l-0.6,3.4l-4.5,5.9l2.4,0.3l2.2,1.7l3,7.2l-1.5,1.9l-0.2,5.1l-3.3,3.1l-0.1,2.6l-2.2,0.4l-1.5,1.7l1.1,4.3l9,6.5l1.5,2.6 l4.3,2.7l3.7,4.8l1.81,7.3l3.79-0.5l0.7,2.8l6.4,1.7l0.6,5.8l6.1,4.3l9.4,1l2-5l0.1-0.4v-0.1L286.5,374.8z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Corr\u00E8ze", "data-department", "19", "d", "m363.6,392.3l-8.1,0.8l-3.5-7l-3.2-0.7l-0.2-3 l-2.3-1.5l2-1.8l-1.7-3l3.6-4.6l-2.9-4.7l1.6-2.7l2.5,1.2l4.7-4l5.7-1.3l4.9-4.6l8.7-4l7-3.4l11.2,5.2l2.3-2.6l2.7,0.8l2.4-2.4 l1.2,5.6l-1.7,2.4l1.2,7.9l0.7,6l-6.2-2l-0.6,3.5l-7.6,9.5l1.8,2.2l-2.3,1.9l-0.3,3.5l-3.1,1.1l1.5,3.4l-3.2,1.9h-0.1l-6.7-0.2 l-5.3,2.7L363.6,392.3z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Creuse", "data-department", "23", "d", "m396.6,343.5l4.4,5.5l-2.4,2.4l-2.7-0.8 l-2.3,2.6l-11.2-5.2l-7,3.4l-0.6-5.9l-4.7-3l-6.4-0.5l-0.1-2.8l-2.9-1.5l0.9-3.4l-1.8-5.2l-6.6-9.8l3-5.3l-1.2-2.6l2.8-2.9l11.5-1.1 l1.9-2.5l13.2,2.7l2.7-0.8l4.9,0.2l1.1,3.9c2.5,1.6,4.9,3.2,7.4,4.8l3.6,8.4l-0.5,4.1l2.3,6.7L396.6,343.5z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Dordogne", "data-department", "24", "d", "m307.7,414.3l-2.8-6.4l-1-1.3l0.9-2.9l-2.4-2.6l-2,3.2l-9.8-2.3l2-2 l0.2-5.7l2.8-5.5l-1.2-2.8l-3.7,0.6l2-5l0.1-0.4l2-2l5.5-0.7l4.1-5.8l0.7-5.7l7.9-5.9l2.1-5.3l5.7-6.1l6.2,3l-0.1,4.7l9.5-1.1 l7.2,5.6l-2,2.7l5.7,2.2l2.9,4.7l-3.6,4.6l1.7,3l-2,1.8l2.3,1.5l0.2,3l3.2,0.7l3.5,7l-0.7,5l-1.4,5.3l-4.5,3.2l0.6,3.6l-6,3.4 l-4.7,6.5l-4.2-4.2l-5.4,2.7l-1.5-6l-6.1,1l-2.2-1.8l-2.8,2L307.7,414.3z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Gironde", "data-department", "33", "d", "m243.9,420.1l-5.8,2.6v-4.6l2.2-3.2l0.5-2.3 l1.9-1.7l1.8,1.4l3.1-0.2l-1.1-4.6l-3.5-3.4l-2.8,4l-1.2,3.8l6.2-50l0.9-2.8l3.3-3.4l1.4,4.7l9,9l2.8,7.6l1.7-3.1l-0.59-2.4 l3.79-0.5l0.7,2.8l6.4,1.7l0.6,5.8l6.1,4.3l9.4,1l3.7-0.6l1.2,2.8l-2.8,5.5l-0.2,5.7l-2,2l9.8,2.3l2-3.2l2.4,2.6l-0.9,2.9l1,1.3 l-3.1-0.1l-1.2,2.5l-2.7-0.9l-1.1,3.3l2.9,1.4l-8.5,8.6l-0.6,8.9l-3,2.3l1.5,2.5l-4.5,4l-2.1-2.7l-1.6,3.6h-6.4l-0.6-4.7l-11-7.7 l0.4-2.8l-17.2,0.7l1.5-5.4L243.9,420.1z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Landes", "data-department", "40", "d", "m222.32,481.21l1.08-1.51l3.9-7.1l8.8-37.8 l2-11.7v-0.4l5.8-2.6l3.7,1.3l-1.5,5.4l17.2-0.7l-0.4,2.8l11,7.7l0.6,4.7h6.4l1.6-3.6l2.1,2.7l0.4,4.6l11.7,2.9l-3.6,5.2l0.7,2.6 l-0.4,2.9l-2.5,1.3l-0.6-3l-9.4,2.7l0.5,6.4l-4.2,11.1l1.6,2.7l-8.6,1.5l-3.3-1.1l-4.8,1.9l-2.2-2l-2.3,1.5l-2.5-2.3l-9.8,2 l-1.6,2.2l-2.5-1.4l-2.7,1.3l-1.2-2.8l-11,2.5L222.32,481.21z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Lot-et-Garonne", "data-department", "47", "d", "m293.8,455.6v0.1l-0.7-2.6l3.6-5.2L285,445 l-0.4-4.6l4.5-4l-1.5-2.5l3-2.3l0.6-8.9l8.5-8.6l-2.9-1.4l1.1-3.3l2.7,0.9l1.2-2.5l3.1,0.1l2.8,6.4l8.9-0.5l2.8-2l2.2,1.8l6.1-1 l1.5,6l5.4-2.7l4.2,4.2l-3.4,3.1l2.7,9.1l-7.5,2v2.9l2.4,1.4l-4.4,5.5l1.3,2.7l-2.8-0.2l-3.6,4.7l-2.7,1.3l-8.6-1l-5,2.9l-8.3-0.7 l-1.4,2.5L293.8,455.6z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Pyr\u00E9n\u00E9es-Atlantiques", "data-department", "64", "d", "m276.9,513.4l3.4-0.8l-0.4-2.9l8-9.3l-0.8-3.1 l2.7-1.4l-0.5-7.2h-2.9l1.5-2.8l-2.5-5.8l-6.6-0.3l-8.6,1.5l-3.3-1.1l-4.8,1.9l-2.2-2l-2.3,1.5l-2.5-2.3l-9.8,2l-1.6,2.2l-2.5-1.4 l-2.7,1.3l-1.2-2.8l-11,2.5l-3.98-1.89l-3.52,4.89l-2.7,1.9l-4.5,0.9l1.9,4.5l4.5-0.2l0.2,2.2l2.4,1l2.2-2.1l2.4,1.3l2.5,0.1 l1.4,2.8l-2.5,6.7l-2.1,2.2l1.3,2.2l4.3-0.1l0.7-3.4l2.3-0.1l-1.3,2.4l5.9,2.3l1.5,1.8h2.5l6.1,3.8l5.8,0.4l2.3-1l1.4,2.1l0.3,2.8 l2.7,1.3l3.9,4l2.1,0.9l1.1-2.1l2.7,2.1l3.6-1.1l0.19-0.16l1.41-9.34L276.9,513.4z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Deux-S\u00E8vres", "data-department", "79", "d", "m292.3,331.6l-2.7,1.6l-3.6-4.7l-17.4-6.7 l-5.9-6.5v-3.7l9.1-4.5l-2.5-2l0.2-7.4l-4.7-17.9l-4.2-4.1l-2.3-5.7l12.3-0.8l3.7-4.8l5.6-0.8l9.6-0.2l2.3,2.3l3.4,9l-0.8,3l2.7,1.2 l-4.5,14.1l2.7-0.9l1.5,3l-3.4,5.5l0.5,5.8l2.1,2l-0.1,2.8l6.4,0.2l-3.2,8.5l4.5,3l-0.8,2.8h-0.1l-0.8,0.8l-3.5-0.4l-5.8,2.5 L292.3,331.6z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Vienne", "data-department", "86", "d", "m329.6,320.8v3.5l-4.8-0.6l-1.3,2.5l-3.8,1.4 l-4.1-3.9l-2.2,1.8l1.4,2.4l-2.9,0.8l-9.1-3l0.8-2.8l-4.5-3l3.2-8.5l-6.4-0.2l0.1-2.8l-2.1-2l-0.5-5.8l3.4-5.5l-1.5-3l-2.7,0.9 l4.5-14.1l-2.7-1.2l0.8-3l-3.4-9l6.6-5.9l5.5,3.2l0.3,3.2l2.9-0.3l1.3,6.1l2.8,1.4l10-0.4l-1.4-2.9l5.3,3l0.3,3.1l7.1,10l2.1,3 l-0.8,5.8l4.6,4.4h2.9l2.6,5.4l2.5,1.3l-1.5,2.8l-0.8-0.3l-1.3,2.4l-3.3-0.9l-1.3,3l-5.6,2.7L329.6,320.8z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Haute-Vienne", "data-department", "87", "d", "m348.9,364.1l-1.6,2.7l-5.7-2.2l2-2.7l-7.2-5.6 l-9.5,1.1l0.1-4.7l-6.2-3h-0.1l3.4-3.8l2.8-0.6l4-8l2.6-1.1l0.6-3.2l-4.7-3.6l0.2-5.1v-3.5l3-5l5.6-2.7l1.3-3l3.3,0.9l1.3-2.4 l0.8,0.3l2.6,1.1l5.8-1.1l1.7,2.5l1.2,2.6l-3,5.3l6.6,9.8l1.8,5.2l-0.9,3.4l2.9,1.5l0.1,2.8l6.4,0.5l4.7,3l0.6,5.9l-8.7,4l-4.9,4.6 l-5.7,1.3l-4.7,4L348.9,364.1z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Occitanie", "data-code_insee", "76", 1, "region"], ["data-name", "Ari\u00E8ge", "data-department", "09", "d", "m369.82,543.59l0.78-0.89l-2.6-1.1l-2-2.1 l-3.7-0.1l-1.7-1.7l-2.8,0.4l-1.3,2.1l-2.4-0.8l-2.8-5.9l-10-0.6l-1.3-2.8l-13.2-3.9l-0.5-1.4l3.8-5.2l2.8-1v-5.9l3.9-4l2.8-1.1 l6.2,4.1l-0.4-5.6l5.4-1.6l-3-4.8l2.8-1.1l3.4,5.5l2.8-0.5l0.6-2.8l5.7,2.2l2-2.3l2.2,5.5l8.7,3.9l2.2,5.2l0.2,3.1l-2.2,2.3l2.4,2.5 l-1.2,3l-3.2,0.6l0.8,5.7l3.4,1.5l3.3-1.2l4.8,5.6l-7.4,0.2l-1.3,2.6L369.82,543.59z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Aude", "data-department", "11", "d", "m435.07,504.37l-1.47,1.53l-5.2,9.3l-0.9,3.5 l0.15,9.57l-9.45-5.57l-8.2,5.4l-13.6-1l-2.7,1.4l1.4,6l-8.6,3.9l-4.8-5.6l-3.3,1.2l-3.4-1.5l-0.8-5.7l3.2-0.6l1.2-3l-2.4-2.5 l2.2-2.3l-0.2-3.1l-2.2-5.2l-8.7-3.9l-2.2-5.5l8.4-10l1.4,2.7l5.2-1.8l0.5-0.8l1.8,2.3l6.3,0.9l1.1-3.3l2.8-0.5l12,1.4l-0.5,2.8 l3.5,5l2.5-1.6l1.4,2.9l3.1-0.8l3.8-5.3l1,2.9l13.8,4.7l1.7,2L435.07,504.37z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Aveyron", "data-department", "12", "d", "m430.8,440.7l9.4,4.5l-2,3.9l-2.8,1.1l8.4,4.1 l-4.3,5.3l0.3,1.5l-3.7,1l-3,5.3l-6.3-1.3l-0.1,8.7l-5.7-0.1l-1.3-2.8l-11.1-1.3l-4.2-5l-4.3-11.5l-4.8-4.3L385,444l-6.1,2.8 l-4.3-3.6l2.3-2.4l-3.1-2.7l0.4-3l-0.8-9.1l7.6-5l5.9-1.4l1.7-1.5h0.1l5.1-3.2l6.4,1.5l3.8-4.8l3-9.1l4.7-4.2l5.2,4l1.3,4.2l2.4,1.6 l-0.5,3l2.6,5.1v0.1l4.2,4.5l2.9,8.8l-0.5,8.7L430.8,440.7z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Gard", "data-department", "30", "d", "m480,487.2l-2.8-0.6l-1.9-1.6l-1.1-3.4h-0.1 l3.3-4.4l-1.5-3l-6.1-6.7l-3-0.2l-0.2-3l-6.8-1.4l0.9-2.7l-1.9-2.6l-3.9,0.6l-4.2,3.9l-0.1,2.8l-5.3-2.5l-2.2,1.7l-0.4-2.9l-2.9-0.1 l-0.3-1.5l4.3-5.3l-8.4-4.1l2.8-1.1l2-3.9l7.8,3.4l3.9-0.5l0.1-3.3l8.7,2.2l6.3-1.8l-1.4-3l1.2-2.9l-3.9-7.7l3.6-2.5l1.1-2.1 l2.7,5.9l7.8,5l7.1-4.3l0.1,3.1l2.5-2.3h2.8l6,3.5l2.6,4.4l0.2,5.5l6.3,6.4l-4.5,5l-3.9,4.1l-1.9,10.6l-3.3-0.9l-4.2,4.8l1,2.7 l-5.8,1.8L480,487.2z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Haute-Garonne", "data-department", "31", "d", "m326.8,526.2l-5.5-1.5l-1.2,2.4l0.2,7.6 l-8.8-0.7l-1.7,0.3l-0.6-7l5.5-3.2l2.6-5.3l-0.8-2.7l-3.1,0.3l0.6-3.5l-4.6-4l7.1-11.2l3.1-1.1l3.5-5.3l11.4,2.5l0.7-5.8l6.5-6.1 l-9.1-13.3l9.9-0.9l1.7,2.3l5.8-2.5l-2.2-2.3l11.7-4.3l1.4,6.3l2.6,1.2l0.2,2.8l2.3,2.1l-0.7,5.4l14.3,9.3l1,2.8l-0.5,0.8l-5.2,1.8 l-1.4-2.7l-8.4,10l-2,2.3l-5.7-2.2l-0.6,2.8l-2.8,0.5l-3.4-5.5l-2.8,1.1l3,4.8l-5.4,1.6l0.4,5.6l-6.2-4.1l-2.8,1.1l-3.9,4v5.9 l-2.8,1l-3.8,5.2L326.8,526.2z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Gers", "data-department", "32", "d", "m330.6,461.7l2,6.9l9.1,13.3l-6.5,6.1l-0.7,5.8 l-11.4-2.5l-3.5,5.3l-3.1,1.1l-12.4-2.2l-1.4-3l-5.5,0.6l-2.6-8.7l-3.3-1.3l-2-3.5l-3.9,0.5l-6.6-0.3l-1.6-2.7l4.2-11.1l-0.5-6.4 l9.4-2.7l0.6,3l2.5-1.3l0.4-2.9v-0.1l3.7,0.7l1.4-2.5l8.3,0.7l5-2.9l8.6,1l2.7-1.3l5.3,1.7l-3.3,4.6L330.6,461.7z", 1, "departement", 3, "ngClass", "click"], ["data-name", "H\u00E9rault", "data-department", "34", "d", "m474.1,481.6l-2.4-0.1l-5.9,2.6l-3.6,3.2 l-7.2,4.6l-4.3,4.2l2.1-3.5l-4.3,6.6h-6.8l-5.5,4l-1.13,1.17l-0.17-0.17l-1.7-2l-13.8-4.7l-1-2.9l-3.8,5.3l-3.1,0.8l-1.4-2.9 l-2.5,1.6l-3.5-5l0.5-2.8l3.4-2l0.8-3l-0.7-9.7l6.1,2.2c2.3-1.5,4.6-2.9,6.8-4.4l5.7,0.1l0.1-8.7l6.3,1.3l3-5.3l3.7-1l2.9,0.1 l0.4,2.9l2.2-1.7l5.3,2.5l0.1-2.8l4.2-3.9l3.9-0.6l1.9,2.6l-0.9,2.7l6.8,1.4l0.2,3l3,0.2l6.1,6.7l1.5,3L474.1,481.6z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Lot", "data-department", "46", "d", "m385.4,413.1l3.3,5h-0.1l-1.7,1.5L381,421 l-7.6,5l0.8,9.1l-6.2,0.8l-7.5,5.5l-2.6-2.3l-8.7,2.5l-0.5-4l-2.4,1.5l-2.7-1l-4.5-4l2.1-2.3l-3.1,0.5l-2.7-9.1l3.4-3.1l4.7-6.5 l6-3.4l-0.6-3.6l4.5-3.2l1.4-5.3l0.7-5l8.1-0.8l6.7,6.1l5.3-2.7l6.7,0.2l1,5.4l3.8,6L385.4,413.1z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Loz\u00E8re", "data-department", "48", "d", "m463.4,418.7l4.2,8.3l-1.1,2.1l-3.6,2.5 l3.9,7.7l-1.2,2.9l1.4,3l-6.3,1.8l-8.7-2.2l-0.1,3.3l-3.9,0.5l-7.8-3.4l-9.4-4.5l-1.5-2.4l0.5-8.7l-2.9-8.8l-4.2-4.5v-0.1l6.9-15.9 l1.7,2.3l6.8-5.7l1-1l2.3,1.7l1.5,5.7l6.4,1.2l0.1-2.8l2.9,0.2l9,7.7L463.4,418.7z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Hautes-Pyr\u00E9n\u00E9es", "data-department", "65", "d", "m314.7,524.1l-5.5,3.2l0.6,7l-0.7,0.2l-2.3-1.6 l-2.4,1.8l-2.5-0.5l-1.9-1.7l-3.9-0.3l-6.9,2.1l-2.2-0.9l-2.1-1.7l-1.1-2.5l-7.8-5.5l-2.11,1.84l1.41-9.34l1.6-2.8l3.4-0.8l-0.4-2.9 l8-9.3l-0.8-3.1l2.7-1.4l-0.5-7.2h-2.9l1.5-2.8l-2.5-5.8l3.9-0.5l2,3.5l3.3,1.3l2.6,8.7l5.5-0.6l1.4,3l12.4,2.2l-7.1,11.2l4.6,4 l-0.6,3.5l3.1-0.3l0.8,2.7L314.7,524.1z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Pyr\u00E9n\u00E9es-Orientales", "data-department", "66", "d", "m427.65,528.27l0.25,15.63l3.9,3.3l1.9,3.8 h-2.3l-8.1-2.7l-6.9,3.9l-3-0.2l-2.4,1.1l-0.6,2.4l-2.1,1.2l-2.4-0.7l-2.9,1l-4-3.1l-7-2.9l-2.5,1.4h-3l-1,2.1l-4.6,2l-1.9-1.7 l-1.7-4.8l-7.5-2l-2-2.1l2.02-2.31l7.98-2.39l1.3-2.6l7.4-0.2l8.6-3.9l-1.4-6l2.7-1.4l13.6,1l8.2-5.4L427.65,528.27z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Tarn", "data-department", "81", "d", "m419.7,471.9l1.3,2.8c-2.2,1.5-4.5,2.9-6.8,4.4 l-6.1-2.2l0.7,9.7l-0.8,3l-3.4,2l-12-1.4l-2.8,0.5l-1.1,3.3l-6.3-0.9l-1.8-2.3l-1-2.8l-14.3-9.3l0.7-5.4l-2.3-2.1l-0.2-2.8l-2.6-1.2 l-1.4-6.3l0.5-2.8l4.8-3.2l1-2.7L364,450l3-1.1l2.7,1.1l9.2-3.2l6.1-2.8l10.3,5.8l4.8,4.3l4.3,11.5l4.2,5L419.7,471.9z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Tarn-et-Garonne", "data-department", "82", "d", "m360,458.1l-0.5,2.8l-11.7,4.3l2.2,2.3 l-5.8,2.5l-1.7-2.3l-9.9,0.9l-2-6.9l-5.1-4.1l3.3-4.6l-5.3-1.7l3.6-4.7l2.8,0.2l-1.3-2.7l4.4-5.5l-2.4-1.4v-2.9l7.5-2l3.1-0.5 l-2.1,2.3l4.5,4l2.7,1l2.4-1.5l0.5,4l8.7-2.5l2.6,2.3l7.5-5.5l6.2-0.8l-0.4,3l3.1,2.7l-2.3,2.4l4.3,3.6l-9.2,3.2l-2.7-1.1l-3,1.1 l1.8,2.2l-1,2.7L360,458.1z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Auvergne-Rhone-Alpes", "data-code_insee", "84", 1, "region"], ["data-name", "Ain", "data-department", "01", "d", "m542,347l-5.7,6.7l-11.2-15.2l-2.8,0.7l-3,5.1 l-6-2l-6.4,0.5l-3.7-5.7l-2.8,0.5l-3.1-9.2l1.5-8l5.9-20.9l5.9,1.5l5.4-1.3l4.8,3.3l4.3,7.7h2.9l0.1,3l2.9-0.1l4-4.4l3.4,1.6 l0.4,2.8l3.8-0.2l5.5-3.2l5.3-7.2l4.5,2.7l-1.8,4.7l0.3,2.5l-4.4,1.5l-1.9,2l0.2,2.8l0.46,0.19l-4.36,4.71h-2.9l0.8,9.3L542,347z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Allier", "data-department", "03", "d", "m443.1,292.3l5.9-6l6.7,13.5l7.9,2.9l1.6,2.4l-0.5,5.5l-3.7,4.6 l-3.9,1.3l-0.5,3l1.5,12.4l-5.5,4.8l-3.5-4.3l-6.4-0.4l-1.4-3.2l-13.1-0.5l-1.6-2.5l-3.3,0.5l-4.4-4.5l1.2-2.8l-2.3-1.7l-11.2,8 l-2.5-1.2l-3.6-8.4c-2.5-1.6-4.9-3.2-7.4-4.8L392,307v-0.1l3.5-5.9l8.7-1l1.7-2.4l-1.7-5.3l2.3-1.9l8.4-2.9l4.8-3.7h4h0.1l5.7,6.3 l6.4,0.2l2.8-1.7L443.1,292.3z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Ard\u00E8che", "data-department", "07", "d", "m496.5,434.2l0.1,3.7l-6-3.5h-2.8l-2.5,2.3 l-0.1-3.1l-7.1,4.3l-7.8-5l-2.7-5.9l-4.2-8.3l-2.1-9.1l6.7-6.4l5.9-1.9l3.4-5.9l3.4-0.4l-0.7-2.8l2.6-2.3l1.5-5.2l2.6,1.2v-3.1 l0.9-4.1l3.5-0.8l3.2-4.9l5-2.7l2,4.2l0.5,10.3l3.8,11.3l-1.5,6.2l-3.5,4.5l1,7.1l-3,5.9L496.5,434.2z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Cantal", "data-department", "15", "d", "m435.6,387.9l3.5,8l-1,1l-6.8,5.7l-1.7-2.3 l-6.9,15.9l-2.6-5.1l0.5-3l-2.4-1.6l-1.3-4.2l-5.2-4l-4.7,4.2l-3,9.1l-3.8,4.8l-6.4-1.5l-5.1,3.2l-3.3-5l1.7-5.8l-3.8-6l-1-5.4h0.1 l3.2-1.9l-1.5-3.4l3.1-1.1l0.3-3.5l2.3-1.9l-1.8-2.2l7.6-9.5l0.6-3.5l6.2,2l-0.7-6l7.5,3.5l1.5,2.5l6.7,0.3l6.5,5.4l3.7-4.1v3.9 l5.5,1.5l3.3,8.7l2.6,1.1L435.6,387.9z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Drome", "data-department", "26", "d", "m535.1,404.4l-3,0.5l-0.8-17.5l-3,1.7l-8.2-1.9 l-2.7,1l1.1-6.3l-3.3-7.8l-4.9-2.7l-9,3.1l0.5,10.3l3.8,11.3l-1.5,6.2l-3.5,4.5l1,7.1l-3,5.9l-2.1,14.4l5.9,0.7l3.5,4.2l8.7-3.9 l2.4,1.4l2.5-2.2l0.5,5.8l9.3,0.9l0.1,2.8l5.2,2.3l4.3-4.8l2.3-0.1l1-0.2l0.2-4.7l-10-5.7l-1.5-2.6l3.2-5.1l4.2,1.4l2.5-2.5l-3-2.3 l2.5-6.7l5.8-0.3l0.3-3.4l-5.9-0.8L535.1,404.4z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Is\u00E8re", "data-department", "38", "d", "m513.6,349.4l-0.3-7.1l6,2l3-5.1l2.8-0.7 l11.2,15.2l6.5,10.5l6.2,0.2l0.3-2.8l9.4,2.1l2.7,6.3l-2.3,5.5l1,5.4l5.2,1.5l-1.6,3.8l1.8,4.2l4.4,3.1l-0.4,5.8l-3.1-1.1l-12.6,3.9 l-0.9,2.8l-5.5,1.2l-1,3.1l-5.9-0.8l-5.4-4l-3,0.5l-0.8-17.5l-3,1.7l-8.2-1.9l-2.7,1l1.1-6.3l-3.3-7.8l-4.9-2.7l-9,3.1l-2-4.2v-4.4 l-0.2-1.1h0.1l4.4-3.9l-1.9-2.5l2.5-2.5l6.9-1.5L513.6,349.4z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Loire", "data-department", "42", "d", "m499.3,365.9v4.4l-5,2.7l-3.2,4.9l-3.5,0.8 l-2.2-2.4l-2.6,1l-0.7-5.5l-6-2.2l-6.2,3l-2.8,0.4l-2.3-2l-2.8,0.8l3-7.1l-2.7-7.5l-4.6-3.8l-4.7-7.7l2.1-6.3l-2.5-2.7l5.5-4.8 l-1.5-12.4l0.5-3l3.9-1.3v3l5.2,3.3l8-1.5l2.1,2.1l5.7-3.8l0.01-0.09l2.09,2.99l-4.9,3.5l-1.6,8.6l5.2,6.7l-1.7,5.9l2.3,1.6 l-1.3,2.5l1.1,3l4.6,4.1l5.9,2.1l0.9,3l4.6,2.6h-0.1L499.3,365.9z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Haute-Loire", "data-department", "43", "d", "m485.4,376.3l2.2,2.4l-0.9,4.1v3.1l-2.6-1.2 l-1.5,5.2l-2.6,2.3l0.7,2.8l-3.4,0.4l-3.4,5.9l-5.9,1.9l-6.7,6.4l-9-7.7l-2.9-0.2l-0.1,2.8l-6.4-1.2l-1.5-5.7l-2.3-1.7l-3.5-8 l3.4-0.2l-2.6-1.1l-3.3-8.7l-5.5-1.5v-3.9v-0.1l9.6-3.2l8.5,0.1l5.2,3.2l11.1-0.7l2.8-0.8l2.3,2l2.8-0.4l6.2-3l6,2.2l0.7,5.5 L485.4,376.3z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Puy-de-Dome", "data-department", "63", "d", "m449.1,332.4l3.5,4.3l2.5,2.7l-2.1,6.3l4.7,7.7 l4.6,3.8l2.7,7.5l-3,7.1l-11.1,0.7l-5.2-3.2l-8.5-0.1l-9.6,3.2v0.1l-3.7,4.1l-6.5-5.4l-6.7-0.3l-1.5-2.5l-7.5-3.5l-1.2-7.9l1.7-2.4 L401,349l-4.4-5.5l9.3-8.6l-2.3-6.7l0.5-4.1l2.5,1.2l11.2-8l2.3,1.7l-1.2,2.8l4.4,4.5l3.3-0.5l1.6,2.5l13.1,0.5l1.4,3.2L449.1,332.4z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Rhone", "data-department", "69", "d", "m493.1,312.7l5.7,7.7l-1.5,8l3.1,9.2l2.8-0.5 l3.7,5.7l6.4-0.5l0.3,7.1l-2.5,5l-6.9,1.5l-2.5,2.5l1.9,2.5l-4.4,3.9l-4.6-2.6l-0.9-3l-5.9-2.1l-4.6-4.1l-1.1-3l1.3-2.5l-2.3-1.6 l1.7-5.9l-5.2-6.7l1.6-8.6l4.9-3.5l-2.09-2.99l0.29-2.91l2.3-1.9l2.2,1.7l2.2-1.6l2.5,1.5L493.1,312.7z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Savoie", "data-department", "73", "d", "m603.7,362l-1,10.3l-3.1,1.4l-2.2,0.7l-4.5,3.4 l-1.5,2.4l-2.5-1.4l-5.1,1.3l-2,1.8v0.1l-6.8,1.9l-2,2l-7.7-3.5l-5.2-1.5l-1-5.4l2.3-5.5l-2.7-6.3l-9.4-2.1l-0.3,2.8l-6.2-0.2 l-6.5-10.5l5.7-6.7l2.3-13.6l2.7,6.7l2.7,0.9l1.3,2.5l3,1.7l2.6-1.6l3.2,0.8l4.6,3.6l9.4-13.9l2.4,1.6l-0.6,3l2.3,1.8l6.2,2.3 l2.2-1.5l0.62-0.76l1.88,4.66l2.7,1.1l1.5,1.9l2.8,0.4l-0.7,3l1.3,5.2l5.1,4L603.7,362z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Haute-Savoie", "data-department", "74", "d", "m547,340.1l-2.7-6.7l-0.8-9.3h2.9l4.36-4.71 l2.24,0.91l2.3-1l2.3,0.1l3.4-3.5l2.1-1l1-2.3l-2.8-1.3l1.8-5.1l2.4-0.8l2.3,1l3.6-2.9l9.5-1.3l3.2,0.6l-0.5,2.7l4.2,4.1l-2.1,6.4 l-0.6,1.5l4.6,1.7l-0.1,4.8l2-1.4l4.6,6.6l-1.3,5l-2.5,1.7l-4.9,0.9l-0.6,3.7l0.02,0.04l-0.62,0.76l-2.2,1.5l-6.2-2.3l-2.3-1.8 l0.6-3l-2.4-1.6l-9.4,13.9l-4.6-3.6l-3.2-0.8l-2.6,1.6l-3-1.7l-1.3-2.5L547,340.1z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Provence-Alpes-Cote d'Azur", "data-code_insee", "93", 1, "region"], ["data-name", "Alpes-de-Haute-Provence", "data-department", "04", "d", "m596.5,409.9l0.57-0.5l-0.37,4.5l-2.2,1.5 l-0.6,2.9l3.5,4l-1.8,4.8l0.19,0.21L589,435.1l-2,5.3l4.3,8.5l7,7.7l-5.2-0.6l-5.2,3.8l1.2,2.6l-3,1.4l-9.8,0.4l-1.2,3.5l-5.9-3.6 l-10.1,8.5l-4-4.8l-2.7,1.8l-5.3-0.2l-6.1-6l-3.4-1.1l1.7-2.5l-3.7-5.2l1.2-3l-2.2-5.4l4.3-4.8l2.3-0.1l1-0.2l5.9-1.4l3.8,1 l-3.4-4.9l3.9,1.1l1.4-8.6l5.3-4l3.3-0.7l3.5,4.5l0.7-3.8l3.8-4.2l11.1,3.3l9-10.2L596.5,409.9z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Hautes-Alpes", "data-department", "05", "d", "m597.1,409l-0.03,0.4l-0.57,0.5l-6,3.3l-9,10.2 l-11.1-3.3l-3.8,4.2l-0.7,3.8l-3.5-4.5l-3.3,0.7l-5.3,4l-1.4,8.6l-3.9-1.1l3.4,4.9l-3.8-1l-5.9,1.4l0.2-4.7l-10-5.7l-1.5-2.6 l3.2-5.1l4.2,1.4l2.5-2.5l-3-2.3l2.5-6.7l5.8-0.3l0.3-3.4l1-3.1l5.5-1.2l0.9-2.8l12.6-3.9l3.1,1.1l0.4-5.8l-4.4-3.1l-1.8-4.2 l1.6-3.8l7.7,3.5l2-2l6.8-1.9l1.8,4.5l2.4,0.6l1.1,2l0.4,3l1.2,2.2l3,2.3l5.7,0.5l2.2,1.3l-0.7,2.1l3.2,4.7l-3,1.5L597.1,409z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Alpes-Maritimes", "data-department", "06", "d", "m605.3,477.1l-3.2-0.1l-1.3,1.8l-0.1,2.2 l-0.42,0.77l-2.18-3.97l0.8-2.9l-5.6-2.6l-1.7-5.6l-5.5-2.9l3-1.4l-1.2-2.6l5.2-3.8l5.2,0.6l-7-7.7l-4.3-8.5l2-5.3l6.79-7.79 l6.91,7.79l6.9,1.6l4.2,2.8l2.5-0.4l1.8,1.4l10.3-2.4l2.7-1.8l-0.3,2.6l1.5,2.2l0.3,3.2l-1.6,1.9l-0.2,2.3l-2.7,1.6l-3.3,5l-0.5,1.6 l1.1,2.7l-1.1,2.7l-3.5,2.9l-2.3,0.5l-0.9,2.4l-3-0.9l-1.5,2.1l-2.3,0.5L609,472l0.1,2.8l-2.4,0.6L605.3,477.1z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Bouches-du-Rhone", "data-department", "13", "d", "m545,500.2l2.5-2l-2.2-6.3l1.1-2.6l2.7-0.5 l-5.5-9.1l2-5.3l3.3-0.8l-1.9-3.8l-0.1-0.1l-6.6,4.3l-3.2,0.2l-12-4.8l-3.5,0.7l-4.5-2.3l-5.5-5.7l-10.4-2.9l-3.9,4.1l-1.9,10.6 l-3.3-0.9l-4.2,4.8l1,2.7l-5.8,1.8l-3.1,4.9l0.2,0.1h13.2l2.2,0.9l1,2.2l-1.6,1.5l2.2,1.4l7.4,0.1l3.2,1.3l1.8-1.7l-1.5-2.8l0.4-2.4 l4.9,1l3,5.3l10-0.8l2.6-1.1l1.8,2l-0.2,2.5l1,2l-1.2,2.2h9.2l1.3,2l2.2-0.8l1.7,0.2L545,500.2z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Var", "data-department", "83", "d", "m600.28,481.77l-1.38,2.53l-6.8,1.7l-0.7,2.5 l-5.5,5.7l5,0.7l-2,4.8l-4,0.2l-4.8,2.5l-3.5,1.1l0.1,2.7l-4.9-1.5l-2.7,0.5l-1.6,1.6l-0.4,2.3l-2.2,1.6l1.4-1.8l-2.4-1.7l-2.2,0.7 l-1.6-1.6l-3.1,0.1l0.9,2.2l-2.3-0.4l-1.5,1.7l-3-1.1l0.6-2.3l-6.4-4.1l-0.5-0.1l0.2-2.1l2.5-2l-2.2-6.3l1.1-2.6l2.7-0.5l-5.5-9.1 l2-5.3l3.3-0.8l-1.9-3.8l0.1-0.4l5.3,0.2l2.7-1.8l4,4.8l10.1-8.5l5.9,3.6l1.2-3.5l9.8-0.4l5.5,2.9l1.7,5.6l5.6,2.6l-0.8,2.9 L600.28,481.77z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Vaucluse", "data-department", "84", "d", "m541,463.4l6.1,6l-0.1,0.4l-0.1-0.1l-6.6,4.3 l-3.2,0.2l-12-4.8l-3.5,0.7l-4.5-2.3l-5.5-5.7l-10.4-2.9l4.5-5l-6.3-6.4l-0.2-5.5l-2.6-4.4l-0.1-3.7l5.9,0.7l3.5,4.2l8.7-3.9 l2.4,1.4l2.5-2.2l0.5,5.8l9.3,0.9l0.1,2.8l5.2,2.3l2.2,5.4l-1.2,3l3.7,5.2l-1.7,2.5L541,463.4z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Corse", "data-department", "20", "data-code_insee", "94", 1, "region"], ["data-name", "Corse-du-Sud", "data-department", "2A", "d", "m640.5,554.2l3.2-1.7l0.7,8.4l-0.15,0.54 l-1.85,4.86l-2.7,1.9l3.3,0.4l-5.8,14.7l-3.1-1.2l-1.2-2.8l-11.2-3.4l-4.8-4.4l0.2-3l4.9-3.3l-9.5-1.9l2.7-7l-0.9-5.8l-7.3,2.6 l3-8.4l2.6-1.6l-7.9-4.4l-1.1-5.5l5.3-3.8l-3.8-4.2l-2.6,1l0.5-2.7l13.6,2.1l1.2,3.5l6,3.4l6,5.9l0.5,3.2l2.7,1.1l3.7,11 L640.5,554.2z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Haute-Corse", "data-department", "2B", "d", "m643.7,551.5v1l-3.2,1.7l-3.8-0.5l-3.7-11 l-2.7-1.1l-0.5-3.2l-6-5.9l-6-3.4l-1.2-3.5l-13.6-2.1v-0.2l3.9-5l-0.3-3.4l2.2-2.8l2.8-0.3l0.9-2.9l10.7-4.2l3.5-4.9l8.6,1.3 l-0.5-17.4l2.4-2l2.9,1.1l0.18,0.89l1.52,8.21l-0.5,10.6l4,5.6l3.8,26l-5.4,11.9V551.5L643.7,551.5z", 1, "departement", 3, "ngClass", "click"]], template: function MapFrenchComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnamespaceSVG"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "svg", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "g", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "path", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_5_listener() { return ctx.selectDept(971); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "g", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "path", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_7_listener() { return ctx.selectDept(972); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "g", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "path", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_9_listener() { return ctx.selectDept(973); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "g", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "path", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_11_listener() { return ctx.selectDept(974); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "g", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "path", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_13_listener() { return ctx.selectDept(976); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "g", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "path", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_15_listener() { return ctx.selectDept(75); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "path", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_16_listener() { return ctx.selectDept(77); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "path", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_17_listener() { return ctx.selectDept(78); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "path", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_18_listener() { return ctx.selectDept(91); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "path", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_19_listener() { return ctx.selectDept(92); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "path", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_20_listener() { return ctx.selectDept(93); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "path", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_21_listener() { return ctx.selectDept(94); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "path", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_22_listener() { return ctx.selectDept(95); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "g", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "path", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_24_listener() { return ctx.selectDept(18); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "path", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_25_listener() { return ctx.selectDept(28); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "path", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_26_listener() { return ctx.selectDept(36); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "path", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_27_listener() { return ctx.selectDept(37); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "path", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_28_listener() { return ctx.selectDept(41); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "path", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_29_listener() { return ctx.selectDept(45); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "g", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "path", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_31_listener() { return ctx.selectDept(21); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "path", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_32_listener() { return ctx.selectDept(25); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "path", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_33_listener() { return ctx.selectDept(39); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "path", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_34_listener() { return ctx.selectDept(58); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "path", 35);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_35_listener() { return ctx.selectDept(70); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "path", 36);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_36_listener() { return ctx.selectDept(71); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "path", 37);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_37_listener() { return ctx.selectDept(89); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "path", 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_38_listener() { return ctx.selectDept(90); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "g", 39);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "path", 40);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_40_listener() { return ctx.selectDept(14); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "path", 41);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_41_listener() { return ctx.selectDept(27); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "path", 42);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_42_listener() { return ctx.selectDept(50); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "path", 43);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_43_listener() { return ctx.selectDept(61); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "path", 44);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_44_listener() { return ctx.selectDept(76); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "g", 45);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "path", 46);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_46_listener() { return ctx.selectDept(2); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "path", 47);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_47_listener() { return ctx.selectDept(59); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "path", 48);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_48_listener() { return ctx.selectDept(60); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](49, "path", 49);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_49_listener() { return ctx.selectDept(62); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "path", 50);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_50_listener() { return ctx.selectDept(80); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](51, "g", 51);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](52, "path", 52);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_52_listener() { return ctx.selectDept(8); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](53, "path", 53);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_53_listener() { return ctx.selectDept(10); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](54, "path", 54);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_54_listener() { return ctx.selectDept(51); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](55, "path", 55);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_55_listener() { return ctx.selectDept(52); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](56, "path", 56);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_56_listener() { return ctx.selectDept(54); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](57, "path", 57);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_57_listener() { return ctx.selectDept(55); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](58, "path", 58);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_58_listener() { return ctx.selectDept(57); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](59, "path", 59);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_59_listener() { return ctx.selectDept(67); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](60, "path", 60);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_60_listener() { return ctx.selectDept(68); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](61, "path", 61);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_61_listener() { return ctx.selectDept(88); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](62, "g", 62);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](63, "path", 63);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_63_listener() { return ctx.selectDept(44); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](64, "path", 64);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_64_listener() { return ctx.selectDept(49); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](65, "path", 65);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_65_listener() { return ctx.selectDept(53); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](66, "path", 66);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_66_listener() { return ctx.selectDept(72); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](67, "path", 67);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_67_listener() { return ctx.selectDept(85); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](68, "g", 68);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](69, "path", 69);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_69_listener() { return ctx.selectDept(22); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](70, "path", 70);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_70_listener() { return ctx.selectDept(29); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](71, "path", 71);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_71_listener() { return ctx.selectDept(35); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](72, "path", 72);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_72_listener() { return ctx.selectDept(56); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](73, "g", 73);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](74, "path", 74);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_74_listener() { return ctx.selectDept(16); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](75, "path", 75);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_75_listener() { return ctx.selectDept(17); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](76, "path", 76);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_76_listener() { return ctx.selectDept(19); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](77, "path", 77);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_77_listener() { return ctx.selectDept(23); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](78, "path", 78);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_78_listener() { return ctx.selectDept(24); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](79, "path", 79);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_79_listener() { return ctx.selectDept(33); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](80, "path", 80);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_80_listener() { return ctx.selectDept(40); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](81, "path", 81);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_81_listener() { return ctx.selectDept(47); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](82, "path", 82);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_82_listener() { return ctx.selectDept(64); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](83, "path", 83);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_83_listener() { return ctx.selectDept(79); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](84, "path", 84);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_84_listener() { return ctx.selectDept(86); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](85, "path", 85);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_85_listener() { return ctx.selectDept(87); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](86, "g", 86);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](87, "path", 87);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_87_listener() { return ctx.selectDept(9); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](88, "path", 88);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_88_listener() { return ctx.selectDept(11); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](89, "path", 89);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_89_listener() { return ctx.selectDept(12); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](90, "path", 90);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_90_listener() { return ctx.selectDept(30); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](91, "path", 91);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_91_listener() { return ctx.selectDept(31); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](92, "path", 92);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_92_listener() { return ctx.selectDept(32); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](93, "path", 93);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_93_listener() { return ctx.selectDept(34); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](94, "path", 94);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_94_listener() { return ctx.selectDept(46); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](95, "path", 95);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_95_listener() { return ctx.selectDept(48); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](96, "path", 96);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_96_listener() { return ctx.selectDept(65); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](97, "path", 97);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_97_listener() { return ctx.selectDept(66); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](98, "path", 98);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_98_listener() { return ctx.selectDept(81); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](99, "path", 99);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_99_listener() { return ctx.selectDept(82); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](100, "g", 100);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](101, "path", 101);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_101_listener() { return ctx.selectDept(1); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](102, "path", 102);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_102_listener() { return ctx.selectDept(3); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](103, "path", 103);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_103_listener() { return ctx.selectDept(7); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](104, "path", 104);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_104_listener() { return ctx.selectDept(15); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](105, "path", 105);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_105_listener() { return ctx.selectDept(26); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](106, "path", 106);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_106_listener() { return ctx.selectDept(38); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](107, "path", 107);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_107_listener() { return ctx.selectDept(42); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](108, "path", 108);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_108_listener() { return ctx.selectDept(34); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](109, "path", 109);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_109_listener() { return ctx.selectDept(63); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](110, "path", 110);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_110_listener() { return ctx.selectDept(69); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](111, "path", 111);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_111_listener() { return ctx.selectDept(73); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](112, "path", 112);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_112_listener() { return ctx.selectDept(74); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](113, "g", 113);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](114, "path", 114);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_114_listener() { return ctx.selectDept(4); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](115, "path", 115);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_115_listener() { return ctx.selectDept(5); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](116, "path", 116);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_116_listener() { return ctx.selectDept(6); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](117, "path", 117);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_117_listener() { return ctx.selectDept(13); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](118, "path", 118);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_118_listener() { return ctx.selectDept(83); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](119, "path", 119);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_119_listener() { return ctx.selectDept(84); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](120, "g", 120);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](121, "path", 121);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_121_listener() { return ctx.selectDept("2A"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](122, "path", 122);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_122_listener() { return ctx.selectDept("2B"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](101, _c0, "971" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](103, _c0, "972" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](105, _c0, "973" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](107, _c0, "974" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](109, _c0, "976" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](111, _c0, "75" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](113, _c0, "77" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](115, _c0, "78" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](117, _c0, "91" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](119, _c0, "92" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](121, _c0, "93" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](123, _c0, "94" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](125, _c0, "95" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](127, _c0, "18" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](129, _c0, "28" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](131, _c0, "36" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](133, _c0, "37" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](135, _c0, "41" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](137, _c0, "45" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](139, _c0, "21" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](141, _c0, "25" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](143, _c0, "39" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](145, _c0, "58" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](147, _c0, "70" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](149, _c0, "71" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](151, _c0, "89" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](153, _c0, "90" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](155, _c0, "14" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](157, _c0, "27" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](159, _c0, "50" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](161, _c0, "61" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](163, _c0, "76" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](165, _c0, "02" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](167, _c0, "59" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](169, _c0, "60" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](171, _c0, "62" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](173, _c0, "80" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](175, _c0, "08" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](177, _c0, "10" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](179, _c0, "51" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](181, _c0, "52" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](183, _c0, "54" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](185, _c0, "55" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](187, _c0, "57" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](189, _c0, "67" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](191, _c0, "68" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](193, _c0, "88" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](195, _c0, "44" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](197, _c0, "49" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](199, _c0, "53" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](201, _c0, "72" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](203, _c0, "85" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](205, _c0, "22" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](207, _c0, "29" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](209, _c0, "35" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](211, _c0, "56" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](213, _c0, "16" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](215, _c0, "17" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](217, _c0, "19" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](219, _c0, "23" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](221, _c0, "24" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](223, _c0, "33" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](225, _c0, "40" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](227, _c0, "47" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](229, _c0, "64" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](231, _c0, "79" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](233, _c0, "86" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](235, _c0, "87" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](237, _c0, "09" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](239, _c0, "11" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](241, _c0, "12" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](243, _c0, "30" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](245, _c0, "31" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](247, _c0, "32" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](249, _c0, "34" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](251, _c0, "46" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](253, _c0, "48" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](255, _c0, "65" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](257, _c0, "66" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](259, _c0, "81" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](261, _c0, "82" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](263, _c0, "01" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](265, _c0, "03" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](267, _c0, "07" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](269, _c0, "15" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](271, _c0, "26" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](273, _c0, "38" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](275, _c0, "42" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](277, _c0, "43" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](279, _c0, "63" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](281, _c0, "69" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](283, _c0, "73" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](285, _c0, "74" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](287, _c0, "04" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](289, _c0, "05" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](291, _c0, "06" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](293, _c0, "13" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](295, _c0, "83" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](297, _c0, "84" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](299, _c0, "2A" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](301, _c0, "2B" === ctx.dept));
    } }, directives: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["NgClass"]], styles: [".mapfrench_container[_ngcontent-%COMP%] {\n  height: 100%;\n}\n.mapfrench_container[_ngcontent-%COMP%]   .mapfrench_wrapper[_ngcontent-%COMP%] {\n  display: flex;\n  flex-direction: column;\n}\n.mapfrench_container[_ngcontent-%COMP%]   .mapfrench_wrapper[_ngcontent-%COMP%]   .map_title[_ngcontent-%COMP%] {\n  text-align: center;\n}\n.mapfrench_container[_ngcontent-%COMP%]   .mapfrench_wrapper[_ngcontent-%COMP%]   .map_content[_ngcontent-%COMP%] {\n  height: 100%;\n  max-height: 550px;\n}\n.mapfrench_container[_ngcontent-%COMP%]   .mapfrench_wrapper[_ngcontent-%COMP%]   .map_content[_ngcontent-%COMP%]   g[_ngcontent-%COMP%]   path[_ngcontent-%COMP%] {\n  fill: #7f8076;\n  stroke: white;\n  stroke-width: 1px;\n  cursor: pointer;\n  transition: 0.3s;\n  position: relative;\n}\n.mapfrench_container[_ngcontent-%COMP%]   .mapfrench_wrapper[_ngcontent-%COMP%]   .map_content[_ngcontent-%COMP%]   g[_ngcontent-%COMP%]   path.selected[_ngcontent-%COMP%] {\n  fill: red;\n}\n.mapfrench_container[_ngcontent-%COMP%]   .mapfrench_wrapper[_ngcontent-%COMP%]   .map_content[_ngcontent-%COMP%]   g[_ngcontent-%COMP%]:hover   path[_ngcontent-%COMP%] {\n  fill: blue;\n\n}\npolygon[_ngcontent-%COMP%]{\n  -webkit-clip-path: polygon(0% 0%, 100% 0%, 100% 75%, 75% 75%, 64% 100%, 50% 75%, 0% 75%);\n          clip-path: polygon(0% 0%, 100% 0%, 100% 75%, 75% 75%, 64% 100%, 50% 75%, 0% 75%);\n\n}\n.mapfrench_container[_ngcontent-%COMP%]   .mapfrench_wrapper[_ngcontent-%COMP%]   .map_content[_ngcontent-%COMP%]   .popup[_ngcontent-%COMP%]{\n  outline: solid 1px rgb(182, 182, 182);\n\n  fill: #0000ff;\n}\n.mapfrench_container[_ngcontent-%COMP%]   .mapfrench_wrapper[_ngcontent-%COMP%]   .map_content[_ngcontent-%COMP%]   .popup[_ngcontent-%COMP%]   text[_ngcontent-%COMP%]{\n\n  fill: #0000ff;\n}\n.mapfrench_container[_ngcontent-%COMP%]   .mapfrench_wrapper[_ngcontent-%COMP%]   .map_content[_ngcontent-%COMP%]   g[_ngcontent-%COMP%]   path[_ngcontent-%COMP%]:hover {\n  fill: red;\n  stroke: white;\n  stroke-width: 1px;\n  cursor: pointer;\n}\n.mapfrench_container[_ngcontent-%COMP%]   .mapfrench_wrapper[_ngcontent-%COMP%]   .map_content[_ngcontent-%COMP%]   g[_ngcontent-%COMP%]   .choix[_ngcontent-%COMP%] {\n  fill: red;\n  stroke: white;\n  stroke-width: 1px;\n  cursor: pointer;\n}\n\n#info-box[_ngcontent-%COMP%] {\n  display: none ;\n  position: absolute ;\n  top: 0px ;\n  left: 0px  ;\n  z-index: 1;\n  background-color: #2c2c2c ;\n  border: 2px solid #ffffff ;\n  color: #ffffff;\n  border-radius: 5px ;\n  padding: 5px ;\n  font-family: arial ;\n  width: 200px;\n  height: 30px;\n  box-shadow: rgba(0, 0, 0, 0.15) 1.95px 1.95px 2.6px;\n  text-align: center;\n    }\n.round[_ngcontent-%COMP%]{\n      border-radius: 30px;\n      -webkit-border-radius: 30px;\n      -moz-border-radius: 30px;\n    }\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbWFwLWZyZW5jaC9tYXAtZnJlbmNoLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxZQUFZO0FBQ2Q7QUFDQTtFQUNFLGFBQWE7RUFDYixzQkFBc0I7QUFDeEI7QUFDQTtFQUNFLGtCQUFrQjtBQUNwQjtBQUNBO0VBQ0UsWUFBWTtFQUNaLGlCQUFpQjtBQUNuQjtBQUVBO0VBQ0UsYUFBYTtFQUNiLGFBQWE7RUFDYixpQkFBaUI7RUFDakIsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixrQkFBa0I7QUFDcEI7QUFDQTtFQUNFLFNBQVM7QUFDWDtBQUNBO0VBQ0UsVUFBVTs7QUFFWjtBQUNBO0VBQ0Usd0ZBQWdGO1VBQWhGLGdGQUFnRjs7QUFFbEY7QUFDQTtFQUNFLHFDQUFxQzs7RUFFckMsYUFBYTtBQUNmO0FBQ0E7O0VBRUUsYUFBYTtBQUNmO0FBRUE7RUFDRSxTQUFTO0VBQ1QsYUFBYTtFQUNiLGlCQUFpQjtFQUNqQixlQUFlO0FBQ2pCO0FBRUE7RUFDRSxTQUFTO0VBQ1QsYUFBYTtFQUNiLGlCQUFpQjtFQUNqQixlQUFlO0FBQ2pCO0FBRUEsK0NBQStDO0FBQy9DO0VBQ0UsY0FBYztFQUNkLG1CQUFtQjtFQUNuQixTQUFTO0VBQ1QsV0FBVztFQUNYLFVBQVU7RUFDViwwQkFBMEI7RUFDMUIsMEJBQTBCO0VBQzFCLGNBQWM7RUFDZCxtQkFBbUI7RUFDbkIsYUFBYTtFQUNiLG1CQUFtQjtFQUNuQixZQUFZO0VBQ1osWUFBWTtFQUNaLG1EQUFtRDtFQUNuRCxrQkFBa0I7SUFDaEI7QUFDQTtNQUNFLG1CQUFtQjtNQUNuQiwyQkFBMkI7TUFDM0Isd0JBQXdCO0lBQzFCIiwiZmlsZSI6InNyYy9hcHAvbWFwLWZyZW5jaC9tYXAtZnJlbmNoLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubWFwZnJlbmNoX2NvbnRhaW5lciB7XG4gIGhlaWdodDogMTAwJTtcbn1cbi5tYXBmcmVuY2hfY29udGFpbmVyIC5tYXBmcmVuY2hfd3JhcHBlciB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG59XG4ubWFwZnJlbmNoX2NvbnRhaW5lciAubWFwZnJlbmNoX3dyYXBwZXIgLm1hcF90aXRsZSB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5tYXBmcmVuY2hfY29udGFpbmVyIC5tYXBmcmVuY2hfd3JhcHBlciAubWFwX2NvbnRlbnQge1xuICBoZWlnaHQ6IDEwMCU7XG4gIG1heC1oZWlnaHQ6IDU1MHB4O1xufVxuXG4ubWFwZnJlbmNoX2NvbnRhaW5lciAubWFwZnJlbmNoX3dyYXBwZXIgLm1hcF9jb250ZW50IGcgcGF0aCB7XG4gIGZpbGw6ICM3ZjgwNzY7XG4gIHN0cm9rZTogd2hpdGU7XG4gIHN0cm9rZS13aWR0aDogMXB4O1xuICBjdXJzb3I6IHBvaW50ZXI7XG4gIHRyYW5zaXRpb246IDAuM3M7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cbi5tYXBmcmVuY2hfY29udGFpbmVyIC5tYXBmcmVuY2hfd3JhcHBlciAubWFwX2NvbnRlbnQgZyBwYXRoLnNlbGVjdGVkIHtcbiAgZmlsbDogcmVkO1xufVxuLm1hcGZyZW5jaF9jb250YWluZXIgLm1hcGZyZW5jaF93cmFwcGVyIC5tYXBfY29udGVudCBnOmhvdmVyIHBhdGgge1xuICBmaWxsOiBibHVlO1xuXG59XG5wb2x5Z29ue1xuICBjbGlwLXBhdGg6IHBvbHlnb24oMCUgMCUsIDEwMCUgMCUsIDEwMCUgNzUlLCA3NSUgNzUlLCA2NCUgMTAwJSwgNTAlIDc1JSwgMCUgNzUlKTtcblxufVxuLm1hcGZyZW5jaF9jb250YWluZXIgLm1hcGZyZW5jaF93cmFwcGVyIC5tYXBfY29udGVudCAucG9wdXB7XG4gIG91dGxpbmU6IHNvbGlkIDFweCByZ2IoMTgyLCAxODIsIDE4Mik7XG5cbiAgZmlsbDogIzAwMDBmZjtcbn1cbi5tYXBmcmVuY2hfY29udGFpbmVyIC5tYXBmcmVuY2hfd3JhcHBlciAubWFwX2NvbnRlbnQgLnBvcHVwIHRleHR7XG5cbiAgZmlsbDogIzAwMDBmZjtcbn1cblxuLm1hcGZyZW5jaF9jb250YWluZXIgLm1hcGZyZW5jaF93cmFwcGVyIC5tYXBfY29udGVudCBnIHBhdGg6aG92ZXIge1xuICBmaWxsOiByZWQ7XG4gIHN0cm9rZTogd2hpdGU7XG4gIHN0cm9rZS13aWR0aDogMXB4O1xuICBjdXJzb3I6IHBvaW50ZXI7XG59XG5cbi5tYXBmcmVuY2hfY29udGFpbmVyIC5tYXBmcmVuY2hfd3JhcHBlciAubWFwX2NvbnRlbnQgZyAgLmNob2l4IHtcbiAgZmlsbDogcmVkO1xuICBzdHJva2U6IHdoaXRlO1xuICBzdHJva2Utd2lkdGg6IDFweDtcbiAgY3Vyc29yOiBwb2ludGVyO1xufVxuXG4vKioqKioqKioqKioqKioqKioqKioqKioqKnBvcHVwKioqKioqKioqKioqKioqKi9cbiNpbmZvLWJveCB7XG4gIGRpc3BsYXk6IG5vbmUgO1xuICBwb3NpdGlvbjogYWJzb2x1dGUgO1xuICB0b3A6IDBweCA7XG4gIGxlZnQ6IDBweCAgO1xuICB6LWluZGV4OiAxO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMmMyYzJjIDtcbiAgYm9yZGVyOiAycHggc29saWQgI2ZmZmZmZiA7XG4gIGNvbG9yOiAjZmZmZmZmO1xuICBib3JkZXItcmFkaXVzOiA1cHggO1xuICBwYWRkaW5nOiA1cHggO1xuICBmb250LWZhbWlseTogYXJpYWwgO1xuICB3aWR0aDogMjAwcHg7XG4gIGhlaWdodDogMzBweDtcbiAgYm94LXNoYWRvdzogcmdiYSgwLCAwLCAwLCAwLjE1KSAxLjk1cHggMS45NXB4IDIuNnB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgfVxuICAgIC5yb3VuZHtcbiAgICAgIGJvcmRlci1yYWRpdXM6IDMwcHg7XG4gICAgICAtd2Via2l0LWJvcmRlci1yYWRpdXM6IDMwcHg7XG4gICAgICAtbW96LWJvcmRlci1yYWRpdXM6IDMwcHg7XG4gICAgfVxuXG4gICAgXG5cblxuICAgICJdfQ== */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](MapFrenchComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-map-french',
                templateUrl: './map-french.component.html',
                styleUrls: ['./map-french.component.css']
            }]
    }], function () { return []; }, { myinputDep: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }], myOutput: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
        }] }); })();


/***/ }),

/***/ "./src/app/material-module.ts":
/*!************************************!*\
  !*** ./src/app/material-module.ts ***!
  \************************************/
/*! exports provided: MaterialModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MaterialModule", function() { return MaterialModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_cdk_a11y__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/cdk/a11y */ "./node_modules/@angular/cdk/__ivy_ngcc__/fesm2015/a11y.js");
/* harmony import */ var _angular_cdk_clipboard__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/cdk/clipboard */ "./node_modules/@angular/cdk/__ivy_ngcc__/fesm2015/clipboard.js");
/* harmony import */ var _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/cdk/drag-drop */ "./node_modules/@angular/cdk/__ivy_ngcc__/fesm2015/drag-drop.js");
/* harmony import */ var _angular_cdk_portal__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/cdk/portal */ "./node_modules/@angular/cdk/__ivy_ngcc__/fesm2015/portal.js");
/* harmony import */ var _angular_cdk_scrolling__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/cdk/scrolling */ "./node_modules/@angular/cdk/__ivy_ngcc__/fesm2015/scrolling.js");
/* harmony import */ var _angular_cdk_stepper__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/cdk/stepper */ "./node_modules/@angular/cdk/__ivy_ngcc__/fesm2015/stepper.js");
/* harmony import */ var _angular_cdk_table__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/cdk/table */ "./node_modules/@angular/cdk/__ivy_ngcc__/fesm2015/table.js");
/* harmony import */ var _angular_cdk_tree__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/cdk/tree */ "./node_modules/@angular/cdk/__ivy_ngcc__/fesm2015/tree.js");
/* harmony import */ var _angular_material_autocomplete__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material/autocomplete */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/autocomplete.js");
/* harmony import */ var _angular_material_badge__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material/badge */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/badge.js");
/* harmony import */ var _angular_material_bottom_sheet__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/material/bottom-sheet */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/bottom-sheet.js");
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/material/button */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/button.js");
/* harmony import */ var _angular_material_button_toggle__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/material/button-toggle */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/button-toggle.js");
/* harmony import */ var _angular_material_card__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/material/card */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/card.js");
/* harmony import */ var _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/material/checkbox */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/checkbox.js");
/* harmony import */ var _angular_material_chips__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/material/chips */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/chips.js");
/* harmony import */ var _angular_material_stepper__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @angular/material/stepper */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/stepper.js");
/* harmony import */ var _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @angular/material/datepicker */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/datepicker.js");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/dialog.js");
/* harmony import */ var _angular_material_divider__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @angular/material/divider */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/divider.js");
/* harmony import */ var _angular_material_expansion__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! @angular/material/expansion */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/expansion.js");
/* harmony import */ var _angular_material_grid_list__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! @angular/material/grid-list */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/grid-list.js");
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! @angular/material/icon */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/icon.js");
/* harmony import */ var _angular_material_input__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! @angular/material/input */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/input.js");
/* harmony import */ var _angular_material_list__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! @angular/material/list */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/list.js");
/* harmony import */ var _angular_material_menu__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! @angular/material/menu */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/menu.js");
/* harmony import */ var _angular_material_core__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! @angular/material/core */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_material_paginator__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! @angular/material/paginator */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/paginator.js");
/* harmony import */ var _angular_material_progress_bar__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! @angular/material/progress-bar */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/progress-bar.js");
/* harmony import */ var _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! @angular/material/progress-spinner */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/progress-spinner.js");
/* harmony import */ var _angular_material_radio__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! @angular/material/radio */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/radio.js");
/* harmony import */ var _angular_material_select__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! @angular/material/select */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/select.js");
/* harmony import */ var _angular_material_sidenav__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! @angular/material/sidenav */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/sidenav.js");
/* harmony import */ var _angular_material_slider__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! @angular/material/slider */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/slider.js");
/* harmony import */ var _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(/*! @angular/material/slide-toggle */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/slide-toggle.js");
/* harmony import */ var _angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_36__ = __webpack_require__(/*! @angular/material/snack-bar */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/snack-bar.js");
/* harmony import */ var _angular_material_sort__WEBPACK_IMPORTED_MODULE_37__ = __webpack_require__(/*! @angular/material/sort */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/sort.js");
/* harmony import */ var _angular_material_table__WEBPACK_IMPORTED_MODULE_38__ = __webpack_require__(/*! @angular/material/table */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/table.js");
/* harmony import */ var _angular_material_tabs__WEBPACK_IMPORTED_MODULE_39__ = __webpack_require__(/*! @angular/material/tabs */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/tabs.js");
/* harmony import */ var _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_40__ = __webpack_require__(/*! @angular/material/toolbar */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/toolbar.js");
/* harmony import */ var _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_41__ = __webpack_require__(/*! @angular/material/tooltip */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/tooltip.js");
/* harmony import */ var _angular_material_tree__WEBPACK_IMPORTED_MODULE_42__ = __webpack_require__(/*! @angular/material/tree */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/tree.js");
/* harmony import */ var _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_43__ = __webpack_require__(/*! @angular/cdk/overlay */ "./node_modules/@angular/cdk/__ivy_ngcc__/fesm2015/overlay.js");













































class MaterialModule {
}
MaterialModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({ type: MaterialModule });
MaterialModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({ factory: function MaterialModule_Factory(t) { return new (t || MaterialModule)(); }, imports: [_angular_cdk_a11y__WEBPACK_IMPORTED_MODULE_1__["A11yModule"],
        _angular_cdk_clipboard__WEBPACK_IMPORTED_MODULE_2__["ClipboardModule"],
        _angular_cdk_stepper__WEBPACK_IMPORTED_MODULE_6__["CdkStepperModule"],
        _angular_cdk_table__WEBPACK_IMPORTED_MODULE_7__["CdkTableModule"],
        _angular_cdk_tree__WEBPACK_IMPORTED_MODULE_8__["CdkTreeModule"],
        _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_3__["DragDropModule"],
        _angular_material_autocomplete__WEBPACK_IMPORTED_MODULE_9__["MatAutocompleteModule"],
        _angular_material_badge__WEBPACK_IMPORTED_MODULE_10__["MatBadgeModule"],
        _angular_material_bottom_sheet__WEBPACK_IMPORTED_MODULE_11__["MatBottomSheetModule"],
        _angular_material_button__WEBPACK_IMPORTED_MODULE_12__["MatButtonModule"],
        _angular_material_button_toggle__WEBPACK_IMPORTED_MODULE_13__["MatButtonToggleModule"],
        _angular_material_card__WEBPACK_IMPORTED_MODULE_14__["MatCardModule"],
        _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_15__["MatCheckboxModule"],
        _angular_material_chips__WEBPACK_IMPORTED_MODULE_16__["MatChipsModule"],
        _angular_material_stepper__WEBPACK_IMPORTED_MODULE_17__["MatStepperModule"],
        _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_18__["MatDatepickerModule"],
        _angular_material_dialog__WEBPACK_IMPORTED_MODULE_19__["MatDialogModule"],
        _angular_material_divider__WEBPACK_IMPORTED_MODULE_20__["MatDividerModule"],
        _angular_material_expansion__WEBPACK_IMPORTED_MODULE_21__["MatExpansionModule"],
        _angular_material_grid_list__WEBPACK_IMPORTED_MODULE_22__["MatGridListModule"],
        _angular_material_icon__WEBPACK_IMPORTED_MODULE_23__["MatIconModule"],
        _angular_material_input__WEBPACK_IMPORTED_MODULE_24__["MatInputModule"],
        _angular_material_list__WEBPACK_IMPORTED_MODULE_25__["MatListModule"],
        _angular_material_menu__WEBPACK_IMPORTED_MODULE_26__["MatMenuModule"],
        _angular_material_core__WEBPACK_IMPORTED_MODULE_27__["MatNativeDateModule"],
        _angular_material_paginator__WEBPACK_IMPORTED_MODULE_28__["MatPaginatorModule"],
        _angular_material_progress_bar__WEBPACK_IMPORTED_MODULE_29__["MatProgressBarModule"],
        _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_30__["MatProgressSpinnerModule"],
        _angular_material_radio__WEBPACK_IMPORTED_MODULE_31__["MatRadioModule"],
        _angular_material_core__WEBPACK_IMPORTED_MODULE_27__["MatRippleModule"],
        _angular_material_select__WEBPACK_IMPORTED_MODULE_32__["MatSelectModule"],
        _angular_material_sidenav__WEBPACK_IMPORTED_MODULE_33__["MatSidenavModule"],
        _angular_material_slider__WEBPACK_IMPORTED_MODULE_34__["MatSliderModule"],
        _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_35__["MatSlideToggleModule"],
        _angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_36__["MatSnackBarModule"],
        _angular_material_sort__WEBPACK_IMPORTED_MODULE_37__["MatSortModule"],
        _angular_material_table__WEBPACK_IMPORTED_MODULE_38__["MatTableModule"],
        _angular_material_tabs__WEBPACK_IMPORTED_MODULE_39__["MatTabsModule"],
        _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_40__["MatToolbarModule"],
        _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_41__["MatTooltipModule"],
        _angular_material_tree__WEBPACK_IMPORTED_MODULE_42__["MatTreeModule"],
        _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_43__["OverlayModule"],
        _angular_cdk_portal__WEBPACK_IMPORTED_MODULE_4__["PortalModule"],
        _angular_cdk_scrolling__WEBPACK_IMPORTED_MODULE_5__["ScrollingModule"]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](MaterialModule, { exports: [_angular_cdk_a11y__WEBPACK_IMPORTED_MODULE_1__["A11yModule"],
        _angular_cdk_clipboard__WEBPACK_IMPORTED_MODULE_2__["ClipboardModule"],
        _angular_cdk_stepper__WEBPACK_IMPORTED_MODULE_6__["CdkStepperModule"],
        _angular_cdk_table__WEBPACK_IMPORTED_MODULE_7__["CdkTableModule"],
        _angular_cdk_tree__WEBPACK_IMPORTED_MODULE_8__["CdkTreeModule"],
        _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_3__["DragDropModule"],
        _angular_material_autocomplete__WEBPACK_IMPORTED_MODULE_9__["MatAutocompleteModule"],
        _angular_material_badge__WEBPACK_IMPORTED_MODULE_10__["MatBadgeModule"],
        _angular_material_bottom_sheet__WEBPACK_IMPORTED_MODULE_11__["MatBottomSheetModule"],
        _angular_material_button__WEBPACK_IMPORTED_MODULE_12__["MatButtonModule"],
        _angular_material_button_toggle__WEBPACK_IMPORTED_MODULE_13__["MatButtonToggleModule"],
        _angular_material_card__WEBPACK_IMPORTED_MODULE_14__["MatCardModule"],
        _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_15__["MatCheckboxModule"],
        _angular_material_chips__WEBPACK_IMPORTED_MODULE_16__["MatChipsModule"],
        _angular_material_stepper__WEBPACK_IMPORTED_MODULE_17__["MatStepperModule"],
        _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_18__["MatDatepickerModule"],
        _angular_material_dialog__WEBPACK_IMPORTED_MODULE_19__["MatDialogModule"],
        _angular_material_divider__WEBPACK_IMPORTED_MODULE_20__["MatDividerModule"],
        _angular_material_expansion__WEBPACK_IMPORTED_MODULE_21__["MatExpansionModule"],
        _angular_material_grid_list__WEBPACK_IMPORTED_MODULE_22__["MatGridListModule"],
        _angular_material_icon__WEBPACK_IMPORTED_MODULE_23__["MatIconModule"],
        _angular_material_input__WEBPACK_IMPORTED_MODULE_24__["MatInputModule"],
        _angular_material_list__WEBPACK_IMPORTED_MODULE_25__["MatListModule"],
        _angular_material_menu__WEBPACK_IMPORTED_MODULE_26__["MatMenuModule"],
        _angular_material_core__WEBPACK_IMPORTED_MODULE_27__["MatNativeDateModule"],
        _angular_material_paginator__WEBPACK_IMPORTED_MODULE_28__["MatPaginatorModule"],
        _angular_material_progress_bar__WEBPACK_IMPORTED_MODULE_29__["MatProgressBarModule"],
        _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_30__["MatProgressSpinnerModule"],
        _angular_material_radio__WEBPACK_IMPORTED_MODULE_31__["MatRadioModule"],
        _angular_material_core__WEBPACK_IMPORTED_MODULE_27__["MatRippleModule"],
        _angular_material_select__WEBPACK_IMPORTED_MODULE_32__["MatSelectModule"],
        _angular_material_sidenav__WEBPACK_IMPORTED_MODULE_33__["MatSidenavModule"],
        _angular_material_slider__WEBPACK_IMPORTED_MODULE_34__["MatSliderModule"],
        _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_35__["MatSlideToggleModule"],
        _angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_36__["MatSnackBarModule"],
        _angular_material_sort__WEBPACK_IMPORTED_MODULE_37__["MatSortModule"],
        _angular_material_table__WEBPACK_IMPORTED_MODULE_38__["MatTableModule"],
        _angular_material_tabs__WEBPACK_IMPORTED_MODULE_39__["MatTabsModule"],
        _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_40__["MatToolbarModule"],
        _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_41__["MatTooltipModule"],
        _angular_material_tree__WEBPACK_IMPORTED_MODULE_42__["MatTreeModule"],
        _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_43__["OverlayModule"],
        _angular_cdk_portal__WEBPACK_IMPORTED_MODULE_4__["PortalModule"],
        _angular_cdk_scrolling__WEBPACK_IMPORTED_MODULE_5__["ScrollingModule"]] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](MaterialModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
                exports: [
                    _angular_cdk_a11y__WEBPACK_IMPORTED_MODULE_1__["A11yModule"],
                    _angular_cdk_clipboard__WEBPACK_IMPORTED_MODULE_2__["ClipboardModule"],
                    _angular_cdk_stepper__WEBPACK_IMPORTED_MODULE_6__["CdkStepperModule"],
                    _angular_cdk_table__WEBPACK_IMPORTED_MODULE_7__["CdkTableModule"],
                    _angular_cdk_tree__WEBPACK_IMPORTED_MODULE_8__["CdkTreeModule"],
                    _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_3__["DragDropModule"],
                    _angular_material_autocomplete__WEBPACK_IMPORTED_MODULE_9__["MatAutocompleteModule"],
                    _angular_material_badge__WEBPACK_IMPORTED_MODULE_10__["MatBadgeModule"],
                    _angular_material_bottom_sheet__WEBPACK_IMPORTED_MODULE_11__["MatBottomSheetModule"],
                    _angular_material_button__WEBPACK_IMPORTED_MODULE_12__["MatButtonModule"],
                    _angular_material_button_toggle__WEBPACK_IMPORTED_MODULE_13__["MatButtonToggleModule"],
                    _angular_material_card__WEBPACK_IMPORTED_MODULE_14__["MatCardModule"],
                    _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_15__["MatCheckboxModule"],
                    _angular_material_chips__WEBPACK_IMPORTED_MODULE_16__["MatChipsModule"],
                    _angular_material_stepper__WEBPACK_IMPORTED_MODULE_17__["MatStepperModule"],
                    _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_18__["MatDatepickerModule"],
                    _angular_material_dialog__WEBPACK_IMPORTED_MODULE_19__["MatDialogModule"],
                    _angular_material_divider__WEBPACK_IMPORTED_MODULE_20__["MatDividerModule"],
                    _angular_material_expansion__WEBPACK_IMPORTED_MODULE_21__["MatExpansionModule"],
                    _angular_material_grid_list__WEBPACK_IMPORTED_MODULE_22__["MatGridListModule"],
                    _angular_material_icon__WEBPACK_IMPORTED_MODULE_23__["MatIconModule"],
                    _angular_material_input__WEBPACK_IMPORTED_MODULE_24__["MatInputModule"],
                    _angular_material_list__WEBPACK_IMPORTED_MODULE_25__["MatListModule"],
                    _angular_material_menu__WEBPACK_IMPORTED_MODULE_26__["MatMenuModule"],
                    _angular_material_core__WEBPACK_IMPORTED_MODULE_27__["MatNativeDateModule"],
                    _angular_material_paginator__WEBPACK_IMPORTED_MODULE_28__["MatPaginatorModule"],
                    _angular_material_progress_bar__WEBPACK_IMPORTED_MODULE_29__["MatProgressBarModule"],
                    _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_30__["MatProgressSpinnerModule"],
                    _angular_material_radio__WEBPACK_IMPORTED_MODULE_31__["MatRadioModule"],
                    _angular_material_core__WEBPACK_IMPORTED_MODULE_27__["MatRippleModule"],
                    _angular_material_select__WEBPACK_IMPORTED_MODULE_32__["MatSelectModule"],
                    _angular_material_sidenav__WEBPACK_IMPORTED_MODULE_33__["MatSidenavModule"],
                    _angular_material_slider__WEBPACK_IMPORTED_MODULE_34__["MatSliderModule"],
                    _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_35__["MatSlideToggleModule"],
                    _angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_36__["MatSnackBarModule"],
                    _angular_material_sort__WEBPACK_IMPORTED_MODULE_37__["MatSortModule"],
                    _angular_material_table__WEBPACK_IMPORTED_MODULE_38__["MatTableModule"],
                    _angular_material_tabs__WEBPACK_IMPORTED_MODULE_39__["MatTabsModule"],
                    _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_40__["MatToolbarModule"],
                    _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_41__["MatTooltipModule"],
                    _angular_material_tree__WEBPACK_IMPORTED_MODULE_42__["MatTreeModule"],
                    _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_43__["OverlayModule"],
                    _angular_cdk_portal__WEBPACK_IMPORTED_MODULE_4__["PortalModule"],
                    _angular_cdk_scrolling__WEBPACK_IMPORTED_MODULE_5__["ScrollingModule"],
                ]
            }]
    }], null, null); })();


/***/ }),

/***/ "./src/app/not-found/not-found.component.ts":
/*!**************************************************!*\
  !*** ./src/app/not-found/not-found.component.ts ***!
  \**************************************************/
/*! exports provided: NotFoundComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotFoundComponent", function() { return NotFoundComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


class NotFoundComponent {
    constructor() { }
    ngOnInit() {
    }
}
NotFoundComponent.ɵfac = function NotFoundComponent_Factory(t) { return new (t || NotFoundComponent)(); };
NotFoundComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: NotFoundComponent, selectors: [["app-not-found"]], decls: 11, vars: 0, consts: [[1, "container"], [1, "boo-wrapper"], [1, "boo"], [1, "face"], [1, "shadow"]], template: function NotFoundComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "Whoops!");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, " We couldn't find the page you ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, " were looking for. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } }, styles: ["@keyframes floating {\n    0% {\n        transform: translate3d(0, 0, 0);\n   }\n    45% {\n        transform: translate3d(0, -10%, 0);\n   }\n    55% {\n        transform: translate3d(0, -10%, 0);\n   }\n    100% {\n        transform: translate3d(0, 0, 0);\n   }\n}\n@keyframes floatingShadow {\n    0% {\n        transform: scale(1);\n   }\n    45% {\n        transform: scale(0.85);\n   }\n    55% {\n        transform: scale(0.85);\n   }\n    100% {\n        transform: scale(1);\n   }\n}\nbody[_ngcontent-%COMP%] {\n    background-color: #f7f7f7;\n}\n.container[_ngcontent-%COMP%] {\n    font-family: 'Varela Round', sans-serif;\n    color: #9b9b9b;\n    position: relative;\n    height: 100vh;\n    text-align: center;\n    font-size: 16px;\n}\n.container[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%] {\n    font-size: 32px;\n    margin-top: 32px;\n}\n.boo-wrapper[_ngcontent-%COMP%] {\n    width: 100%;\n    position: absolute;\n    top: 50%;\n    left: 50%;\n    transform: translate(-50%, -50%);\n    paddig-top: 64px;\n    paddig-bottom: 64px;\n}\n.boo[_ngcontent-%COMP%] {\n    width: 160px;\n    height: 184px;\n    background-color: #f7f7f7;\n    margin-left: auto;\n    margin-right: auto;\n    border: 3.3939393939px solid #9b9b9b;\n    border-bottom: 0;\n    overflow: hidden;\n    border-radius: 80px 80px 0 0;\n    box-shadow: -16px 0 0 2px rgba(234, 234, 234, .5) inset;\n    position: relative;\n    padding-bottom: 32px;\n    animation: floating 3s ease-in-out infinite;\n}\n.boo[_ngcontent-%COMP%]::after {\n    content: '';\n    display: block;\n    position: absolute;\n    left: -18.8235294118px;\n    bottom: -8.3116883117px;\n    width: calc(100% + 32px);\n    height: 32px;\n    background-repeat: repeat-x;\n    background-size: 32px 32px;\n    background-position: left bottom;\n    background-image: linear-gradient(-45deg, #f7f7f7 16px, transparent 0), linear-gradient(45deg, #f7f7f7 16px, transparent 0), linear-gradient(-45deg, #9b9b9b 18.8235294118px, transparent 0), linear-gradient(45deg, #9b9b9b 18.8235294118px, transparent 0);\n}\n.boo[_ngcontent-%COMP%]   .face[_ngcontent-%COMP%] {\n    width: 24px;\n    height: 3.2px;\n    border-radius: 5px;\n    background-color: #9b9b9b;\n    position: absolute;\n    left: 50%;\n    bottom: 56px;\n    transform: translateX(-50%);\n}\n.boo[_ngcontent-%COMP%]   .face[_ngcontent-%COMP%]::before, .boo[_ngcontent-%COMP%]   .face[_ngcontent-%COMP%]::after {\n    content: '';\n    display: block;\n    width: 6px;\n    height: 6px;\n    background-color: #9b9b9b;\n    border-radius: 50%;\n    position: absolute;\n    bottom: 40px;\n}\n.boo[_ngcontent-%COMP%]   .face[_ngcontent-%COMP%]::before {\n    left: -24px;\n}\n.boo[_ngcontent-%COMP%]   .face[_ngcontent-%COMP%]::after {\n    right: -24px;\n}\n.shadow[_ngcontent-%COMP%] {\n    width: 128px;\n    height: 16px;\n    background-color: rgba(234, 234, 234, .75);\n    margin-top: 40px;\n    margin-right: auto;\n    margin-left: auto;\n    border-radius: 50%;\n    animation: floatingShadow 3s ease-in-out infinite;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbm90LWZvdW5kL25vdC1mb3VuZC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0k7UUFDSSwrQkFBK0I7R0FDcEM7SUFDQztRQUNJLGtDQUFrQztHQUN2QztJQUNDO1FBQ0ksa0NBQWtDO0dBQ3ZDO0lBQ0M7UUFDSSwrQkFBK0I7R0FDcEM7QUFDSDtBQUNBO0lBQ0k7UUFDSSxtQkFBbUI7R0FDeEI7SUFDQztRQUNJLHNCQUFzQjtHQUMzQjtJQUNDO1FBQ0ksc0JBQXNCO0dBQzNCO0lBQ0M7UUFDSSxtQkFBbUI7R0FDeEI7QUFDSDtBQUNBO0lBQ0kseUJBQXlCO0FBQzdCO0FBQ0E7SUFDSSx1Q0FBdUM7SUFDdkMsY0FBYztJQUNkLGtCQUFrQjtJQUNsQixhQUFhO0lBQ2Isa0JBQWtCO0lBQ2xCLGVBQWU7QUFDbkI7QUFDQTtJQUNJLGVBQWU7SUFDZixnQkFBZ0I7QUFDcEI7QUFDQTtJQUNJLFdBQVc7SUFDWCxrQkFBa0I7SUFDbEIsUUFBUTtJQUNSLFNBQVM7SUFDVCxnQ0FBZ0M7SUFDaEMsZ0JBQWdCO0lBQ2hCLG1CQUFtQjtBQUN2QjtBQUNBO0lBQ0ksWUFBWTtJQUNaLGFBQWE7SUFDYix5QkFBeUI7SUFDekIsaUJBQWlCO0lBQ2pCLGtCQUFrQjtJQUNsQixvQ0FBb0M7SUFDcEMsZ0JBQWdCO0lBQ2hCLGdCQUFnQjtJQUNoQiw0QkFBNEI7SUFDNUIsdURBQXVEO0lBQ3ZELGtCQUFrQjtJQUNsQixvQkFBb0I7SUFDcEIsMkNBQTJDO0FBQy9DO0FBQ0E7SUFDSSxXQUFXO0lBQ1gsY0FBYztJQUNkLGtCQUFrQjtJQUNsQixzQkFBc0I7SUFDdEIsdUJBQXVCO0lBQ3ZCLHdCQUF3QjtJQUN4QixZQUFZO0lBQ1osMkJBQTJCO0lBQzNCLDBCQUEwQjtJQUMxQixnQ0FBZ0M7SUFDaEMsNFBBQTRQO0FBQ2hRO0FBQ0E7SUFDSSxXQUFXO0lBQ1gsYUFBYTtJQUNiLGtCQUFrQjtJQUNsQix5QkFBeUI7SUFDekIsa0JBQWtCO0lBQ2xCLFNBQVM7SUFDVCxZQUFZO0lBQ1osMkJBQTJCO0FBQy9CO0FBQ0E7SUFDSSxXQUFXO0lBQ1gsY0FBYztJQUNkLFVBQVU7SUFDVixXQUFXO0lBQ1gseUJBQXlCO0lBQ3pCLGtCQUFrQjtJQUNsQixrQkFBa0I7SUFDbEIsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksV0FBVztBQUNmO0FBQ0E7SUFDSSxZQUFZO0FBQ2hCO0FBQ0E7SUFDSSxZQUFZO0lBQ1osWUFBWTtJQUNaLDBDQUEwQztJQUMxQyxnQkFBZ0I7SUFDaEIsa0JBQWtCO0lBQ2xCLGlCQUFpQjtJQUNqQixrQkFBa0I7SUFDbEIsaURBQWlEO0FBQ3JEIiwiZmlsZSI6InNyYy9hcHAvbm90LWZvdW5kL25vdC1mb3VuZC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiQGtleWZyYW1lcyBmbG9hdGluZyB7XG4gICAgMCUge1xuICAgICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZTNkKDAsIDAsIDApO1xuICAgfVxuICAgIDQ1JSB7XG4gICAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlM2QoMCwgLTEwJSwgMCk7XG4gICB9XG4gICAgNTUlIHtcbiAgICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUzZCgwLCAtMTAlLCAwKTtcbiAgIH1cbiAgICAxMDAlIHtcbiAgICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUzZCgwLCAwLCAwKTtcbiAgIH1cbn1cbkBrZXlmcmFtZXMgZmxvYXRpbmdTaGFkb3cge1xuICAgIDAlIHtcbiAgICAgICAgdHJhbnNmb3JtOiBzY2FsZSgxKTtcbiAgIH1cbiAgICA0NSUge1xuICAgICAgICB0cmFuc2Zvcm06IHNjYWxlKDAuODUpO1xuICAgfVxuICAgIDU1JSB7XG4gICAgICAgIHRyYW5zZm9ybTogc2NhbGUoMC44NSk7XG4gICB9XG4gICAgMTAwJSB7XG4gICAgICAgIHRyYW5zZm9ybTogc2NhbGUoMSk7XG4gICB9XG59XG5ib2R5IHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjdmN2Y3O1xufVxuLmNvbnRhaW5lciB7XG4gICAgZm9udC1mYW1pbHk6ICdWYXJlbGEgUm91bmQnLCBzYW5zLXNlcmlmO1xuICAgIGNvbG9yOiAjOWI5YjliO1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICBoZWlnaHQ6IDEwMHZoO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBmb250LXNpemU6IDE2cHg7XG59XG4uY29udGFpbmVyIGgxIHtcbiAgICBmb250LXNpemU6IDMycHg7XG4gICAgbWFyZ2luLXRvcDogMzJweDtcbn1cbi5ib28td3JhcHBlciB7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHRvcDogNTAlO1xuICAgIGxlZnQ6IDUwJTtcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTtcbiAgICBwYWRkaWctdG9wOiA2NHB4O1xuICAgIHBhZGRpZy1ib3R0b206IDY0cHg7XG59XG4uYm9vIHtcbiAgICB3aWR0aDogMTYwcHg7XG4gICAgaGVpZ2h0OiAxODRweDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjdmN2Y3O1xuICAgIG1hcmdpbi1sZWZ0OiBhdXRvO1xuICAgIG1hcmdpbi1yaWdodDogYXV0bztcbiAgICBib3JkZXI6IDMuMzkzOTM5MzkzOXB4IHNvbGlkICM5YjliOWI7XG4gICAgYm9yZGVyLWJvdHRvbTogMDtcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xuICAgIGJvcmRlci1yYWRpdXM6IDgwcHggODBweCAwIDA7XG4gICAgYm94LXNoYWRvdzogLTE2cHggMCAwIDJweCByZ2JhKDIzNCwgMjM0LCAyMzQsIC41KSBpbnNldDtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgcGFkZGluZy1ib3R0b206IDMycHg7XG4gICAgYW5pbWF0aW9uOiBmbG9hdGluZyAzcyBlYXNlLWluLW91dCBpbmZpbml0ZTtcbn1cbi5ib286OmFmdGVyIHtcbiAgICBjb250ZW50OiAnJztcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgbGVmdDogLTE4LjgyMzUyOTQxMThweDtcbiAgICBib3R0b206IC04LjMxMTY4ODMxMTdweDtcbiAgICB3aWR0aDogY2FsYygxMDAlICsgMzJweCk7XG4gICAgaGVpZ2h0OiAzMnB4O1xuICAgIGJhY2tncm91bmQtcmVwZWF0OiByZXBlYXQteDtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IDMycHggMzJweDtcbiAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBsZWZ0IGJvdHRvbTtcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQoLTQ1ZGVnLCAjZjdmN2Y3IDE2cHgsIHRyYW5zcGFyZW50IDApLCBsaW5lYXItZ3JhZGllbnQoNDVkZWcsICNmN2Y3ZjcgMTZweCwgdHJhbnNwYXJlbnQgMCksIGxpbmVhci1ncmFkaWVudCgtNDVkZWcsICM5YjliOWIgMTguODIzNTI5NDExOHB4LCB0cmFuc3BhcmVudCAwKSwgbGluZWFyLWdyYWRpZW50KDQ1ZGVnLCAjOWI5YjliIDE4LjgyMzUyOTQxMThweCwgdHJhbnNwYXJlbnQgMCk7XG59XG4uYm9vIC5mYWNlIHtcbiAgICB3aWR0aDogMjRweDtcbiAgICBoZWlnaHQ6IDMuMnB4O1xuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjOWI5YjliO1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBsZWZ0OiA1MCU7XG4gICAgYm90dG9tOiA1NnB4O1xuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWCgtNTAlKTtcbn1cbi5ib28gLmZhY2U6OmJlZm9yZSwgLmJvbyAuZmFjZTo6YWZ0ZXIge1xuICAgIGNvbnRlbnQ6ICcnO1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIHdpZHRoOiA2cHg7XG4gICAgaGVpZ2h0OiA2cHg7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzliOWI5YjtcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGJvdHRvbTogNDBweDtcbn1cbi5ib28gLmZhY2U6OmJlZm9yZSB7XG4gICAgbGVmdDogLTI0cHg7XG59XG4uYm9vIC5mYWNlOjphZnRlciB7XG4gICAgcmlnaHQ6IC0yNHB4O1xufVxuLnNoYWRvdyB7XG4gICAgd2lkdGg6IDEyOHB4O1xuICAgIGhlaWdodDogMTZweDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDIzNCwgMjM0LCAyMzQsIC43NSk7XG4gICAgbWFyZ2luLXRvcDogNDBweDtcbiAgICBtYXJnaW4tcmlnaHQ6IGF1dG87XG4gICAgbWFyZ2luLWxlZnQ6IGF1dG87XG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xuICAgIGFuaW1hdGlvbjogZmxvYXRpbmdTaGFkb3cgM3MgZWFzZS1pbi1vdXQgaW5maW5pdGU7XG59XG4iXX0= */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](NotFoundComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-not-found',
                templateUrl: './not-found.component.html',
                styleUrls: ['./not-found.component.css']
            }]
    }], function () { return []; }, null); })();


/***/ }),

/***/ "./src/app/security/token-interceptor.service.ts":
/*!*******************************************************!*\
  !*** ./src/app/security/token-interceptor.service.ts ***!
  \*******************************************************/
/*! exports provided: TokenInterceptorService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TokenInterceptorService", function() { return TokenInterceptorService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _services_token_storage_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../services/token-storage.service */ "./src/app/services/token-storage.service.ts");



;
class TokenInterceptorService {
    constructor(token) {
        this.token = token;
    }
    intercept(request, next) {
        request = request.clone({
            setHeaders: {
                Authorization: `Bearer ${this.token.getToken()}`
            }
        });
        return next.handle(request);
    }
}
TokenInterceptorService.ɵfac = function TokenInterceptorService_Factory(t) { return new (t || TokenInterceptorService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_services_token_storage_service__WEBPACK_IMPORTED_MODULE_1__["TokenStorageService"])); };
TokenInterceptorService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: TokenInterceptorService, factory: TokenInterceptorService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](TokenInterceptorService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: _services_token_storage_service__WEBPACK_IMPORTED_MODULE_1__["TokenStorageService"] }]; }, null); })();


/***/ }),

/***/ "./src/app/services/cpn/auth.service.ts":
/*!**********************************************!*\
  !*** ./src/app/services/cpn/auth.service.ts ***!
  \**********************************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var _baseUrl__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../baseUrl */ "./src/app/baseUrl.ts");





const headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]();
headers.append('Content-Type', 'multipart/form-data');
headers.append('Accept', 'application/json');
class AuthService {
    constructor(http) {
        this.http = http;
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Content-Type', 'application/json');
    }
    register(form) {
        return this.http.post(_baseUrl__WEBPACK_IMPORTED_MODULE_2__["baseUrl"] + '/api/inscription', form, { headers, withCredentials: false });
    }
    login(form) {
        return this.http.post(_baseUrl__WEBPACK_IMPORTED_MODULE_2__["baseUrl"] + '/api/login', form, { headers, withCredentials: false });
    }
    sendMail(mail) {
        return this.http.post(_baseUrl__WEBPACK_IMPORTED_MODULE_2__["baseUrl"] + '/api/forgot-password', mail, { headers, withCredentials: false });
    }
    resetPass(form) {
        return this.http.post(_baseUrl__WEBPACK_IMPORTED_MODULE_2__["baseUrl"] + '/api/reset-password', form, { headers, withCredentials: false });
    }
    getUser() {
        return this.http.get(_baseUrl__WEBPACK_IMPORTED_MODULE_2__["baseUrl"] + '/api/profile', { headers, withCredentials: false });
    }
    updatUser(form) {
        return this.http.post(_baseUrl__WEBPACK_IMPORTED_MODULE_2__["baseUrl"] + '/api/update-profile', form, { headers, withCredentials: false });
    }
    getFellower() {
        return this.http.get(_baseUrl__WEBPACK_IMPORTED_MODULE_2__["baseUrl"] + '/api/linkd', { headers, withCredentials: false });
    }
}
AuthService.ɵfac = function AuthService_Factory(t) { return new (t || AuthService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"])); };
AuthService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: AuthService, factory: AuthService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AuthService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] }]; }, null); })();


/***/ }),

/***/ "./src/app/services/cpn/avis.service.ts":
/*!**********************************************!*\
  !*** ./src/app/services/cpn/avis.service.ts ***!
  \**********************************************/
/*! exports provided: AvisService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AvisService", function() { return AvisService; });
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");




const headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpHeaders"]();
headers.append('Content-Type', 'multipart/form-data');
headers.append('Accept', 'application/json');
class AvisService {
    constructor(http) {
        this.http = http;
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpHeaders"]().set('Content-Type', 'application/json');
        this.url = "http://crm.cpn-aide-aux-entreprises.com";
    }
    /*********************** avis/add ****************************/
    addAvis(form) {
        return this.http.post(this.url + "/api/avis/save", form, { headers, withCredentials: false });
    }
}
AvisService.ɵfac = function AvisService_Factory(t) { return new (t || AvisService)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpClient"])); };
AvisService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjectable"]({ token: AvisService, factory: AvisService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](AvisService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"],
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpClient"] }]; }, null); })();


/***/ }),

/***/ "./src/app/services/cpn/test-egibilite.service.ts":
/*!********************************************************!*\
  !*** ./src/app/services/cpn/test-egibilite.service.ts ***!
  \********************************************************/
/*! exports provided: TestEgibiliteService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TestEgibiliteService", function() { return TestEgibiliteService; });
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _baseUrl__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../baseUrl */ "./src/app/baseUrl.ts");





const headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpHeaders"]();
headers.append('Content-Type', 'multipart/form-data');
headers.append('Accept', 'application/json');
class TestEgibiliteService {
    constructor(http) {
        this.http = http;
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpHeaders"]().set('Content-Type', 'application/json');
    }
    /*********************** test/activities/get ****************************/
    getActivites() {
        return this.http.get(_baseUrl__WEBPACK_IMPORTED_MODULE_2__["baseUrl"] + "/api/test/activities/get");
    }
    /*********************** test/transitions/get ****************************/
    getTransitions() {
        return this.http.get(_baseUrl__WEBPACK_IMPORTED_MODULE_2__["baseUrl"] + "/api/test/transitions/get");
    }
    /*********************** test/grants/region/get ****************************/
    regionalGrant(region, budget, naf) {
        return this.http.get(_baseUrl__WEBPACK_IMPORTED_MODULE_2__["baseUrl"] + "/api/test/grants/region/" + region + "/" + budget + "/" + naf);
    }
    /*********************** test/grants/cpn/get ****************************/
    cpnGrant(service, budget) {
        return this.http.get(_baseUrl__WEBPACK_IMPORTED_MODULE_2__["baseUrl"] + "/api/test/grants/cpn/" + service + "/" + budget);
    }
    /*********************** test/events/get ****************************/
    getEvents() {
        return this.http.get(_baseUrl__WEBPACK_IMPORTED_MODULE_2__["baseUrl"] + "/api/test/events/get");
    }
    /*********************** test/events/add ****************************/
    addEvents(form) {
        return this.http.post(_baseUrl__WEBPACK_IMPORTED_MODULE_2__["baseUrl"] + "/api/test/events/add", form);
    }
    /*********************** test/service/turnover ****************************/
    getServiceTurnover(range) {
        return this.http.get(_baseUrl__WEBPACK_IMPORTED_MODULE_2__["baseUrl"] + "/api/test/service/turnover/" + range[0] + "/" + range[1], { withCredentials: false });
    }
    /*********************** test/company/siren ****************************/
    getCompanySiren(siret) {
        return this.http.get(_baseUrl__WEBPACK_IMPORTED_MODULE_2__["baseUrl"] + "/api/test/company/siren/" + siret, { withCredentials: false });
    }
    /*********************** test/contact/save ****************************/
    addContact(form) {
        return this.http.post(_baseUrl__WEBPACK_IMPORTED_MODULE_2__["baseUrl"] + "/api/test/contact/save", form, { withCredentials: false });
    }
    /*********************** test/contact/confirm ****************************/
    contactConfirm(form) {
        return this.http.post(_baseUrl__WEBPACK_IMPORTED_MODULE_2__["baseUrl"] + "/api/test/contact/confirm", form, { withCredentials: false });
    }
    /*********************** test/zoom/generate ****************************/
    addZoom(form) {
        return this.http.post(_baseUrl__WEBPACK_IMPORTED_MODULE_2__["baseUrl"] + "/api/test/zoom/generate", form, { withCredentials: false });
    }
    /*********************** test/timer/save ****************************/
    addTimer(form) {
        return this.http.post(_baseUrl__WEBPACK_IMPORTED_MODULE_2__["baseUrl"] + "/api/test/timer/save", form, { withCredentials: false });
    }
}
TestEgibiliteService.ɵfac = function TestEgibiliteService_Factory(t) { return new (t || TestEgibiliteService)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpClient"])); };
TestEgibiliteService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjectable"]({ token: TestEgibiliteService, factory: TestEgibiliteService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](TestEgibiliteService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"],
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpClient"] }]; }, null); })();


/***/ }),

/***/ "./src/app/services/token-storage.service.ts":
/*!***************************************************!*\
  !*** ./src/app/services/token-storage.service.ts ***!
  \***************************************************/
/*! exports provided: TokenStorageService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TokenStorageService", function() { return TokenStorageService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


const TOKEN_KEY = 'auth-token';
const USER_KEY = 'auth-user';
const projet = 'projet';
class TokenStorageService {
    constructor() { }
    signOut() {
        window.sessionStorage.clear();
    }
    saveToken(token) {
        window.sessionStorage.removeItem(TOKEN_KEY);
        window.sessionStorage.setItem(TOKEN_KEY, token);
    }
    getToken() {
        return window.sessionStorage.getItem(TOKEN_KEY);
    }
    getUSERKEY() {
        return window.sessionStorage.getItem(USER_KEY);
    }
    saveUser(user) {
        window.sessionStorage.removeItem(USER_KEY);
        window.sessionStorage.setItem(USER_KEY, JSON.stringify(user));
    }
    saveProjectId(id) {
        window.sessionStorage.setItem(projet, id);
    }
    getProjectId() {
        window.sessionStorage.getItem(projet);
    }
    getUser() {
        if (window.sessionStorage.getItem(USER_KEY)) {
            const user = window.sessionStorage.getItem(USER_KEY);
            if (user) {
                return user;
            }
        }
        return false;
    }
}
TokenStorageService.ɵfac = function TokenStorageService_Factory(t) { return new (t || TokenStorageService)(); };
TokenStorageService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: TokenStorageService, factory: TokenStorageService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](TokenStorageService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
                providedIn: 'root'
            }]
    }], function () { return []; }, null); })();


/***/ }),

/***/ "./src/app/test/test.component.ts":
/*!****************************************!*\
  !*** ./src/app/test/test.component.ts ***!
  \****************************************/
/*! exports provided: TestComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TestComponent", function() { return TestComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _angular_material_table__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/table */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/table.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _services_cpn_test_egibilite_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/cpn/test-egibilite.service */ "./src/app/services/cpn/test-egibilite.service.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/button */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/button.js");
/* harmony import */ var _map_french_map_french_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../map-french/map-french.component */ "./src/app/map-french/map-french.component.ts");
/* harmony import */ var _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ng-select/ng-select */ "./node_modules/@ng-select/ng-select/__ivy_ngcc__/fesm2015/ng-select-ng-select.js");
/* harmony import */ var _angular_slider_ngx_slider__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular-slider/ngx-slider */ "./node_modules/@angular-slider/ngx-slider/__ivy_ngcc__/fesm2015/angular-slider-ngx-slider.js");
/* harmony import */ var _angular_material_radio__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/material/radio */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/radio.js");
/* harmony import */ var _fullcalendar_angular__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @fullcalendar/angular */ "./node_modules/@fullcalendar/angular/__ivy_ngcc__/fesm2015/fullcalendar-angular.js");















function TestComponent_div_3_Template(rf, ctx) { if (rf & 1) {
    const _r23 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 40);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 42);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Testez votre \u00E9ligibilit\u00E9");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "button", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_3_Template_button_click_6_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r23); const ctx_r22 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r22.checkForm(ctx_r22.test.active.step); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, "Commencer");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function TestComponent_div_4_Template(rf, ctx) { if (rf & 1) {
    const _r25 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "app-map-french", 46);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("myOutput", function TestComponent_div_4_Template_app_map_french_myOutput_2_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r25); const ctx_r24 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r24.GetChildData($event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Choisissez votre Code postal");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "input", 47);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("myinputDep", ctx_r1.testEgibFormGroup.get("codeP").value);
} }
function TestComponent_div_5_Template(rf, ctx) { if (rf & 1) {
    const _r27 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 48);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 49);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Quel est votre secteur d'activit\u00E9 ?");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "fieldset", 35);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "ng-select", 50);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("change", function TestComponent_div_5_Template_ng_select_change_7_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r27); const ctx_r26 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r26.onChange($event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("items", ctx_r2.activities == null ? null : ctx_r2.activities.data);
} }
const _c0 = function (a0) { return { butttonREd: a0 }; };
function TestComponent_div_6_Template(rf, ctx) { if (rf & 1) {
    const _r29 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 51);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 52);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Status juridique");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 53);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 54);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "button", 55);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_6_Template_button_click_8_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r29); const ctx_r28 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r28.getstatus("SARL"); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, "SARL");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "button", 55);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_6_Template_button_click_10_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r29); const ctx_r30 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r30.getstatus("SAS"); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, "SAS");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "div", 54);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "button", 55);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_6_Template_button_click_13_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r29); const ctx_r31 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r31.getstatus("SASU"); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, "SASU");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "button", 55);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_6_Template_button_click_15_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r29); const ctx_r32 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r32.getstatus("EURL"); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, "EURL");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "button", 55);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_6_Template_button_click_17_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r29); const ctx_r33 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r33.getstatus("MICRO-ENT"); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18, "MICRO-ENT");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](5, _c0, ctx_r3.testEgibFormGroup.get("status").value === "SARL"));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](7, _c0, ctx_r3.testEgibFormGroup.get("status").value === "SAS"));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](9, _c0, ctx_r3.testEgibFormGroup.get("status").value === "SASU"));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](11, _c0, ctx_r3.testEgibFormGroup.get("status").value === "EURL"));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](13, _c0, ctx_r3.testEgibFormGroup.get("status").value === "MICRO-ENT"));
} }
function TestComponent_div_7_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 56);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 57);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Nom de votre entreprise");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "input", 58);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
const _c1 = function (a0) { return { baissD: a0 }; };
const _c2 = function (a0) { return { augmD: a0 }; };
function TestComponent_div_8_Template(rf, ctx) { if (rf & 1) {
    const _r37 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 59);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 60);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Avez vous perdu du chiffre d'affaires pendant la crise sanitaire ?");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 53);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 54);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 61);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_8_Template_div_click_8_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r37); const ctx_r36 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r36.changeEtatB(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "Baisse");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 62);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](12, "input", 63, 64);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 65);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "button", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_8_Template_button_click_15_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r37); const ctx_r38 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r38.incTurn(10, 0 - 100, 0); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, "+");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "button", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_8_Template_button_click_17_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r37); const ctx_r39 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r39.decTurn(10, 0 - 100, 0); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18, "-");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "div", 54);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "div", 61);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_8_Template_div_click_20_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r37); const ctx_r40 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r40.changeEtatA(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](22, "Augmentation");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "div", 66);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](24, "input", 63, 64);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "div", 65);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "button", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_8_Template_button_click_27_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r37); const ctx_r41 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r41.incTurn(10, 0, 100); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](28, "+");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "button", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_8_Template_button_click_29_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r37); const ctx_r42 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r42.decTurn(10, 0, 100); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](30, "-");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "div", 54);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](32, "input", 67);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "label", 68);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](34, "Etat Stable");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](11);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](4, _c1, ctx_r5.showB === true));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("readonly", true);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](11);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](6, _c2, ctx_r5.showA === true));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("readonly", true);
} }
function TestComponent_div_9_option_7_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "option", 73);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const item_r44 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r44);
} }
function TestComponent_div_9_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 69);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "Avez vous d\u00E9ja obtenu des aides de l'\u00E9tat");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "img", 70);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "select", 71);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](7, TestComponent_div_9_option_7_Template, 2, 1, "option", 72);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r6.test.data[ctx_r6.test.active.step].options);
} }
function TestComponent_div_10_Template(rf, ctx) { if (rf & 1) {
    const _r46 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 74);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 75);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "img", 76);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "Dernier chiffre d'affaires r\u00E9alis\u00E9 ?");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 77);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 78);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "ngx-slider", 79);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("valueChange", function TestComponent_div_10_Template_ngx_slider_valueChange_9_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r46); const ctx_r45 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r45.minValue = $event; })("highValueChange", function TestComponent_div_10_Template_ngx_slider_highValueChange_9_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r46); const ctx_r47 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r47.maxValue = $event; })("userChangeStart", function TestComponent_div_10_Template_ngx_slider_userChangeStart_9_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r46); const ctx_r48 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r48.onUserChangeStart($event); })("userChange", function TestComponent_div_10_Template_ngx_slider_userChange_9_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r46); const ctx_r49 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r49.onUserChange($event); })("userChangeEnd", function TestComponent_div_10_Template_ngx_slider_userChangeEnd_9_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r46); const ctx_r50 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r50.onUserChangeEnd($event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r7 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](9);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("value", ctx_r7.minValue)("highValue", ctx_r7.maxValue)("options", ctx_r7.options);
} }
function TestComponent_div_11_option_9_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "option");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const item_r52 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r52);
} }
function TestComponent_div_11_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 80);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 81);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Nombre de salari\u00E9s ?");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "select", 82);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "option");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "select les nombres salaries");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](9, TestComponent_div_11_option_9_Template, 2, 1, "option", 83);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r8 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](9);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r8.test.data[ctx_r8.test.active.step].options);
} }
function TestComponent_div_12_div_1_div_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 89);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 90);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Avez vous un site internet pour votre entreprise ?");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "mat-radio-group", 91);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "mat-radio-button", 92);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "Oui");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "mat-radio-button", 93);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "Non");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function TestComponent_div_12_div_1_div_2_Template(rf, ctx) { if (rf & 1) {
    const _r64 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 94);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 95);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Quel est votre type de site internet ?");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "mat-radio-group", 96);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "mat-radio-button", 97);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "E-commerce");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "mat-radio-button", 98);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "Vitrine");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "mat-radio-button", 99);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "Market-place");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 100);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 54);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, "Nombre de Vente");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "div", 101);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](18, "input", 102, 103);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "div", 65);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "button", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_12_div_1_div_2_Template_button_click_21_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r64); const ctx_r63 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](3); return ctx_r63.incVente(10, 0, 100); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](22, "+");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "button", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_12_div_1_div_2_Template_button_click_23_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r64); const ctx_r65 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](3); return ctx_r65.decVente(10, 0, 100); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](24, "-");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "div", 54);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](27, "Nombre de Visite");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "div", 101);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](29, "input", 104, 105);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "div", 65);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "button", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_12_div_1_div_2_Template_button_click_32_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r64); const ctx_r66 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](3); return ctx_r66.incVisite(10, 0, 100); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](33, "+");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "button", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_12_div_1_div_2_Template_button_click_34_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r64); const ctx_r67 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](3); return ctx_r67.decVisite(10, 0, 100); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](35, "-");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "div", 54);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](38, "Nombre d'utilisateurs");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "div", 101);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](40, "input", 106, 107);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "div", 65);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "button", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_12_div_1_div_2_Template_button_click_43_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r64); const ctx_r68 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](3); return ctx_r68.incUser(10, 0, 100); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](44, "+");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "button", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_12_div_1_div_2_Template_button_click_45_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r64); const ctx_r69 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](3); return ctx_r69.decUser(10, 0, 100); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](46, "-");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](18);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("readonly", true);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](11);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("readonly", true);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](11);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("readonly", true);
} }
function TestComponent_div_12_div_1_div_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 108);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 109);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Lien du site ?");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "input", 110);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function TestComponent_div_12_div_1_div_4_option_9_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "option");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const item_r71 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r71);
} }
function TestComponent_div_12_div_1_div_4_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 111);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "D\u00E2te de d\u00E9veloppement ?");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "img", 112);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "select", 113);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "option");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "choisir un date ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](9, TestComponent_div_12_div_1_div_4_option_9_Template, 2, 1, "option", 83);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r58 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](9);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r58.test.data[ctx_r58.test.active.step].website[ctx_r58.test.active.subStep].options);
} }
function TestComponent_div_12_div_1_div_5_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 114);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 109);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "L'agence qui a d\u00E9velopp\u00E9 votre site ?");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "input", 115);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "mat-radio-button", 116);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "Internet/Freelance");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function TestComponent_div_12_div_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, TestComponent_div_12_div_1_div_1_Template, 11, 0, "div", 84);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, TestComponent_div_12_div_1_div_2_Template, 47, 3, "div", 85);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, TestComponent_div_12_div_1_div_3_Template, 7, 0, "div", 86);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](4, TestComponent_div_12_div_1_div_4_Template, 10, 1, "div", 87);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](5, TestComponent_div_12_div_1_div_5_Template, 9, 0, "div", 88);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r53 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r53.test.active.subStep == 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r53.test.active.subStep == 2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r53.test.active.subStep == 3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r53.test.active.subStep == 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r53.test.active.subStep == 5);
} }
function TestComponent_div_12_div_2_div_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 121);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 122);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Utilisez-vous des Plate-formes en ligne ou des logiciels pour faciliter vos ventes ?");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "mat-radio-group", 123);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "mat-radio-button", 92);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "Oui");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "mat-radio-button", 93);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "Non");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function TestComponent_div_12_div_2_div_2_option_10_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "option", 127);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "img", 128);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const item_r78 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r78);
} }
function TestComponent_div_12_div_2_div_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 124);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "Quels types d'outils utilisez-vous pour vos ventes ?");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "(Exemple:un CRM)");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "select", 125);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "option");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, "choisir un crm");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](10, TestComponent_div_12_div_2_div_2_option_10_Template, 3, 1, "option", 126);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r73 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r73.test.data[ctx_r73.test.active.step].crm[ctx_r73.test.active.subStep].options);
} }
function TestComponent_div_12_div_2_div_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 129);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 130);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Utilisez-vous des Plate-formes en ligne ou des logiciels pour faciliter votre logistique interne ?");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "mat-radio-group", 131);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "mat-radio-button", 92);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "Oui");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "mat-radio-button", 93);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "Non");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function TestComponent_div_12_div_2_div_4_option_10_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "option", 127);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "img", 128);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const item_r80 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r80);
} }
function TestComponent_div_12_div_2_div_4_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 132);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "Quels types d'outils utilisez-vous pour votre logistique ?");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "(Exemple:un ERP)");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "select", 133);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "option");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, "choisir un erp");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](10, TestComponent_div_12_div_2_div_4_option_10_Template, 3, 1, "option", 126);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r75 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r75.test.data[ctx_r75.test.active.step].crm[ctx_r75.test.active.subStep].options);
} }
function TestComponent_div_12_div_2_div_5_option_9_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "option");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const item_r82 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r82);
} }
function TestComponent_div_12_div_2_div_5_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 111);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "D\u00E2te de d\u00E9veloppement ?");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "img", 112);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "select", 134);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "option");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "choisir un date ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](9, TestComponent_div_12_div_2_div_5_option_9_Template, 2, 1, "option", 83);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r76 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](9);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r76.test.data[ctx_r76.test.active.step].crm[ctx_r76.test.active.subStep].options);
} }
function TestComponent_div_12_div_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, TestComponent_div_12_div_2_div_1_Template, 11, 0, "div", 117);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, TestComponent_div_12_div_2_div_2_Template, 11, 1, "div", 118);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, TestComponent_div_12_div_2_div_3_Template, 11, 0, "div", 119);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](4, TestComponent_div_12_div_2_div_4_Template, 11, 1, "div", 120);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](5, TestComponent_div_12_div_2_div_5_Template, 10, 1, "div", 87);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r54 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r54.test.active.subStep == 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r54.test.active.subStep == 2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r54.test.active.subStep == 3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r54.test.active.subStep == 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r54.test.active.subStep == 5);
} }
function TestComponent_div_12_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, TestComponent_div_12_div_1_Template, 6, 5, "div", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, TestComponent_div_12_div_2_Template, 6, 5, "div", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r9 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r9.test.active.subStepCat == 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r9.test.active.subStepCat == 2);
} }
function TestComponent_div_13_div_9_Template(rf, ctx) { if (rf & 1) {
    const _r86 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 142);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 143);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 144);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "i", 145);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 146);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 147);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "input", 148);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_13_div_9_Template_input_click_8_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r86); const items_r84 = ctx.$implicit; const ctx_r85 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2); return ctx_r85.selectService(items_r84 == null ? null : items_r84.id); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const items_r84 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](items_r84 == null ? null : items_r84.name);
} }
function TestComponent_div_13_Template(rf, ctx) { if (rf & 1) {
    const _r88 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 135);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 136);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "label", 137);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Service");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "input", 138);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("keyup", function TestComponent_div_13_Template_input_keyup_6_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r88); const ctx_r87 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r87.valuechange($event.target.value); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](7, "i", 139);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 140);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](9, TestComponent_div_13_div_9_Template, 9, 1, "div", 141);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r10 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](9);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r10.dataSource.filteredData);
} }
function TestComponent_div_14_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 149);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "Budget d'investissement");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "img", 70);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "select", 150);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "option");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "choisir un budget");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "option", 151);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "300$");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "option", 152);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "400$");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "option", 153);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, "500$");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "option", 154);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, "600$");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function TestComponent_div_15_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 155);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 156);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Num\u00E9ro de Siret");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "input", 157);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function TestComponent_div_16_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 158);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 159);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Adresse");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 144);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "label", 137);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "Adresse");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "input", 160);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 146);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "label", 137);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "ville");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](13, "input", 161);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "label", 137);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "code postal");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](16, "input", 162);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](16);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("readonly", true);
} }
function TestComponent_div_17_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 163);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "Fiche de rensignement");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "img", 164);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 144);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "label", 137);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "Nom");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "input", 165);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 144);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "label", 137);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "Pr\u00E9nom");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](13, "input", 166);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 144);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "label", 137);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, "mail");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](17, "input", 167);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "div", 144);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "label", 137);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20, "Num\u00E9ro de portable");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](21, "input", 168);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "div", 144);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "label", 137);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](24, "Num\u00E9ro d'entreprise");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](25, "input", 169);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "div", 146);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "mat-radio-group", 170);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "mat-radio-button", 171);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](29, "G\u00E9rant");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "mat-radio-button", 172);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](31, "Associ\u00E9");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "mat-radio-button", 173);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](33, "Dir\u00E9cteur");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "mat-radio-button", 174);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](35, "autre");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function TestComponent_div_18_p_13_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "de ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "strong", 181);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r89 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", ctx_r89.test.result.cpn.amount, " \u20AC");
} }
function TestComponent_div_18_Template(rf, ctx) { if (rf & 1) {
    const _r91 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 175);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 176);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "F\u00E9licitaion");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, "vous \u00EAtes \u00E9ligible");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](8, "img", 177);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "Vous b\u00E9neficiez d'un ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "strong");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "Ch\u00E8que Num\u00E9rique");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](13, TestComponent_div_18_p_13_Template, 4, 1, "p", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "offert par le ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "strong");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17, "CPN");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](18, "img", 178);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "div", 179);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_18_Template_div_click_19_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r91); const ctx_r90 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r90.showResult(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](20, "img", 180);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r15 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](13);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r15.test.result.cpn.amount);
} }
function TestComponent_div_19_Template(rf, ctx) { if (rf & 1) {
    const _r93 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 175);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 176);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "F\u00E9licitaion");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, "vous \u00EAtes \u00E9ligible");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](8, "img", 177);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "Vous b\u00E9neficiez d'un ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "strong");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "Ch\u00E8que commerce connect\u00E9");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, "de ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "strong", 181);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18, "offert par le ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "strong");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20, "CPN");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](21, "img", 178);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "div", 179);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_19_Template_div_click_22_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r93); const ctx_r92 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r92.elgiblTest(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](23, "img", 180);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r16 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](16);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", ctx_r16.test.result.regional.amount, " $");
} }
function TestComponent_div_20_Template(rf, ctx) { if (rf & 1) {
    const _r95 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 175);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "h1", 182);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, "Malheureusement");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "h1", 182);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "vous n'\u00EAtes pas \u00E9ligible");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](7, "img", 183);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, "\u00E0 l'aide de votre r\u00E9gion");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 179);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_20_Template_div_click_10_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r95); const ctx_r94 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r94.elgiblTest(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](11, "img", 180);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function TestComponent_div_21_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 184);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 185);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Vos disponibilit\u00E9s");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 186);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](7, "full-calendar", 187, 188);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "mat-radio-group", 189);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "mat-radio-button", 190);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, "Entretien vid\u00E9o direct");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "mat-radio-button", 191);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "Visite de courtoisie");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r18 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("options", ctx_r18.calendarOption);
} }
function TestComponent_div_22_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 192);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "img", 193);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "F\u00E9licitaion");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "Un conseiller entrera en contact avec");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, " vous dans 30min");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](11, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "div", 194);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, "Suivez-nous");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "div", 195);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](16, "img", 196);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](17, "img", 197);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](18, "img", 198);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](19, "img", 199);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
const _c3 = function (a0) { return { checkIcon: a0 }; };
function TestComponent_div_27_Template(rf, ctx) { if (rf & 1) {
    const _r98 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 200);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "a", 201);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_27_Template_a_click_1_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r98); const ctx_r97 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r97.prevStep(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "i", 202);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 203);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "a");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](5, "i", 204);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "a");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](7, "i", 204);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "a");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "i", 204);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "a");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](11, "i", 204);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "a");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](13, "i", 204);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "a");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](15, "i", 204);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "a");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](17, "i", 204);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "a");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](19, "i", 204);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "a");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](21, "i", 204);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "a");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](23, "i", 204);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "a");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](25, "i", 204);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "a");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](27, "i", 204);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "a");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](29, "i", 204);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "a");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](31, "i", 204);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "a");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](33, "i", 204);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "a");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](35, "i", 204);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "a", 201);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_27_Template_a_click_36_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r98); const ctx_r99 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r99.checkForm(ctx_r99.test.active.step); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](37, "i", 205);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r20 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](16, _c3, ctx_r20.test.active.step == 1));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](18, _c3, ctx_r20.test.active.step == 2));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](20, _c3, ctx_r20.test.active.step == 3));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](22, _c3, ctx_r20.test.active.step == 4));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](24, _c3, ctx_r20.test.active.step == 5));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](26, _c3, ctx_r20.test.active.step == 6));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](28, _c3, ctx_r20.test.active.step == 7));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](30, _c3, ctx_r20.test.active.step == 8));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](32, _c3, ctx_r20.test.active.step == 9));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](34, _c3, ctx_r20.test.active.step == 10));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](36, _c3, ctx_r20.test.active.step == 11));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](38, _c3, ctx_r20.test.active.step == 12));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](40, _c3, ctx_r20.test.active.step == 13));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](42, _c3, ctx_r20.test.active.step == 14));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](44, _c3, ctx_r20.test.active.step == 15));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](46, _c3, ctx_r20.test.active.step == 16));
} }
class TestComponent {
    /***********************************life cycle *******************/
    constructor(_formBuilder, testService) {
        this._formBuilder = _formBuilder;
        this.testService = testService;
        /***************************all variable **************************/
        this.myInputDepartment = "03";
        this.i = 9;
        this.transition = [];
        this.onChange = ($event) => {
            console.log('activitie', this.testEgibFormGroup.value.activite);
            console.log(`SELECTION CHANGED INTO ${$event.name || ''}`);
        };
        /***********************select transition ******************/
        this.dataSource = new _angular_material_table__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"]();
        /**********************************************turnover ******************************************/
        this.turn = 0;
        this.showA = false;
        this.showB = false;
        /**********************************************nombre visite ******************************************/
        this.visite = 0;
        /********************************************** nombre vente ******************************************/
        this.vente = 0;
        /**********************************************nombre user ******************************************/
        this.users = 0;
        /****************************************************** resultat de test **************************************/
        this.eleg = null;
        /*****************************************test step **************************************/
        this.test = {
            active: {
                step: 0,
                subStep: 1,
                subStepCat: 0,
                stepType: "form",
                popup: false,
                confirmed: false,
            },
            result: {
                isOpen: false,
                isLoading: false,
                isCpn: true,
                regional: {
                    id: null,
                    region: null,
                    eligible: false,
                    voucher: null,
                    amount: null,
                },
                cpn: {
                    id: null,
                    amount: null,
                    originalPrice: null,
                    sellPrice: null,
                },
            },
            zoom: {
                generating: false,
                generated: false,
            },
            orientations: [],
            data: [
                {
                    step: 0,
                    title: "Bienvenue"
                },
                {
                    step: 1,
                    title: "Renseigner le code postal",
                },
                {
                    step: 2,
                    title: "Nom de l'entreprise"
                },
                {
                    step: 3,
                    title: "Statut juridique"
                },
                {
                    step: 4,
                    title: "Secteur d'activité",
                    options: [],
                },
                {
                    step: 5,
                    title: "Avez vous perdu du chiffre d'affaires pendant la crise sanitaire",
                    labels: [
                        "Baisse",
                        "",
                        "",
                        "",
                        "",
                        "-50%",
                        "",
                        "",
                        "",
                        "",
                        "Stable",
                        "",
                        "",
                        "",
                        "",
                        "50%",
                        "",
                        "",
                        "",
                        "",
                        "Hausse"
                    ],
                },
                {
                    step: 6,
                    title: "Avez vous déja obtenu des aides de l'état",
                    options: [
                        "Chéque numérique et aide numérique de votre région",
                        "Crédit d'impôt",
                        "Fond de solidarité",
                        "Chaumage partiel",
                        "Aucune aide",
                    ],
                },
                {
                    step: 7,
                    title: "Dernier chiffre d'affaires réalisé",
                    labels: [
                        "5k €",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "700k €",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "3.5m €",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "7.5m €",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "30m €",
                    ],
                    selectedRange: [0, 1],
                    range: [
                        { value: 5000, legend: "5k €" },
                        { value: 50000 },
                        { value: 100000 },
                        { value: 200000 },
                        { value: 300000 },
                        { value: 400000 },
                        { value: 500000 },
                        { value: 600000 },
                        { value: 700000, legend: "700k €" },
                        { value: 800000 },
                        { value: 900000 },
                        { value: 1000000 },
                        { value: 1500000 },
                        { value: 2000000 },
                        { value: 2500000 },
                        { value: 3000000 },
                        { value: 3500000, legend: "3.5m €" },
                        { value: 4000000 },
                        { value: 4500000 },
                        { value: 5000000 },
                        { value: 5500000 },
                        { value: 6000000 },
                        { value: 6500000 },
                        { value: 7000000 },
                        { value: 7500000, legend: "7.5m €" },
                        { value: 8000000 },
                        { value: 8500000 },
                        { value: 9000000 },
                        { value: 9500000 },
                        { value: 10000000 },
                        { value: 15000000 },
                        { value: 20000000 },
                        { value: 25000000 },
                        { value: 30000000, legend: "30m €" },
                    ]
                },
                {
                    step: 8,
                    title: "Nombre de salariés",
                    options: [
                        "de 0 à 5 Personnes",
                        "de 5 à 10 Personnes",
                        "de 10 à 20 Personnes",
                        "de 20 à 30 Personnes",
                        "de 30 à 40 Personnes",
                        "de 40 à 50 Personnes",
                        "plus de 50 Personnes",
                    ],
                },
                {
                    step: 9,
                    title: "Type de site",
                    website: [
                        {
                            subStep: 0,
                            title: ""
                        },
                        {
                            subStep: 1,
                            title: "Avez vous un site internet pour votre entreprise"
                        },
                        {
                            subStep: 2,
                            title: "Type de site"
                        },
                        {
                            subStep: 3,
                            title: "Lien de site"
                        },
                        {
                            subStep: 4,
                            title: "Date de développement",
                            options: [
                                "Avant 2000",
                                "Année 2000-2003",
                                "Année 2003-2006",
                                "Année 2006-2009",
                                "Année 2009-2012",
                                "Année 2012-2015",
                                "Année 2015-2018",
                                "Année 2018-2021",
                            ]
                        },
                        {
                            subStep: 5,
                            title: "L'agence qui a développé votre site"
                        }
                    ],
                    crm: [
                        {
                            subStep: 0,
                            title: ""
                        },
                        {
                            subStep: 1,
                            title: "Avez vous un crm pour votre entreprise"
                        },
                        {
                            subStep: 2,
                            title: "Quel type de CRM vous utilisez",
                            options: [
                                "Zoho",
                                "SAP",
                                "Sage",
                                "Oracle",
                                "NetSuite",
                                "Cegid",
                                "Microsoft Dynamics",
                                "Divalto",
                                "WaveSoft",
                                "Odoo",
                                "Archipelia",
                                "Axonaut",
                            ]
                        },
                        {
                            subStep: 3,
                            title: "Le crm a été développé"
                        },
                        {
                            subStep: 4,
                            title: "Quel type de ERP vous utilisez",
                            options: [
                                "Zoho",
                                "SAP",
                                "Sage",
                                "Oracle",
                                "NetSuite",
                                "Cegid",
                                "Microsoft Dynamics",
                                "Divalto",
                                "WaveSoft",
                                "Odoo",
                                "Archipelia",
                                "Axonaut",
                            ]
                        },
                        {
                            subStep: 5,
                            title: "Date de développement",
                            options: [
                                "Avant 2000",
                                "Année 2000-2003",
                                "Année 2003-2006",
                                "Année 2006-2009",
                                "Année 2009-2012",
                                "Année 2012-2015",
                                "Année 2015-2018",
                                "Année 2018-2021",
                            ]
                        }
                    ]
                },
                {
                    step: 10,
                    title: "Quel projet est à subventionner pour votre transition numérique",
                    tabServices: null,
                    loading: false,
                    services: ["Services éligible", "Services suplémentaire"],
                    tabCategories: null,
                    categories: ["Tous", "Graphique", "Développement", "Montage", "Marketing"],
                    options: [],
                },
                {
                    step: 11,
                    title: "Budget d'investissement",
                    budget: 5,
                    min: 400,
                    target: 500,
                    max: 100000,
                },
                {
                    step: 12,
                    title: "Numéros d'identification",
                    loading: false,
                },
                {
                    step: 13,
                    title: "Adresse",
                },
                {
                    step: 14,
                    title: "Fiche de renseignement",
                    options: [
                        "Gérant",
                        "Directeur",
                        "Associé",
                        "Autre"
                    ],
                },
                {
                    step: 15,
                    title: "Vos disponibilités",
                },
                {
                    step: 16,
                    title: "Type de client",
                    items: ['☹️', '🙁', '😐', '🙂', '😊', '😍'],
                    labels: [
                        "agressif",
                        "indécis",
                        "anxieux",
                        "économe",
                        "compréhensif",
                        "roi",
                    ]
                },
                {
                    step: 17,
                    title: "Merci pour votre temps",
                },
            ],
        };
        this.min = 0;
        this.max = 0;
        this.maxValue = 80;
        this.minValue = this.min;
        this.options = {
            showTicks: true,
            draggableRangeOnly: false,
            stepsArray: this.test.data[7].range,
            translate: (value, label) => {
                console.log('stepprec', value);
                return (value / 1000 > 900) ? ((value / 1000) / 1000).toFixed(1) + "m €" : (value / 1000).toFixed(0) + "k €";
            }
        };
        this.logText = '';
        this.range = [];
        /*************************form  data contact************************/
        this.testEgibFormGroup = this._formBuilder.group({
            codeP: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern("[0-9 ]{5}")]],
            nomSoc: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            activite: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            status: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            help: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            Nvente: ['0%', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            Nvisite: ['0%', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            Nuser: ['0%', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            personneSal: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            turnover: [0, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            lastTurnover: [0, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            haveSite: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            haveCrm: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            haveErp: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            liensite: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            datesite: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            siteVal: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            dateCrm: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            nomCrm: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            nomErp: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            typeCRM: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            typeERP: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            typeSite: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            crmDev: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            agence: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            budget: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            service: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            siret: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern("[0-9 ]{14}")]],
            siren: [''],
            naf: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            adresse: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            region: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            city: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            country: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            prenom: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            nom: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            email: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            phone: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            departement: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            phoneEntrep: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            post: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            contactID: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            meetingType: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            search: [''],
        });
        /*************************form data event *******************************/
        this.addEventForm = this._formBuilder.group({
            title: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]],
            dateDebut: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]],
            cid: this.testEgibFormGroup.get("contactID").value
        });
        /*************************************calandreir ***************************/
        this.calendarOption = {
            customButtons: {
                myCustomButton: {
                    text: 'custom!',
                    click: function () {
                        alert('clicked the custom button!');
                    }
                }
            },
            locale: "fr",
            initialView: 'dayGridMonth',
            //initialEvents: INITIAL_EVENTS, // alternatively, use the events setting to fetch from a feed
            weekends: true,
            editable: true,
            selectable: true,
            selectMirror: true,
            droppable: false,
            displayEventTime: true,
            disableDragging: false,
            timeZone: 'UTC',
            refetchResourcesOnNavigate: true,
            headerToolbar: {
                left: 'prev,next today',
                center: 'title',
                right: 'dayGridMonth,dayGridWeek,dayGridDay'
            },
            dayMaxEvents: true,
            events: [],
            dateClick: this.handleDateClick,
        };
    }
    get f() { return this.addEventForm.controls; }
    ngOnInit() {
        this.getTransition();
        this.getActivite();
    }
    /**********************get departement *******************/
    GetChildData(data) {
        console.log("region", data);
        this.testEgibFormGroup.get('codeP').setValue(data === null || data === void 0 ? void 0 : data.zipCode);
        this.testEgibFormGroup.get('region').setValue(data === null || data === void 0 ? void 0 : data.region);
        this.testEgibFormGroup.get('departement').setValue(data === null || data === void 0 ? void 0 : data.departement);
    }
    /***************************************select date rendez vous *************************/
    /*Show Modal with Forn on dayClick Event*/
    handleDateClick() {
        console.log("dateselect");
    }
    sendRendvous() {
        console.log('event', this.addEventForm.value);
        this.testService.addEvents({ title: this.addEventForm.value.title, dateDebut: this.addEventForm.value.dateDebut, cid: this.cid }).subscribe(res => {
            console.log('event', res);
            sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire({
                position: 'top-end',
                icon: 'success',
                title: 'ajout reussie',
                showConfirmButton: false,
                timer: 1500
            });
        }, error => {
            console.log(error);
            sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'quelque chose est incorrect !',
            });
        });
    }
    /***********************************generate lien zoom ****************/
    generateZoomLink() {
        this.test.zoom.generating = true;
        this.test.zoom.generated = true;
        this.testService.addZoom({ cid: this.cid, type: this.testEgibFormGroup.value.meetingType })
            .subscribe(response => {
            console.log('zoom', response);
            if (!response.error) {
                this.test.zoom.generating = false;
                this.test.zoom.generated = true;
                sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire({
                    icon: 'success',
                    title: 'genrate lien zoom reussie',
                    showConfirmButton: false,
                    timer: 1500
                });
                this.nextStep();
            }
            else {
                this.test.zoom.generating = false;
                sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: response.message + ' !',
                });
            }
        }, error => {
            console.log(error);
            sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'error 500 !',
            });
        });
    }
    /***********************get all  activites ******************/
    getActivite() {
        this.testService.getActivites().subscribe(res => {
            this.activities = res;
            console.log("activi", this.activities);
        });
    }
    /*********************** selection turnover ******************/
    selectedRange(val) {
        this.testEgibFormGroup.value.turnover = val;
    }
    valuechange(val) {
        this.dataSource.filter = val.trim().toLowerCase();
        this.transition = this.dataSource.filteredData;
    }
    getTransition() {
        this.testService.getTransitions().subscribe(res => {
            this.dataSource = new _angular_material_table__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](res === null || res === void 0 ? void 0 : res.data);
            console.log("transition", this.transition);
            //  this.elegible=res?.data.filter(data=>data.category==='Services')
            //  this.graphic=res?.data.filter(data=>data.category==='Graphique')
            // this.montage=res?.data.filter(data=>data.category==='Montage')
            // this.marketing=res?.data.filter(data=>data.category==='Marketing')
            // this.development=res?.data.filter(data=>data.category==='Développement')
            // console.log("elegi",this.elegible)
            // console.log("suplimet",this.development)
        });
    }
    /*************************************services ***************************************/
    selectService(val) {
        console.log('service', val);
        this.testEgibFormGroup.get('service').setValue(val);
    }
    incTurn(val, min, max) {
        console.log('turn', this.turn);
        if (this.turn > max + 10) {
            this.turn = 0;
        }
        if (this.turn < max) {
            this.turn = val + this.turn;
            this.testEgibFormGroup.get('turnover').setValue(this.turn + '%');
        }
    }
    decTurn(val, min, max) {
        console.log('turn', this.turn);
        if (this.turn < min + 10) {
            this.turn = 0;
        }
        if (this.turn > min) {
            this.turn = this.turn - val;
            this.testEgibFormGroup.get('turnover').setValue(this.turn + '%');
        }
    }
    changeEtatA() {
        this.showA = true;
        this.showB = false;
    }
    changeEtatB() {
        this.showA = false;
        this.showB = true;
    }
    incVisite(val, min, max) {
        console.log('turn', this.turn);
        if (this.turn > max + 10) {
            this.turn = 0;
        }
        if (this.turn < max) {
            this.turn = val + this.turn;
            this.testEgibFormGroup.get('Nvisite').setValue(this.turn + '%');
        }
    }
    decVisite(val, min, max) {
        console.log('turn', this.turn);
        if (this.turn < min + 10) {
            this.turn = 0;
        }
        if (this.turn > min) {
            this.turn = this.turn - val;
            this.testEgibFormGroup.get('Nvisite').setValue(this.turn + '%');
        }
    }
    incVente(val, min, max) {
        console.log('turn', this.turn);
        if (this.turn > max + 10) {
            this.turn = 0;
        }
        if (this.turn < max) {
            this.turn = val + this.turn;
            this.testEgibFormGroup.get('Nvente').setValue(this.turn + '%');
        }
    }
    decVente(val, min, max) {
        console.log('turn', this.turn);
        if (this.turn < min + 10) {
            this.turn = 0;
        }
        if (this.turn > min) {
            this.turn = this.turn - val;
            this.testEgibFormGroup.get('Nvente').setValue(this.turn + '%');
        }
    }
    incUser(val, min, max) {
        console.log('turn', this.turn);
        if (this.turn > max + 10) {
            this.turn = 0;
        }
        if (this.turn < max) {
            this.turn = val + this.turn;
            this.testEgibFormGroup.get('Nuser').setValue(this.turn + '%');
        }
    }
    decUser(val, min, max) {
        console.log('turn', this.turn);
        if (this.turn < min + 10) {
            this.turn = 0;
        }
        if (this.turn > min) {
            this.turn = this.turn - val;
            this.testEgibFormGroup.get('Nuser').setValue(this.turn + '%');
        }
    }
    /**********************************************Last turnover ******************************************/
    setLastTurnover(val) {
        const step = this.test.active.step;
        let range = [
            val[0],
            val[1]
        ];
        console.log('val', val);
        console.log('range', range);
        this.testService.getServiceTurnover(range).subscribe(response => {
            console.log('data', response);
            this.testEgibFormGroup.get('service').setValue(response.data.transition_id);
            this.testEgibFormGroup.get('lastTurnover').setValue(response.data.id);
            this.testEgibFormGroup.get('budget').setValue(Math.ceil(response.data.budget / 100) * 100);
            this.test.data[11].budget = Math.ceil(response.data.budget / 100);
            this.test.data[11].min = Math.ceil(response.data.budget_min / 100) * 100;
            this.test.data[11].target = Math.ceil(response.data.budget / 100) * 100;
            this.test.data[11].max = Math.ceil(response.data.budget_max / 100) * 100;
        });
    }
    /******************************************************** get naf with siret**********************************/
    getNafCompany(siret) {
        let siren = siret.substring(0, 9);
        this.testEgibFormGroup.get('siren').setValue(siren);
        this.test.data[this.test.active.step].loading = true;
        siret = this.testEgibFormGroup.value.siret;
        this.testService.getCompanySiren(this.testEgibFormGroup.value.siret).subscribe(response => {
            console.log('siren', response);
            this.test.data[this.test.active.step].loading = false;
            this.testEgibFormGroup.get('naf').setValue(response.ape);
            this.testEgibFormGroup.get('naf').setValue(response.ape);
            return true;
        }, error => {
            console.log(error);
            sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'code siret est introuvable !',
            });
            return false;
        });
        return true;
    }
    /******************************************************** save client to database**********************************/
    setContactForm(formDatas) {
        this.testService.addContact({
            "address": {
                "advisorName": this.testEgibFormGroup.value.nomSoc,
                "line": this.testEgibFormGroup.value.adresse,
                "zipcode": this.testEgibFormGroup.value.codeP,
                "region": this.testEgibFormGroup.value.region,
                "departement": this.testEgibFormGroup.value.departement,
                "city": this.testEgibFormGroup.value.city,
                "country": this.testEgibFormGroup.value.country
            },
            "companies": {
                "name": this.testEgibFormGroup.value.nomSoc,
                "status": this.testEgibFormGroup.value.status,
                "activity": this.testEgibFormGroup.value.activite.id,
                "help": this.testEgibFormGroup.value.help,
                "salaries": this.testEgibFormGroup.value.personneSal,
                "siret": this.testEgibFormGroup.value.siret,
                "siren": this.testEgibFormGroup.value.siren,
                "naf": this.testEgibFormGroup.value.naf,
                "phone": this.testEgibFormGroup.value.phoneEntrep,
                "turnover": this.testEgibFormGroup.value.turnover,
                "lastTurnover": this.testEgibFormGroup.value.lastTurnover
            },
            "contacts": {
                "firstName": this.testEgibFormGroup.value.nom,
                "lastName": this.testEgibFormGroup.value.prenom,
                "email": this.testEgibFormGroup.value.email,
                "phone": this.testEgibFormGroup.value.phone,
                "position": this.testEgibFormGroup.value.post,
                "type": 3,
                "comment": ''
            },
            "development": {
                "haveWebsite": this.testEgibFormGroup.value.haveSite,
                "websiteType": this.testEgibFormGroup.value.typeSite,
                "websiteValue": this.testEgibFormGroup.value.siteVal,
                "websiteLink": this.testEgibFormGroup.value.liensite,
                "websiteDate": this.testEgibFormGroup.value.datesite,
                "haveCrm": this.testEgibFormGroup.value.haveCrm,
                "crmType": this.testEgibFormGroup.value.typeCRM,
                "crmDev": this.testEgibFormGroup.value.crmDev,
                "crmName": this.testEgibFormGroup.value.nomCrm,
                "erpName": this.testEgibFormGroup.value.nomErp,
                "crmDate": this.testEgibFormGroup.value.dateCrm,
                "agencyName": this.testEgibFormGroup.value.agence
            },
            "investment": {
                "service": this.testEgibFormGroup.value.service,
                "budget": this.testEgibFormGroup.value.budget,
                "digitalTransitions": ['test']
            },
            "contactID": '',
            "meetingType": ''
        })
            .subscribe(response => {
            console.log('contactid', response);
            if (response) {
                this.cid = response.cid;
            }
        });
    }
    /********************************************************status**********************************/
    getstatus(data) {
        console.log('activi', data);
        this.testEgibFormGroup.get('status').setValue(data);
    }
    /******************************************nextmodule  *************************************************/
    nextStep() {
        this.test.active.step += 1;
    }
    nextSubStep() {
        this.test.active.subStep += 1;
    }
    /******************************************prevmodule  *************************************************/
    prevStep() {
        this.test.active.step -= 1;
    }
    /**************************test elgible */
    elgiblTest() {
        this.eleg = null;
        this.nextStep();
    }
    /**********************is cpn **************/
    isCpn() {
        console.log("data result", this.testEgibFormGroup.value);
        let budget = this.testEgibFormGroup.value.budget;
        let service = this.testEgibFormGroup.value.service;
        let region = this.testEgibFormGroup.value.region;
        let naf = this.testEgibFormGroup.value.naf;
        console.log("isopen", this.test.result.isOpen);
        console.log("isCpn", this.test.result.isCpn);
        console.log("isLoading", this.test.result.isLoading);
        /**************************calcule cpn *******************/
        console.log("is open false");
        this.test.result.isOpen = true;
        this.test.result.isLoading = true;
        console.log("service", this.testEgibFormGroup.value.service);
        console.log("budget", this.testEgibFormGroup.value.budget);
        this.testService.cpnGrant(service, budget)
            .subscribe(response => {
            console.log("cpnGrant", response);
            this.test.result.cpn.id = response.id;
            this.test.result.cpn.amount = response.grants;
            this.test.result.cpn.originalPrice = response.original_price;
            this.test.result.cpn.sellPrice = response.sell_price;
            this.test.result.isLoading = false;
        });
        console.log("step", this.test.active.step);
        this.nextStep();
    }
    showResult() {
        console.log("data result", this.testEgibFormGroup.value);
        let budget = this.testEgibFormGroup.value.budget;
        let service = this.testEgibFormGroup.value.service;
        let region = this.testEgibFormGroup.value.region;
        let naf = this.testEgibFormGroup.value.naf;
        console.log("isopen", this.test.result.isOpen);
        console.log("isCpn", this.test.result.isCpn);
        console.log("isLoading", this.test.result.isLoading);
        /**************************calcule cpn *******************/
        console.log("is open false");
        this.test.result.isOpen = true;
        this.test.result.isLoading = true;
        this.testService.cpnGrant(service, budget)
            .subscribe(response => {
            console.log("cpnGrant", response);
            this.test.result.cpn.id = response.id;
            this.test.result.cpn.amount = response.grants;
            this.test.result.cpn.originalPrice = response.original_price;
            this.test.result.cpn.sellPrice = response.sell_price;
            this.test.result.isLoading = false;
        });
        console.log("step", this.test.active.step);
        switch (this.test.result.isOpen) {
            case true:
                console.log("is open true");
                console.log("is cpn true");
                this.test.result.isCpn = false;
                this.test.result.isLoading = true;
                this.testService.regionalGrant(region, budget, naf)
                    .subscribe(response => {
                    console.log("regionalGrant", response);
                    this.eleg = response.eligible;
                    if (response.eligible) {
                        this.test.result.regional.id = response.id;
                        this.test.result.regional.eligible = response.eligible;
                        this.test.result.regional.voucher = response.voucher;
                        this.test.result.regional.amount = response.amount;
                        this.test.result.regional.region = response.region;
                        console.log("is eligible", this.test.active.step);
                    }
                    else {
                        this.test.result.regional.eligible = response.eligible;
                        this.test.result.regional.voucher = null;
                        this.test.result.regional.amount = null;
                        console.log("is not eligi", this.test.active.step);
                    }
                    this.test.result.isLoading = false;
                });
                /*  .catch(error=>{
                    this.test.result.isLoading = false;
                    console.log(error)
                  });*/
                console.log("is cpn false");
                this.setContactForm(this.test.formData);
                this.test.result.isCpn = true;
                this.test.result.isOpen = false;
                this.nextStep();
                break;
        }
        console.log("test", this.test.result);
    }
    /******************************************************chekform **************************************/
    checkForm(step) {
        console.log('step', step);
        let subStep = this.test.active.subStep;
        let subStepCat = this.test.active.subStepCat;
        switch (step) {
            case 0:
                this.nextStep();
                break;
            case 1:
                if (this.testEgibFormGroup.value.codeP == '') {
                    alert("champ est obligatoir");
                }
                else {
                    this.nextStep();
                }
                break;
            case 2:
                if (this.testEgibFormGroup.value.activite == '') {
                    alert("champ est obligatoir");
                }
                else {
                    this.nextStep();
                }
                break;
            case 3:
                if (this.testEgibFormGroup.value.status == '') {
                    alert("champ est obligatoir");
                }
                else {
                    this.nextStep();
                }
                break;
            case 4:
                if (this.testEgibFormGroup.value.nomSoc == '') {
                    alert("champ est obligatoir");
                }
                else {
                    this.nextStep();
                }
                break;
            case 5:
                if (this.testEgibFormGroup.value.turnover == '') {
                    alert("champ est obligatoir");
                }
                else {
                    this.nextStep();
                }
                break;
            case 6:
                if (this.testEgibFormGroup.value.help == '') {
                    alert("champ est obligatoir");
                }
                else {
                    this.nextStep();
                }
                break;
            case 7:
                console.log("laste", Math.abs(this.max - this.min));
                if (this.min == 0 || this.max == 0 || (Math.abs(this.max - this.min) != 5000000 && Math.abs(this.max - this.min) != 500000 && Math.abs(this.max - this.min) != 100000 && Math.abs(this.max - this.min) != 50000 && Math.abs(this.max - this.min) != 45000)) {
                    alert("les deux valeur doit être très approcher");
                }
                else {
                    this.setLastTurnover([this.min, this.max]);
                    this.nextStep();
                }
                break;
            case 8:
                console.log('salair', this.testEgibFormGroup.value.personneSal);
                if (this.testEgibFormGroup.value.personneSal == "de 0 à 5 Personnes" || this.testEgibFormGroup.value.personneSal == "de 5 à 10 Personnes") {
                    this.test.active.subStepCat = 1;
                    this.test.active.subStep = 1;
                    this.nextStep();
                }
                else {
                    this.test.active.subStepCat = 2;
                    this.test.active.subStep = 1;
                    this.nextStep();
                }
                break;
            case 9:
                switch (subStepCat) {
                    case 1:
                        switch (subStep) {
                            case 1:
                                if (this.testEgibFormGroup.value.haveSite == "oui") {
                                    this.nextSubStep();
                                }
                                else {
                                    this.nextStep();
                                }
                                ;
                                break;
                            case 2:
                                this.nextSubStep();
                                break;
                            case 3:
                                this.nextSubStep();
                                break;
                            case 4:
                                this.nextSubStep();
                                break;
                            case 5:
                                this.nextStep();
                                break;
                        }
                        break;
                    case 2:
                        switch (subStep) {
                            case 1:
                                if (this.testEgibFormGroup.value.haveCrm == "oui") {
                                    this.nextSubStep();
                                }
                                else {
                                    this.test.active.subStepCat = 1;
                                }
                                ;
                                break;
                            case 2:
                                this.nextSubStep();
                                break;
                            case 3:
                                this.nextSubStep();
                                break;
                            case 4:
                                this.nextSubStep();
                                break;
                            case 5:
                                this.test.active.subStepCat = 1;
                                this.test.active.subStep = 1;
                                break;
                        }
                        break;
                }
                break;
            case 10:
                this.nextStep();
                break;
            case 11:
                if (this.testEgibFormGroup.value.budget == '') {
                    alert("champ est obligatoir");
                }
                else {
                    this.nextStep();
                }
                break;
            case 12:
                this.getNafCompany(this.testEgibFormGroup.value.siret);
                if (this.testEgibFormGroup.value.siret == '') {
                    alert("champ est obligatoir");
                }
                else {
                    this.nextStep();
                }
                break;
            case 13:
                if (this.testEgibFormGroup.value.adresse == '' && this.testEgibFormGroup.value.zipcode == '' && this.testEgibFormGroup.value.region == ''
                    && this.testEgibFormGroup.value.city == '' && this.testEgibFormGroup.value.country == '') {
                    alert("champ est obligatoir");
                }
                else {
                    this.nextStep();
                }
                break;
            case 14:
                if (this.testEgibFormGroup.value.nom == '' && this.testEgibFormGroup.value.prenom == '' && this.testEgibFormGroup.value.email == ''
                    && this.testEgibFormGroup.value.phone == '' && this.testEgibFormGroup.value.phoneEntrep == '') {
                    alert("champ est obligatoir");
                }
                else {
                    this.isCpn();
                }
                break;
            case 15:
                this.nextStep();
                break;
            case 16:
                this.nextStep();
                break;
            case 17:
                this.generateZoomLink();
                break;
        }
    }
    onUserChangeStart(changeContext) {
        console.log('start', changeContext);
        this.min = changeContext.highValue;
        this.max = changeContext.value;
    }
    onUserChange(changeContext) {
        console.log('use', changeContext);
        this.min = changeContext.highValue;
        this.max = changeContext.value;
    }
    onUserChangeEnd(changeContext) {
        this.min = changeContext.highValue;
        this.max = changeContext.value;
    }
    getChangeContextString(changeContext) {
        return; /*`{pointerType: ${changeContext.pointerType === PointerType.Min ? 'Min' : 'Max'}, ` +
               `value: ${changeContext.value}, ` +
               `highValue: ${changeContext.highValue}}`;*/
    }
}
TestComponent.ɵfac = function TestComponent_Factory(t) { return new (t || TestComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_cpn_test_egibilite_service__WEBPACK_IMPORTED_MODULE_4__["TestEgibiliteService"])); };
TestComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: TestComponent, selectors: [["app-test"]], decls: 50, vars: 24, consts: [[1, "container"], [1, "body"], [3, "formGroup"], ["class", "slide0", 4, "ngIf"], ["class", "slide1", 4, "ngIf"], ["class", "slide2", 4, "ngIf"], ["class", "slide3", 4, "ngIf"], ["class", "slide4", 4, "ngIf"], ["class", "slide5", 4, "ngIf"], ["class", "slide6", 4, "ngIf"], ["class", "slide7", 4, "ngIf"], ["class", "slide8", 4, "ngIf"], [4, "ngIf"], ["class", "slide27", 4, "ngIf"], ["class", "slide18", 4, "ngIf"], ["class", "slide19", 4, "ngIf"], ["class", "slide20", 4, "ngIf"], ["class", "slide26", 4, "ngIf"], ["class", "slide21", 4, "ngIf"], ["class", "slide24", 4, "ngIf"], ["class", "slide25", 4, "ngIf"], [1, "footer"], [1, "left"], [3, "routerLink"], ["src", "assets/cpnimages/logo/logo-cpn-blanc.png", "alt", ""], ["class", "center", 4, "ngIf"], ["id", "eventModal", "tabindex", "-1", "role", "dialog", 1, "modal", "fade", "text-left"], [1, "modal-dialog"], [1, "modal-content"], [1, "modal-header"], [1, "modal-title", "align-center"], [1, "modal-body"], [3, "formGroup", "ngSubmit"], [1, "row"], [1, "col-sm-12"], [1, "form-group"], ["placeholder", "cr\u00E9e un \u00E9v\u00E9nement", "type", "text", "formControlName", "title", 1, "titleinp", "form-control"], ["type", "datetime-local", "id", "meeting-time", "name", "meeting-time", "formControlName", "dateDebut", 1, "titleinp", "form-control"], ["dateDebut", ""], ["type", "submit", 1, "btn", "btn-primary"], [1, "slide0"], [1, "image"], ["src", "assets/cpnimages/test-egibilite/1.png", "height", "30%", "alt", ""], [1, "test"], ["mat-stroked-button", "", "color", "primary", 3, "click"], [1, "slide1"], [3, "myinputDep", "myOutput"], ["type", "text", "formControlName", "codeP", "placeholder", "entrer votre code postal"], [1, "slide2"], ["src", "assets/cpnimages/test-egibilite/4.png", "height", "30%", "alt", ""], ["bindLabel", "name", "placeholder", "Choisir un activit\u00E9", "formControlName", "activite", "autofocus", "", 3, "items", "change"], [1, "slide3"], ["src", "assets/cpnimages/test-egibilite/5.png", "height", "30%", "alt", ""], [1, "blockBtn"], [1, "sousBlock"], ["mat-stroked-button", "", 3, "ngClass", "click"], [1, "slide4"], ["src", "assets/cpnimages/test-egibilite/3.png", "height", "30%", "alt", ""], ["type", "text", "placeholder", "nom societe", "formControlName", "nomSoc"], [1, "slide5"], ["src", "assets/cpnimages/test-egibilite/6.png", "height", "30%", "alt", ""], [1, "qty", 3, "click"], [1, "baiss", 3, "ngClass"], ["type", "text", "formControlName", "turnover", 3, "readonly"], ["turnover", ""], [1, "qtyBtn"], [1, "augm", 3, "ngClass"], ["type", "radio", "formControlName", "turnover", "name", "turnover", "id", "turnover", "value", "0", 1, "form-check-input"], ["for", "inlineRadio1", 1, "form-check-label"], [1, "slide6"], ["src", "assets/cpnimages/test-egibilite/7.png", "alt", ""], ["name", "", "formControlName", "help"], ["value", "item", 4, "ngFor", "ngForOf"], ["value", "item"], [1, "slide7"], [1, "colum1"], ["src", "assets/cpnimages/test-egibilite/8.png", "height", "30%", "alt", ""], [1, "colum2"], [1, "__range", "__range-step"], [3, "value", "highValue", "options", "valueChange", "highValueChange", "userChangeStart", "userChange", "userChangeEnd"], [1, "slide8"], ["src", "assets/cpnimages/test-egibilite/9.png", "height", "30%", "alt", ""], ["placeholder", "choisir un Nombre de salari\u00E9s", "formControlName", "personneSal"], [4, "ngFor", "ngForOf"], ["class", "slide9", 4, "ngIf"], ["class", "slide10", 4, "ngIf"], ["class", "slide11", 4, "ngIf"], ["class", "slide12", 4, "ngIf"], ["class", "slide13", 4, "ngIf"], [1, "slide9"], ["src", "assets/cpnimages/test-egibilite/10.png", "height", "30%", "alt", ""], ["aria-label", "Select an option", "formControlName", "haveSite"], ["value", "oui"], ["value", "non"], [1, "slide10"], ["src", "assets/cpnimages/test-egibilite/11.png", "height", "30%", "alt", ""], ["aria-label", "Select an option", "formControlName", "typeSite"], ["value", "E-commerce"], ["value", "Vitrine"], ["value", "Market-place"], [1, "block"], [1, "qty"], ["type", "text", "formControlName", "Nvente", 3, "readonly"], ["Nvente", ""], ["type", "text", "formControlName", "Nvisite", 3, "readonly"], ["Nvisite", ""], ["type", "text", "formControlName", "Nuser", 3, "readonly"], ["Nuser", ""], [1, "slide11"], ["src", "assets/cpnimages/test-egibilite/12.png", "height", "30%", "alt", ""], ["type", "text", "formControlName", "liensite", "placeholder", "entre lien de votre site"], [1, "slide12"], ["src", "assets/cpnimages/test-egibilite/13.png", "alt", ""], ["placeholder", "choisir un date", "formControlName", "datesite"], [1, "slide13"], ["type", "text", "placeholder", "votre agnce svp!", "formControlName", "agence"], ["value", "Internet/Freelance"], ["class", "slide14", 4, "ngIf"], ["class", "slide15", 4, "ngIf"], ["class", "slide16", 4, "ngIf"], ["class", "slide17", 4, "ngIf"], [1, "slide14"], ["src", "assets/cpnimages/test-egibilite/17.png", "height", "30%", "alt", ""], ["aria-label", "Select an option", "formControlName", "haveCrm"], [1, "slide15"], ["name", "", "formControlName", "nomCrm"], ["value", "Sage", 4, "ngFor", "ngForOf"], ["value", "Sage"], ["src", "assets/cpnimages/test-egibilite/logo-sage.png", "height", "20px", "alt", ""], [1, "slide16"], ["src", "assets/cpnimages/test-egibilite/14.png", "height", "30%", "alt", ""], ["aria-label", "Select an option", "formControlName", "haveErp"], [1, "slide17"], ["name", "", "formControlName", "nomErp"], ["placeholder", "choisir un date", "formControlName", "dateCrm"], [1, "slide27"], [1, "bSearch"], ["for", ""], ["type", "text", 1, "search", 3, "keyup"], [1, "far", "fa-search"], [1, "contentTab"], ["class", "list-item", 4, "ngFor", "ngForOf"], [1, "list-item"], [1, "item-content"], [1, "block1"], [1, "fas", "fa-chess-rook"], [1, "block2"], [1, "block3"], ["type", "radio", "name", "service", "id", "service", 3, "click"], [1, "slide18"], ["name", "", "formControlName", "budget"], ["value", "300"], ["value", "400"], ["value", "500"], ["value", "600"], [1, "slide19"], ["src", "assets/cpnimages/test-egibilite/18.png", "height", "30%", "alt", ""], ["type", "text", "formControlName", "siret", "placeholder", "EX: 13168813881"], [1, "slide20"], ["src", "assets/cpnimages/test-egibilite/19.png", "alt", ""], ["type", "text", "formControlName", "adresse"], ["type", "text", "formControlName", "city"], ["type", "text", "formControlName", "codeP", 3, "readonly"], [1, "slide26"], ["src", "assets/cpnimages/test-egibilite/20.png", "alt", ""], ["type", "text", "formControlName", "nom"], ["type", "text", "formControlName", "prenom"], ["type", "text", "formControlName", "email"], ["type", "text", "formControlName", "phone"], ["type", "text", "formControlName", "phoneEntrep"], ["aria-label", "Select an option", "formControlName", "post"], ["value", "G\u00E9rant"], ["value", "Associ\u00E9"], ["value", "Dir\u00E9cteur"], ["value", "autre"], [1, "slide21"], ["src", "assets/cpnimages/test-egibilite/21.png", "alt", ""], ["src", "assets/cpnimages/test-egibilite/24.png", "alt", ""], ["src", "assets/cpnimages/test-egibilite/22.png", "alt", ""], [1, "nextIcon", 3, "click"], ["src", "assets/cpnimages/test-egibilite/23.png", "alt", ""], [1, "prix"], [1, "faild"], ["src", "assets/cpnimages/test-egibilite/25.png", "alt", ""], [1, "slide24"], ["src", "assets/cpnimages/test-egibilite/27.png", "height", "30%", "alt", ""], [1, "calend"], ["data-toggle", "modal", "data-target", "#eventModal", 2, "width", "100%", 3, "options"], ["fullcalendar", ""], ["aria-label", "Select an option", "formControlName", "meetingType"], ["value", "Entretien vid\u00E9o direct"], ["value", "Visite de courtoisie"], [1, "slide25"], ["src", "assets/cpnimages/test-egibilite/28.png", "alt", ""], [1, "nextIcon"], [1, "socialMedia"], ["src", "assets/cpnimages/test-egibilite/icone-Facebook.png", "alt", ""], ["src", "assets/cpnimages/test-egibilite/icone-Instagram.png", "alt", ""], ["src", "assets/cpnimages/test-egibilite/icone-Linkedin.png", "alt", ""], ["src", "assets/cpnimages/test-egibilite/icone-youtube.png", "alt", ""], [1, "center"], [3, "click"], [1, "far", "fa-chevron-left", "iconNex"], [1, "listP"], ["aria-hidden", "true", 1, "fas", "fa-circle", "point", 3, "ngClass"], [1, "far", "fa-chevron-right", "iconNex"]], template: function TestComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "form", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, TestComponent_div_3_Template, 8, 0, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](4, TestComponent_div_4_Template, 7, 1, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](5, TestComponent_div_5_Template, 8, 1, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](6, TestComponent_div_6_Template, 19, 15, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](7, TestComponent_div_7_Template, 7, 0, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](8, TestComponent_div_8_Template, 35, 8, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](9, TestComponent_div_9_Template, 8, 1, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](10, TestComponent_div_10_Template, 10, 3, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](11, TestComponent_div_11_Template, 10, 1, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](12, TestComponent_div_12_Template, 3, 2, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](13, TestComponent_div_13_Template, 10, 1, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](14, TestComponent_div_14_Template, 17, 0, "div", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](15, TestComponent_div_15_Template, 7, 0, "div", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](16, TestComponent_div_16_Template, 17, 1, "div", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](17, TestComponent_div_17_Template, 36, 0, "div", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](18, TestComponent_div_18_Template, 21, 1, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](19, TestComponent_div_19_Template, 24, 1, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](20, TestComponent_div_20_Template, 12, 0, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](21, TestComponent_div_21_Template, 14, 1, "div", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](22, TestComponent_div_22_Template, 20, 0, "div", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "div", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "div", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "a", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](26, "img", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](27, TestComponent_div_27_Template, 38, 48, "div", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "div", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "div", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "div", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "div", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "h4", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](33, " Cr\u00E9er un \u00E9v\u00E9nement ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "div", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "form", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngSubmit", function TestComponent_Template_form_ngSubmit_35_listener() { return ctx.sendRendvous(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "div", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "div", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "div", 35);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "label");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](40, "Titre \u00E9v\u00E9nement");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](41, "input", 36);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "div", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "div", 35);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "label");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](45, "select date");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](46, "input", 37, 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "button", 39);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](49, "Envoyer");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx.testEgibFormGroup);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.eleg == true && ctx.test.active.step == 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.eleg == false && ctx.test.active.step == 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "/home");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step != 0 && ctx.test.active.step != 15 && ctx.test.active.step != 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx.addEventForm);
    } }, directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"], _angular_common__WEBPACK_IMPORTED_MODULE_5__["NgIf"], _angular_router__WEBPACK_IMPORTED_MODULE_6__["RouterLinkWithHref"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControlName"], _angular_material_button__WEBPACK_IMPORTED_MODULE_7__["MatButton"], _map_french_map_french_component__WEBPACK_IMPORTED_MODULE_8__["MapFrenchComponent"], _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_9__["NgSelectComponent"], _angular_common__WEBPACK_IMPORTED_MODULE_5__["NgClass"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["RadioControlValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["SelectControlValueAccessor"], _angular_common__WEBPACK_IMPORTED_MODULE_5__["NgForOf"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgSelectOption"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["ɵangular_packages_forms_forms_x"], _angular_slider_ngx_slider__WEBPACK_IMPORTED_MODULE_10__["ɵa"], _angular_material_radio__WEBPACK_IMPORTED_MODULE_11__["MatRadioGroup"], _angular_material_radio__WEBPACK_IMPORTED_MODULE_11__["MatRadioButton"], _fullcalendar_angular__WEBPACK_IMPORTED_MODULE_12__["FullCalendarComponent"]], styles: [".container[_ngcontent-%COMP%]{\n    display: flex;\n    flex-direction: column;\n    width: 100%;\n    height: 100%;\n    max-width: 100%;\n    margin: 0;\n    padding: 0;\n}\n.body[_ngcontent-%COMP%]{\n    width: 100%;\n    height: 100%;\n    display: flex;\n    flex-direction:column;\n    justify-content: center;\n}\n.footer[_ngcontent-%COMP%]{\n    width: 100%;\n    max-height: 150px;\n    min-height: 150px;\n    height: 100%;\n    background-color:  #111D5Eff;\n    display: flex;\n    flex-direction: row;\n}\n.left[_ngcontent-%COMP%]{\n    display: flex;\n    flex-direction: row;\n    justify-content: flex-start;\n    height: 100%;\n    align-items: center;\n    width: 10%;\n    margin-left: 10px;\n}\n.left[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\n    width: 50%;\n}\n.point[_ngcontent-%COMP%]{\n    font-size: 10px;\n    color:rgb(153, 153, 153);\n    margin-left: 10px;\n    \n}\ni[_ngcontent-%COMP%]{\n    color:rgb(153, 153, 153);\n}\ni[_ngcontent-%COMP%]:hover{\n    color: white;\n    cursor: pointer;\n}\n.iconNex[_ngcontent-%COMP%]{\n    font-size: 25px;\n    margin-top: 8px;\n    margin-left: 10px;\n}\n.checkIcon[_ngcontent-%COMP%]{\n    color: white;\n}\n.center[_ngcontent-%COMP%]{\n width: 100%;\n height: 100%;\n display: flex;\n flex-direction: row;\n justify-content: center;\n align-items: center;\n}\n\n.butttonREd[_ngcontent-%COMP%]{\n    background-color: rgb(206, 0, 0);\n    color: white;\n}\nbutton[_ngcontent-%COMP%]:hover{\n    background-color: rgb(145, 136, 136);\n    color: white;\n}\n\n.slide1[_ngcontent-%COMP%]{\n    display: flex;\n    flex-direction: row;\n    justify-content: space-between;\n    width: 100%;\n    height: 100%;\n}\n.slide1[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\n  order: 2;\n  width: 70%;\n  height: 100%;\n}\n.slide1[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\n order: 1;\n display: flex;\n flex-direction: column;\n   justify-content: center;\n   align-items:center;\nwidth: 40%;\nheight: 100%;\n}\n.slide1[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\n    width: 80%;\n    height: 40px;\n    border-color: rgb(212, 5, 5);\n       }\n\n.slide0[_ngcontent-%COMP%]{\n    display: flex;\n    flex-direction: row;\n    justify-content: space-between;\n    width: 100%;\n    height: 100%;\n}\n.slide0[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\n  order: 2;\n  width: 52%;\n  height: 100%;\n}\n.slide0[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\n    order: 1;\n    display: flex;\n   flex-direction: column;\n   justify-content: center;\n   align-items: center;\n   width:40%;\n   height: 100%;\n   }\n.slide0[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\nfont-size: 50px;\n   }\n.slide0[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   button[_ngcontent-%COMP%]{\n    width: 150px;\n    height: 50px;\n    border-radius: 30px;\n    border-color: rgb(192, 3, 3);\n       }\n\n.slide2[_ngcontent-%COMP%]{\n        display: flex;\n        flex-direction: row;\n        justify-content: space-between;\n        width: 100%;\n        height: 100%;\n    }\n.slide2[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\n      order: 1;\n      width: 55%;\n      height: 100%;\n    }\n.slide2[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\n        order: 2;\n        display: flex;\n       flex-direction: column;\n       justify-content: center;\n       align-items: flex-start;\n       width:40%;\n       height: 100%;\n       }\n.slide2[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\n    font-size: 50px;\n       }\n.slide2[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .ng-select[_ngcontent-%COMP%] {\n            border:0px;\n            min-height: 0px;\n            border-radius: 0;\n            width: 480px;\n            height: 40px;\n            border-color: rgb(212, 5, 5);\n        }\n.slide2[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .ng-select[_ngcontent-%COMP%]   .ng-select-container[_ngcontent-%COMP%]  {            \n            min-height: 0px;\n            border-radius: 0;\n            width: 480px;\n            height: 40px;\n            border-color: rgb(212, 5, 5);\n        }\n.slide2[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .ng-select[_ngcontent-%COMP%]     .ng-select-container  {            \n            min-height: 0px;\n            border-radius: 0;\n            width: 480px;\n            height: 40px;\n            border-color: rgb(212, 5, 5);\n        }\n.slide2[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   ng-select.ng-invalid.ng-touched[_ngcontent-%COMP%]   .ng-select-container[_ngcontent-%COMP%] {\n            border-color: #dc3545;\n            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 0 3px #fde6e8;\n            width: 480px;\n            height: 40px;\n            border-color: rgb(212, 5, 5);\n        }\n\n.slide3[_ngcontent-%COMP%]{\n        display: flex;\n        flex-direction: row;\n        justify-content: space-between;\n        width: 100%;\n        height: 100%;\n    }\n.slide3[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\n        order: 2;\n        width: 54%;\n        height: 100%;\n    }\n.slide3[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\n        order: 1;\n        display: flex;\n        flex-direction: column;\n        justify-content: space-around;\n        align-items: center;\n        margin-top: 50px;\n        width:40%;\n        height: 70%;\n        }\n.slide3[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\n        font-size: 50px;\n        }\n.slide3[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .blockBtn[_ngcontent-%COMP%]{\n           display: flex;\n           flex-direction: column;\n           justify-content: center;\n\n        }\n.slide3[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .blockBtn[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]{\n            display: flex;\n            flex-direction: row;\n            justify-content: center;\n \n         }\n.slide3[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .blockBtn[_ngcontent-%COMP%]   button[_ngcontent-%COMP%]{\n        width: 310px;\n        height: 50px;\n        margin: 5px;\n        border-color: rgb(192, 3, 3);\n            }\n.slide3[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .blockBtn[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]   button[_ngcontent-%COMP%]{\n                width: 150px;\n                margin: 5px;\n                    }\n\n.slide4[_ngcontent-%COMP%]{\n        display: flex;\n        flex-direction: row;\n        justify-content: space-between;\n        width: 100%;\n        height: 100%;\n    }\n.slide4[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\n        margin-left: 50px;\n        order: 1;\n        width: 33%;\n        height: 100%;\n        display: flex;\n        z-index: 1;\n        margin-top: 55px;\n    }\n.slide4[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\n        order: 2;\n        display: flex;\n        flex-direction: column;\n        justify-content: center;\n        align-items: flex-start;\n        width:40%;\n        height: 100%;\n        }\n.slide4[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\n    font-size: 50px;\n        }\n.slide4[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\n        width: 80%;\n        height: 40px;\n        border-color: rgb(212, 5, 5);\n            }\n\n.slide5[_ngcontent-%COMP%]{\n        display: flex;\n        flex-direction: row;\n        justify-content: space-between;\n        width: 100%;\n        height: 100%;\n    }\n.slide5[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\n        order: 1;\n        width: 50%;\n        height: 100%;\n        z-index: 1;\n        display: flex;\n        margin-top: 64px;\n    }\n.slide5[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\n        order: 2;\n        display: flex;\n        flex-direction: column;\n        justify-content: center;\n        align-items: center;\n        width:50%;\n        height: 100%;\n        }\n.slide5[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\n        font-size: 50px;\n        }\n.slide5[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .blockBtn[_ngcontent-%COMP%]{\n           display: flex;\n           flex-direction: row;\n           justify-content: space-around;\n            width: 100%;\n            height: 20%;\n        }\n.slide5[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .blockBtn[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]{\n            display: flex;\n            flex-direction: row;\n            justify-content: flex-start;\n            align-items: center; \n         }\n.slide5[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .blockBtn[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]   .qty[_ngcontent-%COMP%]{\n            display: flex;\n            flex-direction: row;\n            justify-content: center;            \n            }\n.slide5[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .blockBtn[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]   .qty[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\n        order: 2;\n        width: 80px;\n        height: 50px;\n        border: 4px solid rgb(212, 5, 5);\n        text-align: center;\n        }\n.slide5[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .blockBtn[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]   .qty[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\n        order: 1;\n        margin: 0 5px 0 0;\n        padding: 0;\n        display: flex;\n        align-items: center;\n        flex-direction: row;\n            }\n.slide5[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .blockBtn[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]   .qty[_ngcontent-%COMP%]   .baiss[_ngcontent-%COMP%]{ display: none;}\n.slide5[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .blockBtn[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]   .qty[_ngcontent-%COMP%]   .augm[_ngcontent-%COMP%]{ display: none;}\n.slide5[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .blockBtn[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]   .qty[_ngcontent-%COMP%]   .baissD[_ngcontent-%COMP%]{ display: contents;}\n.slide5[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .blockBtn[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]   .qty[_ngcontent-%COMP%]   .augmD[_ngcontent-%COMP%]{ display: contents;}\n.slide5[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .blockBtn[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]   .qty[_ngcontent-%COMP%]   .qtyBtn[_ngcontent-%COMP%]{\n        order: 3;\n        display: flex;\n        flex-direction: column;\n        justify-content: flex-start;\n        }\n.slide5[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .blockBtn[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]   .qty[_ngcontent-%COMP%]   .qtyBtn[_ngcontent-%COMP%]   button[_ngcontent-%COMP%]{\n            border: none;\n            border-bottom: 1px solid white;\n            margin-bottom: 1px;\n            width: 5px;\n            border-radius: 0px;\n            height: 25px;\n            color: white;\n            background-color: rgb(212, 5, 5);\n            display: flex;\n            flex-direction: column;\n            font-size: 20px;\n            justify-content: center;\n            align-items: center;\n            }\n\n.slide6[_ngcontent-%COMP%]{\n                display: flex;\n                flex-direction: row;\n                justify-content: space-between;\n                width: 100%;\n                height: 100%;\n            }\n.slide6[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\n                order: 2;\n                display: flex;\n                flex-direction: column;\n                justify-content: center;\n                align-items:center;\n                width:53%;\n                height: 100%;\n            }\n.slide6[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\n                order: 2;\n                height: 25%;\n                width: 15%;\n                    }\n.slide6[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\n                order: 1;\n                font-size: 50px;\n                text-align: center;\n                margin-bottom: 20px;\n                    }\n.slide6[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\n                order: 2;\n                display: flex;\n                flex-direction: column;\n                justify-content: center;\n                align-items: flex-start;\n                width:40%;\n                height: 100%;\n                }\n.slide6[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   select[_ngcontent-%COMP%]{\n                width: 80%;\n                height: 40px;\n                border-color: rgb(212, 5, 5);\n                    }\n\n.slide7[_ngcontent-%COMP%]{\n    display: flex;\n    flex-direction: column;\n    justify-content: center;\n    width: 100%;\n    height: 100%;\n}\n.slide7[_ngcontent-%COMP%]   .colum1[_ngcontent-%COMP%]{\n    order: 1;\n    display: flex;\n    flex-direction: row;\n    justify-content: space-around;\n    width: 100%;\n    height: 100%;\n    margin-bottom: 20px;\n}\n.slide7[_ngcontent-%COMP%]   .colum2[_ngcontent-%COMP%]{\n    order: 2;\n    display: flex;\n    flex-direction: row;\n    justify-content: center;\n    width: 100%;\n    margin-top: 20px;\n    height: 100%;\n}\n.slide7[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\n    margin-left: 50px;\n    order: 1;\n    width: 37%;\n    height: 100%;\n}\n.slide7[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\n    order: 2;\n    display: flex;\n    flex-direction: column;\n    justify-content: center;\n    align-items: flex-start;\n    width:40%;\n    height: 100%;\n    }\n.slide7[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\nfont-size: 50px;\n    }\n.slide7[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\n    width: 80%;\n    height: 40px;\n    border-color: rgb(212, 5, 5);\n        }\n.slide7[_ngcontent-%COMP%]   .progress[_ngcontent-%COMP%]{\n    order: 3;\n}\n.slider[_ngcontent-%COMP%] {\n    -webkit-appearance: none;\n    width: 100%;\n    height: 15px;\n    background: rgb(255, 255, 255);\n    outline: none;\n    border: 5px solid rgb(189, 8, 8);\n    border-radius: 8px;\n  }\n\n.slider[_ngcontent-%COMP%]::-webkit-slider-thumb {\n    -webkit-appearance: none;\n    appearance: none;\n    width: 20px;\n    height: 60px;\n    background: rgb(248, 224, 5);\n    cursor: pointer;\n    border: 5px solid rgb(248, 224, 5);\n    border-radius: 50px;\n  }\n\n.slider[_ngcontent-%COMP%]::-moz-range-thumb {\n    width: 20px;\n    height: 60px;\n    background: rgb(255, 255, 255);\n    cursor: pointer;\n    border: 5px solid rgb(189, 8, 8);\n    border-radius: 4px;\n  }\n.__range[_ngcontent-%COMP%]{\n    width: 80%;\n    height: 100%;\n}\n.__range-step[_ngcontent-%COMP%]{\n\tposition: relative;                \n}\n.__range-step[_ngcontent-%COMP%]{\n\tposition: relative;                \n}\n.__range-max[_ngcontent-%COMP%]{\n\tfloat: right;\n}\n.__range[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]::range-progress {\tbackground: rgb(189, 8, 8);\n}\n.slider[_ngcontent-%COMP%]   input[type=range][_ngcontent-%COMP%]::-moz-range-progress {\n    background-color: #c657a0;\n  }\n.__range-step[_ngcontent-%COMP%]   datalist[_ngcontent-%COMP%] {\n\tposition:relative;\n\tdisplay: flex;\n\tjustify-content: space-between;\n\theight: auto;\n\tbottom: 10px;\n\t\n\t-webkit-user-select: none;                   \n\tuser-select: none; \n\t\n\tpointer-events:none;  \n}\n.__range-step[_ngcontent-%COMP%]   datalist[_ngcontent-%COMP%]   option[_ngcontent-%COMP%] {\n\twidth: 10px;\n\theight: 10px;\n\tmin-height: 10px;\n\tborder-radius: 100px;\n\t\n\twhite-space: nowrap;       \n  padding:0;\n  line-height: 40px;\n}\n\n.slide8[_ngcontent-%COMP%]{\n    display: flex;\n    flex-direction: row;\n    justify-content: space-around;\n    width: 100%;\n    height: 100%;\n}\n.slide8[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\n    order: 2;\n    display: flex;\n    align-items: center;\n    width: 37%;\n    height: 100%;\n}\n.slide8[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\n    order: 1;\n    display: flex;\n    flex-direction: column;\n    justify-content: center;\n    align-items: flex-start;\n    width:40%;\n    height: 100%;\n    }\n.slide8[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\nfont-size: 50px;\n    }\n.slide8[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   select[_ngcontent-%COMP%]{\n    width: 80%;\n    height: 40px;\n    border-color: rgb(212, 5, 5);\n        }\n\n.slide9[_ngcontent-%COMP%]{\n    display: flex;\n    flex-direction: row;\n    justify-content: space-around;\n    width: 100%;\n    height: 100%;\n}\n.slide9[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\n    order: 1;\n    display: flex;\n    align-items: center;\n    width: 37%;\n    height: 100%;\n}\n.slide9[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\n    order: 2;\n    display: flex;\n    flex-direction: column;\n    justify-content: center;\n    align-items: flex-start;\n    width:40%;\n    height: 100%;\n    }\n.slide9[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\n    font-size: 50px;\n    }\n.slide9[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   mat-radio-group[_ngcontent-%COMP%]{\n        margin-top: 50px;\n        display: flex;\n        flex-direction: row;\n        justify-content: space-around;\n        width: 100%;\n    }\n\n.slide10[_ngcontent-%COMP%]{\n    display: flex;\n    flex-direction: row;\n    justify-content: space-around;\n    width: 100%;\n    height: 100%;\n}\n.slide10[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\n    order: 2;\n    display: flex;\n    align-items: center;\n    width: 37%;\n    height: 100%;\n}\n.slide10[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\n    order: 1;\n    display: flex;\n    flex-direction: column;\n    justify-content: center;\n    align-items: flex-start;\n    width:40%;\n    height: 100%;\n    }\n.slide10[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\n    font-size: 50px;\n    }\n.slide10[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   mat-radio-group[_ngcontent-%COMP%]{\n        margin-top: 50px;\n        display: flex;\n        flex-direction: row;\n        justify-content: space-around;\n        width: 100%;\n    }\n.slide10[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block[_ngcontent-%COMP%]{\n        display: flex;\n        flex-direction: row;\n        justify-content: space-around;\n        width: 100%;\n        margin-top: 20px;\n    }\n.slide10[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]{\n        display: flex;\n        flex-direction: column;\n        justify-content: space-around;\n        align-items: center; \n     }\n.slide10[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]   .qty[_ngcontent-%COMP%]{\n        order: 2;\n        display: flex;\n        flex-direction: row;\n        justify-content: center;            \n        }\n.slide10[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]   .qty[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\n    order: 2;\n    width: 80px;\n    height: 50px;\n    border: 4px solid rgb(212, 5, 5);\n    text-align: center;\n    }\n.slide10[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\n    order: 1;\n    margin: 0 5px 0 0;\n    padding: 0;\n    display: flex;\n    align-items: center;\n    flex-direction: row;\n        }\n.slide10[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]   .qty[_ngcontent-%COMP%]   .qtyBtn[_ngcontent-%COMP%]{\n    order: 3;\n    display: flex;\n    flex-direction: column;\n    justify-content: flex-start;\n    }\n.slide10[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]   .qty[_ngcontent-%COMP%]   .qtyBtn[_ngcontent-%COMP%]   button[_ngcontent-%COMP%]{\n        border: none;\n        border-bottom: 1px solid white;\n        margin-bottom: 1px;\n        width: 5px;\n        border-radius: 0px;\n        height: 25px;\n        color: white;\n        background-color: rgb(212, 5, 5);\n        display: flex;\n        flex-direction: column;\n        font-size: 20px;\n        justify-content: center;\n        align-items: center;\n        }\n\n.slide11[_ngcontent-%COMP%]{\n        display: flex;\n        flex-direction: row;\n        justify-content: space-around;\n        width: 100%;\n        height: 100%;\n    }\n.slide11[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\n        align-items: center;\n        display: flex;\n        order: 1;\n        width: 37%;\n        height: 100%;\n    }\n.slide11[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\n        order: 2;\n        display: flex;\n        flex-direction: column;\n        justify-content: center;\n        align-items: flex-start;\n        width:40%;\n        height: 100%;\n        }\n.slide11[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\n    font-size: 50px;\n        }\n.slide11[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\n        width: 80%;\n        height: 40px;\n        border-color: rgb(212, 5, 5);\n            }\n\n.slide12[_ngcontent-%COMP%]{\n        display: flex;\n        flex-direction: row;\n        justify-content: space-between;\n        width: 100%;\n        height: 100%;\n    }\n.slide12[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\n        order: 2;\n        display: flex;\n        flex-direction: column;\n        justify-content: center;\n        align-items:center;\n        width:53%;\n        height: 100%;\n    }\n.slide12[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\n        order: 2;\n        height: 15%;\n        width: 15%;\n            }\n.slide12[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\n        order: 1;\n        font-size: 50px;\n        text-align: center;\n        margin-bottom: 20px;\n            }\n.slide12[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\n        order: 2;\n        display: flex;\n        flex-direction: column;\n        justify-content: center;\n        align-items: flex-start;\n        width:40%;\n        height: 100%;\n        }\n.slide12[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   select[_ngcontent-%COMP%]{\n        width: 80%;\n        height: 40px;\n        border-color: rgb(212, 5, 5);\n            }\n\n.slide13[_ngcontent-%COMP%]{\n    display: flex;\n    flex-direction: row;\n    justify-content: space-between;\n    width: 100%;\n    height: 100%;\n}\n.slide13[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\n    margin-left: 50px;\n    order: 1;\n    display: flex;\n    align-items:center;\n    width: 37%;\n    height: 100%;\n}\n.slide13[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\n    order: 2;\n    display: flex;\n    flex-direction: column;\n    justify-content: center;\n    align-items: center;\n    width:40%;\n    height: 100%;\n    }\n.slide13[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\nfont-size: 50px;\n    }\n.slide13[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\n    width: 80%;\n    height: 40px;\n    margin: 50px 0 50px 0;\n    border-color: rgb(212, 5, 5);\n        }\n\n.slide14[_ngcontent-%COMP%]{\n    display: flex;\n    flex-direction: row;\n    justify-content: space-around;\n    width: 100%;\n    height: 100%;\n}\n.slide14[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\n    order: 2;\n    display: flex;\n    align-items: center;\n    width: 37%;\n    height: 100%;\n}\n.slide14[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\n    order: 1;\n    display: flex;\n    flex-direction: column;\n    justify-content: center;\n    align-items: flex-start;\n    width:40%;\n    height: 100%;\n    }\n.slide14[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\n    font-size: 50px;\n    }\n.slide14[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   mat-radio-group[_ngcontent-%COMP%]{\n        margin-top: 50px;\n        display: flex;\n        flex-direction: row;\n        justify-content: space-around;\n        width: 100%;\n    }\n\n.slide15[_ngcontent-%COMP%]{\n        display: flex;\n        flex-direction: row;\n        justify-content: space-between;\n        width: 100%;\n        height: 100%;\n    }\n.slide15[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\n        order: 2;\n        display: flex;\n        flex-direction: column;\n        justify-content: center;\n        align-items:center;\n        width:53%;\n        height: 100%;\n    }\n.slide15[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\n        order: 2;\n    \n            }\n.slide15[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\n        order: 1;\n        font-size: 50px;\n        text-align: center;\n        margin-bottom: 20px;\n            }\n.slide15[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\n        order: 2;\n        display: flex;\n        flex-direction: column;\n        justify-content: center;\n        align-items: flex-start;\n        width:40%;\n        height: 100%;\n        }\n.slide15[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   select[_ngcontent-%COMP%]{\n        width: 80%;\n        height: 40px;\n        border-color: rgb(212, 5, 5);\n            }\n\n.slide16[_ngcontent-%COMP%]{\n    display: flex;\n    flex-direction: row;\n    justify-content: space-around;\n    width: 100%;\n    height: 100%;\n}\n.slide16[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\n    order: 2;\n    display: flex;\n    align-items: center;\n    width: 37%;\n    height: 100%;\n}\n.slide16[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\n    order: 1;\n    display: flex;\n    flex-direction: column;\n    justify-content: center;\n    align-items: flex-start;\n    width:40%;\n    height: 100%;\n    }\n.slide16[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\n    font-size: 50px;\n    }\n.slide16[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   mat-radio-group[_ngcontent-%COMP%]{\n        margin-top: 50px;\n        display: flex;\n        flex-direction: row;\n        justify-content: space-around;\n        width: 100%;\n    }\n\n.slide17[_ngcontent-%COMP%]{\n        display: flex;\n        flex-direction: row;\n        justify-content: space-between;\n        width: 100%;\n        height: 100%;\n    }\n.slide17[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\n        order: 2;\n        display: flex;\n        flex-direction: column;\n        justify-content: center;\n        align-items:center;\n        width:53%;\n        height: 100%;\n    }\n.slide17[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\n        order: 2;\n    \n            }\n.slide17[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\n        order: 1;\n        font-size: 50px;\n        text-align: center;\n        margin-bottom: 20px;\n            }\n.slide17[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\n        order: 2;\n        display: flex;\n        flex-direction: column;\n        justify-content: center;\n        align-items: flex-start;\n        width:40%;\n        height: 100%;\n        }\n.slide17[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   select[_ngcontent-%COMP%]{\n        width: 80%;\n        height: 40px;\n        border-color: rgb(212, 5, 5);\n            }\n\n.slide18[_ngcontent-%COMP%]{\n            display: flex;\n            flex-direction: row;\n            justify-content: space-between;\n            width: 100%;\n            height: 100%;\n        }\n.slide18[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\n            order: 2;\n            display: flex;\n            flex-direction: column;\n            justify-content: center;\n            align-items:center;\n            width:53%;\n            height: 100%;\n        }\n.slide18[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\n            order: 2;\n            height: 30%;\n            width: 15%;\n                }\n.slide18[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\n            order: 2;\n            font-size: 50px;\n            text-align: center;\n            margin-bottom: 20px;\n                }\n.slide18[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\n            order: 1;\n            display: flex;\n            flex-direction: column;\n            justify-content: center;\n            align-items: center;\n            width:40%;\n            height: 100%;\n            }\n.slide18[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   select[_ngcontent-%COMP%]{\n            width: 80%;\n            height: 40px;\n            border-color: rgb(212, 5, 5);\n                }\n\n.slide19[_ngcontent-%COMP%]{\n        display: flex;\n        flex-direction: row;\n        justify-content: space-between;\n        width: 100%;\n        height: 100%;\n    }\n.slide19[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\n        margin-left: 50px;\n        display: flex;\n        align-items: center;\n        order: 1;\n        width: 37%;\n        height: 100%;\n    }\n.slide19[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\n        order: 2;\n        display: flex;\n        flex-direction: column;\n        justify-content: center;\n        align-items: flex-start;\n        width:40%;\n        height: 100%;\n        }\n.slide19[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\n    font-size: 50px;\n        }\n.slide19[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\n        width: 80%;\n        height: 40px;\n        border-color: rgb(212, 5, 5);\n            }\n\n.slide20[_ngcontent-%COMP%]{\n    display: flex;\n    flex-direction: row;\n    justify-content: space-between;\n    width: 100%;\n    height: 100%;\n}\n.slide20[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\n    display: flex;\n    align-items: center;\n    justify-content: center;\n    order: 1;\n    width: 60%;\n    height: 80%;\n}\n.slide20[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\n  height: 80%;\n  width: 40%;\n  filter: drop-shadow(0.4rem 0.4rem 0.45rem rgba(0, 0, 30, 0.5));\n}\n.slide20[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\n    order: 2;\n    display: flex;\n    flex-direction: column;\n    justify-content: center;\n    align-items: flex-start;\n    width:60%;\n    height: 100%;\n    }\n.slide20[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\nfont-size: 50px;\n    }\n.slide20[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\n    width: 90%;\n    height: 40px;\n    border-color: rgb(212, 5, 5);\n        }\n.slide20[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]{\n        margin-top: 20px;\n        display: flex;\n        flex-direction: row;\n        justify-content: space-between;\n        width: 90%;\n    }\n.slide20[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block2[_ngcontent-%COMP%] {\n        margin-top: 20px;\n        display: flex;\n        flex-direction: row;\n        justify-content: space-between;\n        width: 90%;\n    }\n.slide20[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block2[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\n        width: 40%;\n    }\n.slide20[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block2[_ngcontent-%COMP%]   label[_ngcontent-%COMP%]{\n        display: flex;\n        align-items: center;\n        margin: 0;\n    }\n.slide20[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\n        width: 90%;\n    }\n.slide20[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]   label[_ngcontent-%COMP%]{\n        display: flex;\n        align-items: center;\n        margin: 0;\n    }\n\n.slide21[_ngcontent-%COMP%]{\n    display: flex;\n    flex-direction: row;\n    justify-content: center;\n    align-items: center;\n    width: 100%;\n    height: 70%;\n}\n.slide21[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\n    display: flex;\n    align-items: flex-end;\n    justify-content: flex-end;\n    order: 1;\n    width: 22%;\n    height: 100%;\n}\n.slide21[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\n  height: 70%;\n  width: 30%;\n}\n.slide21[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\n    order: 2;\n    display: flex;\n    flex-direction: column;\n    justify-content:flex-start;\n    align-items: center;\n    width:70%;\n    height: 80%;\n    }\n.slide21[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\n        line-height: normal;\n       color: green;\n       font-size: 50px;\n        }\n.slide21[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\n          \n            font-size: 100px;\n            margin: 10px;\n                }\n.slide21[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\n        line-height: normal;\n        color: rgb(0, 0, 133);\n        font-size: 30px;\n            }\n.slide21[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .prix[_ngcontent-%COMP%] {\n        color: red;\n        line-height: normal;\n            }\n.slide21[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .faild[_ngcontent-%COMP%]{\n        color: red;\n        line-height: normal;\n        font-size: 60px;\n            }\n.slide21[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\n        width: 100px;\n            }\n.slide21[_ngcontent-%COMP%]   .nextIcon[_ngcontent-%COMP%]{\n    order: 3;\n    display: flex;\n    flex-direction: row;\n    justify-content: flex-start;\n    align-items: flex-end;\n    width: 22%;\n    height: 90%;\n}\n.slide21[_ngcontent-%COMP%]   .nextIcon[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\n color: green;\n width: 80px;\n}\n\n.slide24[_ngcontent-%COMP%]{\n    display: flex;\n    flex-direction: row;\n    justify-content: space-between;\n    width: 100%;\n    height: 100%;\n}\n.slide24[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\n    margin-left: 50px;\n    order: 2;\n    display: flex;\n    align-items:flex-end;\n    width: 47%;\n    height: 100%;\n    margin-top: 10px;\n}\n.slide24[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\n    order: 1;\n    display: flex;\n    flex-direction: column;\n    justify-content: center;\n    align-items: center;\n    width:40%;\n    height: 100%;\n    }\n.slide24[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\n    font-size: 50px;\n    text-align: center;\n    }\n.slide24[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\n    width: 80%;\n    height: 40px;\n    margin: 50px 0 50px 0;\n    border-color: rgb(212, 5, 5);\n        }\n.slide24[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   mat-radio-group[_ngcontent-%COMP%]{\n       display: flex;\n       flex-direction: row;\n       justify-content: space-around;\n       align-items: flex-start;\n       width: 100%;\n\n            }\n.slide24[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .calend[_ngcontent-%COMP%]{\n        width: 450px;\n        height: 450px;\n    }\n\n.slide25[_ngcontent-%COMP%]{\n    display: flex;\n    flex-direction: column;\n    justify-content: center;\n    align-items: center;\n    width: 100%;\n    height: 100%;\n}\n.slide25[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\n    display: flex;\n    align-items: flex-end;\n    justify-content: flex-end;\n    order: 1;\n    width: 22%;\n    height: 20%;\n}\n.slide25[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\n    order: 2;\n    display: flex;\n    flex-direction: column;\n    justify-content:flex-start;\n    align-items: center;\n    width:70%;\n    height: 80%;\n    }\n.slide25[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\n    width: 50px;\n        }\n.slide25[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\n    color: rgb(0, 0, 133);\n    font-size: 40px;\n}\n.slide25[_ngcontent-%COMP%]   .nextIcon[_ngcontent-%COMP%]{\n    order: 3;\n}\n.slide25[_ngcontent-%COMP%]   .nextIcon[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\n    text-align: center;\n}\n.slide25[_ngcontent-%COMP%]   .nextIcon[_ngcontent-%COMP%]   .socialMedia[_ngcontent-%COMP%] {\n    display: flex;\n    flex-direction: row;\n    justify-content: center;\n   }\n.slide25[_ngcontent-%COMP%]   .nextIcon[_ngcontent-%COMP%]   .socialMedia[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\n width: 60px;\n}\n\n.slide26[_ngcontent-%COMP%]{\n    display: flex;\n    flex-direction: row;\n    justify-content: space-between;\n    width: 100%;\n    height: 100%;\n}\n.slide26[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\n    order: 2;\n    display: flex;\n    flex-direction: column;\n    justify-content: center;\n    align-items:center;\n    width:53%;\n    height: 100%;\n}\n.slide26[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\n    order: 2;\n    height: 30%;\n    width: 15%;\n        }\n.slide26[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\n    order: 2;\n    font-size: 50px;\n    text-align: center;\n    margin-bottom: 20px;\n        }\n.slide26[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\n    order: 1;\n    display: flex;\n    flex-direction: column;\n    justify-content: center;\n    align-items: flex-start;\n    width:60%;\n    height: 100%;\n    }\n.slide26[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\nfont-size: 50px;\n    }\n.slide26[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\n    width: 90%;\n    height: 40px;\n    border-color: rgb(212, 5, 5);\n        }\n.slide26[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]{\n        margin: 20px;\n        display: flex;\n        flex-direction: row;\n        justify-content: space-between;\n        align-items: flex-start;\n        width: 90%;\n    }\n.slide26[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block2[_ngcontent-%COMP%] {\n        margin-top: 20px;\n        display: flex;\n        flex-direction: row;\n        justify-content: space-around;\n        width: 100%;\n    }\n.slide26[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block2[_ngcontent-%COMP%]   mat-radio-group[_ngcontent-%COMP%]  {\n        margin-top: 20px;\n        display: flex;\n        flex-direction: row;\n        justify-content: space-around;\n        width: 90%;\n    }\n.slide26[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\n        width: 90%;\n    }\n.slide26[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]   label[_ngcontent-%COMP%]{\n        display: flex;\n        align-items: center;\n        margin: 0;\n    }\n\n.slide27[_ngcontent-%COMP%]{\n    display: flex;\n    flex-direction: row;\n    justify-content: space-between;\n    width: 100%;\n    height: 100%;\n}\n.slide27[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\n    order: 1;\n    display: flex;\n    flex-direction: column;\n    justify-content: center;\n    align-items:center;\n    width:53%;\n    height: 100%;\n}\n.slide27[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\n    order: 2;\n    display: flex;\n    flex-direction: column;\n    justify-content: flex-start;\n    width:40%;\n    height: 100%;\n    \n    }\n.slide27[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .bSearch[_ngcontent-%COMP%]{\n        display: flex;\n        flex-flow: wrap;\n        justify-content: flex-start;\n        align-items: baseline;\n    }\n.slide27[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .bSearch[_ngcontent-%COMP%]   .search[_ngcontent-%COMP%] {\n        margin-bottom: 15px;\n        margin-left: 15px;\n        width: 50%;\n        border-radius: 50px;\n        background-color: rgb(134, 134, 134);\n        color:white;\n      }\n.slide27[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .bSearch[_ngcontent-%COMP%]   i[_ngcontent-%COMP%] {\n        margin-left: -29px;\n        color: white;\n      }\n.slide27[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .list-item[_ngcontent-%COMP%] {  \n\nborder: 3px solid rgb(255, 0, 0);\nborder-radius: 4px;\ncolor: rgb(153, 153, 153);\nline-height: 90px;\nfont-weight: 400;\nbackground-color: rgb(255, 255, 255);\nwidth: 88%;\n}\n.slide27[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .item-content[_ngcontent-%COMP%] {\n    height: 100%;\n    border: none;\n    color: rgb(153, 153, 153);\n    line-height: 45px;\n    background-color: rgb(255, 255, 255);\n    box-shadow: rgba(0,0,0,0.2) 0px 1px 2px 0px;\n    display: flex;\n    flex-direction: row;\n    justify-content: space-around;\n    align-items: center;\n    width: 100%;\n  }\n.slide27[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .item-content[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]{\n    display: flex;\n    flex-direction: column;\n    justify-content: center;\n    align-items: center;\n    width: 10%;\n  }\n.slide27[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .item-content[_ngcontent-%COMP%]   .block2[_ngcontent-%COMP%]{\n    display: flex;\n    flex-direction: column;\n    justify-content: center;\n    align-items: center;\n    height: 100%;\n    width: -moz-available;\n    }\n.slide27[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .item-content[_ngcontent-%COMP%]   .block2[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\n        margin-top: 0;\n        margin-bottom: 1rem;\n        height: 29px;\n        }\n.slide27[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .item-content[_ngcontent-%COMP%]   .block3[_ngcontent-%COMP%]{\n    display: inline;\n    flex-direction: column;\n    width: 10%;\n  }\n.slide27[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .item-content[_ngcontent-%COMP%]   .block3[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\n    width: 100%;\n  }\n.slide27[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .item-content[_ngcontent-%COMP%]:hover{\n    background-color: rgb(218, 98, 98);\n  }\n.slide27[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .item-content[_ngcontent-%COMP%]:hover   p[_ngcontent-%COMP%]{\n    color: white;\n    }\n.slide27[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .item-content[_ngcontent-%COMP%]:hover   input[_ngcontent-%COMP%]{\n        color: white;\n    }\n.slide27[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .item-content[_ngcontent-%COMP%]:hover   i[_ngcontent-%COMP%]{\n        color: white;\n    }\n.slide27[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .contentTab[_ngcontent-%COMP%]{\n          width: 100%;\n          height: 500px;\n          overflow-y: scroll;\n      }\n\n@media screen and (max-width: 768px) {\n   \n    .body[_ngcontent-%COMP%]{\n        width: 100%;\n        height: 100%;\n        display: flex;\n        flex-direction: row;\n        justify-content: space-between;\n    }\n    .footer[_ngcontent-%COMP%]{\n        flex-direction: column;\n    }\n    .left[_ngcontent-%COMP%]{\n        justify-content: center;\n        align-items: center;\n        width: 100%;\n    }\n    .left[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\n        width: 60px;\n    }\n    .point[_ngcontent-%COMP%]{\n        text-align: center;\n        font-size: 7px;\n    }\n     .center[_ngcontent-%COMP%]{\n      width: 90%;\n      margin-left: 20px;  \n     }\n    .center[_ngcontent-%COMP%]   .listP[_ngcontent-%COMP%]{\n   text-align: center;\n    }\n    \n    \n.slide0[_ngcontent-%COMP%]{\n    flex-direction: column;\n    justify-content: flex-start;\n    align-items: center ;\n    height: 100%;\n}\n.slide0[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\n    order: 2;\n    width: 80%;\n    height: 100%;\n  }\n\n.slide0[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\n   width:100%;\n   }\n\n.slide0[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\n    font-size: 30px;\n   }\n\n\n\n.slide1[_ngcontent-%COMP%]{\n    display: flex;\n    flex-direction: column;\n    justify-content: space-between;\n    align-items: center;\n    width: 100%;\n    height: 100%;\n}\n.slide1[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\n  order: 2;\n  width: 90%;\n  height: 100%;\n}\n\n.slide1[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\nwidth: 100%;\n}\n.slide1[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\n    font-size: 30px;\n    }\n\n  \n  .slide2[_ngcontent-%COMP%]{\n    display: flex;\n    flex-direction: column;\n    justify-content:space-between;\n    width: 100%;\n    height: 100%;\n}\n.slide2[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\n  order: 2;\n  width: 100%;\n  height: 100%;\n  display: flex;\n  flex-direction: column;\n  justify-content:flex-end;\n}\n.slide2[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\n    order: 1;\n    display: flex;\n   flex-direction: column;\n   justify-content: space-around;\n   align-items: center;\n   width:100%;\n   height: 100%;\n   }\n   .slide2[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\n    font-size: 30px;\n    text-align: center;\n   }\n \n  \n  .slide3[_ngcontent-%COMP%]{\n    display: flex;\n    flex-direction: column;\n    justify-content: space-between;\n    width: 100%;\n    height: 100%;\n}\n.slide3[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\n    order: 2;\n    width: 100%;\n    height: 100%;\n    display: flex;\n    flex-direction: column;\n    justify-content: flex-end;\n}\n.slide3[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\n    order: 1;\n    display: flex;\n    flex-direction: column;\n    justify-content: space-around;\n    align-items: center;\n    width:100%;\n    height: 100%;\n    }\n    .slide3[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\n    font-size: 30px;\n    text-align: center;\n    }\n \n\n\n.slide4[_ngcontent-%COMP%]{\n    display: flex;\n    flex-direction: column;\n    justify-content:space-between;\n    align-items: center;\n    width: 100%;\n    height: 100%;\n}\n.slide4[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\n    order: 2;\n    width: 80%;\n    height: 100%;\n    display: flex;\n    flex-direction: column;\n    justify-content:flex-end;\n}\n.slide4[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\n    order: 1;\n    display: flex;\n    flex-direction: column;\n    justify-content: space-around;\n    align-items: center;\n    width:100%;\n    height: 100%;\n    }\n    .slide4[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\n    font-size: 30px;\n    text-align: center;\n    }\n\n    \n     \n     .slide5[_ngcontent-%COMP%]{\n        display: flex;\n        flex-direction: column;\n        justify-content: space-between;\n        align-items: center;\n        width: 100%;\n        height: 100%;\n\n    }\n    .slide5[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\n        order: 2;\n        width: 100%;\n        height: 100%;\n        z-index: 1;\n     \n    }\n    .slide5[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\n        order: 1;\n        display: flex;\n        flex-direction: column;\n        justify-content: space-between;\n        align-items: center;\n        width:100%;\n        height: 100%;\n        }\n        .slide5[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\n        font-size: 30px;\n        text-align: center;\n        margin-block: auto;\n        }\n        .slide5[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .blockBtn[_ngcontent-%COMP%]{\n\n           display: flex;\n           flex-direction: column;\n           justify-content: center;\n            width: 100%;\n            height: 20%;\n        }\n        .slide5[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .blockBtn[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]{\n            display: flex;\n            flex-direction: column;\n            justify-content: center;\n            align-items: flex-end;\n            width: 75%;\n         }\n    \n         .slide5[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .blockBtn[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]   .qty[_ngcontent-%COMP%]{\n            display: flex;\n            flex-direction: row;\n            justify-content: center;  \n            margin: 10px;          \n            }\n            \n    .slide5[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .blockBtn[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]   .qty[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\n        order: 2;\n        width: 80px;\n        height: 50.5px;\n        border: 4px solid rgb(212, 5, 5);\n        text-align: center;\n        }\n   \n \n .slide6[_ngcontent-%COMP%]{\n    display: flex;\n    flex-direction: column;\n    justify-content: center;\n    width: 100%;\n    height: 100%;\n}\n.slide6[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\n    order: 2;\n    display: flex;\n    flex-direction: column;\n    justify-content: center;\n    align-items:center;\n    width:100%;\n    height: 100%;\n}\n.slide6[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\n    order: 2;\n    height: 15%;\n    width: 15%;\n        }\n.slide6[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\n    order: 1;\n    font-size: 30px;\n    text-align: center;\n    margin-bottom: 20px;\n        }\n\n\n.slide6[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\n    order: 2;\n    display: flex;\n    flex-direction: column;\n    justify-content: flex-start;\n    align-items: center;\n    width:100%;\n    height: 100%;\n    }\n\n    \n\n.slide7[_ngcontent-%COMP%]{\n    display: flex;\n    flex-direction: column;\n    justify-content: space-between;\n    align-items: center;\n    width: 100%;\n    height: 100%;\n}\n.slide7[_ngcontent-%COMP%]   .colum1[_ngcontent-%COMP%]{\n    order: 1;\n    display: flex;\n    flex-direction: column;\n    justify-content: space-between;\n    width: 100%;\n    height: 100%;\n}\n.slide7[_ngcontent-%COMP%]   .colum2[_ngcontent-%COMP%]{\n    order: 2;\n    display: flex;\n    flex-direction: row;\n    justify-content: center;\n    align-items: flex-end;\n    width: 100%;\n    height: 100%;\n}\n.slide7[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\n    order: 2;\n    width: 90%;\n    height: 100%;\n}\n.slide7[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\n    order: 1;\n    display: flex;\n    flex-direction: column;\n    justify-content: flex-end;\n    align-items: center;\n    width:100%;\n    height: 100%;\n    margin-top: 20px;\n    }\n    .slide7[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\n    font-size: 30px;\n    text-align: center;\n    }\n    .slide7[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\n    width: 80%;\n    height: 20px;\n    border-color: rgb(212, 5, 5);\n        }\n    .slide7[_ngcontent-%COMP%]   .progress[_ngcontent-%COMP%]{\n    order: 3;\n}\n\n.slider[_ngcontent-%COMP%] {\n    -webkit-appearance: none;\n    width: 100%;\n    height: 15px;\n    background: rgb(255, 255, 255);\n    outline: none;\n    border: 5px solid rgb(189, 8, 8);\n    border-radius: 8px;\n  }\n  \n  \n  \n  .slider[_ngcontent-%COMP%]::-webkit-slider-thumb {\n    -webkit-appearance: none;\n    appearance: none;\n    width: 10px;\n    height: 40px;\n    background: rgb(248, 224, 5);\n    cursor: pointer;\n    border: 5px solid rgb(248, 224, 5);\n    border-radius: 30px;\n  }\n  \n  \n  .slider[_ngcontent-%COMP%]::-moz-range-thumb {\n    width: 10px;\n    height: 30px;\n    background: rgb(255, 255, 255);\n    cursor: pointer;\n    border: 5px solid rgb(189, 8, 8);\n    border-radius: 4px;\n  }\n.__range[_ngcontent-%COMP%]{\n    width: 80%;\n    height: 100%;\n}\n.__range-step[_ngcontent-%COMP%]{\n\tposition: relative;                \n}\n.__range-step[_ngcontent-%COMP%]{\n\tposition: relative;                \n}\n\n.__range-max[_ngcontent-%COMP%]{\n\tfloat: right;\n}\n           \n\n\n.__range[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]::range-progress {\tbackground: rgb(189, 8, 8);\n}\n.slider[_ngcontent-%COMP%]   input[type=range][_ngcontent-%COMP%]::-moz-range-progress {\n    background-color: #c657a0;\n  }\n.__range-step[_ngcontent-%COMP%]   datalist[_ngcontent-%COMP%] {\n\tposition:relative;\n\tdisplay: flex;\n\tjustify-content: space-between;\n\theight: auto;\n\tbottom: 6px;\n\t\n\t-webkit-user-select: none;                   \n\tuser-select: none; \n\t\n\tpointer-events:none;  \n}\n.__range-step[_ngcontent-%COMP%]   datalist[_ngcontent-%COMP%]   option[_ngcontent-%COMP%] {\n\twidth: 10px;\n\theight: 10px;\n\tmin-height: 10px;\n\tborder-radius: 100px;\n\t\n\twhite-space: nowrap;       \n  padding:0;\n  line-height: 40px;\n}\n\n\n\n.slide8[_ngcontent-%COMP%]{\n    display: flex;\n    flex-direction: column;\n    justify-content: space-between;\n    width: 100%;\n    height: 100%;\n}\n.slide8[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\n    order: 2;\n    display: flex;\n    align-items: flex-end;\n    width: 100%;\n    height: 100%;\n}\n.slide8[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\n    order: 1;\n    display: flex;\n    flex-direction: column;\n    justify-content: space-around;\n    align-items: center;\n    width:100%;\n    height: 100%;\n    }\n    .slide8[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\n    font-size: 30px;\n    text-align: center;\n    }\n\n    \n.slide9[_ngcontent-%COMP%]{\n    display: flex;\n    flex-direction: column;\n    justify-content: space-between;\n    width: 100%;\n    height: 750px;\n}\n.slide9[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\n    order: 2;\n    display: flex;\n    align-items: center;\n    flex-direction: column;\n    justify-content: flex-end;\n    width: 100%;\n    height: 100%;\n}\n.slide9[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\n    order: 1;\n    display: flex;\n    flex-direction: column;\n    justify-content: space-around;\n    align-items: center;\n    width:100%;\n    height: 100%;\n    }\n    .slide9[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\n    font-size: 30px;\n    text-align: center;\n    }\n    .slide9[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   mat-radio-group[_ngcontent-%COMP%]{\n        margin-top: 50px;\n        display: flex;\n        flex-direction: row;\n        justify-content: space-around;\n        width: 100%;\n    }\n\n\n\n.slide10[_ngcontent-%COMP%]{\n    display: flex;\n    flex-direction: column;\n    justify-content: space-between;\n    width: 100%;\n    height: 750px;\n}\n.slide10[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\n    order: 2;\n    display: flex;\n    align-items: center;\n    width: 100%;\n    height: 100%;\n}\n.slide10[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\n    order: 1;\n    display: flex;\n    flex-direction: column;\n    justify-content: center;\n    align-items: center;\n    width:100%;\n    height: 100%;\n    }\n    .slide10[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\n    font-size: 30px;\n    text-align: center;\n    }\n    .slide10[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block[_ngcontent-%COMP%]{\n        display: flex;\n        flex-direction: column;\n        justify-content: center;\n        width: 100%;\n        margin-top: 20px;\n    }\n\n    .slide10[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]{\n        display: flex;\n        flex-direction: column;\n        justify-content: space-around;\n        align-items: center; \n        margin-top: 10px;\n     }\n  \n     \n        \n\n.slide11[_ngcontent-%COMP%]{\n    display: flex;\n    flex-direction: column;\n    justify-content: space-between;\n    width: 100%;\n    height: 750px;\n}\n.slide11[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\n    align-items: center;\n    display: flex;\n    flex-direction: column;\n    justify-content: flex-end;\n    order: 2;\n    width: 100%;\n    height: 100%;\n}\n.slide11[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\n    order: 1;\n    display: flex;\n    flex-direction: column;\n    justify-content: space-around;\n    align-items: center;\n    width:100%;\n    height: 100%;\n    }\n    .slide11[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\n    font-size: 30px;\n    text-align: center;\n    }\n\n  \n  .slide12[_ngcontent-%COMP%]{\n    display: flex;\n    flex-direction: column;\n    justify-content:center;\n    width: 100%;\n    height: 700px;\n}\n.slide12[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\n    order: 2;\n    display: flex;\n    flex-direction: column;\n    justify-content: space-around;\n    align-items:center;\n    width:100%;\n    height: 100%;\n}\n.slide12[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\n    order: 1;\n    font-size: 30px;\n    text-align: center;\n    margin-bottom: 20px;\n        }\n\n\n.slide12[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\n    order: 2;\n    display: flex;\n    flex-direction: column;\n    justify-content: center;\n    align-items: center;\n    width:100%;\n    height: 100%;\n    }\n \n\n\n.slide13[_ngcontent-%COMP%]{\ndisplay: flex;\nflex-direction: column;\njustify-content: center;\nwidth: 100%;\nheight: 750px;\n}\n.slide13[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\norder: 2;\nwidth: 80%;\nheight: 100%;\n}\n.slide13[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\norder: 1;\ndisplay: flex;\nflex-direction: column;\njustify-content: center;\nalign-items: center;\nwidth:100%;\nheight: 100%;\n}\n.slide13[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\nfont-size: 30px;\ntext-align: center;\n}\n\n\n.slide14[_ngcontent-%COMP%]{\ndisplay: flex;\nflex-direction: column;\njustify-content: space-between;\nwidth: 100%;\nheight: 750px;\n}\n.slide14[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\norder: 2;\ndisplay: flex;\nalign-items: center;\njustify-content: flex-end;\nwidth: 100%;\nheight: 100%;\n}\n.slide14[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\norder: 1;\ndisplay: flex;\nflex-direction: column;\njustify-content: space-around;\nalign-items: center;\nwidth:100%;\nheight: 100%;\n}\n.slide14[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\nfont-size: 30px;\ntext-align: center;\n}\n\n.slide15[_ngcontent-%COMP%]{\n    display: flex;\n    flex-direction: column;\n    justify-content: space-between;\n    width: 100%;\n    height:750px;\n}\n.slide15[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\n    order: 2;\n    display: flex;\n    flex-direction: column;\n    justify-content: center;\n    align-items:center;\n    width:100%;\n    height: 100%;\n}\n.slide15[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\n    order: 2;\n\n        }\n.slide15[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\n    order: 1;\n    font-size: 30px;\n    text-align: center;\n    margin-bottom: 20px;\n        }\n\n\n\n.slide15[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\n    order: 2;\n    display: flex;\n    flex-direction: column;\n    justify-content: flex-start;\n    align-items:center;\n    width:100%;\n    height: 100%;\n    }\n \n\n\n\n.slide16[_ngcontent-%COMP%]{\ndisplay: flex;\nflex-direction: column;\njustify-content: space-between;\nwidth: 100%;\nheight: 750px;\n}\n.slide16[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\norder: 2;\ndisplay: flex;\nalign-items: center;\njustify-content: flex-end;\nwidth: 100%;\nheight: 100%;\n}\n.slide16[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\norder: 1;\ndisplay: flex;\nflex-direction: column;\njustify-content: flex-start;\nalign-items: center;\nwidth:100%;\nheight: 100%;\n}\n.slide16[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\nfont-size: 30px;\ntext-align: center;\n}\n\n\n.slide17[_ngcontent-%COMP%]{\n    display: flex;\n    flex-direction: column;\n    justify-content: space-between;\n    width: 100%;\n    height:750px;\n}\n.slide17[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\n    order: 2;\n    display: flex;\n    flex-direction: column;\n    justify-content: center;\n    align-items:center;\n    width:100%;\n    height: 100%;\n}\n.slide17[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\n    order: 1;\n    font-size: 30px;\n    text-align: center;\n    margin-bottom: 20px;\n        }\n\n.slide17[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\n    order: 2;\n    display: flex;\n    flex-direction: column;\n    justify-content: flex-start;\n    align-items: center;\n    width:100%;\n    height: 100%;\n    }\n \n       \n       .slide18[_ngcontent-%COMP%]{\n        display: flex;\n        flex-direction: column;\n        justify-content: center;\n        width: 100%;\n        height: 100%;\n    }\n    .slide18[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\n        order: 1;\n        display: flex;\n        flex-direction: column;\n        justify-content: center;\n        align-items:center;\n        width:100%;\n        height: 100%;\n    }\n    .slide18[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\n        order: 2;\n        height: 20%;\n        width: 15%;\n            }\n    .slide18[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\n        order: 2;\n        font-size: 30px;\n        text-align: center;\n        margin-bottom: 20px;\n            }\n\n    .slide18[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\n        order: 2;\n        display: flex;\n        flex-direction: column;\n        justify-content: flex-start ;\n        align-items: center;\n        width:100%;\n        height: 100%;\n        }\n     \n    \n\n.slide19[_ngcontent-%COMP%]{\n    display: flex;\n    flex-direction: column;\n    justify-content: center;\n    width: 100%;\n    height: 100%;\n}\n.slide19[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\n    margin-left: 50px;\n    display: flex;\n    align-items: center;\n    justify-content: center;\n    order: 2;\n    width: 90%;\n    height: 100%;\n}\n.slide19[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\n    order: 1;\n    display: flex;\n    flex-direction: column;\n    justify-content: space-around;\n    align-items: center;\n    width:100%;\n    height: 100%;\n    }\n    .slide19[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\n    font-size: 30px;\n    text-align: center;\n    }\n\n        \n\n.slide20[_ngcontent-%COMP%]{\ndisplay: flex;\nflex-direction: column;\njustify-content: center;\nwidth: 100%;\nheight: 100%;\n}\n.slide20[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\ndisplay: flex;\nalign-items: center;\njustify-content: center;\norder: 1;\nwidth: 100%;\nheight: 80%;\n}\n.slide20[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\nheight: 60%;\nwidth: 40%;\nfilter: drop-shadow(0.4rem 0.4rem 0.45rem rgba(0, 0, 30, 0.5));\n}\n.slide20[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\norder: 2;\ndisplay: flex;\nflex-direction: column;\njustify-content: flex-start;\nalign-items: center;\nwidth:100%;\nheight: 100%;\n}\n.slide20[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\nfont-size: 30px;\ntext-align: center;\n}\n.slide20[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]{\n    margin-top: 0px;\n    display: flex;\n    flex-direction: column;\n    justify-content: center;\n    width: 90%;\n}\n.slide20[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block2[_ngcontent-%COMP%] {\n    margin-top: 0px;\n    display: flex;\n    flex-direction: column;\n    justify-content: center;\n    width: 90%;\n    }\n\n.slide20[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block2[_ngcontent-%COMP%]   label[_ngcontent-%COMP%]{\n    display: flex;\n    align-items: flex-start;\n    flex-direction: column;\n    margin-top: 10px;\n}\n\n\n.slide21[_ngcontent-%COMP%]{\ndisplay: flex;\nflex-direction: column;\njustify-content: center;\nalign-items: center;\nwidth: 100%;\nheight: 100%;\n}\n.slide21[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\ndisplay:none;\nflex-direction: column;\nalign-items: center;\njustify-content: center;\norder: 1;\nwidth: 100%;\nheight: 10%;\n}\n.slide21[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\norder: 1;\ndisplay: flex;\nflex-direction: column;\njustify-content:center;\nalign-items: center;\nwidth:80%;\nheight: 80%;    \n}\n\n.slide21[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\n    line-height: normal;\n   color: green;\n   font-size: 30px;\n   text-align: center;\n    }\n    .slide21[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\n        line-height: normal;\n        font-size: 80px;\n        margin: 10px;\n            }\n.slide21[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\n    line-height: normal;\n    color: rgb(0, 0, 133);\n    font-size: 30px;\n    text-align: center;\n        }\n.slide21[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .prix[_ngcontent-%COMP%] {\n    color: red;\n        }\n.slide21[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .faild[_ngcontent-%COMP%]{\n    line-height: normal;\n    width: 360px;\n    color: red;\n    font-size: 30px;\n    text-align: center;\n        }\n.slide21[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\n    width: 80px;\n        }\n\n.slide21[_ngcontent-%COMP%]   .nextIcon[_ngcontent-%COMP%]{\n    order: 3;\n    display: flex;\n    flex-direction: row;\n    justify-content: flex-end;\n    align-items: flex-end;\n    width:97%;\n    height: 15%;\n}\n.slide21[_ngcontent-%COMP%]   .nextIcon[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\n    color: green;\n    width: 80px;\n}\n        \n\n\n\n.slide24[_ngcontent-%COMP%]{\ndisplay: flex;\nflex-direction: column;\njustify-content: center;\nwidth: 100%;\nheight: 100%;\n}\n.slide24[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\nmargin-left: 50px;\norder: 2;\ndisplay: flex;\nalign-items:flex-end;\nwidth: 88%;\nheight: 100%;\n}\n.slide24[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\norder: 1;\ndisplay: flex;\nflex-direction: column;\njustify-content: flex-end;\nalign-items: center;\nwidth:100%;\nheight: 100%;\nmargin-top: 20px;\n}\n.slide24[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\nfont-size: 30px;\ntext-align: center;\n}\n.slide24[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\nwidth: 80%;\nheight: 40px;\nmargin: 50px 0 50px 0;\nborder-color: rgb(212, 5, 5);\n    }\n.slide24[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   mat-radio-group[_ngcontent-%COMP%]{\n   display: flex;\n   flex-direction: row;\n   justify-content: space-around;\n   align-items: flex-start;\n   width: 100%;\n   margin-top: 20px;\n\n        }\n.slide24[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .calend[_ngcontent-%COMP%]{\n    width: 350px;\n    height: 350px;\n}\n\n\n\n.slide25[_ngcontent-%COMP%]{\ndisplay: flex;\nflex-direction: column;\njustify-content: center;\nalign-items: center;\nwidth: 100%;\nheight: 100%;\n}\n.slide25[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\ndisplay: flex;\nalign-items: flex-end;\njustify-content: flex-end;\norder: 1;\nwidth: 22%;\nheight: 20%;\n}\n\n.slide25[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\norder: 2;\ndisplay: flex;\nflex-direction: column;\njustify-content:flex-start;\nalign-items: center;\nwidth:100%;\nheight: 80%;\n}\n\n.slide25[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\nwidth: 50px;\nmargin-bottom: 30px;\n    }\n.slide25[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\n    text-align: center;\ncolor: rgb(0, 0, 133);\nfont-size: 30px;\nline-height: normal;\n}\n\n.slide25[_ngcontent-%COMP%]   .nextIcon[_ngcontent-%COMP%]{\norder: 3;\n}\n.slide25[_ngcontent-%COMP%]   .nextIcon[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\ntext-align: center;\nline-height: normal;\n}\n.slide25[_ngcontent-%COMP%]   .nextIcon[_ngcontent-%COMP%]   .socialMedia[_ngcontent-%COMP%] {\ndisplay: flex;\nflex-direction: row;\njustify-content: center;\n}\n.slide25[_ngcontent-%COMP%]   .nextIcon[_ngcontent-%COMP%]   .socialMedia[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\nwidth: 60px;\n}\n\n          \n\n.slide26[_ngcontent-%COMP%]{\n    display: flex;\n    flex-direction: column;\n    justify-content: space-between;\n    align-items: center;\n    width: 100%;\n    height: 100%;\n}\n.slide26[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\n    order: 1;\n    display: flex;\n    flex-direction: column;\n    justify-content: center;\n    align-items:center;\n    width:53%;\n    height: 100%;\n}\n.slide26[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\n    order: 2;\n    height: 20%;\n    width: 15%;\n        }\n.slide26[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\n    order: 2;\n    font-size: 30px;\n    text-align: center;\n    margin-bottom: 20px;\n        }\n\n.slide26[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\n    order: 2;\n    display: flex;\n    flex-direction: column;\n    justify-content: center;\n    align-items: flex-start;\n    width:100%;\n    height: 100%;\n    }\n    .slide26[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\n     font-size: 50px;\n    }\n    .slide26[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\n    width: 90%;\n    height: 40px;\n    border-color: rgb(212, 5, 5);\n    margin-left: 5px;\n        }\n\n    .slide26[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]{\n        margin: 20px;\n        display: flex;\n        flex-direction: column;\n        justify-content: space-around;\n        align-items: flex-start;\n        width: 90%;\n    }\n    .slide26[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block2[_ngcontent-%COMP%] {\n        margin-top: 20px;\n        display: flex;\n        flex-direction: row;\n        justify-content: space-around;\n        width: 100%;\n    }\n    .slide26[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block2[_ngcontent-%COMP%]   mat-radio-group[_ngcontent-%COMP%]  {\n        margin-top: 20px;\n        display: flex;\n        flex-direction: row;\n        justify-content: space-around;\n        width: 90%;\n    }\n\n    \n    .slide26[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\n        width: 90%;\n    }\n    .slide26[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]   label[_ngcontent-%COMP%]{\n        display: flex;\n        align-items: center;\n        margin: 0;\n    }\n\n\n    }\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdGVzdC90ZXN0LmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLFdBQVc7SUFDWCxZQUFZO0lBQ1osZUFBZTtJQUNmLFNBQVM7SUFDVCxVQUFVO0FBQ2Q7QUFDQTtJQUNJLFdBQVc7SUFDWCxZQUFZO0lBQ1osYUFBYTtJQUNiLHFCQUFxQjtJQUNyQix1QkFBdUI7QUFDM0I7QUFDQTtJQUNJLFdBQVc7SUFDWCxpQkFBaUI7SUFDakIsaUJBQWlCO0lBQ2pCLFlBQVk7SUFDWiw0QkFBNEI7SUFDNUIsYUFBYTtJQUNiLG1CQUFtQjtBQUN2QjtBQUNBO0lBQ0ksYUFBYTtJQUNiLG1CQUFtQjtJQUNuQiwyQkFBMkI7SUFDM0IsWUFBWTtJQUNaLG1CQUFtQjtJQUNuQixVQUFVO0lBQ1YsaUJBQWlCO0FBQ3JCO0FBQ0E7SUFDSSxVQUFVO0FBQ2Q7QUFDQTtJQUNJLGVBQWU7SUFDZix3QkFBd0I7SUFDeEIsaUJBQWlCOztBQUVyQjtBQUNBO0lBQ0ksd0JBQXdCO0FBQzVCO0FBQ0E7SUFDSSxZQUFZO0lBQ1osZUFBZTtBQUNuQjtBQUNBO0lBQ0ksZUFBZTtJQUNmLGVBQWU7SUFDZixpQkFBaUI7QUFDckI7QUFDQTtJQUNJLFlBQVk7QUFDaEI7QUFFQTtDQUNDLFdBQVc7Q0FDWCxZQUFZO0NBQ1osYUFBYTtDQUNiLG1CQUFtQjtDQUNuQix1QkFBdUI7Q0FDdkIsbUJBQW1CO0FBQ3BCO0FBRUEsK0VBQStFO0FBQy9FO0lBQ0ksZ0NBQWdDO0lBQ2hDLFlBQVk7QUFDaEI7QUFDQztJQUNHLG9DQUFvQztJQUNwQyxZQUFZO0FBQ2hCO0FBQ0EsNkVBQTZFO0FBQzdFO0lBQ0ksYUFBYTtJQUNiLG1CQUFtQjtJQUNuQiw4QkFBOEI7SUFDOUIsV0FBVztJQUNYLFlBQVk7QUFDaEI7QUFDQTtFQUNFLFFBQVE7RUFDUixVQUFVO0VBQ1YsWUFBWTtBQUNkO0FBRUE7Q0FDQyxRQUFRO0NBQ1IsYUFBYTtDQUNiLHNCQUFzQjtHQUNwQix1QkFBdUI7R0FDdkIsa0JBQWtCO0FBQ3JCLFVBQVU7QUFDVixZQUFZO0FBQ1o7QUFFRztJQUNDLFVBQVU7SUFDVixZQUFZO0lBQ1osNEJBQTRCO09BQ3pCO0FBRVAsNkVBQTZFO0FBQzdFO0lBQ0ksYUFBYTtJQUNiLG1CQUFtQjtJQUNuQiw4QkFBOEI7SUFDOUIsV0FBVztJQUNYLFlBQVk7QUFDaEI7QUFDQTtFQUNFLFFBQVE7RUFDUixVQUFVO0VBQ1YsWUFBWTtBQUNkO0FBQ0E7SUFDSSxRQUFRO0lBQ1IsYUFBYTtHQUNkLHNCQUFzQjtHQUN0Qix1QkFBdUI7R0FDdkIsbUJBQW1CO0dBQ25CLFNBQVM7R0FDVCxZQUFZO0dBQ1o7QUFDQTtBQUNILGVBQWU7R0FDWjtBQUNBO0lBQ0MsWUFBWTtJQUNaLFlBQVk7SUFDWixtQkFBbUI7SUFDbkIsNEJBQTRCO09BQ3pCO0FBRUEsNkVBQTZFO0FBQzdFO1FBQ0MsYUFBYTtRQUNiLG1CQUFtQjtRQUNuQiw4QkFBOEI7UUFDOUIsV0FBVztRQUNYLFlBQVk7SUFDaEI7QUFDQTtNQUNFLFFBQVE7TUFDUixVQUFVO01BQ1YsWUFBWTtJQUNkO0FBQ0E7UUFDSSxRQUFRO1FBQ1IsYUFBYTtPQUNkLHNCQUFzQjtPQUN0Qix1QkFBdUI7T0FDdkIsdUJBQXVCO09BQ3ZCLFNBQVM7T0FDVCxZQUFZO09BQ1o7QUFDQTtJQUNILGVBQWU7T0FDWjtBQUdJO1lBQ0MsVUFBVTtZQUNWLGVBQWU7WUFDZixnQkFBZ0I7WUFDaEIsWUFBWTtZQUNaLFlBQVk7WUFDWiw0QkFBNEI7UUFDaEM7QUFDQTtZQUNJLGVBQWU7WUFDZixnQkFBZ0I7WUFDaEIsWUFBWTtZQUNaLFlBQVk7WUFDWiw0QkFBNEI7UUFDaEM7QUFDQTtZQUNJLGVBQWU7WUFDZixnQkFBZ0I7WUFDaEIsWUFBWTtZQUNaLFlBQVk7WUFDWiw0QkFBNEI7UUFDaEM7QUFDQTtZQUNJLHFCQUFxQjtZQUNyQixtRUFBbUU7WUFDbkUsWUFBWTtZQUNaLFlBQVk7WUFDWiw0QkFBNEI7UUFDaEM7QUFFSCw2RUFBNkU7QUFDMUU7UUFDQSxhQUFhO1FBQ2IsbUJBQW1CO1FBQ25CLDhCQUE4QjtRQUM5QixXQUFXO1FBQ1gsWUFBWTtJQUNoQjtBQUNBO1FBQ0ksUUFBUTtRQUNSLFVBQVU7UUFDVixZQUFZO0lBQ2hCO0FBQ0E7UUFDSSxRQUFRO1FBQ1IsYUFBYTtRQUNiLHNCQUFzQjtRQUN0Qiw2QkFBNkI7UUFDN0IsbUJBQW1CO1FBQ25CLGdCQUFnQjtRQUNoQixTQUFTO1FBQ1QsV0FBVztRQUNYO0FBQ0E7UUFDQSxlQUFlO1FBQ2Y7QUFDQTtXQUNHLGFBQWE7V0FDYixzQkFBc0I7V0FDdEIsdUJBQXVCOztRQUUxQjtBQUNBO1lBQ0ksYUFBYTtZQUNiLG1CQUFtQjtZQUNuQix1QkFBdUI7O1NBRTFCO0FBQ0Q7UUFDQSxZQUFZO1FBQ1osWUFBWTtRQUNaLFdBQVc7UUFDWCw0QkFBNEI7WUFDeEI7QUFDQTtnQkFDSSxZQUFZO2dCQUNaLFdBQVc7b0JBQ1A7QUFFcEIsNkVBQTZFO0FBQ3pFO1FBQ0ksYUFBYTtRQUNiLG1CQUFtQjtRQUNuQiw4QkFBOEI7UUFDOUIsV0FBVztRQUNYLFlBQVk7SUFDaEI7QUFDQTtRQUNJLGlCQUFpQjtRQUNqQixRQUFRO1FBQ1IsVUFBVTtRQUNWLFlBQVk7UUFDWixhQUFhO1FBQ2IsVUFBVTtRQUNWLGdCQUFnQjtJQUNwQjtBQUNBO1FBQ0ksUUFBUTtRQUNSLGFBQWE7UUFDYixzQkFBc0I7UUFDdEIsdUJBQXVCO1FBQ3ZCLHVCQUF1QjtRQUN2QixTQUFTO1FBQ1QsWUFBWTtRQUNaO0FBQ0E7SUFDSixlQUFlO1FBQ1g7QUFDQTtRQUNBLFVBQVU7UUFDVixZQUFZO1FBQ1osNEJBQTRCO1lBQ3hCO0FBR1AsNkVBQTZFO0FBQzdFO1FBQ0csYUFBYTtRQUNiLG1CQUFtQjtRQUNuQiw4QkFBOEI7UUFDOUIsV0FBVztRQUNYLFlBQVk7SUFDaEI7QUFDQTtRQUNJLFFBQVE7UUFDUixVQUFVO1FBQ1YsWUFBWTtRQUNaLFVBQVU7UUFDVixhQUFhO1FBQ2IsZ0JBQWdCO0lBQ3BCO0FBR0E7UUFDSSxRQUFRO1FBQ1IsYUFBYTtRQUNiLHNCQUFzQjtRQUN0Qix1QkFBdUI7UUFDdkIsbUJBQW1CO1FBQ25CLFNBQVM7UUFDVCxZQUFZO1FBQ1o7QUFDQTtRQUNBLGVBQWU7UUFDZjtBQUNBO1dBQ0csYUFBYTtXQUNiLG1CQUFtQjtXQUNuQiw2QkFBNkI7WUFDNUIsV0FBVztZQUNYLFdBQVc7UUFDZjtBQUNBO1lBQ0ksYUFBYTtZQUNiLG1CQUFtQjtZQUNuQiwyQkFBMkI7WUFDM0IsbUJBQW1CO1NBQ3RCO0FBRUE7WUFDRyxhQUFhO1lBQ2IsbUJBQW1CO1lBQ25CLHVCQUF1QjtZQUN2QjtBQUVSO1FBQ0ksUUFBUTtRQUNSLFdBQVc7UUFDWCxZQUFZO1FBQ1osZ0NBQWdDO1FBQ2hDLGtCQUFrQjtRQUNsQjtBQUNKO1FBQ0ksUUFBUTtRQUNSLGlCQUFpQjtRQUNqQixVQUFVO1FBQ1YsYUFBYTtRQUNiLG1CQUFtQjtRQUNuQixtQkFBbUI7WUFDZjtBQUNBLGdEQUFnRCxhQUFhLENBQUM7QUFDOUQsK0NBQStDLGFBQWEsQ0FBQztBQUM3RCxrREFBa0QsaUJBQWlCLENBQUM7QUFDcEUsZ0RBQWdELGlCQUFpQixDQUFDO0FBQzFFO1FBQ0ksUUFBUTtRQUNSLGFBQWE7UUFDYixzQkFBc0I7UUFDdEIsMkJBQTJCO1FBQzNCO0FBQ0E7WUFDSSxZQUFZO1lBQ1osOEJBQThCO1lBQzlCLGtCQUFrQjtZQUNsQixVQUFVO1lBQ1Ysa0JBQWtCO1lBQ2xCLFlBQVk7WUFDWixZQUFZO1lBQ1osZ0NBQWdDO1lBQ2hDLGFBQWE7WUFDYixzQkFBc0I7WUFDdEIsZUFBZTtZQUNmLHVCQUF1QjtZQUN2QixtQkFBbUI7WUFDbkI7QUFFQSw2RUFBNkU7QUFDN0U7Z0JBQ0ksYUFBYTtnQkFDYixtQkFBbUI7Z0JBQ25CLDhCQUE4QjtnQkFDOUIsV0FBVztnQkFDWCxZQUFZO1lBQ2hCO0FBQ0E7Z0JBQ0ksUUFBUTtnQkFDUixhQUFhO2dCQUNiLHNCQUFzQjtnQkFDdEIsdUJBQXVCO2dCQUN2QixrQkFBa0I7Z0JBQ2xCLFNBQVM7Z0JBQ1QsWUFBWTtZQUNoQjtBQUNBO2dCQUNJLFFBQVE7Z0JBQ1IsV0FBVztnQkFDWCxVQUFVO29CQUNOO0FBQ1I7Z0JBQ0ksUUFBUTtnQkFDUixlQUFlO2dCQUNmLGtCQUFrQjtnQkFDbEIsbUJBQW1CO29CQUNmO0FBR1I7Z0JBQ0ksUUFBUTtnQkFDUixhQUFhO2dCQUNiLHNCQUFzQjtnQkFDdEIsdUJBQXVCO2dCQUN2Qix1QkFBdUI7Z0JBQ3ZCLFNBQVM7Z0JBQ1QsWUFBWTtnQkFDWjtBQUVBO2dCQUNBLFVBQVU7Z0JBQ1YsWUFBWTtnQkFDWiw0QkFBNEI7b0JBQ3hCO0FBR3BCLDZFQUE2RTtBQUM3RTtJQUNJLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsdUJBQXVCO0lBQ3ZCLFdBQVc7SUFDWCxZQUFZO0FBQ2hCO0FBQ0E7SUFDSSxRQUFRO0lBQ1IsYUFBYTtJQUNiLG1CQUFtQjtJQUNuQiw2QkFBNkI7SUFDN0IsV0FBVztJQUNYLFlBQVk7SUFDWixtQkFBbUI7QUFDdkI7QUFDQTtJQUNJLFFBQVE7SUFDUixhQUFhO0lBQ2IsbUJBQW1CO0lBQ25CLHVCQUF1QjtJQUN2QixXQUFXO0lBQ1gsZ0JBQWdCO0lBQ2hCLFlBQVk7QUFDaEI7QUFDQTtJQUNJLGlCQUFpQjtJQUNqQixRQUFRO0lBQ1IsVUFBVTtJQUNWLFlBQVk7QUFDaEI7QUFDQTtJQUNJLFFBQVE7SUFDUixhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLHVCQUF1QjtJQUN2Qix1QkFBdUI7SUFDdkIsU0FBUztJQUNULFlBQVk7SUFDWjtBQUNBO0FBQ0osZUFBZTtJQUNYO0FBQ0E7SUFDQSxVQUFVO0lBQ1YsWUFBWTtJQUNaLDRCQUE0QjtRQUN4QjtBQUNSO0lBQ0ksUUFBUTtBQUNaO0FBRUE7SUFDSSx3QkFBd0I7SUFDeEIsV0FBVztJQUNYLFlBQVk7SUFDWiw4QkFBOEI7SUFDOUIsYUFBYTtJQUNiLGdDQUFnQztJQUNoQyxrQkFBa0I7RUFDcEI7QUFHQSxzQkFBc0I7QUFDdEI7SUFDRSx3QkFBd0I7SUFDeEIsZ0JBQWdCO0lBQ2hCLFdBQVc7SUFDWCxZQUFZO0lBQ1osNEJBQTRCO0lBQzVCLGVBQWU7SUFDZixrQ0FBa0M7SUFDbEMsbUJBQW1CO0VBQ3JCO0FBRUEsZ0JBQWdCO0FBQ2hCO0lBQ0UsV0FBVztJQUNYLFlBQVk7SUFDWiw4QkFBOEI7SUFDOUIsZUFBZTtJQUNmLGdDQUFnQztJQUNoQyxrQkFBa0I7RUFDcEI7QUFDRjtJQUNJLFVBQVU7SUFDVixZQUFZO0FBQ2hCO0FBQ0E7Q0FDQyxrQkFBa0I7QUFDbkI7QUFDQTtDQUNDLGtCQUFrQjtBQUNuQjtBQUVBO0NBQ0MsWUFBWTtBQUNiO0FBSUEsaUNBQWlDLDBCQUEwQjtBQUMzRDtBQUNBO0lBQ0kseUJBQXlCO0VBQzNCO0FBQ0Y7Q0FDQyxpQkFBaUI7Q0FDakIsYUFBYTtDQUNiLDhCQUE4QjtDQUM5QixZQUFZO0NBQ1osWUFBWTtDQUNaLDJCQUEyQjtDQUMzQix5QkFBeUIsRUFBRSxXQUFXLEVBQ2QsWUFBWSxFQUNiLGVBQWU7Q0FDdEMsaUJBQWlCLEVBQUUsYUFBYTtDQUNoQyx5QkFBeUI7Q0FDekIsbUJBQW1CO0FBQ3BCO0FBQ0E7Q0FDQyxXQUFXO0NBQ1gsWUFBWTtDQUNaLGdCQUFnQjtDQUNoQixvQkFBb0I7Q0FDcEIsY0FBYztDQUNkLG1CQUFtQjtFQUNsQixTQUFTO0VBQ1QsaUJBQWlCO0FBQ25CO0FBR0EsNkVBQTZFO0FBQzdFO0lBQ0ksYUFBYTtJQUNiLG1CQUFtQjtJQUNuQiw2QkFBNkI7SUFDN0IsV0FBVztJQUNYLFlBQVk7QUFDaEI7QUFDQTtJQUNJLFFBQVE7SUFDUixhQUFhO0lBQ2IsbUJBQW1CO0lBQ25CLFVBQVU7SUFDVixZQUFZO0FBQ2hCO0FBQ0E7SUFDSSxRQUFRO0lBQ1IsYUFBYTtJQUNiLHNCQUFzQjtJQUN0Qix1QkFBdUI7SUFDdkIsdUJBQXVCO0lBQ3ZCLFNBQVM7SUFDVCxZQUFZO0lBQ1o7QUFDQTtBQUNKLGVBQWU7SUFDWDtBQUNBO0lBQ0EsVUFBVTtJQUNWLFlBQVk7SUFDWiw0QkFBNEI7UUFDeEI7QUFFUiw0RUFBNEU7QUFDNUU7SUFDSSxhQUFhO0lBQ2IsbUJBQW1CO0lBQ25CLDZCQUE2QjtJQUM3QixXQUFXO0lBQ1gsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksUUFBUTtJQUNSLGFBQWE7SUFDYixtQkFBbUI7SUFDbkIsVUFBVTtJQUNWLFlBQVk7QUFDaEI7QUFDQTtJQUNJLFFBQVE7SUFDUixhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLHVCQUF1QjtJQUN2Qix1QkFBdUI7SUFDdkIsU0FBUztJQUNULFlBQVk7SUFDWjtBQUNBO0lBQ0EsZUFBZTtJQUNmO0FBQ0E7UUFDSSxnQkFBZ0I7UUFDaEIsYUFBYTtRQUNiLG1CQUFtQjtRQUNuQiw2QkFBNkI7UUFDN0IsV0FBVztJQUNmO0FBR0osNkVBQTZFO0FBQzdFO0lBQ0ksYUFBYTtJQUNiLG1CQUFtQjtJQUNuQiw2QkFBNkI7SUFDN0IsV0FBVztJQUNYLFlBQVk7QUFDaEI7QUFDQTtJQUNJLFFBQVE7SUFDUixhQUFhO0lBQ2IsbUJBQW1CO0lBQ25CLFVBQVU7SUFDVixZQUFZO0FBQ2hCO0FBQ0E7SUFDSSxRQUFRO0lBQ1IsYUFBYTtJQUNiLHNCQUFzQjtJQUN0Qix1QkFBdUI7SUFDdkIsdUJBQXVCO0lBQ3ZCLFNBQVM7SUFDVCxZQUFZO0lBQ1o7QUFDQTtJQUNBLGVBQWU7SUFDZjtBQUNBO1FBQ0ksZ0JBQWdCO1FBQ2hCLGFBQWE7UUFDYixtQkFBbUI7UUFDbkIsNkJBQTZCO1FBQzdCLFdBQVc7SUFDZjtBQUVBO1FBQ0ksYUFBYTtRQUNiLG1CQUFtQjtRQUNuQiw2QkFBNkI7UUFDN0IsV0FBVztRQUNYLGdCQUFnQjtJQUNwQjtBQUVBO1FBQ0ksYUFBYTtRQUNiLHNCQUFzQjtRQUN0Qiw2QkFBNkI7UUFDN0IsbUJBQW1CO0tBQ3RCO0FBQ0Q7UUFDSSxRQUFRO1FBQ1IsYUFBYTtRQUNiLG1CQUFtQjtRQUNuQix1QkFBdUI7UUFDdkI7QUFFUjtJQUNJLFFBQVE7SUFDUixXQUFXO0lBQ1gsWUFBWTtJQUNaLGdDQUFnQztJQUNoQyxrQkFBa0I7SUFDbEI7QUFDSjtJQUNJLFFBQVE7SUFDUixpQkFBaUI7SUFDakIsVUFBVTtJQUNWLGFBQWE7SUFDYixtQkFBbUI7SUFDbkIsbUJBQW1CO1FBQ2Y7QUFFUjtJQUNJLFFBQVE7SUFDUixhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLDJCQUEyQjtJQUMzQjtBQUNBO1FBQ0ksWUFBWTtRQUNaLDhCQUE4QjtRQUM5QixrQkFBa0I7UUFDbEIsVUFBVTtRQUNWLGtCQUFrQjtRQUNsQixZQUFZO1FBQ1osWUFBWTtRQUNaLGdDQUFnQztRQUNoQyxhQUFhO1FBQ2Isc0JBQXNCO1FBQ3RCLGVBQWU7UUFDZix1QkFBdUI7UUFDdkIsbUJBQW1CO1FBQ25CO0FBR1IsOEVBQThFO0FBQzFFO1FBQ0ksYUFBYTtRQUNiLG1CQUFtQjtRQUNuQiw2QkFBNkI7UUFDN0IsV0FBVztRQUNYLFlBQVk7SUFDaEI7QUFDQTtRQUNJLG1CQUFtQjtRQUNuQixhQUFhO1FBQ2IsUUFBUTtRQUNSLFVBQVU7UUFDVixZQUFZO0lBQ2hCO0FBQ0E7UUFDSSxRQUFRO1FBQ1IsYUFBYTtRQUNiLHNCQUFzQjtRQUN0Qix1QkFBdUI7UUFDdkIsdUJBQXVCO1FBQ3ZCLFNBQVM7UUFDVCxZQUFZO1FBQ1o7QUFDQTtJQUNKLGVBQWU7UUFDWDtBQUNBO1FBQ0EsVUFBVTtRQUNWLFlBQVk7UUFDWiw0QkFBNEI7WUFDeEI7QUFFTiw4RUFBOEU7QUFDOUU7UUFDRSxhQUFhO1FBQ2IsbUJBQW1CO1FBQ25CLDhCQUE4QjtRQUM5QixXQUFXO1FBQ1gsWUFBWTtJQUNoQjtBQUNBO1FBQ0ksUUFBUTtRQUNSLGFBQWE7UUFDYixzQkFBc0I7UUFDdEIsdUJBQXVCO1FBQ3ZCLGtCQUFrQjtRQUNsQixTQUFTO1FBQ1QsWUFBWTtJQUNoQjtBQUNBO1FBQ0ksUUFBUTtRQUNSLFdBQVc7UUFDWCxVQUFVO1lBQ047QUFDUjtRQUNJLFFBQVE7UUFDUixlQUFlO1FBQ2Ysa0JBQWtCO1FBQ2xCLG1CQUFtQjtZQUNmO0FBR1I7UUFDSSxRQUFRO1FBQ1IsYUFBYTtRQUNiLHNCQUFzQjtRQUN0Qix1QkFBdUI7UUFDdkIsdUJBQXVCO1FBQ3ZCLFNBQVM7UUFDVCxZQUFZO1FBQ1o7QUFFQTtRQUNBLFVBQVU7UUFDVixZQUFZO1FBQ1osNEJBQTRCO1lBQ3hCO0FBR1osOEVBQThFO0FBQzlFO0lBQ0ksYUFBYTtJQUNiLG1CQUFtQjtJQUNuQiw4QkFBOEI7SUFDOUIsV0FBVztJQUNYLFlBQVk7QUFDaEI7QUFDQTtJQUNJLGlCQUFpQjtJQUNqQixRQUFRO0lBQ1IsYUFBYTtJQUNiLGtCQUFrQjtJQUNsQixVQUFVO0lBQ1YsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksUUFBUTtJQUNSLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsdUJBQXVCO0lBQ3ZCLG1CQUFtQjtJQUNuQixTQUFTO0lBQ1QsWUFBWTtJQUNaO0FBQ0E7QUFDSixlQUFlO0lBQ1g7QUFDQTtJQUNBLFVBQVU7SUFDVixZQUFZO0lBQ1oscUJBQXFCO0lBQ3JCLDRCQUE0QjtRQUN4QjtBQUdSLDZFQUE2RTtBQUM3RTtJQUNJLGFBQWE7SUFDYixtQkFBbUI7SUFDbkIsNkJBQTZCO0lBQzdCLFdBQVc7SUFDWCxZQUFZO0FBQ2hCO0FBQ0E7SUFDSSxRQUFRO0lBQ1IsYUFBYTtJQUNiLG1CQUFtQjtJQUNuQixVQUFVO0lBQ1YsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksUUFBUTtJQUNSLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsdUJBQXVCO0lBQ3ZCLHVCQUF1QjtJQUN2QixTQUFTO0lBQ1QsWUFBWTtJQUNaO0FBQ0E7SUFDQSxlQUFlO0lBQ2Y7QUFDQTtRQUNJLGdCQUFnQjtRQUNoQixhQUFhO1FBQ2IsbUJBQW1CO1FBQ25CLDZCQUE2QjtRQUM3QixXQUFXO0lBQ2Y7QUFFQSw4RUFBOEU7QUFDOUU7UUFDSSxhQUFhO1FBQ2IsbUJBQW1CO1FBQ25CLDhCQUE4QjtRQUM5QixXQUFXO1FBQ1gsWUFBWTtJQUNoQjtBQUNBO1FBQ0ksUUFBUTtRQUNSLGFBQWE7UUFDYixzQkFBc0I7UUFDdEIsdUJBQXVCO1FBQ3ZCLGtCQUFrQjtRQUNsQixTQUFTO1FBQ1QsWUFBWTtJQUNoQjtBQUNBO1FBQ0ksUUFBUTs7WUFFSjtBQUNSO1FBQ0ksUUFBUTtRQUNSLGVBQWU7UUFDZixrQkFBa0I7UUFDbEIsbUJBQW1CO1lBQ2Y7QUFJUjtRQUNJLFFBQVE7UUFDUixhQUFhO1FBQ2Isc0JBQXNCO1FBQ3RCLHVCQUF1QjtRQUN2Qix1QkFBdUI7UUFDdkIsU0FBUztRQUNULFlBQVk7UUFDWjtBQUVBO1FBQ0EsVUFBVTtRQUNWLFlBQVk7UUFDWiw0QkFBNEI7WUFDeEI7QUFHWiw2RUFBNkU7QUFDN0U7SUFDSSxhQUFhO0lBQ2IsbUJBQW1CO0lBQ25CLDZCQUE2QjtJQUM3QixXQUFXO0lBQ1gsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksUUFBUTtJQUNSLGFBQWE7SUFDYixtQkFBbUI7SUFDbkIsVUFBVTtJQUNWLFlBQVk7QUFDaEI7QUFDQTtJQUNJLFFBQVE7SUFDUixhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLHVCQUF1QjtJQUN2Qix1QkFBdUI7SUFDdkIsU0FBUztJQUNULFlBQVk7SUFDWjtBQUNBO0lBQ0EsZUFBZTtJQUNmO0FBQ0E7UUFDSSxnQkFBZ0I7UUFDaEIsYUFBYTtRQUNiLG1CQUFtQjtRQUNuQiw2QkFBNkI7UUFDN0IsV0FBVztJQUNmO0FBSUEsOEVBQThFO0FBQzlFO1FBQ0ksYUFBYTtRQUNiLG1CQUFtQjtRQUNuQiw4QkFBOEI7UUFDOUIsV0FBVztRQUNYLFlBQVk7SUFDaEI7QUFDQTtRQUNJLFFBQVE7UUFDUixhQUFhO1FBQ2Isc0JBQXNCO1FBQ3RCLHVCQUF1QjtRQUN2QixrQkFBa0I7UUFDbEIsU0FBUztRQUNULFlBQVk7SUFDaEI7QUFDQTtRQUNJLFFBQVE7O1lBRUo7QUFDUjtRQUNJLFFBQVE7UUFDUixlQUFlO1FBQ2Ysa0JBQWtCO1FBQ2xCLG1CQUFtQjtZQUNmO0FBSVI7UUFDSSxRQUFRO1FBQ1IsYUFBYTtRQUNiLHNCQUFzQjtRQUN0Qix1QkFBdUI7UUFDdkIsdUJBQXVCO1FBQ3ZCLFNBQVM7UUFDVCxZQUFZO1FBQ1o7QUFFQTtRQUNBLFVBQVU7UUFDVixZQUFZO1FBQ1osNEJBQTRCO1lBQ3hCO0FBR0QsOEVBQThFO0FBQzlFO1lBQ0MsYUFBYTtZQUNiLG1CQUFtQjtZQUNuQiw4QkFBOEI7WUFDOUIsV0FBVztZQUNYLFlBQVk7UUFDaEI7QUFDQTtZQUNJLFFBQVE7WUFDUixhQUFhO1lBQ2Isc0JBQXNCO1lBQ3RCLHVCQUF1QjtZQUN2QixrQkFBa0I7WUFDbEIsU0FBUztZQUNULFlBQVk7UUFDaEI7QUFDQTtZQUNJLFFBQVE7WUFDUixXQUFXO1lBQ1gsVUFBVTtnQkFDTjtBQUNSO1lBQ0ksUUFBUTtZQUNSLGVBQWU7WUFDZixrQkFBa0I7WUFDbEIsbUJBQW1CO2dCQUNmO0FBR1I7WUFDSSxRQUFRO1lBQ1IsYUFBYTtZQUNiLHNCQUFzQjtZQUN0Qix1QkFBdUI7WUFDdkIsbUJBQW1CO1lBQ25CLFNBQVM7WUFDVCxZQUFZO1lBQ1o7QUFFQTtZQUNBLFVBQVU7WUFDVixZQUFZO1lBQ1osNEJBQTRCO2dCQUN4QjtBQUloQiw4RUFBOEU7QUFDMUU7UUFDSSxhQUFhO1FBQ2IsbUJBQW1CO1FBQ25CLDhCQUE4QjtRQUM5QixXQUFXO1FBQ1gsWUFBWTtJQUNoQjtBQUNBO1FBQ0ksaUJBQWlCO1FBQ2pCLGFBQWE7UUFDYixtQkFBbUI7UUFDbkIsUUFBUTtRQUNSLFVBQVU7UUFDVixZQUFZO0lBQ2hCO0FBQ0E7UUFDSSxRQUFRO1FBQ1IsYUFBYTtRQUNiLHNCQUFzQjtRQUN0Qix1QkFBdUI7UUFDdkIsdUJBQXVCO1FBQ3ZCLFNBQVM7UUFDVCxZQUFZO1FBQ1o7QUFDQTtJQUNKLGVBQWU7UUFDWDtBQUNBO1FBQ0EsVUFBVTtRQUNWLFlBQVk7UUFDWiw0QkFBNEI7WUFDeEI7QUFJWiw4RUFBOEU7QUFDOUU7SUFDSSxhQUFhO0lBQ2IsbUJBQW1CO0lBQ25CLDhCQUE4QjtJQUM5QixXQUFXO0lBQ1gsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksYUFBYTtJQUNiLG1CQUFtQjtJQUNuQix1QkFBdUI7SUFDdkIsUUFBUTtJQUNSLFVBQVU7SUFDVixXQUFXO0FBQ2Y7QUFDQTtFQUNFLFdBQVc7RUFDWCxVQUFVO0VBQ1YsOERBQThEO0FBQ2hFO0FBQ0E7SUFDSSxRQUFRO0lBQ1IsYUFBYTtJQUNiLHNCQUFzQjtJQUN0Qix1QkFBdUI7SUFDdkIsdUJBQXVCO0lBQ3ZCLFNBQVM7SUFDVCxZQUFZO0lBQ1o7QUFDQTtBQUNKLGVBQWU7SUFDWDtBQUNBO0lBQ0EsVUFBVTtJQUNWLFlBQVk7SUFDWiw0QkFBNEI7UUFDeEI7QUFFSjtRQUNJLGdCQUFnQjtRQUNoQixhQUFhO1FBQ2IsbUJBQW1CO1FBQ25CLDhCQUE4QjtRQUM5QixVQUFVO0lBQ2Q7QUFDQTtRQUNJLGdCQUFnQjtRQUNoQixhQUFhO1FBQ2IsbUJBQW1CO1FBQ25CLDhCQUE4QjtRQUM5QixVQUFVO0lBQ2Q7QUFFQTtRQUNJLFVBQVU7SUFDZDtBQUNBO1FBQ0ksYUFBYTtRQUNiLG1CQUFtQjtRQUNuQixTQUFTO0lBQ2I7QUFDQTtRQUNJLFVBQVU7SUFDZDtBQUNBO1FBQ0ksYUFBYTtRQUNiLG1CQUFtQjtRQUNuQixTQUFTO0lBQ2I7QUFFSiw4RUFBOEU7QUFDOUU7SUFDSSxhQUFhO0lBQ2IsbUJBQW1CO0lBQ25CLHVCQUF1QjtJQUN2QixtQkFBbUI7SUFDbkIsV0FBVztJQUNYLFdBQVc7QUFDZjtBQUNBO0lBQ0ksYUFBYTtJQUNiLHFCQUFxQjtJQUNyQix5QkFBeUI7SUFDekIsUUFBUTtJQUNSLFVBQVU7SUFDVixZQUFZO0FBQ2hCO0FBQ0E7RUFDRSxXQUFXO0VBQ1gsVUFBVTtBQUNaO0FBQ0E7SUFDSSxRQUFRO0lBQ1IsYUFBYTtJQUNiLHNCQUFzQjtJQUN0QiwwQkFBMEI7SUFDMUIsbUJBQW1CO0lBQ25CLFNBQVM7SUFDVCxXQUFXO0lBQ1g7QUFFQTtRQUNJLG1CQUFtQjtPQUNwQixZQUFZO09BQ1osZUFBZTtRQUNkO0FBQ0E7O1lBRUksZ0JBQWdCO1lBQ2hCLFlBQVk7Z0JBQ1I7QUFDWjtRQUNJLG1CQUFtQjtRQUNuQixxQkFBcUI7UUFDckIsZUFBZTtZQUNYO0FBQ1I7UUFDSSxVQUFVO1FBQ1YsbUJBQW1CO1lBQ2Y7QUFDUjtRQUNJLFVBQVU7UUFDVixtQkFBbUI7UUFDbkIsZUFBZTtZQUNYO0FBQ1I7UUFDSSxZQUFZO1lBQ1I7QUFJWjtJQUNJLFFBQVE7SUFDUixhQUFhO0lBQ2IsbUJBQW1CO0lBQ25CLDJCQUEyQjtJQUMzQixxQkFBcUI7SUFDckIsVUFBVTtJQUNWLFdBQVc7QUFDZjtBQUNBO0NBQ0MsWUFBWTtDQUNaLFdBQVc7QUFDWjtBQUdBLDhFQUE4RTtBQUM5RTtJQUNJLGFBQWE7SUFDYixtQkFBbUI7SUFDbkIsOEJBQThCO0lBQzlCLFdBQVc7SUFDWCxZQUFZO0FBQ2hCO0FBQ0E7SUFDSSxpQkFBaUI7SUFDakIsUUFBUTtJQUNSLGFBQWE7SUFDYixvQkFBb0I7SUFDcEIsVUFBVTtJQUNWLFlBQVk7SUFDWixnQkFBZ0I7QUFDcEI7QUFDQTtJQUNJLFFBQVE7SUFDUixhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLHVCQUF1QjtJQUN2QixtQkFBbUI7SUFDbkIsU0FBUztJQUNULFlBQVk7SUFDWjtBQUNBO0lBQ0EsZUFBZTtJQUNmLGtCQUFrQjtJQUNsQjtBQUNBO0lBQ0EsVUFBVTtJQUNWLFlBQVk7SUFDWixxQkFBcUI7SUFDckIsNEJBQTRCO1FBQ3hCO0FBQ0o7T0FDRyxhQUFhO09BQ2IsbUJBQW1CO09BQ25CLDZCQUE2QjtPQUM3Qix1QkFBdUI7T0FDdkIsV0FBVzs7WUFFTjtBQUNSO1FBQ0ksWUFBWTtRQUNaLGFBQWE7SUFDakI7QUFHSiw4RUFBOEU7QUFDOUU7SUFDSSxhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLHVCQUF1QjtJQUN2QixtQkFBbUI7SUFDbkIsV0FBVztJQUNYLFlBQVk7QUFDaEI7QUFDQTtJQUNJLGFBQWE7SUFDYixxQkFBcUI7SUFDckIseUJBQXlCO0lBQ3pCLFFBQVE7SUFDUixVQUFVO0lBQ1YsV0FBVztBQUNmO0FBRUE7SUFDSSxRQUFRO0lBQ1IsYUFBYTtJQUNiLHNCQUFzQjtJQUN0QiwwQkFBMEI7SUFDMUIsbUJBQW1CO0lBQ25CLFNBQVM7SUFDVCxXQUFXO0lBQ1g7QUFFSjtJQUNJLFdBQVc7UUFDUDtBQUNSO0lBQ0kscUJBQXFCO0lBQ3JCLGVBQWU7QUFDbkI7QUFFQTtJQUNJLFFBQVE7QUFDWjtBQUNBO0lBQ0ksa0JBQWtCO0FBQ3RCO0FBQ0E7SUFDSSxhQUFhO0lBQ2IsbUJBQW1CO0lBQ25CLHVCQUF1QjtHQUN4QjtBQUNIO0NBQ0MsV0FBVztBQUNaO0FBR0EsOEVBQThFO0FBQzlFO0lBQ0ksYUFBYTtJQUNiLG1CQUFtQjtJQUNuQiw4QkFBOEI7SUFDOUIsV0FBVztJQUNYLFlBQVk7QUFDaEI7QUFDQTtJQUNJLFFBQVE7SUFDUixhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLHVCQUF1QjtJQUN2QixrQkFBa0I7SUFDbEIsU0FBUztJQUNULFlBQVk7QUFDaEI7QUFDQTtJQUNJLFFBQVE7SUFDUixXQUFXO0lBQ1gsVUFBVTtRQUNOO0FBQ1I7SUFDSSxRQUFRO0lBQ1IsZUFBZTtJQUNmLGtCQUFrQjtJQUNsQixtQkFBbUI7UUFDZjtBQUVSO0lBQ0ksUUFBUTtJQUNSLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsdUJBQXVCO0lBQ3ZCLHVCQUF1QjtJQUN2QixTQUFTO0lBQ1QsWUFBWTtJQUNaO0FBQ0E7QUFDSixlQUFlO0lBQ1g7QUFDQTtJQUNBLFVBQVU7SUFDVixZQUFZO0lBQ1osNEJBQTRCO1FBQ3hCO0FBRUo7UUFDSSxZQUFZO1FBQ1osYUFBYTtRQUNiLG1CQUFtQjtRQUNuQiw4QkFBOEI7UUFDOUIsdUJBQXVCO1FBQ3ZCLFVBQVU7SUFDZDtBQUNBO1FBQ0ksZ0JBQWdCO1FBQ2hCLGFBQWE7UUFDYixtQkFBbUI7UUFDbkIsNkJBQTZCO1FBQzdCLFdBQVc7SUFDZjtBQUNBO1FBQ0ksZ0JBQWdCO1FBQ2hCLGFBQWE7UUFDYixtQkFBbUI7UUFDbkIsNkJBQTZCO1FBQzdCLFVBQVU7SUFDZDtBQUdBO1FBQ0ksVUFBVTtJQUNkO0FBQ0E7UUFDSSxhQUFhO1FBQ2IsbUJBQW1CO1FBQ25CLFNBQVM7SUFDYjtBQUVILDhFQUE4RTtBQUM5RTtJQUNHLGFBQWE7SUFDYixtQkFBbUI7SUFDbkIsOEJBQThCO0lBQzlCLFdBQVc7SUFDWCxZQUFZO0FBQ2hCO0FBQ0E7SUFDSSxRQUFRO0lBQ1IsYUFBYTtJQUNiLHNCQUFzQjtJQUN0Qix1QkFBdUI7SUFDdkIsa0JBQWtCO0lBQ2xCLFNBQVM7SUFDVCxZQUFZO0FBQ2hCO0FBS0E7SUFDSSxRQUFRO0lBQ1IsYUFBYTtJQUNiLHNCQUFzQjtJQUN0QiwyQkFBMkI7SUFDM0IsU0FBUztJQUNULFlBQVk7O0lBRVo7QUFDQTtRQUNJLGFBQWE7UUFDYixlQUFlO1FBQ2YsMkJBQTJCO1FBQzNCLHFCQUFxQjtJQUN6QjtBQUNBO1FBQ0ksbUJBQW1CO1FBQ25CLGlCQUFpQjtRQUNqQixVQUFVO1FBQ1YsbUJBQW1CO1FBQ25CLG9DQUFvQztRQUNwQyxXQUFXO01BQ2I7QUFDQTtRQUNFLGtCQUFrQjtRQUNsQixZQUFZO01BQ2Q7QUFFQTs7QUFFTixnQ0FBZ0M7QUFDaEMsa0JBQWtCO0FBQ2xCLHlCQUF5QjtBQUN6QixpQkFBaUI7QUFDakIsZ0JBQWdCO0FBQ2hCLG9DQUFvQztBQUNwQyxVQUFVO0FBQ1Y7QUFFRTtJQUNFLFlBQVk7SUFDWixZQUFZO0lBQ1oseUJBQXlCO0lBQ3pCLGlCQUFpQjtJQUNqQixvQ0FBb0M7SUFDcEMsMkNBQTJDO0lBQzNDLGFBQWE7SUFDYixtQkFBbUI7SUFDbkIsNkJBQTZCO0lBQzdCLG1CQUFtQjtJQUNuQixXQUFXO0VBQ2I7QUFHQTtJQUNFLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsdUJBQXVCO0lBQ3ZCLG1CQUFtQjtJQUNuQixVQUFVO0VBQ1o7QUFFQTtJQUNFLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsdUJBQXVCO0lBQ3ZCLG1CQUFtQjtJQUNuQixZQUFZO0lBQ1oscUJBQXFCO0lBQ3JCO0FBRUE7UUFDSSxhQUFhO1FBQ2IsbUJBQW1CO1FBQ25CLFlBQVk7UUFDWjtBQUVOO0lBQ0UsZUFBZTtJQUNmLHNCQUFzQjtJQUN0QixVQUFVO0VBQ1o7QUFDQTtJQUNFLFdBQVc7RUFDYjtBQUNBO0lBQ0Usa0NBQWtDO0VBQ3BDO0FBQ0E7SUFDRSxZQUFZO0lBQ1o7QUFDQTtRQUNJLFlBQVk7SUFDaEI7QUFDQTtRQUNJLFlBQVk7SUFDaEI7QUFHRTtVQUNJLFdBQVc7VUFDWCxhQUFhO1VBQ2Isa0JBQWtCO01BQ3RCO0FBTU4sMEdBQTBHO0FBQzFHOztJQUVJO1FBQ0ksV0FBVztRQUNYLFlBQVk7UUFDWixhQUFhO1FBQ2IsbUJBQW1CO1FBQ25CLDhCQUE4QjtJQUNsQztJQUNBO1FBQ0ksc0JBQXNCO0lBQzFCO0lBQ0E7UUFDSSx1QkFBdUI7UUFDdkIsbUJBQW1CO1FBQ25CLFdBQVc7SUFDZjtJQUNBO1FBQ0ksV0FBVztJQUNmO0lBQ0E7UUFDSSxrQkFBa0I7UUFDbEIsY0FBYztJQUNsQjtLQUNDO01BQ0MsVUFBVTtNQUNWLGlCQUFpQjtLQUNsQjtJQUNEO0dBQ0Qsa0JBQWtCO0lBQ2pCOztJQUVBLDZFQUE2RTtBQUNqRjtJQUNJLHNCQUFzQjtJQUN0QiwyQkFBMkI7SUFDM0Isb0JBQW9CO0lBQ3BCLFlBQVk7QUFDaEI7QUFDQTtJQUNJLFFBQVE7SUFDUixVQUFVO0lBQ1YsWUFBWTtFQUNkOztBQUVGO0dBQ0csVUFBVTtHQUNWOztBQUVIO0lBQ0ksZUFBZTtHQUNoQjs7O0FBR0gsNkVBQTZFO0FBQzdFO0lBQ0ksYUFBYTtJQUNiLHNCQUFzQjtJQUN0Qiw4QkFBOEI7SUFDOUIsbUJBQW1CO0lBQ25CLFdBQVc7SUFDWCxZQUFZO0FBQ2hCO0FBQ0E7RUFDRSxRQUFRO0VBQ1IsVUFBVTtFQUNWLFlBQVk7QUFDZDs7QUFFQTtBQUNBLFdBQVc7QUFDWDtBQUNBO0lBQ0ksZUFBZTtJQUNmOztFQUVGLDZFQUE2RTtFQUM3RTtJQUNFLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsNkJBQTZCO0lBQzdCLFdBQVc7SUFDWCxZQUFZO0FBQ2hCO0FBQ0E7RUFDRSxRQUFRO0VBQ1IsV0FBVztFQUNYLFlBQVk7RUFDWixhQUFhO0VBQ2Isc0JBQXNCO0VBQ3RCLHdCQUF3QjtBQUMxQjtBQUNBO0lBQ0ksUUFBUTtJQUNSLGFBQWE7R0FDZCxzQkFBc0I7R0FDdEIsNkJBQTZCO0dBQzdCLG1CQUFtQjtHQUNuQixVQUFVO0dBQ1YsWUFBWTtHQUNaO0dBQ0E7SUFDQyxlQUFlO0lBQ2Ysa0JBQWtCO0dBQ25COztFQUVELDZFQUE2RTtFQUM3RTtJQUNFLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsOEJBQThCO0lBQzlCLFdBQVc7SUFDWCxZQUFZO0FBQ2hCO0FBQ0E7SUFDSSxRQUFRO0lBQ1IsV0FBVztJQUNYLFlBQVk7SUFDWixhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLHlCQUF5QjtBQUM3QjtBQUNBO0lBQ0ksUUFBUTtJQUNSLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsNkJBQTZCO0lBQzdCLG1CQUFtQjtJQUNuQixVQUFVO0lBQ1YsWUFBWTtJQUNaO0lBQ0E7SUFDQSxlQUFlO0lBQ2Ysa0JBQWtCO0lBQ2xCOzs7QUFHSiw2RUFBNkU7QUFDN0U7SUFDSSxhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLDZCQUE2QjtJQUM3QixtQkFBbUI7SUFDbkIsV0FBVztJQUNYLFlBQVk7QUFDaEI7QUFDQTtJQUNJLFFBQVE7SUFDUixVQUFVO0lBQ1YsWUFBWTtJQUNaLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsd0JBQXdCO0FBQzVCO0FBQ0E7SUFDSSxRQUFRO0lBQ1IsYUFBYTtJQUNiLHNCQUFzQjtJQUN0Qiw2QkFBNkI7SUFDN0IsbUJBQW1CO0lBQ25CLFVBQVU7SUFDVixZQUFZO0lBQ1o7SUFDQTtJQUNBLGVBQWU7SUFDZixrQkFBa0I7SUFDbEI7OztLQUdDLDZFQUE2RTtLQUM3RTtRQUNHLGFBQWE7UUFDYixzQkFBc0I7UUFDdEIsOEJBQThCO1FBQzlCLG1CQUFtQjtRQUNuQixXQUFXO1FBQ1gsWUFBWTs7SUFFaEI7SUFDQTtRQUNJLFFBQVE7UUFDUixXQUFXO1FBQ1gsWUFBWTtRQUNaLFVBQVU7O0lBRWQ7SUFDQTtRQUNJLFFBQVE7UUFDUixhQUFhO1FBQ2Isc0JBQXNCO1FBQ3RCLDhCQUE4QjtRQUM5QixtQkFBbUI7UUFDbkIsVUFBVTtRQUNWLFlBQVk7UUFDWjtRQUNBO1FBQ0EsZUFBZTtRQUNmLGtCQUFrQjtRQUNsQixrQkFBa0I7UUFDbEI7UUFDQTs7V0FFRyxhQUFhO1dBQ2Isc0JBQXNCO1dBQ3RCLHVCQUF1QjtZQUN0QixXQUFXO1lBQ1gsV0FBVztRQUNmO1FBQ0E7WUFDSSxhQUFhO1lBQ2Isc0JBQXNCO1lBQ3RCLHVCQUF1QjtZQUN2QixxQkFBcUI7WUFDckIsVUFBVTtTQUNiOztTQUVBO1lBQ0csYUFBYTtZQUNiLG1CQUFtQjtZQUNuQix1QkFBdUI7WUFDdkIsWUFBWTtZQUNaOztJQUVSO1FBQ0ksUUFBUTtRQUNSLFdBQVc7UUFDWCxjQUFjO1FBQ2QsZ0NBQWdDO1FBQ2hDLGtCQUFrQjtRQUNsQjs7Q0FFUCw2RUFBNkU7Q0FDN0U7SUFDRyxhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLHVCQUF1QjtJQUN2QixXQUFXO0lBQ1gsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksUUFBUTtJQUNSLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsdUJBQXVCO0lBQ3ZCLGtCQUFrQjtJQUNsQixVQUFVO0lBQ1YsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksUUFBUTtJQUNSLFdBQVc7SUFDWCxVQUFVO1FBQ047QUFDUjtJQUNJLFFBQVE7SUFDUixlQUFlO0lBQ2Ysa0JBQWtCO0lBQ2xCLG1CQUFtQjtRQUNmOzs7QUFHUjtJQUNJLFFBQVE7SUFDUixhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLDJCQUEyQjtJQUMzQixtQkFBbUI7SUFDbkIsVUFBVTtJQUNWLFlBQVk7SUFDWjs7O0FBR0osNkVBQTZFO0FBQzdFO0lBQ0ksYUFBYTtJQUNiLHNCQUFzQjtJQUN0Qiw4QkFBOEI7SUFDOUIsbUJBQW1CO0lBQ25CLFdBQVc7SUFDWCxZQUFZO0FBQ2hCO0FBQ0E7SUFDSSxRQUFRO0lBQ1IsYUFBYTtJQUNiLHNCQUFzQjtJQUN0Qiw4QkFBOEI7SUFDOUIsV0FBVztJQUNYLFlBQVk7QUFDaEI7QUFDQTtJQUNJLFFBQVE7SUFDUixhQUFhO0lBQ2IsbUJBQW1CO0lBQ25CLHVCQUF1QjtJQUN2QixxQkFBcUI7SUFDckIsV0FBVztJQUNYLFlBQVk7QUFDaEI7QUFDQTtJQUNJLFFBQVE7SUFDUixVQUFVO0lBQ1YsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksUUFBUTtJQUNSLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIseUJBQXlCO0lBQ3pCLG1CQUFtQjtJQUNuQixVQUFVO0lBQ1YsWUFBWTtJQUNaLGdCQUFnQjtJQUNoQjtJQUNBO0lBQ0EsZUFBZTtJQUNmLGtCQUFrQjtJQUNsQjtJQUNBO0lBQ0EsVUFBVTtJQUNWLFlBQVk7SUFDWiw0QkFBNEI7UUFDeEI7SUFDSjtJQUNBLFFBQVE7QUFDWjs7QUFFQTtJQUNJLHdCQUF3QjtJQUN4QixXQUFXO0lBQ1gsWUFBWTtJQUNaLDhCQUE4QjtJQUM5QixhQUFhO0lBQ2IsZ0NBQWdDO0lBQ2hDLGtCQUFrQjtFQUNwQjs7O0VBR0Esc0JBQXNCO0VBQ3RCO0lBQ0Usd0JBQXdCO0lBQ3hCLGdCQUFnQjtJQUNoQixXQUFXO0lBQ1gsWUFBWTtJQUNaLDRCQUE0QjtJQUM1QixlQUFlO0lBQ2Ysa0NBQWtDO0lBQ2xDLG1CQUFtQjtFQUNyQjs7RUFFQSxnQkFBZ0I7RUFDaEI7SUFDRSxXQUFXO0lBQ1gsWUFBWTtJQUNaLDhCQUE4QjtJQUM5QixlQUFlO0lBQ2YsZ0NBQWdDO0lBQ2hDLGtCQUFrQjtFQUNwQjtBQUNGO0lBQ0ksVUFBVTtJQUNWLFlBQVk7QUFDaEI7QUFDQTtDQUNDLGtCQUFrQjtBQUNuQjtBQUNBO0NBQ0Msa0JBQWtCO0FBQ25COztBQUVBO0NBQ0MsWUFBWTtBQUNiOzs7O0FBSUEsaUNBQWlDLDBCQUEwQjtBQUMzRDtBQUNBO0lBQ0kseUJBQXlCO0VBQzNCO0FBQ0Y7Q0FDQyxpQkFBaUI7Q0FDakIsYUFBYTtDQUNiLDhCQUE4QjtDQUM5QixZQUFZO0NBQ1osV0FBVztDQUNYLDJCQUEyQjtDQUMzQix5QkFBeUIsRUFBRSxXQUFXLEVBQ2QsWUFBWSxFQUNiLGVBQWU7Q0FDdEMsaUJBQWlCLEVBQUUsYUFBYTtDQUNoQyx5QkFBeUI7Q0FDekIsbUJBQW1CO0FBQ3BCO0FBQ0E7Q0FDQyxXQUFXO0NBQ1gsWUFBWTtDQUNaLGdCQUFnQjtDQUNoQixvQkFBb0I7Q0FDcEIsY0FBYztDQUNkLG1CQUFtQjtFQUNsQixTQUFTO0VBQ1QsaUJBQWlCO0FBQ25COzs7QUFHQSw2RUFBNkU7QUFDN0U7SUFDSSxhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLDhCQUE4QjtJQUM5QixXQUFXO0lBQ1gsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksUUFBUTtJQUNSLGFBQWE7SUFDYixxQkFBcUI7SUFDckIsV0FBVztJQUNYLFlBQVk7QUFDaEI7QUFDQTtJQUNJLFFBQVE7SUFDUixhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLDZCQUE2QjtJQUM3QixtQkFBbUI7SUFDbkIsVUFBVTtJQUNWLFlBQVk7SUFDWjtJQUNBO0lBQ0EsZUFBZTtJQUNmLGtCQUFrQjtJQUNsQjs7SUFFQSw0RUFBNEU7QUFDaEY7SUFDSSxhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLDhCQUE4QjtJQUM5QixXQUFXO0lBQ1gsYUFBYTtBQUNqQjtBQUNBO0lBQ0ksUUFBUTtJQUNSLGFBQWE7SUFDYixtQkFBbUI7SUFDbkIsc0JBQXNCO0lBQ3RCLHlCQUF5QjtJQUN6QixXQUFXO0lBQ1gsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksUUFBUTtJQUNSLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsNkJBQTZCO0lBQzdCLG1CQUFtQjtJQUNuQixVQUFVO0lBQ1YsWUFBWTtJQUNaO0lBQ0E7SUFDQSxlQUFlO0lBQ2Ysa0JBQWtCO0lBQ2xCO0lBQ0E7UUFDSSxnQkFBZ0I7UUFDaEIsYUFBYTtRQUNiLG1CQUFtQjtRQUNuQiw2QkFBNkI7UUFDN0IsV0FBVztJQUNmOzs7QUFHSiw2RUFBNkU7QUFDN0U7SUFDSSxhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLDhCQUE4QjtJQUM5QixXQUFXO0lBQ1gsYUFBYTtBQUNqQjtBQUNBO0lBQ0ksUUFBUTtJQUNSLGFBQWE7SUFDYixtQkFBbUI7SUFDbkIsV0FBVztJQUNYLFlBQVk7QUFDaEI7QUFDQTtJQUNJLFFBQVE7SUFDUixhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLHVCQUF1QjtJQUN2QixtQkFBbUI7SUFDbkIsVUFBVTtJQUNWLFlBQVk7SUFDWjtJQUNBO0lBQ0EsZUFBZTtJQUNmLGtCQUFrQjtJQUNsQjtJQUNBO1FBQ0ksYUFBYTtRQUNiLHNCQUFzQjtRQUN0Qix1QkFBdUI7UUFDdkIsV0FBVztRQUNYLGdCQUFnQjtJQUNwQjs7SUFFQTtRQUNJLGFBQWE7UUFDYixzQkFBc0I7UUFDdEIsNkJBQTZCO1FBQzdCLG1CQUFtQjtRQUNuQixnQkFBZ0I7S0FDbkI7Ozs7QUFJTCw4RUFBOEU7QUFDOUU7SUFDSSxhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLDhCQUE4QjtJQUM5QixXQUFXO0lBQ1gsYUFBYTtBQUNqQjtBQUNBO0lBQ0ksbUJBQW1CO0lBQ25CLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIseUJBQXlCO0lBQ3pCLFFBQVE7SUFDUixXQUFXO0lBQ1gsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksUUFBUTtJQUNSLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsNkJBQTZCO0lBQzdCLG1CQUFtQjtJQUNuQixVQUFVO0lBQ1YsWUFBWTtJQUNaO0lBQ0E7SUFDQSxlQUFlO0lBQ2Ysa0JBQWtCO0lBQ2xCOztFQUVGLDhFQUE4RTtFQUM5RTtJQUNFLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsc0JBQXNCO0lBQ3RCLFdBQVc7SUFDWCxhQUFhO0FBQ2pCO0FBQ0E7SUFDSSxRQUFRO0lBQ1IsYUFBYTtJQUNiLHNCQUFzQjtJQUN0Qiw2QkFBNkI7SUFDN0Isa0JBQWtCO0lBQ2xCLFVBQVU7SUFDVixZQUFZO0FBQ2hCO0FBQ0E7SUFDSSxRQUFRO0lBQ1IsZUFBZTtJQUNmLGtCQUFrQjtJQUNsQixtQkFBbUI7UUFDZjs7O0FBR1I7SUFDSSxRQUFRO0lBQ1IsYUFBYTtJQUNiLHNCQUFzQjtJQUN0Qix1QkFBdUI7SUFDdkIsbUJBQW1CO0lBQ25CLFVBQVU7SUFDVixZQUFZO0lBQ1o7OztBQUdKLDhFQUE4RTtBQUM5RTtBQUNBLGFBQWE7QUFDYixzQkFBc0I7QUFDdEIsdUJBQXVCO0FBQ3ZCLFdBQVc7QUFDWCxhQUFhO0FBQ2I7QUFDQTtBQUNBLFFBQVE7QUFDUixVQUFVO0FBQ1YsWUFBWTtBQUNaO0FBQ0E7QUFDQSxRQUFRO0FBQ1IsYUFBYTtBQUNiLHNCQUFzQjtBQUN0Qix1QkFBdUI7QUFDdkIsbUJBQW1CO0FBQ25CLFVBQVU7QUFDVixZQUFZO0FBQ1o7QUFDQTtBQUNBLGVBQWU7QUFDZixrQkFBa0I7QUFDbEI7O0FBRUEsNkVBQTZFO0FBQzdFO0FBQ0EsYUFBYTtBQUNiLHNCQUFzQjtBQUN0Qiw4QkFBOEI7QUFDOUIsV0FBVztBQUNYLGFBQWE7QUFDYjtBQUNBO0FBQ0EsUUFBUTtBQUNSLGFBQWE7QUFDYixtQkFBbUI7QUFDbkIseUJBQXlCO0FBQ3pCLFdBQVc7QUFDWCxZQUFZO0FBQ1o7QUFDQTtBQUNBLFFBQVE7QUFDUixhQUFhO0FBQ2Isc0JBQXNCO0FBQ3RCLDZCQUE2QjtBQUM3QixtQkFBbUI7QUFDbkIsVUFBVTtBQUNWLFlBQVk7QUFDWjtBQUNBO0FBQ0EsZUFBZTtBQUNmLGtCQUFrQjtBQUNsQjtBQUNBLDhFQUE4RTtBQUM5RTtJQUNJLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsOEJBQThCO0lBQzlCLFdBQVc7SUFDWCxZQUFZO0FBQ2hCO0FBQ0E7SUFDSSxRQUFRO0lBQ1IsYUFBYTtJQUNiLHNCQUFzQjtJQUN0Qix1QkFBdUI7SUFDdkIsa0JBQWtCO0lBQ2xCLFVBQVU7SUFDVixZQUFZO0FBQ2hCO0FBQ0E7SUFDSSxRQUFROztRQUVKO0FBQ1I7SUFDSSxRQUFRO0lBQ1IsZUFBZTtJQUNmLGtCQUFrQjtJQUNsQixtQkFBbUI7UUFDZjs7OztBQUlSO0lBQ0ksUUFBUTtJQUNSLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsMkJBQTJCO0lBQzNCLGtCQUFrQjtJQUNsQixVQUFVO0lBQ1YsWUFBWTtJQUNaOzs7O0FBSUosNkVBQTZFO0FBQzdFO0FBQ0EsYUFBYTtBQUNiLHNCQUFzQjtBQUN0Qiw4QkFBOEI7QUFDOUIsV0FBVztBQUNYLGFBQWE7QUFDYjtBQUNBO0FBQ0EsUUFBUTtBQUNSLGFBQWE7QUFDYixtQkFBbUI7QUFDbkIseUJBQXlCO0FBQ3pCLFdBQVc7QUFDWCxZQUFZO0FBQ1o7QUFDQTtBQUNBLFFBQVE7QUFDUixhQUFhO0FBQ2Isc0JBQXNCO0FBQ3RCLDJCQUEyQjtBQUMzQixtQkFBbUI7QUFDbkIsVUFBVTtBQUNWLFlBQVk7QUFDWjtBQUNBO0FBQ0EsZUFBZTtBQUNmLGtCQUFrQjtBQUNsQjs7QUFFQSw4RUFBOEU7QUFDOUU7SUFDSSxhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLDhCQUE4QjtJQUM5QixXQUFXO0lBQ1gsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksUUFBUTtJQUNSLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsdUJBQXVCO0lBQ3ZCLGtCQUFrQjtJQUNsQixVQUFVO0lBQ1YsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksUUFBUTtJQUNSLGVBQWU7SUFDZixrQkFBa0I7SUFDbEIsbUJBQW1CO1FBQ2Y7O0FBRVI7SUFDSSxRQUFRO0lBQ1IsYUFBYTtJQUNiLHNCQUFzQjtJQUN0QiwyQkFBMkI7SUFDM0IsbUJBQW1CO0lBQ25CLFVBQVU7SUFDVixZQUFZO0lBQ1o7O09BRUcsOEVBQThFO09BQzlFO1FBQ0MsYUFBYTtRQUNiLHNCQUFzQjtRQUN0Qix1QkFBdUI7UUFDdkIsV0FBVztRQUNYLFlBQVk7SUFDaEI7SUFDQTtRQUNJLFFBQVE7UUFDUixhQUFhO1FBQ2Isc0JBQXNCO1FBQ3RCLHVCQUF1QjtRQUN2QixrQkFBa0I7UUFDbEIsVUFBVTtRQUNWLFlBQVk7SUFDaEI7SUFDQTtRQUNJLFFBQVE7UUFDUixXQUFXO1FBQ1gsVUFBVTtZQUNOO0lBQ1I7UUFDSSxRQUFRO1FBQ1IsZUFBZTtRQUNmLGtCQUFrQjtRQUNsQixtQkFBbUI7WUFDZjs7SUFFUjtRQUNJLFFBQVE7UUFDUixhQUFhO1FBQ2Isc0JBQXNCO1FBQ3RCLDRCQUE0QjtRQUM1QixtQkFBbUI7UUFDbkIsVUFBVTtRQUNWLFlBQVk7UUFDWjs7O0FBR1IsOEVBQThFO0FBQzlFO0lBQ0ksYUFBYTtJQUNiLHNCQUFzQjtJQUN0Qix1QkFBdUI7SUFDdkIsV0FBVztJQUNYLFlBQVk7QUFDaEI7QUFDQTtJQUNJLGlCQUFpQjtJQUNqQixhQUFhO0lBQ2IsbUJBQW1CO0lBQ25CLHVCQUF1QjtJQUN2QixRQUFRO0lBQ1IsVUFBVTtJQUNWLFlBQVk7QUFDaEI7QUFDQTtJQUNJLFFBQVE7SUFDUixhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLDZCQUE2QjtJQUM3QixtQkFBbUI7SUFDbkIsVUFBVTtJQUNWLFlBQVk7SUFDWjtJQUNBO0lBQ0EsZUFBZTtJQUNmLGtCQUFrQjtJQUNsQjs7O0FBR0osOEVBQThFO0FBQzlFO0FBQ0EsYUFBYTtBQUNiLHNCQUFzQjtBQUN0Qix1QkFBdUI7QUFDdkIsV0FBVztBQUNYLFlBQVk7QUFDWjtBQUNBO0FBQ0EsYUFBYTtBQUNiLG1CQUFtQjtBQUNuQix1QkFBdUI7QUFDdkIsUUFBUTtBQUNSLFdBQVc7QUFDWCxXQUFXO0FBQ1g7QUFDQTtBQUNBLFdBQVc7QUFDWCxVQUFVO0FBQ1YsOERBQThEO0FBQzlEO0FBQ0E7QUFDQSxRQUFRO0FBQ1IsYUFBYTtBQUNiLHNCQUFzQjtBQUN0QiwyQkFBMkI7QUFDM0IsbUJBQW1CO0FBQ25CLFVBQVU7QUFDVixZQUFZO0FBQ1o7QUFDQTtBQUNBLGVBQWU7QUFDZixrQkFBa0I7QUFDbEI7QUFDQTtJQUNJLGVBQWU7SUFDZixhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLHVCQUF1QjtJQUN2QixVQUFVO0FBQ2Q7QUFDQTtJQUNJLGVBQWU7SUFDZixhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLHVCQUF1QjtJQUN2QixVQUFVO0lBQ1Y7O0FBRUo7SUFDSSxhQUFhO0lBQ2IsdUJBQXVCO0lBQ3ZCLHNCQUFzQjtJQUN0QixnQkFBZ0I7QUFDcEI7O0FBRUEsOEVBQThFO0FBQzlFO0FBQ0EsYUFBYTtBQUNiLHNCQUFzQjtBQUN0Qix1QkFBdUI7QUFDdkIsbUJBQW1CO0FBQ25CLFdBQVc7QUFDWCxZQUFZO0FBQ1o7QUFDQTtBQUNBLFlBQVk7QUFDWixzQkFBc0I7QUFDdEIsbUJBQW1CO0FBQ25CLHVCQUF1QjtBQUN2QixRQUFRO0FBQ1IsV0FBVztBQUNYLFdBQVc7QUFDWDtBQUNBO0FBQ0EsUUFBUTtBQUNSLGFBQWE7QUFDYixzQkFBc0I7QUFDdEIsc0JBQXNCO0FBQ3RCLG1CQUFtQjtBQUNuQixTQUFTO0FBQ1QsV0FBVztBQUNYOztBQUVBO0lBQ0ksbUJBQW1CO0dBQ3BCLFlBQVk7R0FDWixlQUFlO0dBQ2Ysa0JBQWtCO0lBQ2pCO0lBQ0E7UUFDSSxtQkFBbUI7UUFDbkIsZUFBZTtRQUNmLFlBQVk7WUFDUjtBQUNaO0lBQ0ksbUJBQW1CO0lBQ25CLHFCQUFxQjtJQUNyQixlQUFlO0lBQ2Ysa0JBQWtCO1FBQ2Q7QUFDUjtJQUNJLFVBQVU7UUFDTjtBQUNSO0lBQ0ksbUJBQW1CO0lBQ25CLFlBQVk7SUFDWixVQUFVO0lBQ1YsZUFBZTtJQUNmLGtCQUFrQjtRQUNkO0FBQ1I7SUFDSSxXQUFXO1FBQ1A7O0FBRVI7SUFDSSxRQUFRO0lBQ1IsYUFBYTtJQUNiLG1CQUFtQjtJQUNuQix5QkFBeUI7SUFDekIscUJBQXFCO0lBQ3JCLFNBQVM7SUFDVCxXQUFXO0FBQ2Y7QUFDQTtJQUNJLFlBQVk7SUFDWixXQUFXO0FBQ2Y7Ozs7QUFJQSw4RUFBOEU7QUFDOUU7QUFDQSxhQUFhO0FBQ2Isc0JBQXNCO0FBQ3RCLHVCQUF1QjtBQUN2QixXQUFXO0FBQ1gsWUFBWTtBQUNaO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakIsUUFBUTtBQUNSLGFBQWE7QUFDYixvQkFBb0I7QUFDcEIsVUFBVTtBQUNWLFlBQVk7QUFDWjtBQUNBO0FBQ0EsUUFBUTtBQUNSLGFBQWE7QUFDYixzQkFBc0I7QUFDdEIseUJBQXlCO0FBQ3pCLG1CQUFtQjtBQUNuQixVQUFVO0FBQ1YsWUFBWTtBQUNaLGdCQUFnQjtBQUNoQjtBQUNBO0FBQ0EsZUFBZTtBQUNmLGtCQUFrQjtBQUNsQjtBQUNBO0FBQ0EsVUFBVTtBQUNWLFlBQVk7QUFDWixxQkFBcUI7QUFDckIsNEJBQTRCO0lBQ3hCO0FBQ0o7R0FDRyxhQUFhO0dBQ2IsbUJBQW1CO0dBQ25CLDZCQUE2QjtHQUM3Qix1QkFBdUI7R0FDdkIsV0FBVztHQUNYLGdCQUFnQjs7UUFFWDtBQUNSO0lBQ0ksWUFBWTtJQUNaLGFBQWE7QUFDakI7OztBQUdBLDhFQUE4RTtBQUM5RTtBQUNBLGFBQWE7QUFDYixzQkFBc0I7QUFDdEIsdUJBQXVCO0FBQ3ZCLG1CQUFtQjtBQUNuQixXQUFXO0FBQ1gsWUFBWTtBQUNaO0FBQ0E7QUFDQSxhQUFhO0FBQ2IscUJBQXFCO0FBQ3JCLHlCQUF5QjtBQUN6QixRQUFRO0FBQ1IsVUFBVTtBQUNWLFdBQVc7QUFDWDs7QUFFQTtBQUNBLFFBQVE7QUFDUixhQUFhO0FBQ2Isc0JBQXNCO0FBQ3RCLDBCQUEwQjtBQUMxQixtQkFBbUI7QUFDbkIsVUFBVTtBQUNWLFdBQVc7QUFDWDs7QUFFQTtBQUNBLFdBQVc7QUFDWCxtQkFBbUI7SUFDZjtBQUNKO0lBQ0ksa0JBQWtCO0FBQ3RCLHFCQUFxQjtBQUNyQixlQUFlO0FBQ2YsbUJBQW1CO0FBQ25COztBQUVBO0FBQ0EsUUFBUTtBQUNSO0FBQ0E7QUFDQSxrQkFBa0I7QUFDbEIsbUJBQW1CO0FBQ25CO0FBQ0E7QUFDQSxhQUFhO0FBQ2IsbUJBQW1CO0FBQ25CLHVCQUF1QjtBQUN2QjtBQUNBO0FBQ0EsV0FBVztBQUNYOzs7QUFHQSw4RUFBOEU7QUFDOUU7SUFDSSxhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLDhCQUE4QjtJQUM5QixtQkFBbUI7SUFDbkIsV0FBVztJQUNYLFlBQVk7QUFDaEI7QUFDQTtJQUNJLFFBQVE7SUFDUixhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLHVCQUF1QjtJQUN2QixrQkFBa0I7SUFDbEIsU0FBUztJQUNULFlBQVk7QUFDaEI7QUFDQTtJQUNJLFFBQVE7SUFDUixXQUFXO0lBQ1gsVUFBVTtRQUNOO0FBQ1I7SUFDSSxRQUFRO0lBQ1IsZUFBZTtJQUNmLGtCQUFrQjtJQUNsQixtQkFBbUI7UUFDZjs7QUFFUjtJQUNJLFFBQVE7SUFDUixhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLHVCQUF1QjtJQUN2Qix1QkFBdUI7SUFDdkIsVUFBVTtJQUNWLFlBQVk7SUFDWjtJQUNBO0tBQ0MsZUFBZTtJQUNoQjtJQUNBO0lBQ0EsVUFBVTtJQUNWLFlBQVk7SUFDWiw0QkFBNEI7SUFDNUIsZ0JBQWdCO1FBQ1o7O0lBRUo7UUFDSSxZQUFZO1FBQ1osYUFBYTtRQUNiLHNCQUFzQjtRQUN0Qiw2QkFBNkI7UUFDN0IsdUJBQXVCO1FBQ3ZCLFVBQVU7SUFDZDtJQUNBO1FBQ0ksZ0JBQWdCO1FBQ2hCLGFBQWE7UUFDYixtQkFBbUI7UUFDbkIsNkJBQTZCO1FBQzdCLFdBQVc7SUFDZjtJQUNBO1FBQ0ksZ0JBQWdCO1FBQ2hCLGFBQWE7UUFDYixtQkFBbUI7UUFDbkIsNkJBQTZCO1FBQzdCLFVBQVU7SUFDZDs7O0lBR0E7UUFDSSxVQUFVO0lBQ2Q7SUFDQTtRQUNJLGFBQWE7UUFDYixtQkFBbUI7UUFDbkIsU0FBUztJQUNiOzs7SUFHQSIsImZpbGUiOiJzcmMvYXBwL3Rlc3QvdGVzdC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNvbnRhaW5lcntcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIG1heC13aWR0aDogMTAwJTtcbiAgICBtYXJnaW46IDA7XG4gICAgcGFkZGluZzogMDtcbn1cbi5ib2R5e1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogMTAwJTtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOmNvbHVtbjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbn1cbi5mb290ZXJ7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgbWF4LWhlaWdodDogMTUwcHg7XG4gICAgbWluLWhlaWdodDogMTUwcHg7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIGJhY2tncm91bmQtY29sb3I6ICAjMTExRDVFZmY7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xufVxuLmxlZnR7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICB3aWR0aDogMTAlO1xuICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xufVxuLmxlZnQgaW1ne1xuICAgIHdpZHRoOiA1MCU7XG59XG4ucG9pbnR7XG4gICAgZm9udC1zaXplOiAxMHB4O1xuICAgIGNvbG9yOnJnYigxNTMsIDE1MywgMTUzKTtcbiAgICBtYXJnaW4tbGVmdDogMTBweDtcbiAgICBcbn1cbml7XG4gICAgY29sb3I6cmdiKDE1MywgMTUzLCAxNTMpO1xufVxuaTpob3ZlcntcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgY3Vyc29yOiBwb2ludGVyO1xufVxuLmljb25OZXh7XG4gICAgZm9udC1zaXplOiAyNXB4O1xuICAgIG1hcmdpbi10b3A6IDhweDtcbiAgICBtYXJnaW4tbGVmdDogMTBweDtcbn1cbi5jaGVja0ljb257XG4gICAgY29sb3I6IHdoaXRlO1xufVxuXG4uY2VudGVye1xuIHdpZHRoOiAxMDAlO1xuIGhlaWdodDogMTAwJTtcbiBkaXNwbGF5OiBmbGV4O1xuIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4ganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cblxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqYnRuKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbi5idXR0dG9uUkVke1xuICAgIGJhY2tncm91bmQtY29sb3I6IHJnYigyMDYsIDAsIDApO1xuICAgIGNvbG9yOiB3aGl0ZTtcbn1cbiBidXR0b246aG92ZXJ7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogcmdiKDE0NSwgMTM2LCAxMzYpO1xuICAgIGNvbG9yOiB3aGl0ZTtcbn1cbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSAxICoqKioqKioqKioqKioqKioqKioqKioqL1xuLnNsaWRlMXtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogMTAwJTtcbn1cbi5zbGlkZTEgLmltYWdle1xuICBvcmRlcjogMjtcbiAgd2lkdGg6IDcwJTtcbiAgaGVpZ2h0OiAxMDAlO1xufVxuXG4uc2xpZGUxIC50ZXN0e1xuIG9yZGVyOiAxO1xuIGRpc3BsYXk6IGZsZXg7XG4gZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgYWxpZ24taXRlbXM6Y2VudGVyO1xud2lkdGg6IDQwJTtcbmhlaWdodDogMTAwJTtcbn1cblxuICAgLnNsaWRlMSAudGVzdCBpbnB1dHtcbiAgICB3aWR0aDogODAlO1xuICAgIGhlaWdodDogNDBweDtcbiAgICBib3JkZXItY29sb3I6IHJnYigyMTIsIDUsIDUpO1xuICAgICAgIH1cblxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnNsaWRlIDAgKioqKioqKioqKioqKioqKioqKioqKiovXG4uc2xpZGUwe1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgaGVpZ2h0OiAxMDAlO1xufVxuLnNsaWRlMCAuaW1hZ2V7XG4gIG9yZGVyOiAyO1xuICB3aWR0aDogNTIlO1xuICBoZWlnaHQ6IDEwMCU7XG59XG4uc2xpZGUwIC50ZXN0e1xuICAgIG9yZGVyOiAxO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgd2lkdGg6NDAlO1xuICAgaGVpZ2h0OiAxMDAlO1xuICAgfVxuICAgLnNsaWRlMCAudGVzdCBoMXtcbmZvbnQtc2l6ZTogNTBweDtcbiAgIH1cbiAgIC5zbGlkZTAgLnRlc3QgYnV0dG9ue1xuICAgIHdpZHRoOiAxNTBweDtcbiAgICBoZWlnaHQ6IDUwcHg7XG4gICAgYm9yZGVyLXJhZGl1czogMzBweDtcbiAgICBib3JkZXItY29sb3I6IHJnYigxOTIsIDMsIDMpO1xuICAgICAgIH1cbiAgIFxuICAgICAgIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSAyICoqKioqKioqKioqKioqKioqKioqKioqL1xuICAgICAgIC5zbGlkZTJ7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIGhlaWdodDogMTAwJTtcbiAgICB9XG4gICAgLnNsaWRlMiAuaW1hZ2V7XG4gICAgICBvcmRlcjogMTtcbiAgICAgIHdpZHRoOiA1NSU7XG4gICAgICBoZWlnaHQ6IDEwMCU7XG4gICAgfVxuICAgIC5zbGlkZTIgLnRlc3R7XG4gICAgICAgIG9yZGVyOiAyO1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XG4gICAgICAgd2lkdGg6NDAlO1xuICAgICAgIGhlaWdodDogMTAwJTtcbiAgICAgICB9XG4gICAgICAgLnNsaWRlMiAudGVzdCBoMXtcbiAgICBmb250LXNpemU6IDUwcHg7XG4gICAgICAgfVxuICAgICAgIFxuICAgICAgIFxuICAgICAgICAgICAuc2xpZGUyIC50ZXN0IC5uZy1zZWxlY3Qge1xuICAgICAgICAgICAgYm9yZGVyOjBweDtcbiAgICAgICAgICAgIG1pbi1oZWlnaHQ6IDBweDtcbiAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDA7XG4gICAgICAgICAgICB3aWR0aDogNDgwcHg7XG4gICAgICAgICAgICBoZWlnaHQ6IDQwcHg7XG4gICAgICAgICAgICBib3JkZXItY29sb3I6IHJnYigyMTIsIDUsIDUpO1xuICAgICAgICB9XG4gICAgICAgIC5zbGlkZTIgLnRlc3QgIC5uZy1zZWxlY3QgLm5nLXNlbGVjdC1jb250YWluZXIgIHsgICAgICAgICAgICBcbiAgICAgICAgICAgIG1pbi1oZWlnaHQ6IDBweDtcbiAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDA7XG4gICAgICAgICAgICB3aWR0aDogNDgwcHg7XG4gICAgICAgICAgICBoZWlnaHQ6IDQwcHg7XG4gICAgICAgICAgICBib3JkZXItY29sb3I6IHJnYigyMTIsIDUsIDUpO1xuICAgICAgICB9XG4gICAgICAgIC5zbGlkZTIgLnRlc3QgIC5uZy1zZWxlY3QgOjpuZy1kZWVwIC5uZy1zZWxlY3QtY29udGFpbmVyICB7ICAgICAgICAgICAgXG4gICAgICAgICAgICBtaW4taGVpZ2h0OiAwcHg7XG4gICAgICAgICAgICBib3JkZXItcmFkaXVzOiAwO1xuICAgICAgICAgICAgd2lkdGg6IDQ4MHB4O1xuICAgICAgICAgICAgaGVpZ2h0OiA0MHB4O1xuICAgICAgICAgICAgYm9yZGVyLWNvbG9yOiByZ2IoMjEyLCA1LCA1KTtcbiAgICAgICAgfVxuICAgICAgICAuc2xpZGUyIC50ZXN0IG5nLXNlbGVjdC5uZy1pbnZhbGlkLm5nLXRvdWNoZWQgLm5nLXNlbGVjdC1jb250YWluZXIge1xuICAgICAgICAgICAgYm9yZGVyLWNvbG9yOiAjZGMzNTQ1O1xuICAgICAgICAgICAgYm94LXNoYWRvdzogaW5zZXQgMCAxcHggMXB4IHJnYmEoMCwgMCwgMCwgMC4wNzUpLCAwIDAgMCAzcHggI2ZkZTZlODtcbiAgICAgICAgICAgIHdpZHRoOiA0ODBweDtcbiAgICAgICAgICAgIGhlaWdodDogNDBweDtcbiAgICAgICAgICAgIGJvcmRlci1jb2xvcjogcmdiKDIxMiwgNSwgNSk7XG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSAzICoqKioqKioqKioqKioqKioqKioqKioqL1xuICAgICAgICAuc2xpZGUze1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICBoZWlnaHQ6IDEwMCU7XG4gICAgfVxuICAgIC5zbGlkZTMgLmltYWdle1xuICAgICAgICBvcmRlcjogMjtcbiAgICAgICAgd2lkdGg6IDU0JTtcbiAgICAgICAgaGVpZ2h0OiAxMDAlO1xuICAgIH1cbiAgICAuc2xpZGUzIC50ZXN0e1xuICAgICAgICBvcmRlcjogMTtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgIG1hcmdpbi10b3A6IDUwcHg7XG4gICAgICAgIHdpZHRoOjQwJTtcbiAgICAgICAgaGVpZ2h0OiA3MCU7XG4gICAgICAgIH1cbiAgICAgICAgLnNsaWRlMyAudGVzdCBoMXtcbiAgICAgICAgZm9udC1zaXplOiA1MHB4O1xuICAgICAgICB9XG4gICAgICAgIC5zbGlkZTMgLnRlc3QgLmJsb2NrQnRue1xuICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcblxuICAgICAgICB9XG4gICAgICAgIC5zbGlkZTMgLnRlc3QgLmJsb2NrQnRuIC5zb3VzQmxvY2t7XG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuIFxuICAgICAgICAgfVxuICAgICAgICAuc2xpZGUzIC50ZXN0IC5ibG9ja0J0biBidXR0b257XG4gICAgICAgIHdpZHRoOiAzMTBweDtcbiAgICAgICAgaGVpZ2h0OiA1MHB4O1xuICAgICAgICBtYXJnaW46IDVweDtcbiAgICAgICAgYm9yZGVyLWNvbG9yOiByZ2IoMTkyLCAzLCAzKTtcbiAgICAgICAgICAgIH0gIFxuICAgICAgICAgICAgLnNsaWRlMyAudGVzdCAuYmxvY2tCdG4gLnNvdXNCbG9jayBidXR0b257XG4gICAgICAgICAgICAgICAgd2lkdGg6IDE1MHB4O1xuICAgICAgICAgICAgICAgIG1hcmdpbjogNXB4O1xuICAgICAgICAgICAgICAgICAgICB9ICAgXG5cbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSA0ICoqKioqKioqKioqKioqKioqKioqKioqL1xuICAgIC5zbGlkZTR7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIGhlaWdodDogMTAwJTtcbiAgICB9XG4gICAgLnNsaWRlNCAuaW1hZ2V7XG4gICAgICAgIG1hcmdpbi1sZWZ0OiA1MHB4O1xuICAgICAgICBvcmRlcjogMTtcbiAgICAgICAgd2lkdGg6IDMzJTtcbiAgICAgICAgaGVpZ2h0OiAxMDAlO1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICB6LWluZGV4OiAxO1xuICAgICAgICBtYXJnaW4tdG9wOiA1NXB4O1xuICAgIH1cbiAgICAuc2xpZGU0IC50ZXN0e1xuICAgICAgICBvcmRlcjogMjtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xuICAgICAgICB3aWR0aDo0MCU7XG4gICAgICAgIGhlaWdodDogMTAwJTtcbiAgICAgICAgfVxuICAgICAgICAuc2xpZGU0IC50ZXN0IGgxe1xuICAgIGZvbnQtc2l6ZTogNTBweDtcbiAgICAgICAgfVxuICAgICAgICAuc2xpZGU0IC50ZXN0IGlucHV0e1xuICAgICAgICB3aWR0aDogODAlO1xuICAgICAgICBoZWlnaHQ6IDQwcHg7XG4gICAgICAgIGJvcmRlci1jb2xvcjogcmdiKDIxMiwgNSwgNSk7XG4gICAgICAgICAgICB9XG5cbiAgIFxuICAgICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqc2xpZGUgNSAqKioqKioqKioqKioqKioqKioqKioqKi9cbiAgICAgLnNsaWRlNXtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgaGVpZ2h0OiAxMDAlO1xuICAgIH1cbiAgICAuc2xpZGU1IC5pbWFnZXtcbiAgICAgICAgb3JkZXI6IDE7XG4gICAgICAgIHdpZHRoOiA1MCU7XG4gICAgICAgIGhlaWdodDogMTAwJTtcbiAgICAgICAgei1pbmRleDogMTtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgbWFyZ2luLXRvcDogNjRweDtcbiAgICB9XG5cbiAgICBcbiAgICAuc2xpZGU1IC50ZXN0e1xuICAgICAgICBvcmRlcjogMjtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgIHdpZHRoOjUwJTtcbiAgICAgICAgaGVpZ2h0OiAxMDAlO1xuICAgICAgICB9XG4gICAgICAgIC5zbGlkZTUgLnRlc3QgaDF7XG4gICAgICAgIGZvbnQtc2l6ZTogNTBweDtcbiAgICAgICAgfVxuICAgICAgICAuc2xpZGU1IC50ZXN0IC5ibG9ja0J0bntcbiAgICAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XG4gICAgICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgICAgIGhlaWdodDogMjAlO1xuICAgICAgICB9XG4gICAgICAgIC5zbGlkZTUgLnRlc3QgLmJsb2NrQnRuIC5zb3VzQmxvY2t7XG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7IFxuICAgICAgICAgfVxuICAgIFxuICAgICAgICAgLnNsaWRlNSAudGVzdCAuYmxvY2tCdG4gLnNvdXNCbG9jayAucXR5e1xuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjsgICAgICAgICAgICBcbiAgICAgICAgICAgIH1cbiAgICAgICBcbiAgICAuc2xpZGU1IC50ZXN0IC5ibG9ja0J0biAuc291c0Jsb2NrIC5xdHkgaW5wdXR7XG4gICAgICAgIG9yZGVyOiAyO1xuICAgICAgICB3aWR0aDogODBweDtcbiAgICAgICAgaGVpZ2h0OiA1MHB4O1xuICAgICAgICBib3JkZXI6IDRweCBzb2xpZCByZ2IoMjEyLCA1LCA1KTtcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICB9XG4gICAgLnNsaWRlNSAudGVzdCAuYmxvY2tCdG4gLnNvdXNCbG9jayAucXR5IHB7XG4gICAgICAgIG9yZGVyOiAxO1xuICAgICAgICBtYXJnaW46IDAgNXB4IDAgMDtcbiAgICAgICAgcGFkZGluZzogMDtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIC5zbGlkZTUgLnRlc3QgLmJsb2NrQnRuIC5zb3VzQmxvY2sgLnF0eSAuYmFpc3N7IGRpc3BsYXk6IG5vbmU7fVxuICAgICAgICAgICAgLnNsaWRlNSAudGVzdCAuYmxvY2tCdG4gLnNvdXNCbG9jayAucXR5IC5hdWdteyBkaXNwbGF5OiBub25lO31cbiAgICAgICAgICAgIC5zbGlkZTUgLnRlc3QgLmJsb2NrQnRuIC5zb3VzQmxvY2sgLnF0eSAgLmJhaXNzRHsgZGlzcGxheTogY29udGVudHM7fVxuICAgICAgICAgICAgLnNsaWRlNSAudGVzdCAuYmxvY2tCdG4gLnNvdXNCbG9jayAucXR5IC5hdWdtRHsgZGlzcGxheTogY29udGVudHM7fVxuICAgIC5zbGlkZTUgLnRlc3QgLmJsb2NrQnRuIC5zb3VzQmxvY2sgLnF0eSAucXR5QnRue1xuICAgICAgICBvcmRlcjogMztcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xuICAgICAgICB9XG4gICAgICAgIC5zbGlkZTUgLnRlc3QgLmJsb2NrQnRuIC5zb3VzQmxvY2sgLnF0eSAucXR5QnRuIGJ1dHRvbntcbiAgICAgICAgICAgIGJvcmRlcjogbm9uZTtcbiAgICAgICAgICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCB3aGl0ZTtcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDFweDtcbiAgICAgICAgICAgIHdpZHRoOiA1cHg7XG4gICAgICAgICAgICBib3JkZXItcmFkaXVzOiAwcHg7XG4gICAgICAgICAgICBoZWlnaHQ6IDI1cHg7XG4gICAgICAgICAgICBjb2xvcjogd2hpdGU7XG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoMjEyLCA1LCA1KTtcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgICAgICAgICAgZm9udC1zaXplOiAyMHB4O1xuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgXG4gICAgICAgICAgICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqc2xpZGUgNiAqKioqKioqKioqKioqKioqKioqKioqKi9cbiAgICAgICAgICAgIC5zbGlkZTZ7XG4gICAgICAgICAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgICAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgICAgICAgICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IDEwMCU7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAuc2xpZGU2IC5pbWFnZXtcbiAgICAgICAgICAgICAgICBvcmRlcjogMjtcbiAgICAgICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICAgICAgICAgICAgYWxpZ24taXRlbXM6Y2VudGVyO1xuICAgICAgICAgICAgICAgIHdpZHRoOjUzJTtcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IDEwMCU7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAuc2xpZGU2IC5pbWFnZSBpbWd7XG4gICAgICAgICAgICAgICAgb3JkZXI6IDI7XG4gICAgICAgICAgICAgICAgaGVpZ2h0OiAyNSU7XG4gICAgICAgICAgICAgICAgd2lkdGg6IDE1JTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgLnNsaWRlNiAuaW1hZ2UgaDF7XG4gICAgICAgICAgICAgICAgb3JkZXI6IDE7XG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiA1MHB4O1xuICAgICAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAyMHB4O1xuICAgICAgICAgICAgICAgICAgICB9XG5cblxuICAgICAgICAgICAgLnNsaWRlNiAudGVzdHtcbiAgICAgICAgICAgICAgICBvcmRlcjogMjtcbiAgICAgICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICAgICAgICAgICAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XG4gICAgICAgICAgICAgICAgd2lkdGg6NDAlO1xuICAgICAgICAgICAgICAgIGhlaWdodDogMTAwJTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgLnNsaWRlNiAudGVzdCBzZWxlY3R7XG4gICAgICAgICAgICAgICAgd2lkdGg6IDgwJTtcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IDQwcHg7XG4gICAgICAgICAgICAgICAgYm9yZGVyLWNvbG9yOiByZ2IoMjEyLCA1LCA1KTtcbiAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqc2xpZGUgNyAqKioqKioqKioqKioqKioqKioqKioqKi9cbi5zbGlkZTd7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogMTAwJTtcbn1cbi5zbGlkZTcgLmNvbHVtMXtcbiAgICBvcmRlcjogMTtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIG1hcmdpbi1ib3R0b206IDIwcHg7XG59XG4uc2xpZGU3IC5jb2x1bTJ7XG4gICAgb3JkZXI6IDI7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIG1hcmdpbi10b3A6IDIwcHg7XG4gICAgaGVpZ2h0OiAxMDAlO1xufVxuLnNsaWRlNyAuaW1hZ2V7XG4gICAgbWFyZ2luLWxlZnQ6IDUwcHg7XG4gICAgb3JkZXI6IDE7XG4gICAgd2lkdGg6IDM3JTtcbiAgICBoZWlnaHQ6IDEwMCU7XG59XG4uc2xpZGU3IC50ZXN0e1xuICAgIG9yZGVyOiAyO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcbiAgICB3aWR0aDo0MCU7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIH1cbiAgICAuc2xpZGU3IC50ZXN0IGgxe1xuZm9udC1zaXplOiA1MHB4O1xuICAgIH1cbiAgICAuc2xpZGU3IC50ZXN0IGlucHV0e1xuICAgIHdpZHRoOiA4MCU7XG4gICAgaGVpZ2h0OiA0MHB4O1xuICAgIGJvcmRlci1jb2xvcjogcmdiKDIxMiwgNSwgNSk7XG4gICAgICAgIH1cbi5zbGlkZTcgLnByb2dyZXNze1xuICAgIG9yZGVyOiAzO1xufVxuXG4uc2xpZGVyIHtcbiAgICAtd2Via2l0LWFwcGVhcmFuY2U6IG5vbmU7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgaGVpZ2h0OiAxNXB4O1xuICAgIGJhY2tncm91bmQ6IHJnYigyNTUsIDI1NSwgMjU1KTtcbiAgICBvdXRsaW5lOiBub25lO1xuICAgIGJvcmRlcjogNXB4IHNvbGlkIHJnYigxODksIDgsIDgpO1xuICAgIGJvcmRlci1yYWRpdXM6IDhweDtcbiAgfVxuICBcbiAgXG4gIC8qIGZvciBjaHJvbWUvc2FmYXJpICovXG4gIC5zbGlkZXI6Oi13ZWJraXQtc2xpZGVyLXRodW1iIHtcbiAgICAtd2Via2l0LWFwcGVhcmFuY2U6IG5vbmU7XG4gICAgYXBwZWFyYW5jZTogbm9uZTtcbiAgICB3aWR0aDogMjBweDtcbiAgICBoZWlnaHQ6IDYwcHg7XG4gICAgYmFja2dyb3VuZDogcmdiKDI0OCwgMjI0LCA1KTtcbiAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgYm9yZGVyOiA1cHggc29saWQgcmdiKDI0OCwgMjI0LCA1KTtcbiAgICBib3JkZXItcmFkaXVzOiA1MHB4O1xuICB9XG4gIFxuICAvKiBmb3IgZmlyZWZveCAqL1xuICAuc2xpZGVyOjotbW96LXJhbmdlLXRodW1iIHtcbiAgICB3aWR0aDogMjBweDtcbiAgICBoZWlnaHQ6IDYwcHg7XG4gICAgYmFja2dyb3VuZDogcmdiKDI1NSwgMjU1LCAyNTUpO1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICBib3JkZXI6IDVweCBzb2xpZCByZ2IoMTg5LCA4LCA4KTtcbiAgICBib3JkZXItcmFkaXVzOiA0cHg7XG4gIH1cbi5fX3Jhbmdle1xuICAgIHdpZHRoOiA4MCU7XG4gICAgaGVpZ2h0OiAxMDAlO1xufVxuLl9fcmFuZ2Utc3RlcHtcblx0cG9zaXRpb246IHJlbGF0aXZlOyAgICAgICAgICAgICAgICBcbn1cbi5fX3JhbmdlLXN0ZXB7XG5cdHBvc2l0aW9uOiByZWxhdGl2ZTsgICAgICAgICAgICAgICAgXG59XG5cbi5fX3JhbmdlLW1heHtcblx0ZmxvYXQ6IHJpZ2h0O1xufVxuICAgICAgICAgICBcblxuXG4uX19yYW5nZSBpbnB1dDo6cmFuZ2UtcHJvZ3Jlc3Mge1x0YmFja2dyb3VuZDogcmdiKDE4OSwgOCwgOCk7XG59XG4uc2xpZGVyIGlucHV0W3R5cGU9cmFuZ2VdOjotbW96LXJhbmdlLXByb2dyZXNzIHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjYzY1N2EwO1xuICB9XG4uX19yYW5nZS1zdGVwIGRhdGFsaXN0IHtcblx0cG9zaXRpb246cmVsYXRpdmU7XG5cdGRpc3BsYXk6IGZsZXg7XG5cdGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2Vlbjtcblx0aGVpZ2h0OiBhdXRvO1xuXHRib3R0b206IDEwcHg7XG5cdC8qIGRpc2FibGUgdGV4dCBzZWxlY3Rpb24gKi9cblx0LXdlYmtpdC11c2VyLXNlbGVjdDogbm9uZTsgLyogU2FmYXJpICovICAgICAgICBcblx0LW1vei11c2VyLXNlbGVjdDogbm9uZTsgLyogRmlyZWZveCAqL1xuXHQtbXMtdXNlci1zZWxlY3Q6IG5vbmU7IC8qIElFMTArL0VkZ2UgKi8gICAgICAgICAgICAgICAgXG5cdHVzZXItc2VsZWN0OiBub25lOyAvKiBTdGFuZGFyZCAqL1xuXHQvKiBkaXNhYmxlIGNsaWNrIGV2ZW50cyAqL1xuXHRwb2ludGVyLWV2ZW50czpub25lOyAgXG59XG4uX19yYW5nZS1zdGVwIGRhdGFsaXN0IG9wdGlvbiB7XG5cdHdpZHRoOiAxMHB4O1xuXHRoZWlnaHQ6IDEwcHg7XG5cdG1pbi1oZWlnaHQ6IDEwcHg7XG5cdGJvcmRlci1yYWRpdXM6IDEwMHB4O1xuXHQvKiBoaWRlIHRleHQgKi9cblx0d2hpdGUtc3BhY2U6IG5vd3JhcDsgICAgICAgXG4gIHBhZGRpbmc6MDtcbiAgbGluZS1oZWlnaHQ6IDQwcHg7XG59XG5cblxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnNsaWRlIDggKioqKioqKioqKioqKioqKioqKioqKiovXG4uc2xpZGU4e1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBoZWlnaHQ6IDEwMCU7XG59XG4uc2xpZGU4IC5pbWFnZXtcbiAgICBvcmRlcjogMjtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgd2lkdGg6IDM3JTtcbiAgICBoZWlnaHQ6IDEwMCU7XG59XG4uc2xpZGU4IC50ZXN0e1xuICAgIG9yZGVyOiAxO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcbiAgICB3aWR0aDo0MCU7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIH1cbiAgICAuc2xpZGU4IC50ZXN0IGgxe1xuZm9udC1zaXplOiA1MHB4O1xuICAgIH1cbiAgICAuc2xpZGU4IC50ZXN0IHNlbGVjdHtcbiAgICB3aWR0aDogODAlO1xuICAgIGhlaWdodDogNDBweDtcbiAgICBib3JkZXItY29sb3I6IHJnYigyMTIsIDUsIDUpO1xuICAgICAgICB9XG5cbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSA5KioqKioqKioqKioqKioqKioqKioqKiovXG4uc2xpZGU5e1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBoZWlnaHQ6IDEwMCU7XG59XG4uc2xpZGU5IC5pbWFnZXtcbiAgICBvcmRlcjogMTtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgd2lkdGg6IDM3JTtcbiAgICBoZWlnaHQ6IDEwMCU7XG59XG4uc2xpZGU5IC50ZXN0e1xuICAgIG9yZGVyOiAyO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcbiAgICB3aWR0aDo0MCU7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIH1cbiAgICAuc2xpZGU5IC50ZXN0IGgxe1xuICAgIGZvbnQtc2l6ZTogNTBweDtcbiAgICB9XG4gICAgLnNsaWRlOSAudGVzdCBtYXQtcmFkaW8tZ3JvdXB7XG4gICAgICAgIG1hcmdpbi10b3A6IDUwcHg7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xuICAgICAgICB3aWR0aDogMTAwJTtcbiAgICB9XG5cbiAgICBcbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSAxMCoqKioqKioqKioqKioqKioqKioqKioqL1xuLnNsaWRlMTB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogMTAwJTtcbn1cbi5zbGlkZTEwIC5pbWFnZXtcbiAgICBvcmRlcjogMjtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgd2lkdGg6IDM3JTtcbiAgICBoZWlnaHQ6IDEwMCU7XG59XG4uc2xpZGUxMCAudGVzdHtcbiAgICBvcmRlcjogMTtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XG4gICAgd2lkdGg6NDAlO1xuICAgIGhlaWdodDogMTAwJTtcbiAgICB9XG4gICAgLnNsaWRlMTAgLnRlc3QgaDF7XG4gICAgZm9udC1zaXplOiA1MHB4O1xuICAgIH1cbiAgICAuc2xpZGUxMCAudGVzdCBtYXQtcmFkaW8tZ3JvdXB7XG4gICAgICAgIG1hcmdpbi10b3A6IDUwcHg7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xuICAgICAgICB3aWR0aDogMTAwJTtcbiAgICB9XG5cbiAgICAuc2xpZGUxMCAudGVzdCAuYmxvY2t7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xuICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgbWFyZ2luLXRvcDogMjBweDtcbiAgICB9XG5cbiAgICAuc2xpZGUxMCAudGVzdCAuYmxvY2sgLnNvdXNCbG9ja3tcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7IFxuICAgICB9XG4gICAgLnNsaWRlMTAgLnRlc3QgLmJsb2NrIC5zb3VzQmxvY2sgLnF0eXtcbiAgICAgICAgb3JkZXI6IDI7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyOyAgICAgICAgICAgIFxuICAgICAgICB9XG4gICAgICAgIFxuLnNsaWRlMTAgLnRlc3QgLmJsb2NrIC5zb3VzQmxvY2sgLnF0eSBpbnB1dHtcbiAgICBvcmRlcjogMjtcbiAgICB3aWR0aDogODBweDtcbiAgICBoZWlnaHQ6IDUwcHg7XG4gICAgYm9yZGVyOiA0cHggc29saWQgcmdiKDIxMiwgNSwgNSk7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIH1cbi5zbGlkZTEwIC50ZXN0IC5ibG9jayAuc291c0Jsb2NrICBwe1xuICAgIG9yZGVyOiAxO1xuICAgIG1hcmdpbjogMCA1cHggMCAwO1xuICAgIHBhZGRpbmc6IDA7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAgICAgIH1cblxuLnNsaWRlMTAgLnRlc3QgLmJsb2NrIC5zb3VzQmxvY2sgLnF0eSAucXR5QnRue1xuICAgIG9yZGVyOiAzO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XG4gICAgfVxuICAgIC5zbGlkZTEwIC50ZXN0IC5ibG9jayAuc291c0Jsb2NrIC5xdHkgLnF0eUJ0biBidXR0b257XG4gICAgICAgIGJvcmRlcjogbm9uZTtcbiAgICAgICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIHdoaXRlO1xuICAgICAgICBtYXJnaW4tYm90dG9tOiAxcHg7XG4gICAgICAgIHdpZHRoOiA1cHg7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDBweDtcbiAgICAgICAgaGVpZ2h0OiAyNXB4O1xuICAgICAgICBjb2xvcjogd2hpdGU7XG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6IHJnYigyMTIsIDUsIDUpO1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgICAgICBmb250LXNpemU6IDIwcHg7XG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgICB9XG4gICAgXG4gICAgICAgIFxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnNsaWRlIDExICoqKioqKioqKioqKioqKioqKioqKioqL1xuICAgIC5zbGlkZTExe1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIGhlaWdodDogMTAwJTtcbiAgICB9XG4gICAgLnNsaWRlMTEgLmltYWdle1xuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICBvcmRlcjogMTtcbiAgICAgICAgd2lkdGg6IDM3JTtcbiAgICAgICAgaGVpZ2h0OiAxMDAlO1xuICAgIH1cbiAgICAuc2xpZGUxMSAudGVzdHtcbiAgICAgICAgb3JkZXI6IDI7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgICAgICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcbiAgICAgICAgd2lkdGg6NDAlO1xuICAgICAgICBoZWlnaHQ6IDEwMCU7XG4gICAgICAgIH1cbiAgICAgICAgLnNsaWRlMTEgLnRlc3QgaDF7XG4gICAgZm9udC1zaXplOiA1MHB4O1xuICAgICAgICB9XG4gICAgICAgIC5zbGlkZTExIC50ZXN0IGlucHV0e1xuICAgICAgICB3aWR0aDogODAlO1xuICAgICAgICBoZWlnaHQ6IDQwcHg7XG4gICAgICAgIGJvcmRlci1jb2xvcjogcmdiKDIxMiwgNSwgNSk7XG4gICAgICAgICAgICB9XG5cbiAgICAgIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSAxMiAqKioqKioqKioqKioqKioqKioqKioqKi9cbiAgICAgIC5zbGlkZTEye1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICBoZWlnaHQ6IDEwMCU7XG4gICAgfVxuICAgIC5zbGlkZTEyIC5pbWFnZXtcbiAgICAgICAgb3JkZXI6IDI7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgICAgICBhbGlnbi1pdGVtczpjZW50ZXI7XG4gICAgICAgIHdpZHRoOjUzJTtcbiAgICAgICAgaGVpZ2h0OiAxMDAlO1xuICAgIH1cbiAgICAuc2xpZGUxMiAuaW1hZ2UgaW1ne1xuICAgICAgICBvcmRlcjogMjtcbiAgICAgICAgaGVpZ2h0OiAxNSU7XG4gICAgICAgIHdpZHRoOiAxNSU7XG4gICAgICAgICAgICB9XG4gICAgLnNsaWRlMTIgLmltYWdlIGgxe1xuICAgICAgICBvcmRlcjogMTtcbiAgICAgICAgZm9udC1zaXplOiA1MHB4O1xuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgIG1hcmdpbi1ib3R0b206IDIwcHg7XG4gICAgICAgICAgICB9XG5cblxuICAgIC5zbGlkZTEyIC50ZXN0e1xuICAgICAgICBvcmRlcjogMjtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xuICAgICAgICB3aWR0aDo0MCU7XG4gICAgICAgIGhlaWdodDogMTAwJTtcbiAgICAgICAgfVxuICAgICBcbiAgICAgICAgLnNsaWRlMTIgLnRlc3Qgc2VsZWN0e1xuICAgICAgICB3aWR0aDogODAlO1xuICAgICAgICBoZWlnaHQ6IDQwcHg7XG4gICAgICAgIGJvcmRlci1jb2xvcjogcmdiKDIxMiwgNSwgNSk7XG4gICAgICAgICAgICB9XG5cbiAgICBcbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSAxMyAqKioqKioqKioqKioqKioqKioqKioqKi9cbi5zbGlkZTEze1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgaGVpZ2h0OiAxMDAlO1xufVxuLnNsaWRlMTMgLmltYWdle1xuICAgIG1hcmdpbi1sZWZ0OiA1MHB4O1xuICAgIG9yZGVyOiAxO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgYWxpZ24taXRlbXM6Y2VudGVyO1xuICAgIHdpZHRoOiAzNyU7XG4gICAgaGVpZ2h0OiAxMDAlO1xufVxuLnNsaWRlMTMgLnRlc3R7XG4gICAgb3JkZXI6IDI7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgd2lkdGg6NDAlO1xuICAgIGhlaWdodDogMTAwJTtcbiAgICB9XG4gICAgLnNsaWRlMTMgLnRlc3QgaDF7XG5mb250LXNpemU6IDUwcHg7XG4gICAgfVxuICAgIC5zbGlkZTEzIC50ZXN0IGlucHV0e1xuICAgIHdpZHRoOiA4MCU7XG4gICAgaGVpZ2h0OiA0MHB4O1xuICAgIG1hcmdpbjogNTBweCAwIDUwcHggMDtcbiAgICBib3JkZXItY29sb3I6IHJnYigyMTIsIDUsIDUpO1xuICAgICAgICB9XG5cblxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnNsaWRlIDE0KioqKioqKioqKioqKioqKioqKioqKiovXG4uc2xpZGUxNHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgaGVpZ2h0OiAxMDAlO1xufVxuLnNsaWRlMTQgLmltYWdle1xuICAgIG9yZGVyOiAyO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICB3aWR0aDogMzclO1xuICAgIGhlaWdodDogMTAwJTtcbn1cbi5zbGlkZTE0IC50ZXN0e1xuICAgIG9yZGVyOiAxO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcbiAgICB3aWR0aDo0MCU7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIH1cbiAgICAuc2xpZGUxNCAudGVzdCBoMXtcbiAgICBmb250LXNpemU6IDUwcHg7XG4gICAgfVxuICAgIC5zbGlkZTE0IC50ZXN0IG1hdC1yYWRpby1ncm91cHtcbiAgICAgICAgbWFyZ2luLXRvcDogNTBweDtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XG4gICAgICAgIHdpZHRoOiAxMDAlO1xuICAgIH1cblxuICAgIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSAxNSAqKioqKioqKioqKioqKioqKioqKioqKi9cbiAgICAuc2xpZGUxNXtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgaGVpZ2h0OiAxMDAlO1xuICAgIH1cbiAgICAuc2xpZGUxNSAuaW1hZ2V7XG4gICAgICAgIG9yZGVyOiAyO1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICAgICAgYWxpZ24taXRlbXM6Y2VudGVyO1xuICAgICAgICB3aWR0aDo1MyU7XG4gICAgICAgIGhlaWdodDogMTAwJTtcbiAgICB9XG4gICAgLnNsaWRlMTUgLmltYWdlIHB7XG4gICAgICAgIG9yZGVyOiAyO1xuICAgIFxuICAgICAgICAgICAgfVxuICAgIC5zbGlkZTE1IC5pbWFnZSBoMXtcbiAgICAgICAgb3JkZXI6IDE7XG4gICAgICAgIGZvbnQtc2l6ZTogNTBweDtcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICBtYXJnaW4tYm90dG9tOiAyMHB4O1xuICAgICAgICAgICAgfVxuXG5cblxuICAgIC5zbGlkZTE1IC50ZXN0e1xuICAgICAgICBvcmRlcjogMjtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xuICAgICAgICB3aWR0aDo0MCU7XG4gICAgICAgIGhlaWdodDogMTAwJTtcbiAgICAgICAgfVxuICAgICBcbiAgICAgICAgLnNsaWRlMTUgLnRlc3Qgc2VsZWN0e1xuICAgICAgICB3aWR0aDogODAlO1xuICAgICAgICBoZWlnaHQ6IDQwcHg7XG4gICAgICAgIGJvcmRlci1jb2xvcjogcmdiKDIxMiwgNSwgNSk7XG4gICAgICAgICAgICB9XG5cblxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnNsaWRlIDE2KioqKioqKioqKioqKioqKioqKioqKiovXG4uc2xpZGUxNntcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgaGVpZ2h0OiAxMDAlO1xufVxuLnNsaWRlMTYgLmltYWdle1xuICAgIG9yZGVyOiAyO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICB3aWR0aDogMzclO1xuICAgIGhlaWdodDogMTAwJTtcbn1cbi5zbGlkZTE2IC50ZXN0e1xuICAgIG9yZGVyOiAxO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcbiAgICB3aWR0aDo0MCU7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIH1cbiAgICAuc2xpZGUxNiAudGVzdCBoMXtcbiAgICBmb250LXNpemU6IDUwcHg7XG4gICAgfVxuICAgIC5zbGlkZTE2IC50ZXN0IG1hdC1yYWRpby1ncm91cHtcbiAgICAgICAgbWFyZ2luLXRvcDogNTBweDtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XG4gICAgICAgIHdpZHRoOiAxMDAlO1xuICAgIH1cblxuXG4gICAgXG4gICAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnNsaWRlIDE3ICoqKioqKioqKioqKioqKioqKioqKioqL1xuICAgIC5zbGlkZTE3e1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICBoZWlnaHQ6IDEwMCU7XG4gICAgfVxuICAgIC5zbGlkZTE3IC5pbWFnZXtcbiAgICAgICAgb3JkZXI6IDI7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgICAgICBhbGlnbi1pdGVtczpjZW50ZXI7XG4gICAgICAgIHdpZHRoOjUzJTtcbiAgICAgICAgaGVpZ2h0OiAxMDAlO1xuICAgIH1cbiAgICAuc2xpZGUxNyAuaW1hZ2UgcHtcbiAgICAgICAgb3JkZXI6IDI7XG4gICAgXG4gICAgICAgICAgICB9XG4gICAgLnNsaWRlMTcgLmltYWdlIGgxe1xuICAgICAgICBvcmRlcjogMTtcbiAgICAgICAgZm9udC1zaXplOiA1MHB4O1xuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgIG1hcmdpbi1ib3R0b206IDIwcHg7XG4gICAgICAgICAgICB9XG5cblxuXG4gICAgLnNsaWRlMTcgLnRlc3R7XG4gICAgICAgIG9yZGVyOiAyO1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICAgICAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XG4gICAgICAgIHdpZHRoOjQwJTtcbiAgICAgICAgaGVpZ2h0OiAxMDAlO1xuICAgICAgICB9XG4gICAgIFxuICAgICAgICAuc2xpZGUxNyAudGVzdCBzZWxlY3R7XG4gICAgICAgIHdpZHRoOiA4MCU7XG4gICAgICAgIGhlaWdodDogNDBweDtcbiAgICAgICAgYm9yZGVyLWNvbG9yOiByZ2IoMjEyLCA1LCA1KTtcbiAgICAgICAgICAgIH1cblxuXG4gICAgICAgICAgIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSAxOCAqKioqKioqKioqKioqKioqKioqKioqKi9cbiAgICAgICAgICAgLnNsaWRlMTh7XG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICAgICAgaGVpZ2h0OiAxMDAlO1xuICAgICAgICB9XG4gICAgICAgIC5zbGlkZTE4IC5pbWFnZXtcbiAgICAgICAgICAgIG9yZGVyOiAyO1xuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOmNlbnRlcjtcbiAgICAgICAgICAgIHdpZHRoOjUzJTtcbiAgICAgICAgICAgIGhlaWdodDogMTAwJTtcbiAgICAgICAgfVxuICAgICAgICAuc2xpZGUxOCAuaW1hZ2UgaW1ne1xuICAgICAgICAgICAgb3JkZXI6IDI7XG4gICAgICAgICAgICBoZWlnaHQ6IDMwJTtcbiAgICAgICAgICAgIHdpZHRoOiAxNSU7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAuc2xpZGUxOCAuaW1hZ2UgaDF7XG4gICAgICAgICAgICBvcmRlcjogMjtcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogNTBweDtcbiAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDIwcHg7XG4gICAgICAgICAgICAgICAgfVxuXG5cbiAgICAgICAgLnNsaWRlMTggLnRlc3R7XG4gICAgICAgICAgICBvcmRlcjogMTtcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgICAgICAgd2lkdGg6NDAlO1xuICAgICAgICAgICAgaGVpZ2h0OiAxMDAlO1xuICAgICAgICAgICAgfVxuICAgICAgICAgXG4gICAgICAgICAgICAuc2xpZGUxOCAudGVzdCBzZWxlY3R7XG4gICAgICAgICAgICB3aWR0aDogODAlO1xuICAgICAgICAgICAgaGVpZ2h0OiA0MHB4O1xuICAgICAgICAgICAgYm9yZGVyLWNvbG9yOiByZ2IoMjEyLCA1LCA1KTtcbiAgICAgICAgICAgICAgICB9XG5cblxuICAgICAgICBcbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSAxOSAqKioqKioqKioqKioqKioqKioqKioqKi9cbiAgICAuc2xpZGUxOXtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgaGVpZ2h0OiAxMDAlO1xuICAgIH1cbiAgICAuc2xpZGUxOSAuaW1hZ2V7XG4gICAgICAgIG1hcmdpbi1sZWZ0OiA1MHB4O1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgICBvcmRlcjogMTtcbiAgICAgICAgd2lkdGg6IDM3JTtcbiAgICAgICAgaGVpZ2h0OiAxMDAlO1xuICAgIH1cbiAgICAuc2xpZGUxOSAudGVzdHtcbiAgICAgICAgb3JkZXI6IDI7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgICAgICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcbiAgICAgICAgd2lkdGg6NDAlO1xuICAgICAgICBoZWlnaHQ6IDEwMCU7XG4gICAgICAgIH1cbiAgICAgICAgLnNsaWRlMTkgLnRlc3QgaDF7XG4gICAgZm9udC1zaXplOiA1MHB4O1xuICAgICAgICB9XG4gICAgICAgIC5zbGlkZTE5IC50ZXN0IGlucHV0e1xuICAgICAgICB3aWR0aDogODAlO1xuICAgICAgICBoZWlnaHQ6IDQwcHg7XG4gICAgICAgIGJvcmRlci1jb2xvcjogcmdiKDIxMiwgNSwgNSk7XG4gICAgICAgICAgICB9XG5cblxuICAgICAgICAgICAgXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqc2xpZGUgMjAgKioqKioqKioqKioqKioqKioqKioqKiovXG4uc2xpZGUyMHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogMTAwJTtcbn1cbi5zbGlkZTIwIC5pbWFnZXtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgb3JkZXI6IDE7XG4gICAgd2lkdGg6IDYwJTtcbiAgICBoZWlnaHQ6IDgwJTtcbn1cbi5zbGlkZTIwIC5pbWFnZSBpbWd7XG4gIGhlaWdodDogODAlO1xuICB3aWR0aDogNDAlO1xuICBmaWx0ZXI6IGRyb3Atc2hhZG93KDAuNHJlbSAwLjRyZW0gMC40NXJlbSByZ2JhKDAsIDAsIDMwLCAwLjUpKTtcbn1cbi5zbGlkZTIwIC50ZXN0e1xuICAgIG9yZGVyOiAyO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcbiAgICB3aWR0aDo2MCU7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIH1cbiAgICAuc2xpZGUyMCAudGVzdCBoMXtcbmZvbnQtc2l6ZTogNTBweDtcbiAgICB9XG4gICAgLnNsaWRlMjAgLnRlc3QgaW5wdXR7XG4gICAgd2lkdGg6IDkwJTtcbiAgICBoZWlnaHQ6IDQwcHg7XG4gICAgYm9yZGVyLWNvbG9yOiByZ2IoMjEyLCA1LCA1KTtcbiAgICAgICAgfVxuXG4gICAgLnNsaWRlMjAgLnRlc3QgLmJsb2NrMXtcbiAgICAgICAgbWFyZ2luLXRvcDogMjBweDtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgICAgICB3aWR0aDogOTAlO1xuICAgIH1cbiAgICAuc2xpZGUyMCAudGVzdCAuYmxvY2syIHtcbiAgICAgICAgbWFyZ2luLXRvcDogMjBweDtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgICAgICB3aWR0aDogOTAlO1xuICAgIH1cblxuICAgIC5zbGlkZTIwIC50ZXN0IC5ibG9jazIgaW5wdXR7XG4gICAgICAgIHdpZHRoOiA0MCU7XG4gICAgfVxuICAgIC5zbGlkZTIwIC50ZXN0IC5ibG9jazIgbGFiZWx7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgIG1hcmdpbjogMDtcbiAgICB9XG4gICAgLnNsaWRlMjAgLnRlc3QgLmJsb2NrMSBpbnB1dHtcbiAgICAgICAgd2lkdGg6IDkwJTtcbiAgICB9XG4gICAgLnNsaWRlMjAgLnRlc3QgLmJsb2NrMSBsYWJlbHtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgICAgbWFyZ2luOiAwO1xuICAgIH1cblxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnNsaWRlIDIxICoqKioqKioqKioqKioqKioqKioqKioqL1xuLnNsaWRlMjF7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgaGVpZ2h0OiA3MCU7XG59XG4uc2xpZGUyMSAuaW1hZ2V7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBhbGlnbi1pdGVtczogZmxleC1lbmQ7XG4gICAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcbiAgICBvcmRlcjogMTtcbiAgICB3aWR0aDogMjIlO1xuICAgIGhlaWdodDogMTAwJTtcbn1cbi5zbGlkZTIxIC5pbWFnZSBpbWd7XG4gIGhlaWdodDogNzAlO1xuICB3aWR0aDogMzAlO1xufVxuLnNsaWRlMjEgLnRlc3R7XG4gICAgb3JkZXI6IDI7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIGp1c3RpZnktY29udGVudDpmbGV4LXN0YXJ0O1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgd2lkdGg6NzAlO1xuICAgIGhlaWdodDogODAlO1xuICAgIH1cblxuICAgIC5zbGlkZTIxIC50ZXN0IGgxe1xuICAgICAgICBsaW5lLWhlaWdodDogbm9ybWFsO1xuICAgICAgIGNvbG9yOiBncmVlbjtcbiAgICAgICBmb250LXNpemU6IDUwcHg7XG4gICAgICAgIH1cbiAgICAgICAgLnNsaWRlMjEgLnRlc3QgaW1ne1xuICAgICAgICAgIFxuICAgICAgICAgICAgZm9udC1zaXplOiAxMDBweDtcbiAgICAgICAgICAgIG1hcmdpbjogMTBweDtcbiAgICAgICAgICAgICAgICB9XG4gICAgLnNsaWRlMjEgLnRlc3QgcHtcbiAgICAgICAgbGluZS1oZWlnaHQ6IG5vcm1hbDtcbiAgICAgICAgY29sb3I6IHJnYigwLCAwLCAxMzMpO1xuICAgICAgICBmb250LXNpemU6IDMwcHg7XG4gICAgICAgICAgICB9XG4gICAgLnNsaWRlMjEgLnRlc3QgLnByaXgge1xuICAgICAgICBjb2xvcjogcmVkO1xuICAgICAgICBsaW5lLWhlaWdodDogbm9ybWFsO1xuICAgICAgICAgICAgfVxuICAgIC5zbGlkZTIxIC50ZXN0IC5mYWlsZHtcbiAgICAgICAgY29sb3I6IHJlZDtcbiAgICAgICAgbGluZS1oZWlnaHQ6IG5vcm1hbDtcbiAgICAgICAgZm9udC1zaXplOiA2MHB4O1xuICAgICAgICAgICAgfVxuICAgIC5zbGlkZTIxIC50ZXN0IGltZ3tcbiAgICAgICAgd2lkdGg6IDEwMHB4O1xuICAgICAgICAgICAgfVxuXG5cblxuLnNsaWRlMjEgLm5leHRJY29ue1xuICAgIG9yZGVyOiAzO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XG4gICAgYWxpZ24taXRlbXM6IGZsZXgtZW5kO1xuICAgIHdpZHRoOiAyMiU7XG4gICAgaGVpZ2h0OiA5MCU7XG59XG4uc2xpZGUyMSAubmV4dEljb24gaW1ne1xuIGNvbG9yOiBncmVlbjtcbiB3aWR0aDogODBweDtcbn1cblxuICAgXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqc2xpZGUgMjQgKioqKioqKioqKioqKioqKioqKioqKiovXG4uc2xpZGUyNHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogMTAwJTtcbn1cbi5zbGlkZTI0IC5pbWFnZXtcbiAgICBtYXJnaW4tbGVmdDogNTBweDtcbiAgICBvcmRlcjogMjtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGFsaWduLWl0ZW1zOmZsZXgtZW5kO1xuICAgIHdpZHRoOiA0NyU7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIG1hcmdpbi10b3A6IDEwcHg7XG59XG4uc2xpZGUyNCAudGVzdHtcbiAgICBvcmRlcjogMTtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICB3aWR0aDo0MCU7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIH1cbiAgICAuc2xpZGUyNCAudGVzdCBoMXtcbiAgICBmb250LXNpemU6IDUwcHg7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIH1cbiAgICAuc2xpZGUyNCAudGVzdCBpbnB1dHtcbiAgICB3aWR0aDogODAlO1xuICAgIGhlaWdodDogNDBweDtcbiAgICBtYXJnaW46IDUwcHggMCA1MHB4IDA7XG4gICAgYm9yZGVyLWNvbG9yOiByZ2IoMjEyLCA1LCA1KTtcbiAgICAgICAgfVxuICAgIC5zbGlkZTI0IC50ZXN0IG1hdC1yYWRpby1ncm91cHtcbiAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XG4gICAgICAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XG4gICAgICAgd2lkdGg6IDEwMCU7XG5cbiAgICAgICAgICAgIH1cbiAgICAuc2xpZGUyNCAudGVzdCAuY2FsZW5ke1xuICAgICAgICB3aWR0aDogNDUwcHg7XG4gICAgICAgIGhlaWdodDogNDUwcHg7XG4gICAgfVxuXG4gICAgXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqc2xpZGUgMjUgKioqKioqKioqKioqKioqKioqKioqKiovXG4uc2xpZGUyNXtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBoZWlnaHQ6IDEwMCU7XG59XG4uc2xpZGUyNSAuaW1hZ2V7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBhbGlnbi1pdGVtczogZmxleC1lbmQ7XG4gICAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcbiAgICBvcmRlcjogMTtcbiAgICB3aWR0aDogMjIlO1xuICAgIGhlaWdodDogMjAlO1xufVxuXG4uc2xpZGUyNSAudGVzdHtcbiAgICBvcmRlcjogMjtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAganVzdGlmeS1jb250ZW50OmZsZXgtc3RhcnQ7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICB3aWR0aDo3MCU7XG4gICAgaGVpZ2h0OiA4MCU7XG4gICAgfVxuXG4uc2xpZGUyNSAudGVzdCBpbWd7XG4gICAgd2lkdGg6IDUwcHg7XG4gICAgICAgIH1cbi5zbGlkZTI1IC50ZXN0IHB7XG4gICAgY29sb3I6IHJnYigwLCAwLCAxMzMpO1xuICAgIGZvbnQtc2l6ZTogNDBweDtcbn1cblxuLnNsaWRlMjUgLm5leHRJY29ue1xuICAgIG9yZGVyOiAzO1xufVxuLnNsaWRlMjUgLm5leHRJY29uIHB7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuLnNsaWRlMjUgLm5leHRJY29uIC5zb2NpYWxNZWRpYSB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgfVxuLnNsaWRlMjUgLm5leHRJY29uIC5zb2NpYWxNZWRpYSBpbWd7XG4gd2lkdGg6IDYwcHg7XG59XG5cbiAgICAgICAgICAgXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqc2xpZGUgMjYgKioqKioqKioqKioqKioqKioqKioqKiovXG4uc2xpZGUyNntcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogMTAwJTtcbn1cbi5zbGlkZTI2IC5pbWFnZXtcbiAgICBvcmRlcjogMjtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgYWxpZ24taXRlbXM6Y2VudGVyO1xuICAgIHdpZHRoOjUzJTtcbiAgICBoZWlnaHQ6IDEwMCU7XG59XG4uc2xpZGUyNiAuaW1hZ2UgaW1ne1xuICAgIG9yZGVyOiAyO1xuICAgIGhlaWdodDogMzAlO1xuICAgIHdpZHRoOiAxNSU7XG4gICAgICAgIH1cbi5zbGlkZTI2IC5pbWFnZSBoMXtcbiAgICBvcmRlcjogMjtcbiAgICBmb250LXNpemU6IDUwcHg7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIG1hcmdpbi1ib3R0b206IDIwcHg7XG4gICAgICAgIH1cblxuLnNsaWRlMjYgLnRlc3R7XG4gICAgb3JkZXI6IDE7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xuICAgIHdpZHRoOjYwJTtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgfVxuICAgIC5zbGlkZTI2IC50ZXN0IGgxe1xuZm9udC1zaXplOiA1MHB4O1xuICAgIH1cbiAgICAuc2xpZGUyNiAudGVzdCBpbnB1dHtcbiAgICB3aWR0aDogOTAlO1xuICAgIGhlaWdodDogNDBweDtcbiAgICBib3JkZXItY29sb3I6IHJnYigyMTIsIDUsIDUpO1xuICAgICAgICB9XG5cbiAgICAuc2xpZGUyNiAudGVzdCAuYmxvY2sxe1xuICAgICAgICBtYXJnaW46IDIwcHg7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgICAgICAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XG4gICAgICAgIHdpZHRoOiA5MCU7XG4gICAgfVxuICAgIC5zbGlkZTI2IC50ZXN0IC5ibG9jazIge1xuICAgICAgICBtYXJnaW4tdG9wOiAyMHB4O1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgfVxuICAgIC5zbGlkZTI2IC50ZXN0IC5ibG9jazIgbWF0LXJhZGlvLWdyb3VwICB7XG4gICAgICAgIG1hcmdpbi10b3A6IDIwcHg7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xuICAgICAgICB3aWR0aDogOTAlO1xuICAgIH1cblxuICAgIFxuICAgIC5zbGlkZTI2IC50ZXN0IC5ibG9jazEgaW5wdXR7XG4gICAgICAgIHdpZHRoOiA5MCU7XG4gICAgfVxuICAgIC5zbGlkZTI2IC50ZXN0IC5ibG9jazEgbGFiZWx7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgIG1hcmdpbjogMDtcbiAgICB9XG5cbiAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqc2xpZGUgMjcgKioqKioqKioqKioqKioqKioqKioqKiovXG4gLnNsaWRlMjd7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBoZWlnaHQ6IDEwMCU7XG59XG4uc2xpZGUyNyAuaW1hZ2V7XG4gICAgb3JkZXI6IDE7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGFsaWduLWl0ZW1zOmNlbnRlcjtcbiAgICB3aWR0aDo1MyU7XG4gICAgaGVpZ2h0OiAxMDAlO1xufVxuXG5cblxuXG4uc2xpZGUyNyAudGVzdHtcbiAgICBvcmRlcjogMjtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xuICAgIHdpZHRoOjQwJTtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgXG4gICAgfVxuICAgIC5zbGlkZTI3IC50ZXN0IC5iU2VhcmNoe1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICBmbGV4LWZsb3c6IHdyYXA7XG4gICAgICAgIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcbiAgICAgICAgYWxpZ24taXRlbXM6IGJhc2VsaW5lO1xuICAgIH1cbiAgICAuc2xpZGUyNyAudGVzdCAuYlNlYXJjaCAuc2VhcmNoIHtcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMTVweDtcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDE1cHg7XG4gICAgICAgIHdpZHRoOiA1MCU7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDUwcHg7XG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6IHJnYigxMzQsIDEzNCwgMTM0KTtcbiAgICAgICAgY29sb3I6d2hpdGU7XG4gICAgICB9XG4gICAgICAuc2xpZGUyNyAudGVzdCAuYlNlYXJjaCBpIHtcbiAgICAgICAgbWFyZ2luLWxlZnQ6IC0yOXB4O1xuICAgICAgICBjb2xvcjogd2hpdGU7XG4gICAgICB9XG4gICAgICBcbiAgICAgIC5zbGlkZTI3IC50ZXN0IC5saXN0LWl0ZW0geyAgXG5cbmJvcmRlcjogM3B4IHNvbGlkIHJnYigyNTUsIDAsIDApO1xuYm9yZGVyLXJhZGl1czogNHB4O1xuY29sb3I6IHJnYigxNTMsIDE1MywgMTUzKTtcbmxpbmUtaGVpZ2h0OiA5MHB4O1xuZm9udC13ZWlnaHQ6IDQwMDtcbmJhY2tncm91bmQtY29sb3I6IHJnYigyNTUsIDI1NSwgMjU1KTtcbndpZHRoOiA4OCU7XG59XG4gIFxuICAuc2xpZGUyNyAudGVzdCAuaXRlbS1jb250ZW50IHtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgYm9yZGVyOiBub25lO1xuICAgIGNvbG9yOiByZ2IoMTUzLCAxNTMsIDE1Myk7XG4gICAgbGluZS1oZWlnaHQ6IDQ1cHg7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogcmdiKDI1NSwgMjU1LCAyNTUpO1xuICAgIGJveC1zaGFkb3c6IHJnYmEoMCwwLDAsMC4yKSAwcHggMXB4IDJweCAwcHg7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgd2lkdGg6IDEwMCU7XG4gIH1cbiAgXG4gICBcbiAgLnNsaWRlMjcgLnRlc3QgLml0ZW0tY29udGVudCAuYmxvY2sxe1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIHdpZHRoOiAxMCU7XG4gIH1cblxuICAuc2xpZGUyNyAudGVzdCAuaXRlbS1jb250ZW50IC5ibG9jazJ7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIHdpZHRoOiAtbW96LWF2YWlsYWJsZTtcbiAgICB9XG4gIFxuICAgIC5zbGlkZTI3IC50ZXN0IC5pdGVtLWNvbnRlbnQgLmJsb2NrMiBwe1xuICAgICAgICBtYXJnaW4tdG9wOiAwO1xuICAgICAgICBtYXJnaW4tYm90dG9tOiAxcmVtO1xuICAgICAgICBoZWlnaHQ6IDI5cHg7XG4gICAgICAgIH1cbiAgXG4gIC5zbGlkZTI3IC50ZXN0IC5pdGVtLWNvbnRlbnQgLmJsb2NrM3tcbiAgICBkaXNwbGF5OiBpbmxpbmU7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICB3aWR0aDogMTAlO1xuICB9XG4gIC5zbGlkZTI3IC50ZXN0IC5pdGVtLWNvbnRlbnQgLmJsb2NrMyBpbnB1dHtcbiAgICB3aWR0aDogMTAwJTtcbiAgfVxuICAuc2xpZGUyNyAudGVzdCAuaXRlbS1jb250ZW50OmhvdmVye1xuICAgIGJhY2tncm91bmQtY29sb3I6IHJnYigyMTgsIDk4LCA5OCk7XG4gIH1cbiAgLnNsaWRlMjcgLnRlc3QgLml0ZW0tY29udGVudDpob3ZlciBwe1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICB9XG4gICAgLnNsaWRlMjcgLnRlc3QgLml0ZW0tY29udGVudDpob3ZlciBpbnB1dHtcbiAgICAgICAgY29sb3I6IHdoaXRlO1xuICAgIH1cbiAgICAuc2xpZGUyNyAudGVzdCAuaXRlbS1jb250ZW50OmhvdmVyIGl7XG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcbiAgICB9XG4gXG4gICAgICBcbiAgICAgIC5zbGlkZTI3IC50ZXN0IC5jb250ZW50VGFie1xuICAgICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICAgIGhlaWdodDogNTAwcHg7XG4gICAgICAgICAgb3ZlcmZsb3cteTogc2Nyb2xsO1xuICAgICAgfVxuICBcblxuXG5cblxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqcmVzcG9uc2l2ZSBjc3MgKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDc2OHB4KSB7XG4gICBcbiAgICAuYm9keXtcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIGhlaWdodDogMTAwJTtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgIH1cbiAgICAuZm9vdGVye1xuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIH1cbiAgICAubGVmdHtcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgIHdpZHRoOiAxMDAlO1xuICAgIH1cbiAgICAubGVmdCBpbWd7XG4gICAgICAgIHdpZHRoOiA2MHB4O1xuICAgIH1cbiAgICAucG9pbnR7XG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgZm9udC1zaXplOiA3cHg7XG4gICAgfVxuICAgICAuY2VudGVye1xuICAgICAgd2lkdGg6IDkwJTtcbiAgICAgIG1hcmdpbi1sZWZ0OiAyMHB4OyAgXG4gICAgIH1cbiAgICAuY2VudGVyIC5saXN0UHtcbiAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICB9XG4gICAgXG4gICAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnNsaWRlIDAgKioqKioqKioqKioqKioqKioqKioqKiovXG4uc2xpZGUwe1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXIgO1xuICAgIGhlaWdodDogMTAwJTtcbn1cbi5zbGlkZTAgLmltYWdle1xuICAgIG9yZGVyOiAyO1xuICAgIHdpZHRoOiA4MCU7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICB9XG5cbi5zbGlkZTAgLnRlc3R7XG4gICB3aWR0aDoxMDAlO1xuICAgfVxuXG4uc2xpZGUwIC50ZXN0IGgxe1xuICAgIGZvbnQtc2l6ZTogMzBweDtcbiAgIH1cblxuXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqc2xpZGUgMSAqKioqKioqKioqKioqKioqKioqKioqKi9cbi5zbGlkZTF7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogMTAwJTtcbn1cbi5zbGlkZTEgLmltYWdle1xuICBvcmRlcjogMjtcbiAgd2lkdGg6IDkwJTtcbiAgaGVpZ2h0OiAxMDAlO1xufVxuXG4uc2xpZGUxIC50ZXN0e1xud2lkdGg6IDEwMCU7XG59XG4uc2xpZGUxIC50ZXN0IGgxe1xuICAgIGZvbnQtc2l6ZTogMzBweDtcbiAgICB9XG5cbiAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnNsaWRlIDIgKioqKioqKioqKioqKioqKioqKioqKiovXG4gIC5zbGlkZTJ7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIGp1c3RpZnktY29udGVudDpzcGFjZS1iZXR3ZWVuO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogMTAwJTtcbn1cbi5zbGlkZTIgLmltYWdle1xuICBvcmRlcjogMjtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwJTtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAganVzdGlmeS1jb250ZW50OmZsZXgtZW5kO1xufVxuLnNsaWRlMiAudGVzdHtcbiAgICBvcmRlcjogMTtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xuICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgIHdpZHRoOjEwMCU7XG4gICBoZWlnaHQ6IDEwMCU7XG4gICB9XG4gICAuc2xpZGUyIC50ZXN0IGgxe1xuICAgIGZvbnQtc2l6ZTogMzBweDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICB9XG4gXG4gIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSAzICoqKioqKioqKioqKioqKioqKioqKioqL1xuICAuc2xpZGUze1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgaGVpZ2h0OiAxMDAlO1xufVxuLnNsaWRlMyAuaW1hZ2V7XG4gICAgb3JkZXI6IDI7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xufVxuLnNsaWRlMyAudGVzdHtcbiAgICBvcmRlcjogMTtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICB3aWR0aDoxMDAlO1xuICAgIGhlaWdodDogMTAwJTtcbiAgICB9XG4gICAgLnNsaWRlMyAudGVzdCBoMXtcbiAgICBmb250LXNpemU6IDMwcHg7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIH1cbiBcblxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnNsaWRlIDQgKioqKioqKioqKioqKioqKioqKioqKiovXG4uc2xpZGU0e1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6c3BhY2UtYmV0d2VlbjtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogMTAwJTtcbn1cbi5zbGlkZTQgLmltYWdle1xuICAgIG9yZGVyOiAyO1xuICAgIHdpZHRoOiA4MCU7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6ZmxleC1lbmQ7XG59XG4uc2xpZGU0IC50ZXN0e1xuICAgIG9yZGVyOiAxO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIHdpZHRoOjEwMCU7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIH1cbiAgICAuc2xpZGU0IC50ZXN0IGgxe1xuICAgIGZvbnQtc2l6ZTogMzBweDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgfVxuXG4gICAgXG4gICAgIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSA1ICoqKioqKioqKioqKioqKioqKioqKioqL1xuICAgICAuc2xpZGU1e1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICBoZWlnaHQ6IDEwMCU7XG5cbiAgICB9XG4gICAgLnNsaWRlNSAuaW1hZ2V7XG4gICAgICAgIG9yZGVyOiAyO1xuICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgaGVpZ2h0OiAxMDAlO1xuICAgICAgICB6LWluZGV4OiAxO1xuICAgICBcbiAgICB9XG4gICAgLnNsaWRlNSAudGVzdHtcbiAgICAgICAgb3JkZXI6IDE7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgICAgd2lkdGg6MTAwJTtcbiAgICAgICAgaGVpZ2h0OiAxMDAlO1xuICAgICAgICB9XG4gICAgICAgIC5zbGlkZTUgLnRlc3QgaDF7XG4gICAgICAgIGZvbnQtc2l6ZTogMzBweDtcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICBtYXJnaW4tYmxvY2s6IGF1dG87XG4gICAgICAgIH1cbiAgICAgICAgLnNsaWRlNSAudGVzdCAuYmxvY2tCdG57XG5cbiAgICAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgICAgIGhlaWdodDogMjAlO1xuICAgICAgICB9XG4gICAgICAgIC5zbGlkZTUgLnRlc3QgLmJsb2NrQnRuIC5zb3VzQmxvY2t7XG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGZsZXgtZW5kO1xuICAgICAgICAgICAgd2lkdGg6IDc1JTtcbiAgICAgICAgIH1cbiAgICBcbiAgICAgICAgIC5zbGlkZTUgLnRlc3QgLmJsb2NrQnRuIC5zb3VzQmxvY2sgLnF0eXtcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7ICBcbiAgICAgICAgICAgIG1hcmdpbjogMTBweDsgICAgICAgICAgXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBcbiAgICAuc2xpZGU1IC50ZXN0IC5ibG9ja0J0biAuc291c0Jsb2NrIC5xdHkgaW5wdXR7XG4gICAgICAgIG9yZGVyOiAyO1xuICAgICAgICB3aWR0aDogODBweDtcbiAgICAgICAgaGVpZ2h0OiA1MC41cHg7XG4gICAgICAgIGJvcmRlcjogNHB4IHNvbGlkIHJnYigyMTIsIDUsIDUpO1xuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgIH1cbiAgIFxuIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSA2ICoqKioqKioqKioqKioqKioqKioqKioqL1xuIC5zbGlkZTZ7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogMTAwJTtcbn1cbi5zbGlkZTYgLmltYWdle1xuICAgIG9yZGVyOiAyO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBhbGlnbi1pdGVtczpjZW50ZXI7XG4gICAgd2lkdGg6MTAwJTtcbiAgICBoZWlnaHQ6IDEwMCU7XG59XG4uc2xpZGU2IC5pbWFnZSBpbWd7XG4gICAgb3JkZXI6IDI7XG4gICAgaGVpZ2h0OiAxNSU7XG4gICAgd2lkdGg6IDE1JTtcbiAgICAgICAgfVxuLnNsaWRlNiAuaW1hZ2UgaDF7XG4gICAgb3JkZXI6IDE7XG4gICAgZm9udC1zaXplOiAzMHB4O1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBtYXJnaW4tYm90dG9tOiAyMHB4O1xuICAgICAgICB9XG5cblxuLnNsaWRlNiAudGVzdHtcbiAgICBvcmRlcjogMjtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgd2lkdGg6MTAwJTtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgfVxuXG4gICAgXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqc2xpZGUgNyAqKioqKioqKioqKioqKioqKioqKioqKi9cbi5zbGlkZTd7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogMTAwJTtcbn1cbi5zbGlkZTcgLmNvbHVtMXtcbiAgICBvcmRlcjogMTtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogMTAwJTtcbn1cbi5zbGlkZTcgLmNvbHVtMntcbiAgICBvcmRlcjogMjtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgYWxpZ24taXRlbXM6IGZsZXgtZW5kO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogMTAwJTtcbn1cbi5zbGlkZTcgLmltYWdle1xuICAgIG9yZGVyOiAyO1xuICAgIHdpZHRoOiA5MCU7XG4gICAgaGVpZ2h0OiAxMDAlO1xufVxuLnNsaWRlNyAudGVzdHtcbiAgICBvcmRlcjogMTtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIHdpZHRoOjEwMCU7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIG1hcmdpbi10b3A6IDIwcHg7XG4gICAgfVxuICAgIC5zbGlkZTcgLnRlc3QgaDF7XG4gICAgZm9udC1zaXplOiAzMHB4O1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICB9XG4gICAgLnNsaWRlNyAudGVzdCBpbnB1dHtcbiAgICB3aWR0aDogODAlO1xuICAgIGhlaWdodDogMjBweDtcbiAgICBib3JkZXItY29sb3I6IHJnYigyMTIsIDUsIDUpO1xuICAgICAgICB9XG4gICAgLnNsaWRlNyAucHJvZ3Jlc3N7XG4gICAgb3JkZXI6IDM7XG59XG5cbi5zbGlkZXIge1xuICAgIC13ZWJraXQtYXBwZWFyYW5jZTogbm9uZTtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBoZWlnaHQ6IDE1cHg7XG4gICAgYmFja2dyb3VuZDogcmdiKDI1NSwgMjU1LCAyNTUpO1xuICAgIG91dGxpbmU6IG5vbmU7XG4gICAgYm9yZGVyOiA1cHggc29saWQgcmdiKDE4OSwgOCwgOCk7XG4gICAgYm9yZGVyLXJhZGl1czogOHB4O1xuICB9XG4gIFxuICBcbiAgLyogZm9yIGNocm9tZS9zYWZhcmkgKi9cbiAgLnNsaWRlcjo6LXdlYmtpdC1zbGlkZXItdGh1bWIge1xuICAgIC13ZWJraXQtYXBwZWFyYW5jZTogbm9uZTtcbiAgICBhcHBlYXJhbmNlOiBub25lO1xuICAgIHdpZHRoOiAxMHB4O1xuICAgIGhlaWdodDogNDBweDtcbiAgICBiYWNrZ3JvdW5kOiByZ2IoMjQ4LCAyMjQsIDUpO1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICBib3JkZXI6IDVweCBzb2xpZCByZ2IoMjQ4LCAyMjQsIDUpO1xuICAgIGJvcmRlci1yYWRpdXM6IDMwcHg7XG4gIH1cbiAgXG4gIC8qIGZvciBmaXJlZm94ICovXG4gIC5zbGlkZXI6Oi1tb3otcmFuZ2UtdGh1bWIge1xuICAgIHdpZHRoOiAxMHB4O1xuICAgIGhlaWdodDogMzBweDtcbiAgICBiYWNrZ3JvdW5kOiByZ2IoMjU1LCAyNTUsIDI1NSk7XG4gICAgY3Vyc29yOiBwb2ludGVyO1xuICAgIGJvcmRlcjogNXB4IHNvbGlkIHJnYigxODksIDgsIDgpO1xuICAgIGJvcmRlci1yYWRpdXM6IDRweDtcbiAgfVxuLl9fcmFuZ2V7XG4gICAgd2lkdGg6IDgwJTtcbiAgICBoZWlnaHQ6IDEwMCU7XG59XG4uX19yYW5nZS1zdGVwe1xuXHRwb3NpdGlvbjogcmVsYXRpdmU7ICAgICAgICAgICAgICAgIFxufVxuLl9fcmFuZ2Utc3RlcHtcblx0cG9zaXRpb246IHJlbGF0aXZlOyAgICAgICAgICAgICAgICBcbn1cblxuLl9fcmFuZ2UtbWF4e1xuXHRmbG9hdDogcmlnaHQ7XG59XG4gICAgICAgICAgIFxuXG5cbi5fX3JhbmdlIGlucHV0OjpyYW5nZS1wcm9ncmVzcyB7XHRiYWNrZ3JvdW5kOiByZ2IoMTg5LCA4LCA4KTtcbn1cbi5zbGlkZXIgaW5wdXRbdHlwZT1yYW5nZV06Oi1tb3otcmFuZ2UtcHJvZ3Jlc3Mge1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNjNjU3YTA7XG4gIH1cbi5fX3JhbmdlLXN0ZXAgZGF0YWxpc3Qge1xuXHRwb3NpdGlvbjpyZWxhdGl2ZTtcblx0ZGlzcGxheTogZmxleDtcblx0anVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuXHRoZWlnaHQ6IGF1dG87XG5cdGJvdHRvbTogNnB4O1xuXHQvKiBkaXNhYmxlIHRleHQgc2VsZWN0aW9uICovXG5cdC13ZWJraXQtdXNlci1zZWxlY3Q6IG5vbmU7IC8qIFNhZmFyaSAqLyAgICAgICAgXG5cdC1tb3otdXNlci1zZWxlY3Q6IG5vbmU7IC8qIEZpcmVmb3ggKi9cblx0LW1zLXVzZXItc2VsZWN0OiBub25lOyAvKiBJRTEwKy9FZGdlICovICAgICAgICAgICAgICAgIFxuXHR1c2VyLXNlbGVjdDogbm9uZTsgLyogU3RhbmRhcmQgKi9cblx0LyogZGlzYWJsZSBjbGljayBldmVudHMgKi9cblx0cG9pbnRlci1ldmVudHM6bm9uZTsgIFxufVxuLl9fcmFuZ2Utc3RlcCBkYXRhbGlzdCBvcHRpb24ge1xuXHR3aWR0aDogMTBweDtcblx0aGVpZ2h0OiAxMHB4O1xuXHRtaW4taGVpZ2h0OiAxMHB4O1xuXHRib3JkZXItcmFkaXVzOiAxMDBweDtcblx0LyogaGlkZSB0ZXh0ICovXG5cdHdoaXRlLXNwYWNlOiBub3dyYXA7ICAgICAgIFxuICBwYWRkaW5nOjA7XG4gIGxpbmUtaGVpZ2h0OiA0MHB4O1xufVxuXG5cbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSA4ICoqKioqKioqKioqKioqKioqKioqKioqL1xuLnNsaWRlOHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogMTAwJTtcbn1cbi5zbGlkZTggLmltYWdle1xuICAgIG9yZGVyOiAyO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgYWxpZ24taXRlbXM6IGZsZXgtZW5kO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogMTAwJTtcbn1cbi5zbGlkZTggLnRlc3R7XG4gICAgb3JkZXI6IDE7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgd2lkdGg6MTAwJTtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgfVxuICAgIC5zbGlkZTggLnRlc3QgaDF7XG4gICAgZm9udC1zaXplOiAzMHB4O1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICB9XG5cbiAgICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqc2xpZGUgOSoqKioqKioqKioqKioqKioqKioqKioqL1xuLnNsaWRlOXtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogNzUwcHg7XG59XG4uc2xpZGU5IC5pbWFnZXtcbiAgICBvcmRlcjogMjtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogMTAwJTtcbn1cbi5zbGlkZTkgLnRlc3R7XG4gICAgb3JkZXI6IDE7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgd2lkdGg6MTAwJTtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgfVxuICAgIC5zbGlkZTkgLnRlc3QgaDF7XG4gICAgZm9udC1zaXplOiAzMHB4O1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICB9XG4gICAgLnNsaWRlOSAudGVzdCBtYXQtcmFkaW8tZ3JvdXB7XG4gICAgICAgIG1hcmdpbi10b3A6IDUwcHg7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xuICAgICAgICB3aWR0aDogMTAwJTtcbiAgICB9XG5cblxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnNsaWRlIDEwKioqKioqKioqKioqKioqKioqKioqKiovXG4uc2xpZGUxMHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogNzUwcHg7XG59XG4uc2xpZGUxMCAuaW1hZ2V7XG4gICAgb3JkZXI6IDI7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogMTAwJTtcbn1cbi5zbGlkZTEwIC50ZXN0e1xuICAgIG9yZGVyOiAxO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIHdpZHRoOjEwMCU7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIH1cbiAgICAuc2xpZGUxMCAudGVzdCBoMXtcbiAgICBmb250LXNpemU6IDMwcHg7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIH1cbiAgICAuc2xpZGUxMCAudGVzdCAuYmxvY2t7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgbWFyZ2luLXRvcDogMjBweDtcbiAgICB9XG5cbiAgICAuc2xpZGUxMCAudGVzdCAuYmxvY2sgLnNvdXNCbG9ja3tcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7IFxuICAgICAgICBtYXJnaW4tdG9wOiAxMHB4O1xuICAgICB9XG4gIFxuICAgICBcbiAgICAgICAgXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqc2xpZGUgMTEgKioqKioqKioqKioqKioqKioqKioqKiovXG4uc2xpZGUxMXtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogNzUwcHg7XG59XG4uc2xpZGUxMSAuaW1hZ2V7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcbiAgICBvcmRlcjogMjtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBoZWlnaHQ6IDEwMCU7XG59XG4uc2xpZGUxMSAudGVzdHtcbiAgICBvcmRlcjogMTtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICB3aWR0aDoxMDAlO1xuICAgIGhlaWdodDogMTAwJTtcbiAgICB9XG4gICAgLnNsaWRlMTEgLnRlc3QgaDF7XG4gICAgZm9udC1zaXplOiAzMHB4O1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICB9XG5cbiAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnNsaWRlIDEyICoqKioqKioqKioqKioqKioqKioqKioqL1xuICAuc2xpZGUxMntcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAganVzdGlmeS1jb250ZW50OmNlbnRlcjtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBoZWlnaHQ6IDcwMHB4O1xufVxuLnNsaWRlMTIgLmltYWdle1xuICAgIG9yZGVyOiAyO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcbiAgICBhbGlnbi1pdGVtczpjZW50ZXI7XG4gICAgd2lkdGg6MTAwJTtcbiAgICBoZWlnaHQ6IDEwMCU7XG59XG4uc2xpZGUxMiAuaW1hZ2UgaDF7XG4gICAgb3JkZXI6IDE7XG4gICAgZm9udC1zaXplOiAzMHB4O1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBtYXJnaW4tYm90dG9tOiAyMHB4O1xuICAgICAgICB9XG5cblxuLnNsaWRlMTIgLnRlc3R7XG4gICAgb3JkZXI6IDI7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgd2lkdGg6MTAwJTtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgfVxuIFxuXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqc2xpZGUgMTMgKioqKioqKioqKioqKioqKioqKioqKiovXG4uc2xpZGUxM3tcbmRpc3BsYXk6IGZsZXg7XG5mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuanVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG53aWR0aDogMTAwJTtcbmhlaWdodDogNzUwcHg7XG59XG4uc2xpZGUxMyAuaW1hZ2V7XG5vcmRlcjogMjtcbndpZHRoOiA4MCU7XG5oZWlnaHQ6IDEwMCU7XG59XG4uc2xpZGUxMyAudGVzdHtcbm9yZGVyOiAxO1xuZGlzcGxheTogZmxleDtcbmZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG5qdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbmFsaWduLWl0ZW1zOiBjZW50ZXI7XG53aWR0aDoxMDAlO1xuaGVpZ2h0OiAxMDAlO1xufVxuLnNsaWRlMTMgLnRlc3QgaDF7XG5mb250LXNpemU6IDMwcHg7XG50ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSAxNCoqKioqKioqKioqKioqKioqKioqKioqL1xuLnNsaWRlMTR7XG5kaXNwbGF5OiBmbGV4O1xuZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbmp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbndpZHRoOiAxMDAlO1xuaGVpZ2h0OiA3NTBweDtcbn1cbi5zbGlkZTE0IC5pbWFnZXtcbm9yZGVyOiAyO1xuZGlzcGxheTogZmxleDtcbmFsaWduLWl0ZW1zOiBjZW50ZXI7XG5qdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xud2lkdGg6IDEwMCU7XG5oZWlnaHQ6IDEwMCU7XG59XG4uc2xpZGUxNCAudGVzdHtcbm9yZGVyOiAxO1xuZGlzcGxheTogZmxleDtcbmZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG5qdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcbmFsaWduLWl0ZW1zOiBjZW50ZXI7XG53aWR0aDoxMDAlO1xuaGVpZ2h0OiAxMDAlO1xufVxuLnNsaWRlMTQgLnRlc3QgaDF7XG5mb250LXNpemU6IDMwcHg7XG50ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqc2xpZGUgMTUgKioqKioqKioqKioqKioqKioqKioqKiovXG4uc2xpZGUxNXtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDo3NTBweDtcbn1cbi5zbGlkZTE1IC5pbWFnZXtcbiAgICBvcmRlcjogMjtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgYWxpZ24taXRlbXM6Y2VudGVyO1xuICAgIHdpZHRoOjEwMCU7XG4gICAgaGVpZ2h0OiAxMDAlO1xufVxuLnNsaWRlMTUgLmltYWdlIHB7XG4gICAgb3JkZXI6IDI7XG5cbiAgICAgICAgfVxuLnNsaWRlMTUgLmltYWdlIGgxe1xuICAgIG9yZGVyOiAxO1xuICAgIGZvbnQtc2l6ZTogMzBweDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgbWFyZ2luLWJvdHRvbTogMjBweDtcbiAgICAgICAgfVxuXG5cblxuLnNsaWRlMTUgLnRlc3R7XG4gICAgb3JkZXI6IDI7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcbiAgICBhbGlnbi1pdGVtczpjZW50ZXI7XG4gICAgd2lkdGg6MTAwJTtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgfVxuIFxuXG5cbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSAxNioqKioqKioqKioqKioqKioqKioqKioqL1xuLnNsaWRlMTZ7XG5kaXNwbGF5OiBmbGV4O1xuZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbmp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbndpZHRoOiAxMDAlO1xuaGVpZ2h0OiA3NTBweDtcbn1cbi5zbGlkZTE2IC5pbWFnZXtcbm9yZGVyOiAyO1xuZGlzcGxheTogZmxleDtcbmFsaWduLWl0ZW1zOiBjZW50ZXI7XG5qdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xud2lkdGg6IDEwMCU7XG5oZWlnaHQ6IDEwMCU7XG59XG4uc2xpZGUxNiAudGVzdHtcbm9yZGVyOiAxO1xuZGlzcGxheTogZmxleDtcbmZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG5qdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XG5hbGlnbi1pdGVtczogY2VudGVyO1xud2lkdGg6MTAwJTtcbmhlaWdodDogMTAwJTtcbn1cbi5zbGlkZTE2IC50ZXN0IGgxe1xuZm9udC1zaXplOiAzMHB4O1xudGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqc2xpZGUgMTcgKioqKioqKioqKioqKioqKioqKioqKiovXG4uc2xpZGUxN3tcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDo3NTBweDtcbn1cbi5zbGlkZTE3IC5pbWFnZXtcbiAgICBvcmRlcjogMjtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgYWxpZ24taXRlbXM6Y2VudGVyO1xuICAgIHdpZHRoOjEwMCU7XG4gICAgaGVpZ2h0OiAxMDAlO1xufVxuLnNsaWRlMTcgLmltYWdlIGgxe1xuICAgIG9yZGVyOiAxO1xuICAgIGZvbnQtc2l6ZTogMzBweDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgbWFyZ2luLWJvdHRvbTogMjBweDtcbiAgICAgICAgfVxuXG4uc2xpZGUxNyAudGVzdHtcbiAgICBvcmRlcjogMjtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgd2lkdGg6MTAwJTtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgfVxuIFxuICAgICAgIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSAxOCAqKioqKioqKioqKioqKioqKioqKioqKi9cbiAgICAgICAuc2xpZGUxOHtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICBoZWlnaHQ6IDEwMCU7XG4gICAgfVxuICAgIC5zbGlkZTE4IC5pbWFnZXtcbiAgICAgICAgb3JkZXI6IDE7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgICAgICBhbGlnbi1pdGVtczpjZW50ZXI7XG4gICAgICAgIHdpZHRoOjEwMCU7XG4gICAgICAgIGhlaWdodDogMTAwJTtcbiAgICB9XG4gICAgLnNsaWRlMTggLmltYWdlIGltZ3tcbiAgICAgICAgb3JkZXI6IDI7XG4gICAgICAgIGhlaWdodDogMjAlO1xuICAgICAgICB3aWR0aDogMTUlO1xuICAgICAgICAgICAgfVxuICAgIC5zbGlkZTE4IC5pbWFnZSBoMXtcbiAgICAgICAgb3JkZXI6IDI7XG4gICAgICAgIGZvbnQtc2l6ZTogMzBweDtcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICBtYXJnaW4tYm90dG9tOiAyMHB4O1xuICAgICAgICAgICAgfVxuXG4gICAgLnNsaWRlMTggLnRlc3R7XG4gICAgICAgIG9yZGVyOiAyO1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQgO1xuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgICB3aWR0aDoxMDAlO1xuICAgICAgICBoZWlnaHQ6IDEwMCU7XG4gICAgICAgIH1cbiAgICAgXG4gICAgXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqc2xpZGUgMTkgKioqKioqKioqKioqKioqKioqKioqKiovXG4uc2xpZGUxOXtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgaGVpZ2h0OiAxMDAlO1xufVxuLnNsaWRlMTkgLmltYWdle1xuICAgIG1hcmdpbi1sZWZ0OiA1MHB4O1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBvcmRlcjogMjtcbiAgICB3aWR0aDogOTAlO1xuICAgIGhlaWdodDogMTAwJTtcbn1cbi5zbGlkZTE5IC50ZXN0e1xuICAgIG9yZGVyOiAxO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIHdpZHRoOjEwMCU7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIH1cbiAgICAuc2xpZGUxOSAudGVzdCBoMXtcbiAgICBmb250LXNpemU6IDMwcHg7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIH1cblxuICAgICAgICBcbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSAyMCAqKioqKioqKioqKioqKioqKioqKioqKi9cbi5zbGlkZTIwe1xuZGlzcGxheTogZmxleDtcbmZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG5qdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbndpZHRoOiAxMDAlO1xuaGVpZ2h0OiAxMDAlO1xufVxuLnNsaWRlMjAgLmltYWdle1xuZGlzcGxheTogZmxleDtcbmFsaWduLWl0ZW1zOiBjZW50ZXI7XG5qdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbm9yZGVyOiAxO1xud2lkdGg6IDEwMCU7XG5oZWlnaHQ6IDgwJTtcbn1cbi5zbGlkZTIwIC5pbWFnZSBpbWd7XG5oZWlnaHQ6IDYwJTtcbndpZHRoOiA0MCU7XG5maWx0ZXI6IGRyb3Atc2hhZG93KDAuNHJlbSAwLjRyZW0gMC40NXJlbSByZ2JhKDAsIDAsIDMwLCAwLjUpKTtcbn1cbi5zbGlkZTIwIC50ZXN0e1xub3JkZXI6IDI7XG5kaXNwbGF5OiBmbGV4O1xuZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbmp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcbmFsaWduLWl0ZW1zOiBjZW50ZXI7XG53aWR0aDoxMDAlO1xuaGVpZ2h0OiAxMDAlO1xufVxuLnNsaWRlMjAgLnRlc3QgaDF7XG5mb250LXNpemU6IDMwcHg7XG50ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4uc2xpZGUyMCAudGVzdCAuYmxvY2sxe1xuICAgIG1hcmdpbi10b3A6IDBweDtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgd2lkdGg6IDkwJTtcbn1cbi5zbGlkZTIwIC50ZXN0IC5ibG9jazIge1xuICAgIG1hcmdpbi10b3A6IDBweDtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgd2lkdGg6IDkwJTtcbiAgICB9XG5cbi5zbGlkZTIwIC50ZXN0IC5ibG9jazIgbGFiZWx7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIG1hcmdpbi10b3A6IDEwcHg7XG59XG5cbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSAyMSAqKioqKioqKioqKioqKioqKioqKioqKi9cbi5zbGlkZTIxe1xuZGlzcGxheTogZmxleDtcbmZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG5qdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbmFsaWduLWl0ZW1zOiBjZW50ZXI7XG53aWR0aDogMTAwJTtcbmhlaWdodDogMTAwJTtcbn1cbi5zbGlkZTIxIC5pbWFnZXtcbmRpc3BsYXk6bm9uZTtcbmZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG5hbGlnbi1pdGVtczogY2VudGVyO1xuanVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG5vcmRlcjogMTtcbndpZHRoOiAxMDAlO1xuaGVpZ2h0OiAxMCU7XG59XG4uc2xpZGUyMSAudGVzdHtcbm9yZGVyOiAxO1xuZGlzcGxheTogZmxleDtcbmZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG5qdXN0aWZ5LWNvbnRlbnQ6Y2VudGVyO1xuYWxpZ24taXRlbXM6IGNlbnRlcjtcbndpZHRoOjgwJTtcbmhlaWdodDogODAlOyAgICBcbn1cblxuLnNsaWRlMjEgLnRlc3QgaDF7XG4gICAgbGluZS1oZWlnaHQ6IG5vcm1hbDtcbiAgIGNvbG9yOiBncmVlbjtcbiAgIGZvbnQtc2l6ZTogMzBweDtcbiAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICB9XG4gICAgLnNsaWRlMjEgLnRlc3QgaW1ne1xuICAgICAgICBsaW5lLWhlaWdodDogbm9ybWFsO1xuICAgICAgICBmb250LXNpemU6IDgwcHg7XG4gICAgICAgIG1hcmdpbjogMTBweDtcbiAgICAgICAgICAgIH1cbi5zbGlkZTIxIC50ZXN0IHB7XG4gICAgbGluZS1oZWlnaHQ6IG5vcm1hbDtcbiAgICBjb2xvcjogcmdiKDAsIDAsIDEzMyk7XG4gICAgZm9udC1zaXplOiAzMHB4O1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgfVxuLnNsaWRlMjEgLnRlc3QgLnByaXgge1xuICAgIGNvbG9yOiByZWQ7XG4gICAgICAgIH1cbi5zbGlkZTIxIC50ZXN0IC5mYWlsZHtcbiAgICBsaW5lLWhlaWdodDogbm9ybWFsO1xuICAgIHdpZHRoOiAzNjBweDtcbiAgICBjb2xvcjogcmVkO1xuICAgIGZvbnQtc2l6ZTogMzBweDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgIH1cbi5zbGlkZTIxIC50ZXN0IGltZ3tcbiAgICB3aWR0aDogODBweDtcbiAgICAgICAgfVxuXG4uc2xpZGUyMSAubmV4dEljb257XG4gICAgb3JkZXI6IDM7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XG4gICAgYWxpZ24taXRlbXM6IGZsZXgtZW5kO1xuICAgIHdpZHRoOjk3JTtcbiAgICBoZWlnaHQ6IDE1JTtcbn1cbi5zbGlkZTIxIC5uZXh0SWNvbiBpbWd7XG4gICAgY29sb3I6IGdyZWVuO1xuICAgIHdpZHRoOiA4MHB4O1xufVxuICAgICAgICBcblxuXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqc2xpZGUgMjQgKioqKioqKioqKioqKioqKioqKioqKiovXG4uc2xpZGUyNHtcbmRpc3BsYXk6IGZsZXg7XG5mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuanVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG53aWR0aDogMTAwJTtcbmhlaWdodDogMTAwJTtcbn1cbi5zbGlkZTI0IC5pbWFnZXtcbm1hcmdpbi1sZWZ0OiA1MHB4O1xub3JkZXI6IDI7XG5kaXNwbGF5OiBmbGV4O1xuYWxpZ24taXRlbXM6ZmxleC1lbmQ7XG53aWR0aDogODglO1xuaGVpZ2h0OiAxMDAlO1xufVxuLnNsaWRlMjQgLnRlc3R7XG5vcmRlcjogMTtcbmRpc3BsYXk6IGZsZXg7XG5mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuanVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcbmFsaWduLWl0ZW1zOiBjZW50ZXI7XG53aWR0aDoxMDAlO1xuaGVpZ2h0OiAxMDAlO1xubWFyZ2luLXRvcDogMjBweDtcbn1cbi5zbGlkZTI0IC50ZXN0IGgxe1xuZm9udC1zaXplOiAzMHB4O1xudGV4dC1hbGlnbjogY2VudGVyO1xufVxuLnNsaWRlMjQgLnRlc3QgaW5wdXR7XG53aWR0aDogODAlO1xuaGVpZ2h0OiA0MHB4O1xubWFyZ2luOiA1MHB4IDAgNTBweCAwO1xuYm9yZGVyLWNvbG9yOiByZ2IoMjEyLCA1LCA1KTtcbiAgICB9XG4uc2xpZGUyNCAudGVzdCBtYXQtcmFkaW8tZ3JvdXB7XG4gICBkaXNwbGF5OiBmbGV4O1xuICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xuICAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XG4gICB3aWR0aDogMTAwJTtcbiAgIG1hcmdpbi10b3A6IDIwcHg7XG5cbiAgICAgICAgfVxuLnNsaWRlMjQgLnRlc3QgLmNhbGVuZHtcbiAgICB3aWR0aDogMzUwcHg7XG4gICAgaGVpZ2h0OiAzNTBweDtcbn1cblxuXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqc2xpZGUgMjUgKioqKioqKioqKioqKioqKioqKioqKiovXG4uc2xpZGUyNXtcbmRpc3BsYXk6IGZsZXg7XG5mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuanVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG5hbGlnbi1pdGVtczogY2VudGVyO1xud2lkdGg6IDEwMCU7XG5oZWlnaHQ6IDEwMCU7XG59XG4uc2xpZGUyNSAuaW1hZ2V7XG5kaXNwbGF5OiBmbGV4O1xuYWxpZ24taXRlbXM6IGZsZXgtZW5kO1xuanVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcbm9yZGVyOiAxO1xud2lkdGg6IDIyJTtcbmhlaWdodDogMjAlO1xufVxuXG4uc2xpZGUyNSAudGVzdHtcbm9yZGVyOiAyO1xuZGlzcGxheTogZmxleDtcbmZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG5qdXN0aWZ5LWNvbnRlbnQ6ZmxleC1zdGFydDtcbmFsaWduLWl0ZW1zOiBjZW50ZXI7XG53aWR0aDoxMDAlO1xuaGVpZ2h0OiA4MCU7XG59XG5cbi5zbGlkZTI1IC50ZXN0IGltZ3tcbndpZHRoOiA1MHB4O1xubWFyZ2luLWJvdHRvbTogMzBweDtcbiAgICB9XG4uc2xpZGUyNSAudGVzdCBwe1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbmNvbG9yOiByZ2IoMCwgMCwgMTMzKTtcbmZvbnQtc2l6ZTogMzBweDtcbmxpbmUtaGVpZ2h0OiBub3JtYWw7XG59XG5cbi5zbGlkZTI1IC5uZXh0SWNvbntcbm9yZGVyOiAzO1xufVxuLnNsaWRlMjUgLm5leHRJY29uIHB7XG50ZXh0LWFsaWduOiBjZW50ZXI7XG5saW5lLWhlaWdodDogbm9ybWFsO1xufVxuLnNsaWRlMjUgLm5leHRJY29uIC5zb2NpYWxNZWRpYSB7XG5kaXNwbGF5OiBmbGV4O1xuZmxleC1kaXJlY3Rpb246IHJvdztcbmp1c3RpZnktY29udGVudDogY2VudGVyO1xufVxuLnNsaWRlMjUgLm5leHRJY29uIC5zb2NpYWxNZWRpYSBpbWd7XG53aWR0aDogNjBweDtcbn1cblxuICAgICAgICAgIFxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnNsaWRlIDI2ICoqKioqKioqKioqKioqKioqKioqKioqL1xuLnNsaWRlMjZ7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogMTAwJTtcbn1cbi5zbGlkZTI2IC5pbWFnZXtcbiAgICBvcmRlcjogMTtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgYWxpZ24taXRlbXM6Y2VudGVyO1xuICAgIHdpZHRoOjUzJTtcbiAgICBoZWlnaHQ6IDEwMCU7XG59XG4uc2xpZGUyNiAuaW1hZ2UgaW1ne1xuICAgIG9yZGVyOiAyO1xuICAgIGhlaWdodDogMjAlO1xuICAgIHdpZHRoOiAxNSU7XG4gICAgICAgIH1cbi5zbGlkZTI2IC5pbWFnZSBoMXtcbiAgICBvcmRlcjogMjtcbiAgICBmb250LXNpemU6IDMwcHg7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIG1hcmdpbi1ib3R0b206IDIwcHg7XG4gICAgICAgIH1cblxuLnNsaWRlMjYgLnRlc3R7XG4gICAgb3JkZXI6IDI7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xuICAgIHdpZHRoOjEwMCU7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIH1cbiAgICAuc2xpZGUyNiAudGVzdCBoMXtcbiAgICAgZm9udC1zaXplOiA1MHB4O1xuICAgIH1cbiAgICAuc2xpZGUyNiAudGVzdCBpbnB1dHtcbiAgICB3aWR0aDogOTAlO1xuICAgIGhlaWdodDogNDBweDtcbiAgICBib3JkZXItY29sb3I6IHJnYigyMTIsIDUsIDUpO1xuICAgIG1hcmdpbi1sZWZ0OiA1cHg7XG4gICAgICAgIH1cblxuICAgIC5zbGlkZTI2IC50ZXN0IC5ibG9jazF7XG4gICAgICAgIG1hcmdpbjogMjBweDtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xuICAgICAgICB3aWR0aDogOTAlO1xuICAgIH1cbiAgICAuc2xpZGUyNiAudGVzdCAuYmxvY2syIHtcbiAgICAgICAgbWFyZ2luLXRvcDogMjBweDtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XG4gICAgICAgIHdpZHRoOiAxMDAlO1xuICAgIH1cbiAgICAuc2xpZGUyNiAudGVzdCAuYmxvY2syIG1hdC1yYWRpby1ncm91cCAge1xuICAgICAgICBtYXJnaW4tdG9wOiAyMHB4O1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcbiAgICAgICAgd2lkdGg6IDkwJTtcbiAgICB9XG5cbiAgICBcbiAgICAuc2xpZGUyNiAudGVzdCAuYmxvY2sxIGlucHV0e1xuICAgICAgICB3aWR0aDogOTAlO1xuICAgIH1cbiAgICAuc2xpZGUyNiAudGVzdCAuYmxvY2sxIGxhYmVse1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgICBtYXJnaW46IDA7XG4gICAgfVxuXG5cbiAgICB9Il19 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](TestComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-test',
                templateUrl: './test.component.html',
                styleUrls: ['./test.component.css']
            }]
    }], function () { return [{ type: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"] }, { type: _services_cpn_test_egibilite_service__WEBPACK_IMPORTED_MODULE_4__["TestEgibiliteService"] }]; }, null); })();


/***/ }),

/***/ "./src/app/video/video.component.ts":
/*!******************************************!*\
  !*** ./src/app/video/video.component.ts ***!
  \******************************************/
/*! exports provided: VideoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VideoComponent", function() { return VideoComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


class VideoComponent {
    constructor() { }
    ngOnInit() {
    }
}
VideoComponent.ɵfac = function VideoComponent_Factory(t) { return new (t || VideoComponent)(); };
VideoComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: VideoComponent, selectors: [["app-video"]], decls: 7, vars: 1, consts: [[1, "all"], ["autoplay", "", "loop", "", "id", "myVideo", "autoplay", "", 3, "muted"], ["src", "assets/cpnimages/video/cpn_media.mp4", "type", "video/mp4"], [1, "button", 2, "overflow", "hidden", "padding", "10px 0", "position", "absolute", "bottom", "0", "left", "0", "width", "100%", "display", "flex", "justify-content", "center"], ["href", "/home", 1, "goToWebsite"]], template: function VideoComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "video", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "source", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, " Your browser does not support HTML5 video. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "a", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "Acc\u00E9der au site");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("muted", "muted");
    } }, styles: ["#myVideo[_ngcontent-%COMP%] {\n  position: fixed;\n  right: 0;\n  bottom: 0;\n  width: 100%;\n}\n\n\n\n.content[_ngcontent-%COMP%] {\n  position: fixed;\n  bottom: 0;\n  background: rgba(0, 0, 0, 0.5);\n  color: #f1f1f1;\n  width: 100%;\n  padding: 20px;\n}\n\n\n\n#myBtn[_ngcontent-%COMP%] {\n  width: 200px;\n  font-size: 18px;\n  padding: 10px;\n  border: none;\n  background: #000;\n  color: #fff;\n  cursor: pointer;\n}\n\n#myBtn[_ngcontent-%COMP%]:hover {\n  background: #ddd;\n  color: black;\n}\n\n.goToWebsite[_ngcontent-%COMP%] {\n  text-decoration: none;\n  transform: translate(0, 100px);\n  border: none;\n  background: red;\n  padding: 10px;\n  border-radius: 5px;\n  color: white;\n  margin-bottom: 30px;\n}\n\n.all[_ngcontent-%COMP%]:hover   .goToWebsite[_ngcontent-%COMP%] {\n  transition: 0.3s;\n  transform: translate(0px, 0px);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdmlkZW8vdmlkZW8uY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxlQUFBO0VBQ0EsUUFBQTtFQUNBLFNBQUE7RUFDQSxXQUFBO0FBQ0o7O0FBSUUscURBQUE7O0FBQ0E7RUFDRSxlQUFBO0VBQ0EsU0FBQTtFQUNBLDhCQUFBO0VBQ0EsY0FBQTtFQUNBLFdBQUE7RUFDQSxhQUFBO0FBREo7O0FBSUUsa0RBQUE7O0FBQ0E7RUFDRSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGFBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtBQURKOztBQUlFO0VBQ0UsZ0JBQUE7RUFDQSxZQUFBO0FBREo7O0FBSUU7RUFDRSxxQkFBQTtFQUNDLDhCQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxhQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0QsbUJBQUE7QUFESjs7QUFNTTtFQUFlLGdCQUFBO0VBQ2pCLDhCQUFBO0FBRkoiLCJmaWxlIjoic3JjL2FwcC92aWRlby92aWRlby5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiNteVZpZGVvIHtcclxuICAgIHBvc2l0aW9uOiBmaXhlZDtcclxuICAgIHJpZ2h0OiAwO1xyXG4gICAgYm90dG9tOiAwO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgIC8vIG1pbi13aWR0aDogNTAlO1xyXG4gICAvLyBtaW4taGVpZ2h0OiA3MCU7XHJcbiAgfVxyXG4gIFxyXG4gIC8qIEFkZCBzb21lIGNvbnRlbnQgYXQgdGhlIGJvdHRvbSBvZiB0aGUgdmlkZW8vcGFnZSAqL1xyXG4gIC5jb250ZW50IHtcclxuICAgIHBvc2l0aW9uOiBmaXhlZDtcclxuICAgIGJvdHRvbTogMDtcclxuICAgIGJhY2tncm91bmQ6IHJnYmEoMCwgMCwgMCwgMC41KTtcclxuICAgIGNvbG9yOiAjZjFmMWYxO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBwYWRkaW5nOiAyMHB4O1xyXG4gIH1cclxuICBcclxuICAvKiBTdHlsZSB0aGUgYnV0dG9uIHVzZWQgdG8gcGF1c2UvcGxheSB0aGUgdmlkZW8gKi9cclxuICAjbXlCdG4ge1xyXG4gICAgd2lkdGg6IDIwMHB4O1xyXG4gICAgZm9udC1zaXplOiAxOHB4O1xyXG4gICAgcGFkZGluZzogMTBweDtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICAgIGJhY2tncm91bmQ6ICMwMDA7XHJcbiAgICBjb2xvcjogI2ZmZjtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICB9XHJcbiAgXHJcbiAgI215QnRuOmhvdmVyIHtcclxuICAgIGJhY2tncm91bmQ6ICNkZGQ7XHJcbiAgICBjb2xvcjogYmxhY2s7XHJcbiAgfSBcclxuXHJcbiAgLmdvVG9XZWJzaXRle1xyXG4gICAgdGV4dC1kZWNvcmF0aW9uOm5vbmU7XHJcbiAgICAgdHJhbnNmb3JtOnRyYW5zbGF0ZSgwLDEwMHB4KTtcclxuICAgICBib3JkZXI6bm9uZTtcclxuICAgICBiYWNrZ3JvdW5kOnJlZDtcclxuICAgICBwYWRkaW5nOjEwcHg7XHJcbiAgICAgYm9yZGVyLXJhZGl1czo1cHg7XHJcbiAgICAgY29sb3I6d2hpdGU7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAzMHB4O1xyXG4gIH1cclxuXHJcbiAgICAuYWxsOmhvdmVyICB7XHJcblxyXG4gICAgICAuZ29Ub1dlYnNpdGV7ICB0cmFuc2l0aW9uOiAwLjNzO1xyXG4gICAgdHJhbnNmb3JtIDogdHJhbnNsYXRlKDBweCwwcHgpO1xyXG4gIH19Il19 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](VideoComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-video',
                templateUrl: './video.component.html',
                styleUrls: ['./video.component.scss']
            }]
    }], function () { return []; }, null); })();


/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
/* harmony import */ var src_app_baseUrl__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! src/app/baseUrl */ "./src/app/baseUrl.ts");
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

const environment = {
    production: false,
    apiUrl: src_app_baseUrl__WEBPACK_IMPORTED_MODULE_0__["baseUrl"],
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
_angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["platformBrowser"]().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(err => console.error(err));


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\utilisateur\Downloads\Compressed\frontCrm_2\frontCrm\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map